@echo off

cd %~dp0

set foxyConfigsDir=%cd%\cfg
java -cp .\install\BIKS-Installer.jar "pl.bssg.biks.installer.CreateDb"
java -cp .\install\BIKS-Installer.jar "pl.bssg.biks.installer.RestoreDb" -adminPassword=123

pause