# Przewodnik instalacji BIKS

## Definicje i użyte skróty

**Aplikacja** oznacza aplikację BIKS.

**Serwer aplikacyjny** oznacza wybrane środowisko pozwalające na uruchamianie Aplikacji. W środowisku Polkomtel zastosowano serwer kontenerów Tomcat

**Baza danych** oznacza schemat bazy danych w środowisku MS SQL Server w wersji współpracującej z Aplikacją.

**Serwer** oznacza środowisko sprzętowe i systemowe pozwalające na uruchomienie i poprawne działanie Aplikacji.

**Konektor** oznacza interfejs pozwalający na import metadanych z zewnętrznych systemów do Drzew oraz ich automatyczną aktualizację.

**Drzewo** oznacza obszar informacyjny w Aplikacji składający się z Obiektów i relacji miedzy nimi. Drzewa posiadają właściwości określone w procesie modelowania.

**Obiekt** oznacza zbiór Atrybutów. W Obiekcie występują minimum dwa Atrybuty: nazwa i opis. Obiekty posiadają właściwości określone w procesie modelowania.

**Atrybut** oznacza jednostkową cechę, którą można przypisać do obiektu. Atrybuty posiadają właściwości określone w procesie modelowania.

**Metamodel** oznacza strukturę opisującą Drzewa, Obiekty i Atrybuty oraz relacje między nimi.


## Architektura logiczna i techniczna

​	Dostęp do systemu BIKS dla użytkowników jest zrealizowany poprzez aplikację webową (aplikacja BIKS), uruchomioną na serwerze Apache Tomcat. Aplikacja BIKS działa w architekturze Rich Internet Application (RIA). Aplikacja BIKS została zrealizowana w obiektowym języku programowania Java 1.8 z wykorzystaniem biblioteki SDK Google Web Toolkit 2.7.0 (GWT). Technologia ta wymaga od przeglądarki wbudowanej obsługi JavaScript. W celu najefektywniejszej pracy użytkownicy systemu BIKS powinni korzystać z jednej z przeglądarek: Firefox, Chrome lub Internet Explorer (w wersji 8 lub wyższej).
​	System BIKS wykorzystuje bazę danych Microsoft SQL Server do przechowywania: konfiguracji platformy, Drzew, Obiektów, Atrybutów i Metamodelu. Zawartość w postaci plików i obrazów są przechowywane w katalogu w systemie plików.
​	Aplikacja pozwala na import metadanych z różnych źródeł i ich automatyczną aktualizację za pomocą Konektorów. Podlegają one indywidualnym warunkom licencjonowania i mogą wymagać dodatkowej konfiguracji. W przypadku gdy metadane znajdują się w RDBMS komunikacja odbywa się z wykorzystaniem sterowników JDBC, które nie wymagają instalacji dodatkowych bibliotek sterowników (tzw. client connectivity), wystarczy określenie parametrów połączenia (typu adres serwera, nazwa bazy, login i hasło). 	Komunikacja z Active Directory odbywa się przez protokół LDAP. System BIKS wykorzystuje w tym celu bibliotekę COM4J.
​	Typowe środowisko techniczne składa się z maszyny pełniącej rolę serwera aplikacyjnego, serwera bazodanowego, katalogu w systemie plików do przechowywania danych Aplikacji oraz stacji roboczych użytkowników. W szczególnym przypadku wszystkie elementy mogą być zlokalizowana na jednej maszynie fizycznej lub wirtualnej. Rozdzielenie poszczególnych elementów wymaga skonfigurowania standardowych połączeń sieciowych za pomocą standardowych protokołów i portów sieciowych.


## Wymagania sprzętowe i systemowe. Planowanie instalacji

Minimalne wymagania sprzętowe Serwera aplikacyjnego: Dual-Core 2 GHz CPU, 8 GB RAM, przestrzeni dyskowa serwera 30 GB.

Wymagania sprzętowe Serwera bazy danych dostępne są na stronach producenta.

Wymagany system operacyjny Servera aplikacyjnego: Windows Serwer 2008 x64, Windows Serwer 2008 R2 x64, Windows Serwer 2012 x64, Windows Serwer 2012 R2 x64 lub nowsze. Istnieje możliwość uruchomienia Aplikacji w środowisku Windows 7,8,10 oraz Linux, jednak może to powodować brak dostępności niektórych funkcjonalności.

Minimalne wymagania sprzętowe stacji użytkownika: minimalna rozdzielczość ekranu stacji roboczej: [1024] x [768] rekomendowana [1280] x [800] lub wyższa.

Wymagane oprogramowanie Serwera aplikacyjnego: Tomcat v. 7+, JBoss v. 6+, WebLogic v. 10.3+, WebSphere v. 7.0.0.11+, Jetty v. 8+

Wymagana jest instalacja środowiska Java 8 lub wyższego.

Wymagane oprogramowanie serwera bazy danych: SQL Server 2012, SQL Server 2014, SQL Server 2017 lub nowsze. Serwer bazy danych musi mieć włączoną autoryzację SQL Server oraz komunikację z wykorzystaniem protokołu TCP/IP

Wymagane oprogramowanie stacji roboczej: IE 9+, Firefox v. 35+, Chrome v. 45+, Safari v. 5+, Opera v. 32+

Dodatkowe oprogramowanie aplikacyjne: W celu uruchomienia wbudowanych raportów wymagana jest instalacja Jasper Reports Server Community Edition oraz SQL Server JDBC w wersji 7 lub nowszej. Instrukcja instalacji jest dostępna na stronach producenta oprogramowania.

Przed rozpoczęciem instalacji należy wziąć pod uwagę następujące okoliczności związane z użytkowaniem Aplikacji:

**Liczba i rozmiar plików, które będą składowane:** duża liczba plików oraz ich duży rozmiar wymaga przygotowania odpowiedniej przestrzeni dyskowej do ich przechowywania dostępnych dla serwera.

**Liczba użytkowników i częstotliwość korzystania z Aplikacji:** duża liczba użytkowników wymaga zapewnienia odpowiedniej wydajności bazy danych Aplikacji oraz komunikacji między Bazą danych, Serwerem aplikacyjnym oraz stacjami roboczymi.

**Liczba i sposób aktualizacji Drzew:** duża liczba drzew zasilanych automatycznie dużą ilością metadanych wymaga zapewnienia odpowiedniej ilości przestrzeni dyskowej dla Bazy danych oraz zapewnienia odpowiedniej wydajności procesów zasilania.


## Przygotowanie środowiska eksploatacyjnego BIKS

Przed rozpoczęciem instalacji należy:

- upewnić się, że Serwer aplikacyjny jest dostępny np. pod adresem &quot;http://localhost:8080/",
- upewnić się, że dysponujemy parametrami połączenia do bazy danych oraz loginem i hasłem użytkownika który jest administratorem lub loginem i hasłem użytkownika, który jest właścicielem już istniejącego schematu bazy danych z prawami właściciela,
- sprawdzić konfigurację serwera bazy danych tj. możliwość zalogowania się do serwera z wykorzystaniem autoryzacji SQL Server oraz dostępność komunikacji z bazą za pomocą protokołu TCP/IP. Instrukcja instalacji SQL Server jest dostępna na stronach producenta,
- pcjonalnie upewnić się, że dysponujemy parametrami połączenia oraz loginem i hasłem użytkownika do Jasper Reports Server,
- upewnić się, że przydział pamięci dla JAVA na Serwerze aplikacyjnym wynosi minimum 1024MB,zmiana parametrów pamięci Java w Tomcat za pomocą tomcat8w.exe w katalogu bin. Zmieniamy Internal memory pool: 1024, Maximum memory pool: 4096,
- sprawdzić możliwość kopiowania plików do systemu plików Serwera aplikacyjnego, możliwość modyfikowania konfiguracji Serwera aplikacyjnego, jego uruchamiania i zatrzymywania,
- dostępność odpowiedniej do planowanej instalacji ilość przestrzeni dyskowej z prawami zapisu i odczytu dla konta na którym został uruchomiony Serwer aplikacyjny,
- w przypadku stosowania nazwy domenowej dla Serwera aplikacyjnego należy sprawdzić przekierowanie adresu domenowego na adres IP Servera aplikacyjnego oraz otwarcie odpowiednich portów do komunikacji z serwerem,
- sprawdzić adresy, loginy i hasła dla Konektorów jeśli planujemy je konfigurować w trakcie instalacji.
- sprawdzić czy mamy zdefiniowaną zmienną środowiskową foxyConfigsDir, która powinna wskazywać na katalog w którym znajduje się plik konfiguracyjny aplikacji BIKS np. &quot;c:/BIKS/cfg&quot;. W sytuacji gdy zmienna nie zostanie zdefiniowana aplikacja będzie domyślnie poszukiwać pliku konfiguracyjnego w podkatalogu &quot;lib&quot; Serwera aplikacyjnego.



## Opis pakietu instalacyjnego

Pakiet instalacyjny Aplikacji to archiwum zip. Po wypakowaniu np. do katalogu &quot;c:/BIKS&quot; otrzymujemy następującą strunkturę katalogów i plików:

**app** - katalog zawierający Aplikację w postaci pliku war, pliki wymagane przez niektóre konektory oraz pliki pozwalające na uruchomienie Aplikacji z wbudowanym serwerem Tomcat

**install** - katalog zawiera aplikację instalacyjną oraz skrypty pozwalające na utworzenie i aktualizację Bazy danych

**cfg** - przykładowe pliki konfiguracyjne Aplikacji

**docs** - dokumentacja Aplikacji

**logs** - katalog w którym jest zapisywany log Aplikacji uruchomionej z wbudowanym serwerem Tomcat

**upload** - wzorcowa struktura katalogu w którym Aplikacja przechowuje pliki

**create\_db.bat/create\_db.sh** - skrypt tworzący pliki konfiguracyjne Aplikacji w katalogu cfg oraz w razie potrzeby tworzy potrzebny schemat bazy danych.

**restore\_db.bat/restore\_db.sh** - skrypt tworzący strukturę Bazy danych.

**update\_db.bat/update\_db.sh** - skrypt aktualizujący strukturę Bazy danych do wersji zapisanej w plikach konfiguracyjnych.

**Start.bat** - skrypt uruchamiający Aplikację z wbudowanym serwerem Tomcat.



## Instalacja i konfiguracja systemu BIKS

W celu zainstalowania platformy BIKS:

1. Zatrzymujemy Serwer aplikacyjny.
2. W dedykowanym katalogu w systemie plików rozpakowujemy archiwum z pakietem instalacyjnym.
3. Uruchamiamy skrypt create\_db.bat/create\_db.sh. Skrypt na podstawie wprowadzonych danych: utworzy lub zmodyfikuje plik bikcenter-config.cfg w katalogu cfg pakietu instalacyjnego. W trakcie działania skryptu należy wskazać: port wykorzystywany przez Serwer aplikacyjny (domyślnie 8080), katalog w systemie plików Serwera aplikacyjnego do którego będą zapisywane pliki Aplikacji (domyślnie "c:/BIKS/upload"), nazwę hosta, instancję (jeśli domyślna należy wpisać: null) oraz nazwę schematu bazy danych. Po zatwierdzeniu wprowadzonych parametrów skrypt poprosi o wpisanie loginu i hasła użytkownika z uprawnieniami do utworzenia nowego schematu bazy danych. Oprócz schematu bazy, zostaną stworzone dwa loginy z uprawnieniami do niego. Jeden na potrzeby aplikacji, a drugi na potrzeby procesów importu metadanych. Jeśli baza o podanej nazwie już istnieje, skrypt zaproponuje jej usunięcie. Do pliku konfiguracyjnego zostanie automatycznie wstawiony domyślny login i hasło użytkownika aplikacyjnego tworzonego w trakcie działania skryptu.
4. Uruchamiamy skrypt restore\_db.bat/restore\_db.sh. Skrypt na podstawie utworzonego/zmodyfikowanego pliku bikcenter-config.cfg w katalogu cfg instalacji utworzy wymagane tabele i zapisze dane inicjalne.
5. Plik bikcenter-config.cfg z katalogu cfg kopiujemy do katalogu na który wskazuje zmienna środowiskowa _foxyConfigsDir_ lub do katalogu _lib_ w głównym katalogu Serwera aplikacyjnego.
6. Do katalogu webapp w głównym katalogu Serwera aplikacyjnego kopiujemy plik BIKS.war z katalogu app pakietu instalacyjnego.
7. Po uruchomieniu Serwera aplikacyjnego wpisujemy w przeglądarce adres pod którym jest dostępny Serwer aplikacyjny wraz z nazwą Aplikacji np. _http://localhost:8080/BIKS_. W oknie przeglądarki wyświetli się ekran logowania Aplikacji. Domyślny login i hasło: admin:123. Sprawdzić aktualną wersję aplikacji i bazy w prawym dolnym rogu ekranu.
8. Po zakończeniu instalacji hasło użytkownika „biks\_user\_app&quot; należy zmienić z domyślnego na docelowe. Należy pamiętać o odpowiedniej zmianie hasła na serwerze MS SQL.

Przykładowy plik konfiguracyjny ma postać:

```
{
"dirForUpload:"C:/BIKS//upload/"",
"urlForPublicAccess":"http://localhost:8080/BIKS",
"connConfig": {
	"server":"localhost",
	"instance":null,
	"database":"BIKS",
	"user":"biks_user_app",
	"password":"*******",
	}
}
```

W miejscu **** będzie znajdować się domyślne hasło konta aplikacji na serwerze bazy danych.



## Opcjonalna integracja z Jasper Reports Server

System BIKS wykorzystuje zewnętrzne środowisko raportowe JasperReports. W celu integracji Systemu BIKS ze środowiskiem JasperReports należy wykonać przedstawione poniżej działania.

1. Pobrać aktualną wersję JasperReports Server Community Project pod adresem:

[https://community.jaspersoft.com/project/jasperreports-server/releases](https://community.jaspersoft.com/project/jasperreports-server/releases)

1. Zatrzymujemy serwer Apache Tomcat platformy BIKS.
2. JasperReports Server wykorzystuje serwer aplikacji Apache Tomcat i dedykowaną bazę danych Postgres. W trakcie jego instalacji wybieramy opcję instalacji z wykorzystaniem istniejącego serwera Apache Tomcat i wskazujemy serwer wykorzystywany przez platformę BIKS.
3. Po zakończeniu instalacji i uruchomieniu serwera Apache Tomcat należy sprawdzić dostępność usługi poprzez wywołanie: http://localhost:8080/jasperserver/. Domyślny login i hasło: jasperadmin hasło: jasperadmin.
4. BIKS został skonfigurowany w taki sposób, by uzyskiwać dostęp do JasperReports Server za pomocą: http://localhost:8080/jasperserver/rest\_v2/reports
5. Raporty z poziomu aplikacji BIKS są wywoływane za pomocą umieszczenia w opisie obiektu następującej frazy:

1. Link do raportu można zamieścić za pomocą opcji „Zmień opis&quot; dostępnej z poziomu menu kontekstowego obiektu w drzewie lub link można zamieścić jako wartość atrybutu tekstowego.
2. Do raportu są przekazywane dwa parametry: identyfikator obiektu oraz identyfikator zalogowanego użytkownika.



## Wykonywanie kopii bezpieczeństwa aplikacji

Kopie bezpieczeństwa Systemu BIKS należy wykonywać z uwzględnieniem następujących obszarów zabezpieczenia danych środowiska systemu:

- baza danych BIKS SQL Server przechowująca treść i konfigurację danych Systemu BIKS;
- katalog w systemie plików do przechowywania plików z załącznikami i dokumentami zapisanymi w Systemie BIKS;
- pliki konfiguracyjne przechowujące specyficzne informacje konfiguracyjne,
- kopię repozytorium JasperReports Server Community Project w postaci exportu wszystkich obiektów do katalogu „/BIKS/backup&quot; zgodnie z instrukcją dostępną pod poniższym linkiem:

[https://community.jaspersoft.com/documentation/jasperreports-server-administration-guide/v550/import-and-export-through-web-ui](https://community.jaspersoft.com/documentation/jasperreports-server-administration-guide/v550/import-and-export-through-web-ui)

System BIKS nie posiada wbudowanych własnych mechanizmów i narzędzi wykonywania kopii bezpieczeństwa danych. Do tworzenia i zarządzania kopiami bezpieczeństwa należy wykorzystać standardowe mechanizmy wykonywania kopii bezpieczeństwa danych Bazy SQL Server, oraz standardowe mechanizmy wykonywania kopii bezpieczeństwa zasobów plikowych stosowane w organizacji. Automatyzacja tworzenia kopii zapasowej danych Jaspera Report Server nie jest wymagana. Zalecamy jednak, aby Administrator wykonywał kopię na żądanie za każdym razem gdy będzie importował nowe raporty do Jasper Reports Server.

Zgodnie ze standardową procedurą backupową należy wykonać kopię zapasową wszystkich plików znajdujących się w katalogach wskazanych w pliku bikcenter-config.cfg oraz plików konfiguracyjnych Apache Tomcat.

Dodatkowo można wykonać kopię zapasową pliku „xss.properties&quot; znajdującego się w katalogu aplikacji  BIKS.

**Wykonanie kopii bezpieczeństwa bazy danych SQL Server za pomocą SQL Management Studio (SMS)**

Po podłączeniu do SQL Server wykonać polecenie:

BACKUP DATABASE TU\_WPISAĆ\_NAZWĘ\_BAZY TO DISK=TU_WPISAĆ_.bak

Backup może wykonywać się do kilku minut.



## Odzyskiwanie platformy po całkowitym zniszczeniu środowiska BIKS

W przypadku całkowitego zniszczenia środowiska serwerowego Systemu BIKS należy:

1. Przeprowadzić ponowną instalację serwera MS Windows oraz niezbędnego oprogramowania. Wykonać instalację i konfigurację środowiska Serwera Aplikacyjnego.

2. Następnie należy przeprowadzić instalację i konfigurację Systemu BIKS zgodnie z instrukcją zawartą w _Instalacja i konfiguracja systemu BIKS_. Po zakończeniu instalacji i weryfikacji poprawności działania Systemu BIKS należy zatrzymać Serwer aplikacyjny.
3. Odtworzyć na Serwerze MS SQL bazę danych zawierającą dane i definicje obiektów Systemu BIKS. Należy nadpisać bazę utworzoną w trakcie instalacji.
4. Na odtworzonej bazie danych należy uruchomić następującą sekwencję poleceń SQL (W miejsce gwiazdek należy wpisać odpowiednie hasło dla połączenia serwera raportów):

ALTER USER biks\_user\_app WITH LOGIN = biks\_user\_app

ALTER USER biks\_user\_load WITH LOGIN = biks\_user\_load

Opcjonalnie:

CREATE LOGIN jasper\_server WITH PASSWORD = &#39;\*\*\*\*&#39;

CREATE USER  jasper\_server FOR LOGIN jasper\_server WITH DEFAULT\_SCHEMA=[dbo]

ALTER ROLE [db\_datareader] ADD MEMBER [jasper\_server]

1. Następnie należy przywrócić z kopii zapasowej wszystkie pliki z katalogów wskazanych w pliku bikcenter-config.cfg. UWAGA: W przypadku monitu informującego o istnieniu plików w docelowych konfiguracjach, należy nadpisać istniejące pliki.
2. Po wykonaniu wymienionych czynności należy uruchomić Serwer aplikacyjny. Uruchamiane serwera aplikacyjnego i aplikacji BIKS może potrwać kilka minut. Po upływie 2 minut należy zweryfikować, czy odzyskanie systemu BIKS zakończyło się sukcesem. W tym celu należy wywołać w przeglądarce internetowej adres strony głównej Systemu BIKS.

UWAGA: Jeśli na stronie głównej systemu pojawi się komunikat o niekompatybilnej wersji aplikacji i bazy, należy postępować zgodnie z zaleceniami opisanymi w rozdziale dotyczącym aktualizacji platformy BIKS.



## Aktualizacja i migracja platformy BIKS

### Aktualizacja oprogramowania BIKS poprzez podmianę pliku BIKS.war

Podczas standardowej eksploatacji Systemu BIKS, może zajść konieczność aktualizacji oprogramowania bez zmiany jego wersji (aktualizacja typu &quot;hot fix&quot;). Takie podniesienie wersji nie wymaga wykonania pełnej procedury instalacyjnej.

Wymagane jest:

- zatrzymanie serwera aplikacyjnego Apache Tomcat,
- zamiana pliku BIKS.war w katalogu webapp serwera Apache Tomcat na wersję dostarczoną w pakiecie instalacyjnym w katalogu app,
- uruchomienie serwera aplikacyjnego.

Po zakończeniu aktualizacji należy zweryfikować skuteczność przeprowadzonych działań administracyjnych przez sprawdzenie dostępności aplikacji. Sprawdzić aktualną wersję aplikacji i bazy w prawym dolnym rogu ekranu.

### Aktualizacja wersji

W celu pełnej aktualizacji oprogramowania BIKS należy:

1. Przeczytać instrukcję instalacji otrzymaną razem z pakietem plików aktualizacji oprogramowania BIKS.
2. Rozpoczynając instalację należy zatrzymać usługę serwera Apache Tomcat na aktualizowanym serwerze.
3. Wykonać kopię zapasową bazy danych oraz plików zgodnie z zasadami tworzenia kopii zapasowych Aplikacji
4. Następnie należy usunąć lub przemianować plik „BIKS.war&quot; w katalogu webapp.
5. Skopiować otrzymany w ramach aktualizacji plik „BIKS.war&quot; do katalogu webapp.
6. Skopiować aktualny plik bikcenter-config.cfg do katalogu &quot;cfg&quot; pakietu instalacyjnego.
7. Uruchomić skrypt „update\_db.bat&quot;
8. Uruchomić usługę serwera Apache Tomcat na aktualizowanym serwerze.
9. Po upływie 2 minut należy zweryfikować, czy aktualizacja systemu BIKS zakończyła się sukcesem. W tym celu należy wywołać w przeglądarce internetowej adres strony głównej Systemu BIKS.
10. W przypadku sukcesu przeprowadzonej procedury instalacyjnej w przeglądarce internetowe zostanie wyświetlona strona główna Systemu BIKS, na której nie będą wyświetlone komunikaty o błędach. Sprawdzić aktualną wersję aplikacji i bazy w prawym dolnym rogu ekranu.

### Migracja aplikacji do nowego środowiska

W celu przeniesienia Aplikacji do nowego środowiska należy wykonać kopię bezpieczeństwa systemu, a następnie odtworzyć go w nowym środowisku zgodnie z instrukcją odzyskania platformy BIKS po całkowitym zniszczeniu środowiska serwerowego.



## Rozwiązywanie typowych problemów

**Nieprawidłowa praca Aplikacji po stronie przeglądarki** jest sygnalizowana typowymi komunikatami w postaci okienek. Ich diagnozowanie wymaga odpowiednich narzędzi.

**Nieprawidłowa praca Aplikacji związana z komunikacją z serwerem** jest sygnalizowana komunikatami wyświetlanymi w górnej części ekranu na czerwonym tle, Jednocześnie odpowiednie informacje są zapisywane do logu Serwera aplikacyjnego. W celu właściwego diagnozowania błędów konieczny jest dostęp do logów Serwera aplikacyjnego.

**Większość problemów związanych z działaniem aplikacji wynika z błędów komunikacji z serwerem Bazy danych.** W pierwszej kolejności należy sprawdzić: dostępność servera Bazy danych z serwera aplikacyjnego oraz możliwość zalogowania się do serwera z użyciem loginu i hasła z pliku bikcenter-config.cfg.



## Zarządzanie uprawnieniami użytkowników BIKS

### Role wbudowane Systemu BIKS

BIKS pozwala na zarządzanie uprawnieniami zarówno z poziomu grup uprawnień jak również ról systemowych. Poniżej opis poszczególnych ról:

**Zwykły użytkownik:** Jest to podstawowa rola systemu BIKS. Użytkownik zarejestrowany w systemie ma automatycznie przypisany profil uprawnień jako Zwykły użytkownik. Administrator może dokładnie określić do których drzew i gałęzi z modułów ma dostęp każdy Zwykły Użytkownik. W udostępnionych przez Administratora drzewach i gałęziach, Zwykły Użytkownik ma możliwość przeglądania zawartych w nich informacji, komentowania ich, dodawania do ulubionych oraz oceniania. Administrator może zarządzać dostępem do drzew i gałęzi Zwykłego użytkownika zarówno na poziomie profilu uprawnień (wszystkich użytkowników) jak i na poziomie poszczególnych użytkowników.

**Administrator Systemu (Administrator):** zarządzanie uprawnieniami użytkowników, dodawanie nowych oraz konfiguracja istniejących drzew dynamicznych, dodawanie nowych oraz konfiguracja istniejących atrybutów, edycja treści pomocy, zarządzanie zasileniami systemu, podgląd statystyk systemu, zarządzanie zasileniami systemu, koordynacja logów systemowych, nadzorowanie statystyk, zarządzanie testami jakości, zarządzanie instancjami, połączeniami i archiwizacją danych, zarządzanie metamodelem.

**Administrator aplikacji:** zarządzanie drzewami dynamicznymi, zarządzanie użytkownikami oraz uprawnieniami, zarządzanie menu, zarządzanie metamodelem. Administrator aplikacji różni się od Administratora systemu tym, że nie ma on dostępu do zasileń, harmonogramu czy konfiguracji konektorów. Pozostałe uprawnienia pozostają takie same.

**Ekspert:** rola umożliwiająca edycję informacji we wszystkich drzewach systemu np. edycja treści w ramach całego systemu, dodawanie powiązań pomiędzy obiektami w ramach całego systemu, dodawanie, edycja blogów.

**Twórca:** Poza przeglądaniem informacji w systemie, rola ta umożliwia również ich dodawanie. Rola ta jest nadawana w powiązaniu z konkretnym obiektem lub drzewem. Użytkownik występujący w roli Twórca nie może usuwać informacji, których autorem jest inna osoba. Podstawowe, dodatkowe przypadki użycia systemu w porównaniu do Zwykłego użytkownika: dodawanie / usuwanie / edycja treści w kontekście obiektów do których ma się uprawnienia. (Może usuwać tylko te, które sam dodał), edycja informacji, dodanych przez siebie.

**Autor:** poza przeglądaniem informacji w systemie rola to umożliwia również ich dodawanie. Rola ta jest nadawana w powiązaniu z konkretnym obiektem lub drzewem. Użytkownik występujący w roli Autor może edytować informacje i treści, których autorem jest inna osoba. Podstawowe, dodatkowe przypadki użycia systemu w porównaniu do Zwykłego użytkownika: dodawanie / usuwanie / edycja treści w kontekście obiektów do których ma się uprawnienia, dodawanie powiązań do wprowadzonych przez siebie informacji.

**Użytkownik niepubliczny:** rola umożliwia przeglądanie informacji niepublicznych zawartych w systemie. jest nadawana w powiązaniu z konkretnym obiektem lub drzewem. Użytkownik występujący w roli „Użytkownik niepubliczny&quot; może wyświetlać informacje i treści zawarte w atrybutach niepublicznych. Podstawowe, dodatkowe przypadki użycia systemu w porównaniu do Zwykłego użytkownika: podgląd wartości atrybutów w kontekście obiektów do których ma się uprawnienia.



## Filtrowanie XSS

W systemie BIKS zostały zaimplementowane mechanizmy filtrowania przez serwer aplikacyjny zapisu treści wprowadzanych do Systemu BIKS. Zadaniem tych mechanizmów jest odrzucenie zapisu wszystkich obiektów, które nie spełniają reguł filtrowania atrybutów XSS określonych w pliku „xss.properties&quot;. Jedocześnie istnieje możliwość pominięcia filtrowania dla wybranych użytkowników poprzez nadanie im uprawnienia „Wyłącz filtrowanie HTML&quot;:

Poniżej prezentowana jest domyślna treść pliku „xss.properties&quot; dostarczana wraz z pakietem instalacyjnym Systemu BIKS. W celu rozszerzenia listy akceptowanych elementów html i ich atrybutów należy zmodyfikować treść pliku konfiguracyjnego „xss.properties&quot; Plik „xss.properties&quot; znajduje się w katalogu instalacyjnym Systemu BIKS w podkatalogu …-INF. Domyślna konfiguracja pliku „xss.properties&quot; zezwala jedynie na dodanie elementu img z atrybutem src (parametr img=src). Domyślna treść pliku xss.properties:

```
#lista dodatkowych dozwolonych elementów html i ich atrybutów
#nazwy atrybutów są rozdzielane znakiem ;
#brak atrybutów oznacza, że z html&#39;a zostaną usunięte wszystkie atrybuty danego elementu
#wszystkie elementy nie ujęte na tej liście są usuwane z html&#39;a
a=
b=
blockquote=
br=
caption=
cite=
code=
col=
colgroup=
dd=
div=
dl=
dt=
em=
h1=
h2=
h3=
h4=
h5=
h6=
i=
img=src;
li=
ol=
p=
pre=
q=
small=
span=
strike=
strong=
sub=
sup=
table=
tbody=
td=
tfoot=
th=
thead=
tr=
u=
ul=
```



## Niestandardowe procedury i konfiguracja Aplikacji

**Wyłączenie mechanizmu SSO**

Wyłączenie mechanizmu SSO zalecamy wykonać w dla Aplikacji w środowisku DEV i INS.

W katalogu „Tomcat\webapps\BIKS\WEB-INF&quot; odnajdujemy plik web.xml.
Wstawiamy komentarz w następującym fragmencie kodu:

```
<!-- 
    <filter>
        <filter-name>SecurityFilter</filter-name>
…
	</filter-mapping>
  		--> 
```

**Restart Aplikacji**

- Zatrzymać Serwer Aplikacyjny (Tomcat)
- Usunąć zawartość katalogów w katalogu głównym Serwera Aplikacyjnego (Tomcat): temp, work i webapps/BIKS
- Zarchiwizować logi Serwera Aplikacyjnego (Tomcat)
- Uruchomić Server Aplikacyjny (Tomcat)

**Dodawanie nowych formatów plików do importu**

```
Select val
  from bik\_app\_prop
  where name = 'allowedFileExtensions'
Update bap
  set val=val+'';.xlsx;.xsl;.doc;.docx;.ppt;.pptx'
  from bik\_app\_prop bap
  where name = 'allowedFileExtensions'
```

**Awaryjne włączenie wersjonowania słowników**

```
select \*
--update bik\_lisa\_extradata
--set is\_versioned = 1
from bik\_lisa\_extradata
where node\_id = 3569820
```

**Sprawdzenie parametrów konfiguracyjnych zapisanych w bazie danych**

```
SELECT name, val
FROM bik\_app\_prop
```

