﻿exec sp_check_version '1.7.z.7';
go

	declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')

	while 1 = 1 begin
		declare @rc int = null

		update bik_node 
		set is_deleted = 1
		from bik_node
		inner join bik_node as par on bik_node.parent_node_id = par.id
		where bik_node.tree_id = @tree_id and bik_node.is_deleted = 0 and par.is_deleted = 1

		set @rc = @@rowcount

		if @rc = 0 break
	end
	
	

	-- poprzednia wersja w pliku: alter db for 1.1.9.18 pm.sql
	-- dodanie is_deleted takiego jaki ma rodzic

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_add_menu_node]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_add_menu_node]
GO

create procedure [dbo].[sp_add_menu_node](@parent_obj_id varchar(255), @name varchar(255), @obj_id varchar(255))
as
begin
  declare @parent_node_id int = dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', @parent_obj_id)
  declare @tree_of_trees_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
  declare @visual_order int = coalesce((select max(visual_order) from bik_node 
    where (@parent_node_id is null and parent_node_id is null or @parent_node_id is not null and parent_node_id = @parent_node_id) and
    is_deleted = 0 and tree_id = @tree_of_trees_id), 0) + 1	
  declare @is_deleted int = (select is_deleted from bik_node where tree_id = @tree_of_trees_id  and obj_id = @parent_obj_id)
	
	
  insert into bik_node (parent_node_id, node_kind_id, tree_id, name, obj_id, visual_order,is_deleted)
  values (@parent_node_id, dbo.fn_node_kind_id_by_code('MenuItem'), @tree_of_trees_id, @name, @obj_id, @visual_order, coalesce(@is_deleted,0))
end
go
	


exec sp_update_version '1.7.z.7', '1.7.z.8';
go
