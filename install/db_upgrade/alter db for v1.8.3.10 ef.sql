exec sp_check_version '1.8.3.10';
go
	
	insert into [dbo].[bik_right_role] (code, rank) values ('NonPublicUser', 60) 

	insert into [dbo].[bik_custom_right_role] (code, visual_order, tree_selector, selector_mode) 
	values ('NonPublicUser', (select visual_order from [dbo].[bik_custom_right_role] where  code = 'Creator')+1, --sprawdza
	(select tree_selector from [dbo].[bik_custom_right_role] where code = 'Creator'), 1)
	insert into [dbo].[bik_translation] (code, txt, lang, kind) values ('NonPublicUser', 'Użytkownik nie publiczny', 'pl', 'crr')
	insert into [dbo].[bik_translation] (code, txt, lang, kind) values ('NonPublicUser', 'NonPublicUser', 'en', 'crr')
	insert into [dbo].[bik_translation] (code, txt, lang, kind) values ('NonPublicUser', 'Użytkownik nie publiczny', 'pl', 'adef')
	insert into [dbo].[bik_translation] (code, txt, lang, kind) values ('NonPublicUser', 'NonPublicUser', 'en', 'adef')


	SET ANSI_NULLS ON
	GO
	SET IDENTITY_INSERT [dbo].[bik_node_action_in_custom_right_role] ON
	GO
	declare @id as int;
	declare @creatorId as int = (select id from bik_custom_right_role where code = 'NonPublicUser');
	declare cur CURSOR LOCAL for
	SELECT action_id
	FROM [dbo].[bik_node_action_in_custom_right_role] where role_id = (select id from bik_custom_right_role where code = 'Creator')

	declare @nextId as int

  open cur 
  fetch next from cur into @id 
  while @@FETCH_STATUS = 0 BEGIN 
  select @nextId = max(id)from [dbo].[bik_node_action_in_custom_right_role]
  insert into [dbo].[bik_node_action_in_custom_right_role] (id, action_id, role_id)
  values (@nextId+1, @id, @creatorId)
  fetch next from cur into @id
  end

  close cur
  deallocate cur

  	GO
	
	SET IDENTITY_INSERT [dbo].[bik_node_action_in_custom_right_role] OFF
	GO
  

	GO
	/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]    Script Date: 05.04.2018 12:00:26 ******/
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
		ALTER procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors] as
		begin
		 /* if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
			or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' 
			begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
			end else 
			*/
			if ((select val from bik_app_prop where name = 'biks_id') = '' 
			or (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  
			begin
	 
			/*PB */
			  exec sp_recalculate_Custom_Right_Role_Tree_Selectors_PB
			/*inne*/
			  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
			  if @regUserCrrId is null return;
			  
			  declare @tree_selector_codes_all varchar(max) = '';
			  declare @tree_selector_codes_ru varchar(max) = '';
			  
			  select 
				@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
				@tree_selector_codes_ru =  case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code  
			  from bik_tree where code not in ( 'TreeOfTrees') and is_hidden = 0;
			  
			  update bik_app_prop set val = @tree_selector_codes_all+','+ 'TreeOfTrees' where name = 'customRightRolesTreeSelector';
			  
			  update bik_custom_right_role set tree_selector = @tree_selector_codes_all+','+ 'TreeOfTrees' where code in('AppAdmin','Administrator','Author', 'Creator', 'Expert', 'NonPublicUser');
			  
			  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
		end
		else
		begin
		exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
		end
		end;

		go
  		exec sp_update_version '1.8.3.10', '1.8.4.0';
		go
	