﻿exec sp_check_version '1.8.2.16';
go

declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Wzorzec', @metaCategory, 'hyperlinkInMono', null, 0, 0
  
exec sp_update_version '1.8.2.16', '1.8.2.17';
go

