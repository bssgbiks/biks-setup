﻿exec sp_check_version '1.7.z8.16';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_admin_backup_file') and name = 'is_success')
	alter table bik_admin_backup_file add is_success int default 0; 
go

exec sp_update_version '1.7.z8.16', '1.7.z8.17';
go