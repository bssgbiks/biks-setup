﻿exec sp_check_version '1.7.z10.2';
go

if not exists (select 1 from bik_app_prop where name = 'showTutorials')
	insert into bik_app_prop(name, val) values('showTutorials', 'true');

exec sp_update_version '1.7.z10.2', '1.7.z10.3';
go
