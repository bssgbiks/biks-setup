﻿exec sp_check_version '1.8.5.8';
go

if not exists (select 1 from bik_app_prop where name='showAddNodeButtonOnHyperlinkDialog' )
insert into bik_app_prop(name, val) values ('showAddNodeButtonOnHyperlinkDialog', 'false')
else 
update bik_app_prop set val='false' where name='showAddNodeButtonOnHyperlinkDialog'
go 

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_finally_delete_node_is_deleted_in_metadata_tree]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_finally_delete_node_is_deleted_in_metadata_tree
go

create procedure [dbo].sp_finally_delete_node_is_deleted_in_metadata_tree
as
begin
/*Usunięcie trwale danych oznaczonych na is_deleted=1 */
if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..#node_to_delete'))
drop table #table_to_trunc

create table #node_to_delete (id int not null)

insert into #node_to_delete (id) 
select id  from bik_node where tree_id in (
select id from bik_tree where code=
(select val from bik_app_prop where name='metadataTree'))
and is_deleted=1

delete from bik_statistic_ext where grouping_node_id  in (select id from #node_to_delete)
delete from bik_statistic_ext where clicked_node_id  in (select id from #node_to_delete)
delete from bik_object_author where node_id   in (select id from #node_to_delete)
delete from bik_object_in_history  where node_id in  (select id from #node_to_delete)
delete from bik_attribute_linked where node_id in  (select id from #node_to_delete)
delete from bik_user_in_node  where node_id in  (select id from #node_to_delete)
delete from bik_node where id in  (select id from #node_to_delete)

drop table #node_to_delete
end

go

exec sp_update_version '1.8.5.8','1.8.5.9';
go
