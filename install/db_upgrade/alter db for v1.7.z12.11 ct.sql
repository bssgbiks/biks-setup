﻿exec sp_check_version '1.7.z12.11';
go

if not exists (select 1 from sys.columns where name='is_remote' and object_id=object_id(N'bik_file_system'))
alter table bik_file_system add is_remote int default(0)
go

update bik_file_system set is_remote=1 where is_remote is null
 
exec sp_update_version '1.7.z12.11', '1.7.z12.12';
go