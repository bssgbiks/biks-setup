﻿exec sp_check_version '1.7.z5.16';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

update bik_app_prop set val = '' where name = 'lameLoggersConfig';
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if object_id('[dbo].[fn_get_menu_path_by_tree_code]') is not null
  drop function [dbo].[fn_get_menu_path_by_tree_code]
go

create function [dbo].[fn_get_menu_path_by_tree_code] (@tree_code varchar(255))
returns varchar(max)
as
begin
  declare @tree_of_trees_id int = (select id from bik_tree where code = 'TreeOfTrees');

  -- select * from bik_tree

  declare @node_id int = (select top 1 id from bik_node where tree_id = @tree_of_trees_id and obj_id = '$' + @tree_code and is_deleted = 0);

  declare @node_name varchar(max) = (select name from bik_node where id = @node_id);

  declare @res varchar(max) = case when @node_name = '@' then (select name from bik_tree where code = @tree_code) else @node_name end;

  while 1 = 1 begin
    set @node_id = (select parent_node_id from bik_node where id = @node_id);
    if @node_id is null break;

    set @node_name = (select name from bik_node where id = @node_id);

    set @res = @node_name + ' » ' + @res;
  end;

  return @res;
end;
go


---------------------------------

if object_id('[dbo].[fn_get_menu_path_by_tree_id]') is not null
  drop function [dbo].[fn_get_menu_path_by_tree_id]
go

create function [dbo].[fn_get_menu_path_by_tree_id] (@tree_id int)
returns varchar(max)
as
begin
  declare @tree_code varchar(255) = (select code from bik_tree where id = @tree_id);
  return [dbo].[fn_get_menu_path_by_tree_code](@tree_code);
end;
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

-- drop table bik_crr_label

if object_id('bik_crr_label', 'u') is null
  create table bik_crr_label (
    id int not null identity primary key,
    name varchar(100) not null unique,
    role_id int not null references bik_custom_right_role (id),
    group_id int not null references bik_system_user_group (id),
    unique (role_id, group_id)
  );
go

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

if object_id('[dbo].[fn_get_crr_labels_for_node]') is not null
  drop function [dbo].[fn_get_crr_labels_for_node]
go

create function [dbo].[fn_get_crr_labels_for_node] (@node_id int)
returns varchar(max)
as
begin
  declare @tree_id int, @branch_ids varchar(1000);

  select @tree_id = tree_id, @branch_ids = branch_ids from bik_node where id = @node_id;

  declare @res varchar(max) = '', @sep varchar(max) = ', ';

  select @res = @res + @sep + l.name + case when @branch_ids = n.branch_ids then '' else '*' end
  from bik_custom_right_role_user_entry crrue 
    inner join bik_crr_label l on crrue.role_id = l.role_id and crrue.group_id = l.group_id
    left join bik_node n on n.id = crrue.node_id
  where crrue.tree_id = @tree_id and crrue.user_id is null and (n.id is null or @branch_ids like n.branch_ids + '%')
  order by l.name;

  return substring(@res, datalength(@sep) + 1, len(@res));
end;
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z5.16', '1.7.z5.17';
go
