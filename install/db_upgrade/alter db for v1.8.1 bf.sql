﻿exec sp_check_version '1.8.1';
go

	
if exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind_4_tree_kind') and name='is_branch')
begin

	DECLARE @default_constraint_name sysname
	SELECT @default_constraint_name = object_name(cdefault)
	FROM syscolumns
	WHERE id = object_id('bik_node_kind_4_tree_kind')
	AND name = 'is_branch'
	if @default_constraint_name is not null
	EXEC ('ALTER TABLE ' + 'bik_node_kind_4_tree_kind' + ' DROP CONSTRAINT ' + @default_constraint_name);

	alter table bik_node_kind_4_tree_kind drop column is_branch 
end


if  exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind_4_tree_kind') and name='is_leaf')
begin

--DECLARE @default_constraint_name sysname
	SELECT @default_constraint_name = object_name(cdefault)
	FROM syscolumns
	WHERE id = object_id('bik_node_kind_4_tree_kind')
	AND name = 'is_leaf'
	if @default_constraint_name is not null
	EXEC ('ALTER TABLE ' + 'bik_node_kind_4_tree_kind' + ' DROP CONSTRAINT ' + @default_constraint_name);
		alter table bik_node_kind_4_tree_kind drop column is_leaf 
end
go

if  exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind_4_tree_kind') and name='node_kind_id')
begin
exec sp_rename 'bik_node_kind_4_tree_kind.node_kind_id', 'src_node_kind_id', 'COLUMN'

end

if  not exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind_4_tree_kind') and name='dst_node_kind_id')
begin
alter table bik_node_kind_4_tree_kind add dst_node_kind_id int;--not null
alter table bik_node_kind_4_tree_kind add relation_type varchar(10);--not null  join,link,descendant
end
go

exec sp_update_version '1.8.1', '1.8.1.1';
