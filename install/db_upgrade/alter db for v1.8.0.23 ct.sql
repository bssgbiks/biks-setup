exec sp_check_version '1.8.0.23';
go

if not exists (select 1 from bik_node_action where code='LisaImport')
begin
	insert into bik_node_action(code) values('LisaImport')
end
go

declare @lisaExportActId int = (select id from bik_node_action where code='LisaImport')
delete from bik_node_action_in_custom_right_role where action_id = @lisaExportActId
insert into bik_node_action_in_custom_right_role(action_id, role_id)
select @lisaExportActId, id from bik_custom_right_role where code in ('Administrator', 'AppAdmin', 'ExpertCP', 'ExpertPol', 'Expert')

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

update bik_app_prop set val='' where name='indexing.lastIndexedDate'

exec sp_update_version '1.8.0.23', '1.8.0.24';
go
