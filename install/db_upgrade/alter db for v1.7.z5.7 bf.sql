﻿exec sp_check_version '1.7.z5.7';
go

if ((select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880') 
	begin
		update bik_custom_right_role set selector_mode=0 where code in('Administrator','AppAdmin','ExpertCP','ExpertPol');
		
		insert into bik_node_action (code)
		select x.code
		from
		  (values ('EditOwnComment')
		  ) x(code) left join bik_node_action na on x.code = na.code
		where na.code is null;
		
		
		declare @actionId int;
		select @actionId=id from  bik_node_action where code='EditOwnComment';
		

		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select @actionId,id from  bik_custom_right_role bcr
		where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=@actionId  and role_id=bcr.id) );

		declare @roleId int;
		select @roleId=id from  bik_custom_right_role where code='RegularUser';
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action  bna where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) ) and code in('AddComment','VoteForUsefulness','ShowInTree','DownloadDocument','EditOwnComment');
		
		

		declare @zm varchar(max);
		select @zm=val from bik_app_prop where name='customRightRolesTreeSelector';
	
	if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
	begin
	if not exists(select 1 from bik_system_user_group where distinguished_name = 'DC=corp,DC=plusnet') 
		begin
		insert into bik_system_user_group(name, domain, is_built_in, is_deleted, distinguished_name)
		values ('corp', 'polkomtel', 1, 0, 'DC=corp,DC=plusnet');

		declare @polkomtelId int = scope_identity();
		update bik_system_user_group set scc_root_id = @polkomtelId where id = @polkomtelId
			
		insert into bik_custom_right_role_user_entry (user_id,role_id,tree_id,node_id,group_id)
		select  null,@roleId,id,null,@polkomtelId from bik_tree bt where code in(select str from dbo.fn_split_by_sep(@zm,',',7)) and code <>'TreeOfTrees'
		and not exists (select * from bik_custom_right_role_user_entry where role_id=@roleId and group_id=@polkomtelId
		and tree_id=bt.id)
		end	
	end else if (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' -- CP
	begin
	if not exists(select 1 from bik_system_user_group where distinguished_name = 'DC=polsatc')
		begin
		insert into bik_system_user_group(name, domain, is_built_in, is_deleted, distinguished_name)
		values ('polsatc', 'polsatc', 1, 0, 'DC=polsatc');
		
		declare @cyfrowyId int = scope_identity();
		update bik_system_user_group set scc_root_id = @cyfrowyId where id = @cyfrowyId
		
		insert into bik_custom_right_role_user_entry (user_id,role_id,tree_id,node_id,group_id)
		select  null,@roleId,id,null,@cyfrowyId from bik_tree bt where code in(select str from dbo.fn_split_by_sep(@zm,',',7)) and code <>'TreeOfTrees'
		and not exists (select * from bik_custom_right_role_user_entry where role_id=@roleId and group_id=@cyfrowyId
		and tree_id=bt.id)
		end
	end
		
	update bik_node set is_deleted=0 where  tree_id=(select id from bik_tree where code='TreeOfTrees') and obj_id='#admin:dict:sysGroupUsers'
end	
	;
-- drop table log_msg;

if object_id('log_msg', 'u') is null
  create table log_msg (id int not null identity primary key, msg varchar(max) null, date_added datetime default getdate());
go

-------------------------------------
-------------------------------------
-------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_log_msg]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_log_msg]
go

create procedure [dbo].[sp_log_msg] @msg varchar(max) as
begin
  set nocount on;
  print @msg;
  insert into log_msg (msg) values (@msg);
end;
go

-------------------------------------
-------------------------------------
-------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]
go

create procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] @diag_level int = 0 as
begin
  set nocount on
  
  --declare @diag_level int = 1
  
  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: start!'; exec sp_log_msg @log_msg;
  end;

  if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
  begin
    if @diag_level > 0 begin
      set @log_msg = 'bik_app_prop[customRightRolesTreeSelector] is empty, nothing to do'; exec sp_log_msg @log_msg;
    end;

    return;
  end;
  
  -- 1. 
  -- zakładamy, że zostały zidentyfikowane silnie powiązane komponenty (strongly connected components, scc)
  -- i poprawnie wyliczone jest bik_system_user_group.scc_root_id
  -- jeżeli dana grupa nie tworzy cyklu z innymi, to jest ona jedynym elementem swojego scc, więc 
  -- bik_system_user_group.scc_root_id = bik_system_user_group.id
  
  select distinct scc_root_id as scc_id
  into #rcrrabg_scc
  from bik_system_user_group where is_deleted = 0;
  
  select distinct pg.scc_root_id as parent_scc_id, cg.scc_root_id as child_scc_id
  into #rcrrabg_rel
  from bik_system_group_in_group gig inner join bik_system_user_group pg on gig.group_id = pg.id
  inner join bik_system_user_group cg on gig.member_group_id = cg.id
  where gig.is_deleted = 0 and pg.is_deleted = 0 and cg.is_deleted = 0 and pg.scc_root_id <> cg.scc_root_id;
    
  truncate table bik_custom_right_role_action_branch_grant;

  -- specjalna akcja widoczności w drzewie na zakładkach - ShowInTree
  declare @show_in_tree_act_id int = (select id from bik_node_action where code = 'ShowInTree');

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##rcrrabg_trees_for_roles_0'))
    drop table ##rcrrabg_trees_for_roles_0;

  create table ##rcrrabg_trees_for_roles_0 (role_id int not null, tree_id int not null, unique (role_id, tree_id));

  /*
  declare ##rcrrabg_trees_for_roles_0 table (role_id int not null, tree_id int not null, unique (role_id, tree_id));
  */

  insert into ##rcrrabg_trees_for_roles_0 (role_id, tree_id)
  select distinct crr.id as role_id, t.id as tree_id
  from
    bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str;  

  -- select * from bik_custom_right_role_user_entry
  
  if @diag_level > 1 select * from ##rcrrabg_trees_for_roles_0;
  
  declare @right_for_group_flat table (role_id int not null, group_id int not null, tree_id int null, node_id int null,
    unique(group_id, role_id, tree_id, node_id));

  declare @groups_to_process table (group_id int not null primary key);

  declare @processed_groups table (group_id int not null primary key);

  declare @unprocessed_groups table (group_id int not null primary key);
  
  insert into @unprocessed_groups (group_id) --select id from bik_system_user_group;
  select scc_id from #rcrrabg_scc;

  insert into @groups_to_process (group_id)
  /*select id
  from bik_system_user_group
  where id not in (select member_group_id from bik_system_group_in_group);*/
  select scc_id from #rcrrabg_scc where scc_id not in (select child_scc_id from #rcrrabg_rel);
  
  select scc_id, scc_id as ancestor_scc_id
  into #rcrrabg_ancestors_flat
  from #rcrrabg_scc;
  
  while exists (select 1 from @groups_to_process)
  begin
    if @diag_level > 0
    begin
      declare @gtpstr varchar(max) = null;
      
      select @gtpstr = case when @gtpstr is null then '' else @gtpstr + ',' end + cast(group_id as varchar(20))
      from @groups_to_process;
      
      set @log_msg = 'groups to process: ' + @gtpstr; exec sp_log_msg @log_msg;   
    end;

    insert into #rcrrabg_ancestors_flat (scc_id, ancestor_scc_id)
    select distinct gtp.group_id, af.ancestor_scc_id
    from @groups_to_process gtp inner join #rcrrabg_rel r on gtp.group_id = r.child_scc_id
    inner join #rcrrabg_ancestors_flat af on r.parent_scc_id = af.scc_id;
    
    insert into @processed_groups (group_id) select group_id from @groups_to_process;

    delete from @unprocessed_groups where group_id in (select group_id from @groups_to_process);

    delete from @groups_to_process;
         
    insert into @groups_to_process (group_id)
    select group_id
    from @unprocessed_groups
    where group_id not in 
      --(select member_group_id from bik_system_group_in_group where group_id in (select group_id from @unprocessed_groups));
      (select child_scc_id from #rcrrabg_rel where parent_scc_id in (select group_id from @unprocessed_groups));
  end;

  if @diag_level > 0 and exists(select 1 from @unprocessed_groups)
  begin
    declare @ugstr varchar(max) = null;
    
    select @ugstr = case when @ugstr is null then '' else @ugstr + ',' end + cast(group_id as varchar(20))
    from @unprocessed_groups;
    
    set @log_msg = 'nothing new to process, but there are unprocessed groups: ' + @ugstr; exec sp_log_msg @log_msg;    
  end;

  -- poniższe nie wyłapuje sytuacji gdy istnieje wpis nadrzędny i podrzędny 
  -- (w sensie węzłów w tym samym drzewie, dla tej samej roli) - wrzucane są oba wpisy
  insert into @right_for_group_flat (role_id, group_id, tree_id, node_id)
  select distinct crrue.role_id, cg.id, crrue.tree_id, crrue.node_id
  from #rcrrabg_ancestors_flat af inner join bik_system_user_group ag on af.ancestor_scc_id = ag.scc_root_id
  inner join bik_custom_right_role_user_entry crrue on crrue.group_id = ag.id
  inner join bik_system_user_group cg on af.scc_id = cg.scc_root_id
  where ag.is_deleted = 0;

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##rcrrabg_right_for_user_flat'))
    drop table ##rcrrabg_right_for_user_flat;

  create table ##rcrrabg_right_for_user_flat (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));
    
  /*
  declare ##rcrrabg_right_for_user_flat table (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));
  */

  insert into ##rcrrabg_right_for_user_flat (user_id, role_id, tree_id, node_id)
  select distinct user_id, role_id, tree_id, node_id
  from
    (select uig.user_id, rfgf.role_id, rfgf.tree_id, rfgf.node_id
     from bik_system_user_in_group uig inner join @right_for_group_flat rfgf on uig.group_id = rfgf.group_id
     union
     select user_id, role_id, tree_id, node_id
     from bik_custom_right_role_user_entry crrue where user_id is not null) x;

  if @diag_level > 1 select * from bik_custom_right_role_user_entry;

  if @diag_level > 1 select *
  from --bik_custom_right_role_user_entry 
    ##rcrrabg_right_for_user_flat crrue left join
    ##rcrrabg_trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id;      

  if @diag_level > 1 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: before semi-final insert'; exec sp_log_msg @log_msg;
  end;

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct @show_in_tree_act_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    ##rcrrabg_right_for_user_flat crrue left join
    ##rcrrabg_trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    not exists -- nie dorzucamy wpisu, jeżeli występuje dla niego wpis nadrzędny (na wyższym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          ##rcrrabg_right_for_user_flat crrue2 left join
          ##rcrrabg_trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );

  if @diag_level > 1 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: before final insert'; exec sp_log_msg @log_msg;
  end;

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct naicrr.action_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    ##rcrrabg_right_for_user_flat crrue inner join bik_node_action_in_custom_right_role naicrr on crrue.role_id = naicrr.role_id left join
    ##rcrrabg_trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    naicrr.action_id <> @show_in_tree_act_id and
    not exists -- nie dorzucamy wpisu, jeżeli występuje dla niego wpis nadrzędny (na wyższym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          ##rcrrabg_right_for_user_flat crrue2 inner join bik_node_action_in_custom_right_role naicrr2 on crrue2.role_id = naicrr2.role_id left join
          ##rcrrabg_trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where naicrr2.action_id = naicrr.action_id and crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );
  
  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: end'; exec sp_log_msg @log_msg;
  end;
end;
go
	
exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
go
	
	

	
	
exec sp_update_version '1.7.z5.7', '1.7.z5.8';
go
