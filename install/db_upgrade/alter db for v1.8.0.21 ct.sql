exec sp_check_version '1.8.0.21';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_childless_folders]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_childless_folders
go

create procedure [dbo].sp_delete_childless_folders
as
begin
	declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees');
	
	declare @candidates bik_unique_not_null_id_table_type;
	
	insert into @candidates(id)
	select id
	from (
	select *, (select count(*) from bik_node where parent_node_id = bn.id) as childCount 
	from bik_node bn 
	where bn.tree_id = @treeOfTrees
	and substring(obj_id, 1, 1) not in ('#', '@', '$', '&')
	) x
	where x.childCount = 0;
	
	--delete from bik_node_name_chunk where node_id in (select id from @candidates);
	
	exec sp_delete_node_history_by_node_ids @candidates;
	delete from bik_node where id in (select id from @candidates);
end;
go

exec sp_update_version '1.8.0.21', '1.8.0.22';
go
