﻿exec sp_check_version '1.7.z5.8';
go

		insert into bik_node_action (code)
		select x.code
		from
		  (values ('#admin:dict:sysGroupUsers')
		  ) x(code) left join bik_node_action na on x.code = na.code
		where na.code is null;

		declare @roleId int;
		select @roleId=id from  bik_custom_right_role where code='Administrator';
		if(@roleId is not null) 
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action  bna where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) ) and code in('#admin:dict:sysGroupUsers');
		

		
		exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
		go
	
		update bik_node_action set code='EditOwnComment' where code='EditOwnCommen'
	
	
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL]') AND type in (N'P', N'PC'))
		DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL]
		GO
	
		create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL] as
		begin
		  set nocount on;
		  
		  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
		  if @regUserCrrId is null return;
		  
		  declare @tree_selector_codes_all varchar(max) = '';
		  declare @tree_selector_codes_ru varchar(max) = '';
		  
		  select 
			@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
			@tree_selector_codes_ru =  case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code  
		  from bik_tree where code not in ( 'TreeOfTrees') and is_hidden = 0;
		  
		  update bik_app_prop set val = @tree_selector_codes_all+','+ 'TreeOfTrees' where name = 'customRightRolesTreeSelector';
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_all+','+ 'TreeOfTrees' where code in('AppAdmin','Administrator','Author');
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
		  
		  /* DLA CP */
		  
		  
			declare @tree_selector_codes_autor varchar(max) = 'TreeOfTrees';

			select 
			@tree_selector_codes_autor = @tree_selector_codes_autor + ','  + code
			from bik_tree where id in(
			select distinct(tree_id) from bik_custom_right_role_user_entry where group_id=(select id from bik_system_user_group where name='polsatc'))
			update bik_custom_right_role set tree_selector = @tree_selector_codes_autor where code = 'ExpertCP';

		  /*DLA Pol */
			select @tree_selector_codes_autor='TreeOfTrees';
			select 
			@tree_selector_codes_autor = @tree_selector_codes_autor + ','+ code
			from bik_tree where id in(
			select distinct(tree_id) from bik_custom_right_role_user_entry where group_id=(select id from bik_system_user_group where name='corp'))
			update bik_custom_right_role set tree_selector = @tree_selector_codes_autor where code = 'ExpertPol';

		end;
		go	
	
	
	
	
exec sp_update_version '1.7.z5.8', '1.7.z5.9';
go
