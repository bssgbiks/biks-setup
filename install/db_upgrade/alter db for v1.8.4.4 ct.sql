﻿exec sp_check_version '1.8.4.4';
go


if not exists (select 1 from bik_app_prop where name='showAddChildNodeBtn' )
insert into bik_app_prop(name, val) values ('showAddChildNodeBtn', 'true')
else 
update bik_app_prop set val='true' where name='showAddChildNodeBtn' 

exec sp_update_version '1.8.4.4', '1.8.4.5';
go
