﻿exec sp_check_version '1.7.z7.4';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


if not exists(select 1 from sys.columns where name = N'xlsx_file_name' and object_id = object_id(N'bik_dqm_request'))
begin
	alter table bik_dqm_request add xlsx_file_name varchar(256) null; 
end
go

if not exists(select 1 from sys.columns where name = N'csv_file_name' and object_id = object_id(N'bik_dqm_request'))
begin
	alter table bik_dqm_request add csv_file_name varchar(256) null;
end
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.4', '1.7.z7.5';
go
