﻿exec sp_check_version '1.8.4.21';
go
 
if not exists (select 1 from bik_app_prop where name='indexing.maxReturnedResults') 
insert into bik_app_prop(name, val) values ('indexing.maxReturnedResults', 500)

update bik_attribute set type_attr=null where ltrim(type_attr)=''

if object_id('fn_get_excluded_trees_from_indexing') is not null
drop function fn_get_excluded_trees_from_indexing
go

create function fn_get_excluded_trees_from_indexing() returns @res table(id int)
as
begin

	insert into @res
	select id from bik_tree where code='TreeOfTrees'
	 
	return
end
go

if object_id('sp_implement_metamodel') is not null
drop procedure sp_implement_metamodel
go

create procedure sp_implement_metamodel
as
begin
	declare @metaTree int = (select id from bik_tree where code in (select val from bik_app_prop where name='metadataTree'))
	update bik_attr_category set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	truncate table aaa_attr_category
	insert into aaa_attr_category(id, code)
	select bn.id, bn.name
	from bik_node bn join bik_node_kind nk on nk.id=bn.node_kind_id join bik_tree bt on bt.id=bn.tree_id
	where nk.code='metaBiksAttributesCatalog' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_attr_category', 'metaBiks.Nazwa katalogu', 'name', null

	update bik_attr_category set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	--Kategorie atrybutow
	merge bik_attr_category cat
	using aaa_attr_category x on x.code=cat.name
	when matched then update set is_deleted=0, name=x.name
	when not matched by target then insert (name) values(x.name);

	update bn set bn.name=x.name
	from bik_node bn join aaa_attr_category x on x.code=bn.name

	update bik_attr_def set is_deleted=1 where is_built_in=0 and name not like 'metaBiks%'

	truncate table aaa_attr_def
	insert into aaa_attr_def(id, code, attr_category_id)
	select bn1.id, bn1.name, cat.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk on bn1.node_kind_id=nk.id
	join bik_attr_category cat on cat.name=bn2.name and cat.is_built_in=0
	where bn1.is_deleted=0 and nk.code='metaBiksAttributeDef' and bt.id=@metaTree

	declare @binaryConverter varchar(max)='case x.value when ''Tak'' then 1 else 0 end'
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Nazwa atrybutu', 'name', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Lista wartości', 'value_opt', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Treść pomocy', 'hint', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Typ atrybutu', 'type_attr', null
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Używaj w systemie zarządzania regułami', 'in_drools', @binaryConverter
	exec sp_update_table_column 'aaa_attr_def', 'metaBiks.Wyświetlaj jako liczbę', 'display_as_number', @binaryConverter

	--Atrybuty
	merge bik_attr_def adef
	using aaa_attr_def x on x.code=adef.name and adef.is_built_in=0
	when matched then update set is_deleted=0, name=x.name, hint=x.hint, type_attr=x.type_attr, value_opt=x.value_opt, display_as_number=x.display_as_number, in_drools=x.in_drools
	when not matched by target then
	insert(name, attr_category_id, hint, type_attr, value_opt, display_as_number, in_drools)
	values(x.name, x.attr_category_id, x.hint, x.type_attr, x.value_opt, x.display_as_number, x.in_drools);

	declare attr_def_cursor cursor local
		for select id, name from aaa_attr_def
	declare @node_id int
	declare @attr_name varchar(max)
	open attr_def_cursor
	fetch next from attr_def_cursor into @node_id, @attr_name
	while @@fetch_status=0 begin
		exec sp_meta_biks_change_node_name @node_id, @attr_name
		fetch next from attr_def_cursor into @node_id, @attr_name
	end
	close attr_def_cursor

	update bik_attr_def set hint=null where ltrim(hint)=''
	update bik_attr_def set value_opt=null where ltrim(value_opt)=''

	update bik_node_kind set is_deleted=1 where code not like 'metaBiks%'

	--node_kind
	truncate table aaa_node_kind
	insert into aaa_node_kind(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	truncate table aaa_help
	insert into aaa_help(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Atrybuty dzieci w tabeli', 'children_attr_names', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Dzieci w tabeli', 'children_kinds', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Ikona', 'icon_name', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Rejestr', 'is_registry', @binaryConverter
	exec sp_update_table_column 'aaa_help', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla autora', 'help_author', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla innych', 'help_user', null

	update bik_node_kind set is_deleted=1 where code not like 'metaBiks%' 
	merge bik_node_kind nk
	using aaa_node_kind x on x.code=nk.code
	when matched then update set is_deleted=0, children_attr_names=x.children_attr_names, children_kinds=x.children_kinds, icon_name=x.icon_name, caption=x.caption, is_registry=x.is_registry
	when not matched by target then
	insert(code, children_attr_names, children_kinds, icon_name, caption, is_registry)
	values(x.code, x.children_attr_names, x.children_kinds, x.icon_name, x.caption, x.is_registry);

	--built_in_node_kind
	truncate table aaa_node_kind
	insert into aaa_node_kind(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksBuiltInNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	insert into aaa_help(id, code)
	select bn.id, bn.name from bik_node bn join bik_tree bt on bn.tree_id=bt.id
	join bik_node_kind nk on nk.id=bn.node_kind_id
	where nk.code='metaBiksBuiltInNodeKind' and bn.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Nazwa typu obiektu', 'caption', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla autora', 'help_author', null
	exec sp_update_table_column 'aaa_help', 'metaBiks.Treść pomocy dla innych', 'help_user', null

	
	merge bik_node_kind nk
	using aaa_node_kind x on x.code=nk.code
	when matched then update set is_deleted=0, caption=x.caption;

	--help
	merge bik_help bh
	using aaa_help x on x.code+':author'=bh.help_key and bh.lang='pl'
	when matched then update set is_hidden=0, caption=x.caption, text_help=x.help_author
	when not matched by target then
	insert(help_key, caption, text_help, lang)
	values(x.code+':author', x.caption, x.help_author, 'pl');

	merge bik_help bh
	using aaa_help x on x.code+':user'=bh.help_key and bh.lang='pl'
	when matched then update set is_hidden=0, caption=x.caption, text_help=x.help_author
	when not matched by target then
	insert(help_key, caption, text_help, lang)
	values(x.code+':user', x.caption, x.help_user, 'pl');

	update attr set is_deleted=1
	from bik_attribute attr join bik_attr_def adef on adef.id=attr.attr_def_id
	join bik_node_kind nk on nk.id=attr.node_kind_id
	where nk.code not like 'metaBiks%' and adef.name not like 'metaBiks%' and adef.is_built_in=0

	--atrybuty dla node_kind
	truncate table aaa_attribute
	insert into aaa_attribute(id, node_kind_id, attr_def_id)
	select bn1.id, nk2.id, adef.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk1 on nk1.id=bn1.node_kind_id
	join bik_node_kind nk2 on nk2.code=bn2.name
	join bik_attr_def adef on adef.name=bn1.name
	where nk1.code='metaBiksAttribute4NodeKind' and bn1.is_deleted=0 and bt.id=@metaTree and adef.is_built_in=0

	--select * from aaa_attribute where id=136376

	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Lista wartości', 'value_opt', null
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Może być pusty', 'is_empty', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Pokaż na formatce nowego obiektu', 'is_required', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Publiczny', 'is_public', @binaryConverter
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Typ atrybutu', 'type_attr', null
	exec sp_update_table_column 'aaa_attribute', 'metaBiks.Wartość domyślna', 'default_value', null

	merge bik_attribute attr
	using aaa_attribute x on attr.node_kind_id=x.node_kind_id and attr.attr_def_id=x.attr_def_id
	when matched then update set is_deleted=0, default_value=x.default_value, is_required=x.is_required, type_attr=x.type_attr, value_opt=x.value_opt, is_empty=x.is_empty, is_public=x.is_public
	when not matched by target then
	insert(node_kind_id, attr_def_id, default_value, is_required, type_attr, value_opt, is_empty, is_public)
	values(x.node_kind_id, x.attr_def_id, x.default_value, x.is_required, x.type_attr, x.value_opt, x.is_empty, x.is_public);

	update bik_attribute set default_value=null where ltrim(default_value)=''
	update bik_attribute set value_opt=null where ltrim(value_opt)=''
	update bik_attribute set type_attr=null where ltrim(type_attr)=''

	--tree_kind
	update bik_tree_kind set is_deleted=1 where code not like 'metaBiks%'

	truncate table aaa_tree_kind
	insert into aaa_tree_kind(id, code)
	select bn.id, bn.name
	from bik_node bn join bik_node_kind nk on nk.id=bn.node_kind_id
	join bik_tree bt on bt.id=bn.tree_id
	where nk.code='metaBiksTreeKind' and bn.is_deleted=0 and bt.id=@metaTree
	--select * from aaa_tree_kind

	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Nazwa typu drzewa', 'caption', null
	exec sp_update_table_column 'aaa_tree_kind', 'metaBiks.Pozwolić dowiązać', 'allow_linking', @binaryConverter

	merge bik_tree_kind tk
	using aaa_tree_kind x on tk.code=x.code
	when matched then update set is_deleted=0, caption=x.caption, allow_linking=x.allow_linking
	when not matched by target then
	insert(code, caption, allow_linking)
	values(x.code, x.caption, x.allow_linking);

	truncate table aaa_node_kind_in_tree_kind
	insert into aaa_node_kind_in_tree_kind(id, tree_kind_id, node_kind_id)
	select bn1.id, tk.id, nk2.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_node_kind nk1 on nk1.id=bn1.node_kind_id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk2 on nk2.code=bn1.name
	join bik_tree_kind tk on tk.code=bn2.name
	where nk1.code='metaBiksNodeKind4TreeKind' and bn1.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind_in_tree_kind', 'metaBiks.Typ w drzewie', 'type', null

	update tk set branch_node_kind_id=x.node_kind_id
	from bik_tree_kind tk join aaa_node_kind_in_tree_kind x on x.tree_kind_id=tk.id
	where charindex('Gałąź', x.type, 0)>0 or charindex('Główna gałąź', x.type, 0)>0

	update tk set leaf_node_kind_id=x.node_kind_id
	from bik_tree_kind tk join aaa_node_kind_in_tree_kind x on x.tree_kind_id=tk.id
	where charindex('Liść', x.type, 0)>0

	update nk4tk set is_deleted=1
	from bik_node_kind_4_tree_kind nk4tk join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
	left join bik_node_kind snk on snk.id=nk4tk.src_node_kind_id
	left join bik_node_kind dnk on dnk.id=nk4tk.dst_node_kind_id
	where tk.code not like 'metaBiks%' and (snk.code is null or snk.code not like 'metaBiks%') and (dnk.code is null or dnk.code not like 'metaBiks%')

	truncate table aaa_node_kind_4_tree_kind

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, null, node_kind_id, null
	from aaa_node_kind_in_tree_kind x
	where charindex('Główna gałąź', x.type, 0)>0

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, node_kind_id, null, null
	from aaa_node_kind_in_tree_kind x
	where charindex('Liść', x.type, 0)>0

	truncate table aaa_node_kind_relation_in_tree
	insert into aaa_node_kind_relation_in_tree(id, tree_kind_id, src_node_kind_id, dst_node_kind_id)
	select bn1.id, tk.id, snk.id, dnk.id
	from bik_node bn1 join bik_node bn2 on bn1.parent_node_id=bn2.id
	join bik_node bn3 on bn2.parent_node_id=bn3.id
	join bik_tree bt on bt.id=bn1.tree_id
	join bik_node_kind nk on bn1.node_kind_id=nk.id
	join bik_node_kind snk on bn2.name=snk.code
	join bik_node_kind dnk on bn1.name=dnk.code
	join bik_tree_kind tk on bn3.name=tk.code
	where nk.code='metaBiksNodeKindInRelation' and bn1.is_deleted=0 and bt.id=@metaTree

	exec sp_update_table_column 'aaa_node_kind_relation_in_tree', 'metaBiks.Typ relacji', 'relation_type', null

	insert into aaa_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Powiązanie' from aaa_node_kind_relation_in_tree where charindex('Powiązanie', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Dowiązanie' from aaa_node_kind_relation_in_tree where charindex('Dowiązanie', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Hyperlink' from aaa_node_kind_relation_in_tree where charindex('Hyperlink', relation_type, 0) > 0 union
	select tree_kind_id, src_node_kind_id, dst_node_kind_id, 'Dziecko' from aaa_node_kind_relation_in_tree where charindex('Dziecko', relation_type, 0) > 0

	update nk4tk set nk4tk.is_deleted=1
	from bik_node_kind_4_tree_kind nk4tk join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
	where tk.code not like 'metaBiks%'

	merge bik_node_kind_4_tree_kind nk4tk
	using aaa_node_kind_4_tree_kind x on x.tree_kind_id=nk4tk.tree_kind_id and x.src_node_kind_id=nk4tk.src_node_kind_id and x.dst_node_kind_id=nk4tk.dst_node_kind_id and x.relation_type=nk4tk.relation_type
	when matched then update set is_deleted=0
	when not matched by target then
	insert(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type)
	values(x.tree_kind_id, x.src_node_kind_id, x.dst_node_kind_id, x.relation_type);

	delete from bik_node_kind_4_tree_kind where is_deleted=1
	
	declare nk4tk_cursor cursor local
		for select tk.code as tree_code, tk.caption as tree_name, snk.code as src_code, dnk.code as dst_code 
		from bik_tree_kind tk  
		join bik_node_kind snk on snk.id=tk.branch_node_kind_id
		join bik_node_kind dnk on dnk.id=tk.leaf_node_kind_id
		where tk.code not like 'metaBiks%' and tk.is_deleted=0
	declare @tree_code varchar(max)
	declare @tree_name varchar(max)
	declare @src_code varchar(max)
	declare @dst_code varchar(max)
	open nk4tk_cursor
	fetch next from nk4tk_cursor into @tree_code, @tree_name, @src_code, @dst_code
	while @@fetch_status=0 begin
		set @tree_code='@'+@tree_code
		if @src_code is not null set @src_code='&'+@src_code
		set @dst_code='&'+@dst_code

		exec sp_add_menu_node 'knowledge', @tree_name, @tree_code
		if @src_code is null 
			exec sp_add_menu_node @tree_code, '@', @dst_code
		else begin
			exec sp_add_menu_node @tree_code, '@', @src_code
			if @src_code <> @dst_code exec sp_add_menu_node @src_code, '@', @dst_code
		end

		fetch next from nk4tk_cursor into @tree_code, @tree_name, @src_code, @dst_code
	end
	close nk4tk_cursor

	declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees')
	exec sp_node_init_branch_id @treeOfTrees, null

	update bt set bt.node_kind_id=null from bik_tree bt join bik_tree_kind tk on bt.tree_kind=tk.code
	left join bik_node_kind nk on nk.id=bt.node_kind_id
	where tk.code not like 'metaBiks%' and bt.is_built_in=0 and bt.is_hidden=0 and (tk.is_deleted=1 or nk.is_deleted=1)
end
go

update bik_app_prop set val='' where name='indexing.lastIndexedDate'

if object_id('bik_search') is null 
create table bik_search(
	id int identity(1,1) not null primary key,
	user_id int not null foreign key references bik_system_user(id),
	query varchar(max),
	mode varchar(max)
)
go

if object_id('bik_search_result') is null 
create table bik_search_result(
	id int identity(1,1) not null primary key,
	search_id int not null foreign key references bik_search(id),
	node_id int not null
)
go

exec sp_update_version '1.8.4.21', '1.8.4.22';
go
