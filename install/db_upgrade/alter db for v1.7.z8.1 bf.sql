﻿exec sp_check_version '1.7.z8.1';
go


update bik_app_prop set val=
'<body>
<div style="color:#094f8b">Witaj %{name}%, <br/><br/>  
<table width="100%" border="0">
<tr><td style="text-align:center"> <h2 style="color:#094f8b">Aktualności</h2></td></tr>
<tr><td style="text-align:center;padding-bottom: 15px;"> Ostatnio dodane wiadomości w systemie BIKS:</td></tr>
 <tr><td style="border: 2px solid #094f8b;padding-left: 20px;padding-right: 20px; padding-bottom: 20px;">%{bodyNews}%</td></tr>
<tr><td style="text-align:center"> <h2 style="color:#094f8b">Ulubione </h2></td></tr>
<tr><td style="text-align:center;padding-bottom: 15px;"> Ostatnie zmiany w obiektach, które śledzisz:<br/></td></tr>
<tr><td style="border: 2px solid #094f8b;padding: 20px;">%{bodyFvs}% </td></tr>
<tr><td style="text-align:center"> <h2 style="color:#094f8b">Sugerowane elementy </h2></td></tr>
<tr><td style="text-align:center;padding-bottom: 15px;"> System BIKS zauważył, że oglądałeś ostatnio poniższe obiekty. Może warto by było dodać je do ulubionych i być informowany o zmianach?<br/></td></tr>
 <tr><td style="border: 2px solid #094f8b;padding: 20px;">%{suggestedElements}% </td></tr> 
<tr><td><br/><br/> </td></tr>  
<tr><td style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Ten e-mail został wygenerowany automatycznie. Prosimy na niego nie odpowiadać. </td></tr>
<tr><td style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Jeśli nie chcesz otrzymywać powiadomień wejdź na %{page}% i odznacz opcję w edycji danych. </td></tr>
</table>
</body>'

where name='emailScheduleBody'

go

exec sp_update_version '1.7.z8.1', '1.7.z8.2';
go
