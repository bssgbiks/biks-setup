﻿exec sp_check_version '1.8.1.6';
go

if not exists (select 1 from bik_node_kind where code = 'metaBiksDefinitionGroup')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksDefinitionGroup', 'Grupa definicji', 'folder', 'metaBIKS', 1)
else update bik_node_kind set caption='Grupa definicji', icon_name='folder', tree_kind='metaBIKS', is_folder=1 where code='metaBiksDefinitionGroup'

if not exists (select 1 from bik_node_kind where code = 'metaBiksAttributesCatalog')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksAttributesCatalog', 'Kategoria atrybutów', 'dimension', 'metaBIKS', 1)
else update bik_node_kind set caption='Kategoria atrybutów', icon_name='dimension', tree_kind='metaBIKS', is_folder=1 where code='metaBiksAttributesCatalog'

if not exists (select 1 from bik_node_kind where code = 'metaBiksAttributeDef')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksAttributeDef', 'Definicja atrybutu', 'aliasTable', 'metaBIKS', 0)
else update bik_node_kind set caption='Definicja atrybutu', icon_name='aliasTable', tree_kind='metaBIKS', is_folder=0 where code='metaBiksAttributeDef'

if not exists (select 1 from bik_node_kind where code = 'metaBiksAttribute4NodeKind')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksAttribute4NodeKind', 'Atrybut', 'client', 'metaBIKS', 0)
else update bik_node_kind set caption='Atrybut', icon_name='client', tree_kind='metaBIKS', is_folder=0 where code='metaBiksAttribute4NodeKind'

if not exists (select 1 from bik_node_kind where code = 'metaBiksNodeKind')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksNodeKind', 'Definicja typu obiektu', 'bwcube', 'metaBIKS', 1)
else update bik_node_kind set caption='Definicja typu obiektu', icon_name='bwcube', tree_kind='metaBIKS', is_folder=1 where code='metaBiksNodeKind'

if not exists (select 1 from bik_node_kind where code = 'metaBiksTreeKind')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksTreeKind', 'Definicja typu drzewa', 'bwarea', 'metaBIKS', 1)
else update bik_node_kind set caption='Definicja typu drzewa', icon_name='bwarea', tree_kind='metaBIKS', is_folder=1 where code='metaBiksTreeKind'

if not exists (select 1 from bik_node_kind where code = 'metaBiksNodeKind4TreeKind')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksNodeKind4TreeKind', 'Typ obiektu w drzewie', 'universeColumn', 'metaBIKS', 1)
else update bik_node_kind set caption='Typ obiektu w drzewie', icon_name='universeColumn', tree_kind='metaBIKS', is_folder=1 where code='metaBiksNodeKind4TreeKind'

if not exists (select 1 from bik_node_kind where code = 'metaBiksNodeKindInRelation')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksNodeKindInRelation', 'Związany typ obiektu', 'universeColumn', 'metaBIKS', 1)
else update bik_node_kind set caption='Związany typ obiektu', icon_name='postgresSchema', tree_kind='metaBIKS', is_folder=1 where code='metaBiksNodeKindInRelation'

exec sp_update_version '1.8.1.6', '1.8.1.7';

