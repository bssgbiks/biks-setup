exec sp_check_version '1.7.z7.10';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_object_in_fvs_change_ex') and name = 'cut_parent_id') begin
  alter table bik_object_in_fvs_change_ex add cut_parent_id int null foreign key references bik_node (id);
  alter table bik_object_in_fvs_change_ex add is_pasted int not null default 0;
end;
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.10', '1.7.z7.11';
go
