﻿exec sp_check_version '1.8.4.17';
go

UPDATE bik_app_prop
SET val = '.pdf;.jpeg;.gif;.jpg;.png;.txt;.metadata;.csv'
WHERE name = 'allowedFileExtensions'

exec sp_update_version '1.8.4.17', '1.8.4.18';
go
