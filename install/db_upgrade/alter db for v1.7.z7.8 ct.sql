﻿exec sp_check_version '1.7.z7.8';
go

create table bik_lisa_dict_import_cache(
	server_file_name varchar(max),
	line_nr int,
	pk_value varchar(max),
	action_code varchar(10)
);

exec sp_update_version '1.7.z7.8', '1.7.z7.9';
go