﻿exec sp_check_version '1.7.z8.6';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_file_system') and name = 'file_since')
	alter table bik_file_system add file_since varchar(max)
go

exec sp_update_version '1.7.z8.6', '1.7.z8.7';
go

