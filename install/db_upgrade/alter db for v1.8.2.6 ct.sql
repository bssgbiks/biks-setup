﻿exec sp_check_version '1.8.2.6';
go

update bik_node_kind set is_folder = 0 where code='metaBiksNodeKindInRelation'
update bik_tree_kind set leaf_node_kind_id = (select top 1 id from bik_node_kind where is_folder=0 and tree_kind='metaBIKS') where code='metaBIKS'

declare @treeId int = (select id from bik_tree where code = 'metaBIKS@tmp')
  
delete from bik_tree_icon where tree_id = @treeId

delete from bik_node_vote where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_object_in_fvs_change where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_object_in_fvs_change_ex where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_attachment where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_blog where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_authors where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_object_author where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_node_author where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_user_right where tree_id = @treeId

delete from bik_attribute_linked where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)


delete from  bik_joined_obj_attribute_linked where joined_obj_id in
    (select id from bik_joined_objs where dst_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)
)

delete from  bik_joined_obj_attribute_linked where joined_obj_id in
    (select id from bik_joined_objs where src_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)
)

delete from bik_joined_objs where dst_node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_joined_objs where src_node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_note where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_obj_in_body_node where linked_node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_object_author where node_id in (
    select id
    from bik_node
    where tree_id = @treeId) 
    or node_id in (select id from bik_node   where linked_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)
)


delete from bik_object_in_fvs where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_object_in_history where node_id in (
    select id
    from bik_node
    where tree_id = @treeId) 
    or node_id in (select id from bik_node   where linked_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)
)


delete from bik_user_in_node where node_id in (
    select id
    from bik_node
    where tree_id = @treeId) 
    or node_id in (select id from bik_node   where linked_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)
)


delete from bik_metapedia where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_confluence_extradata where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

delete from bik_confluence_attachment where node_id in (
    select id
    from bik_node
    where tree_id = @treeId
)

    

delete from bik_statistic_ext where clicked_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)
    or clicked_node_id in (
    select bn.id
    from bik_node bn
    inner join bik_node oryg on oryg.id = bn.linked_node_id
    where oryg.tree_id = @treeId
)

delete from bik_statistic_dict  where
    exists ( select * from bik_tree bt
    where bt.id=@treeId and bik_statistic_dict.counter_name=bt.code
);

declare @treeOfTreesId int = (select id from bik_tree where code = 'TreeOfTrees');
declare @tree_code varchar(255) = (select code from bik_tree where id = 77);
declare @node_ids bik_unique_not_null_id_table_type;

insert into @node_ids (id)
select id from bik_node where tree_id = @treeId;

insert into @node_ids (id)
select id from bik_node where tree_id = @treeOfTreesId and obj_id = '$' + @tree_code;

exec sp_delete_node_history_by_node_ids @node_ids

    
delete from bik_custom_right_role_user_entry where tree_id=@treeId

delete from bik_node
where linked_node_id in (
select id
from bik_node
where tree_id = @treeId)

delete from bik_node
where tree_id = @treeId

delete from bik_custom_right_role_action_branch_grant where tree_id=@treeId
delete from bik_custom_right_role_rut_entry where tree_id=@treeId
delete from bik_custom_right_role_user_entry where tree_id=@treeId

delete from bik_node
	where parent_node_id in (
    select id
    from bik_node
    where tree_id = @treeId)

delete from bik_node where tree_id = @treeId

update bik_plain_file set tree_id = null where tree_id = @treeId

delete from bik_tree
    where id = @treeId    

delete from bik_node
    where tree_id = @treeOfTreesId and obj_id = '$' + @tree_code

delete from bik_translation where code=(select code from bik_tree where id=@treeId)
    and kind='tree'

exec sp_delete_childless_folders;

exec sp_update_version '1.8.2.6', '1.8.2.7';
go
