﻿exec sp_check_version '1.7.z8.9';
go

if object_id('[dbo].[fn_node_id_by_tree_code_and_obj_id]') is not null
  drop function [dbo].[fn_node_id_by_tree_code_and_obj_id]
go

create function [dbo].[fn_node_id_by_tree_code_and_obj_id](@tree_code varchar(256), @obj_id varchar(900))
returns int
as
begin
  return (select id from bik_node where tree_id = dbo.fn_tree_id_by_code(@tree_code) and obj_id = @obj_id)
end
go

exec sp_update_version '1.7.z8.9', '1.7.z8.10';
go