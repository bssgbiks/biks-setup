﻿exec sp_check_version '1.8.1.3';
go

update bik_node_kind set caption = substring(caption, 0, charindex('-'+code, caption, len(caption)-len(code)-1)) where caption like ('%-'+code)

exec sp_update_version '1.8.1.3', '1.8.1.4';