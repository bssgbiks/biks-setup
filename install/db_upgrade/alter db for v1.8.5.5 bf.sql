﻿exec sp_check_version '1.8.5.5';
go


if not exists (select * from bik_app_prop where name='availableConnectors' and val like '%Permissions%') 
update bik_app_prop set val=val+',Permissions' where name='availableConnectors'


if not exists (select * from bik_admin where param='permissions.isActive')
insert into bik_admin(param,value) values('permissions.isActive','0')

if not exists (select * from bik_admin where param='permissions.sql')
insert into bik_admin(param,value) values('permissions.sql','delete from bik_custom_right_role_user_entry where rights_processor=''sql''
	declare @roleId int = (select id from bik_custom_right_role where code=''nazwa roli np Author'')
	
	insert into bik_custom_right_role_user_entry(user_id,role_id,tree_id,node_id,rights_processor)	
	select 
		bsu.id
		,@roleId
		,x.tree_id
		,x.node_id
		,''sql'' 
	from bik_system_user bsu 
	join bik_user bu on bsu.user_id=bu.id
	join (
		SELECT bal.node_id,bal.attribute_id,bal.value,bn.tree_id,
		SUBSTRING(str,0, CHARINDEX(''_'',str,0))as user_node_id FROM bik_attribute_linked  bal
		cross apply dbo.fn_split_by_sep(bal.value, ''|'', 7) as sp 
		join bik_attribute ba on bal.attribute_id=ba.id
		join bik_attr_def bad on bad.id=ba.attr_def_id 
		join bik_node bn on bn.id=bal.node_id
		where ba.is_deleted=0 and bad.is_deleted=0 
		and bad.name in (''Nazwy atrybutow po przecinku'',''np Właściciel'',''np Opiekun''))x on x.user_node_id=bu.node_id
	
	
	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants')

if not exists( select * from bik_data_source_def where description = 'Permissions' )  
  begin
	insert into bik_data_source_def(description)
	values ('Permissions')    
	
    declare @sourceId int, @priority int;
    select @sourceId = id from bik_data_source_def where description = 'Permissions'

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority)
	values(@sourceId, 0, 0, 1, 0, 11)
  end

exec sp_update_version '1.8.5.5','1.8.5.6';
go
