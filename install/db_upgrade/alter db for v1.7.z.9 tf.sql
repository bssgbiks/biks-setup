﻿exec sp_check_version '1.7.z.9';
go

declare @newActions table (
	code varchar(512) not null
);
declare @roleEditor int = (select id from bik_custom_right_role where code = 'DqmEditor') ;

insert into @newActions(code)
values ('DQMAddEvaluation'),
('DQMAddReport'),
('DQMCheckRate'),
('DQMDownloadReport'),
('DQMEditEvaluation'),
('DQMRefreshReport')

insert into bik_node_action
select code from @newActions as ac
where not exists (select 1 from bik_node_action where code = ac.code)

if @roleEditor is not null
  insert into bik_node_action_in_custom_right_role(action_id, role_id)
  select na.id, @roleEditor
  from @newActions as ac
  inner join bik_node_action as na on na.code = ac.code
  where not exists (select * from bik_node_action_in_custom_right_role as crr where crr.role_id = @roleEditor and crr.action_id = na.id)
go

exec sp_update_version '1.7.z.9', '1.7.z1';
go
