﻿exec sp_check_version '1.7.z8.15';
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_sapbo_schedule') and name = 'destination_folder_path')
alter table bik_sapbo_schedule add destination_folder_path varchar(max) null;
go


if not exists(select 1 from syscolumns sc where id = OBJECT_ID('aaa_report_schedule') and name = 'destination_folder_path')
alter table aaa_report_schedule add destination_folder_path varchar(max) null;
go

-- poprzednia wersja w: alter db for v1.6.z5.2 tf.sql
-- dodanie do BO harmonogramów folderow harmonogramow
if exists(select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_node_sapbo_extradata]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
go

create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();

	delete from bik_sapbo_extradata
	from bik_sapbo_extradata
	inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	where bn.tree_id in (@reportTreeId , @universeTreeId, @connectionTreeId)
	
	
	declare @tempTable table (
		id int identity(1,1) not null,
		column_name varchar(50) not null,
		table_name varchar(50) not null
	);
	
	-- ważna jest kolejnosc wstawiania
	insert into @tempTable(column_name,table_name)
	values
		('SI_AUTHOR','INFO_WEBI'),
		('SI_OWNER','aaa_global_props'),
		('SI_KEYWORD','aaa_global_props'),
		('SI_FILES__SI_PATH','aaa_global_props'),
		('SI_FILES__SI_FILE1','aaa_global_props'),
		('SI_CREATION_TIME','aaa_global_props'),
		('SI_UPDATE_TS','aaa_global_props'),
		('SI_CUID','aaa_global_props'),
		('SI_FILES__SI_VALUE1','aaa_global_props'),
		('SI_SIZE','aaa_global_props'),
		('SI_HAS_CHILDREN','aaa_global_props'),
		('SI_GUID','aaa_global_props'),
		('SI_INSTANCE','aaa_global_props'),
		('SI_OWNERID','aaa_global_props'),
		('SI_PROGID','aaa_global_props'),
		('SI_OBTYPE','aaa_global_props'),
		('SI_FLAGS','aaa_global_props'),
		('SI_CHILDREN','aaa_global_props'),
		('SI_RUID','aaa_global_props'),
		('SI_RUNNABLE_OBJECT','aaa_global_props'),
		('SI_CONTENT_LOCALE','aaa_global_props'),
		('SI_IS_SCHEDULABLE','aaa_global_props'),
		('SI_WEBI_DOC_PROPERTIES','aaa_global_props'),
		('SI_READ_ONLY','aaa_global_props'),
		('SI_LAST_SUCCESSFUL_INSTANCE_ID','aaa_global_props'),
		('SI_LAST_RUN_TIME','aaa_global_props'),
		('SI_TIMESTAMP','INFO_WEBI'),
		('SI_PROGID_MACHINE','INFO_WEBI'),
		('SI_ENDTIME','aaa_global_props'),
		('SI_STARTTIME','aaa_global_props'),
		('SI_SCHEDULE_STATUS','aaa_global_props'),
		('SI_RECURRING','aaa_global_props'),
		('SI_NEXTRUNTIME','aaa_global_props'),
		('SI_DOC_SENDER','aaa_global_props'),
		('SI_REVISIONNUM','APP_UNIVERSE'),
		('SI_APPLICATION_OBJECT','APP_UNIVERSE'),
		('SI_SHORTNAME','APP_UNIVERSE'),
		('SI_DATACONNECTION__SI_TOTAL','APP_UNIVERSE'),
		('SI_UNIVERSE__SI_TOTAL','INFO_WEBI')
		
	declare @sqlText varchar(max);
	declare @count int;
	select @count = count(*) from @tempTable
	
	set @sqlText = 'insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id, '
		
	declare kurs cursor for 
	select column_name, table_name from @tempTable order by id 

	open kurs;
	declare @column varchar(50);
	declare @table varchar(50);
	declare @i int = 1;

	fetch next from kurs into @column, @table;
	while @@fetch_status = 0
	begin
		if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
		begin
			set @sqlText = @sqlText + @table + '.' + @column
		end
		else
		begin
			set @sqlText = @sqlText + 'null'
		end
		if(@i <> @count)
		begin
			set @sqlText = @sqlText + ', '
		end
		--
		set @i = @i + 1;
		fetch next from kurs into @column, @table;
	end
	close kurs;
	deallocate kurs;	

	set @sqlText = @sqlText + ' from aaa_global_props
    left join INFO_WEBI on aaa_global_props.si_id = INFO_WEBI.si_id
    left join APP_UNIVERSE on aaa_global_props.si_id = APP_UNIVERSE.si_id
    inner join bik_node bik on convert(varchar(30),aaa_global_props.si_id) = bik.obj_id
    where bik.tree_id in (' + convert(varchar(10),@reportTreeId) + ', ' + convert(varchar(10),@universeTreeId) + ', ' + convert(varchar(10),@connectionTreeId) + ') 
    and bik.is_deleted = 0
    and bik.linked_node_id is null
    order by bik.id'
    
    exec(@sqlText);
    
    delete from bik_sapbo_schedule
    from bik_sapbo_schedule
	inner join bik_node bn on bn.id = bik_sapbo_schedule.node_id
	where bn.tree_id = @reportTreeId
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to, destination_folder_path)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to, destination_folder_path
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    --inner join bik_tree bt on bt.id = bn.tree_id
    where bn.tree_id = @reportTreeId 
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    --and bt.code in ('Reports')
    

    
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_COMMONCONNECTION')
	begin
		    delete from bik_sapbo_olap_connection
			from bik_sapbo_olap_connection
			inner join bik_node bn on bik_sapbo_olap_connection.node_id = bn.id
			where bn.tree_id = @connectionTreeId
			
			delete from @tempTable;
			
			insert into @tempTable(column_name,table_name)
			values
				('SI_CONNECTION_TYPE','APP_COMMONCONNECTION'),
				('SI_PROVIDER_CAPTION','APP_COMMONCONNECTION'),
				('SI_CUBE_CAPTION','APP_COMMONCONNECTION'),
				('SI_CUID','APP_COMMONCONNECTION'),
				('SI_CREATION_TIME','APP_COMMONCONNECTION'),
				('SI_UPDATE_TS','APP_COMMONCONNECTION'),
				('SI_OWNER','APP_COMMONCONNECTION'),
				('SI_NETWORK_LAYER','APP_COMMONCONNECTION'),
				('SI_MD_USERNAME','APP_COMMONCONNECTION')
				
			set @sqlText = '';
			set @count = 0;;
			select @count = count(*) from @tempTable
			
			set @sqlText = 'insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, ';
				
			declare kurs cursor for 
			select column_name, table_name from @tempTable order by id 

			open kurs;
			set @column = '';
			set @table = '';
			set @i = 1;

			fetch next from kurs into @column, @table;
			while @@fetch_status = 0
			begin
				if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
				begin
					set @sqlText = @sqlText + @table + '.' + @column
				end
				else
				begin
					set @sqlText = @sqlText + 'null'
				end
				if(@i <> @count)
				begin
					set @sqlText = @sqlText + ', '
				end
				--
				set @i = @i + 1;
				fetch next from kurs into @column, @table;
			end
			close kurs;
			deallocate kurs;	

			set @sqlText = @sqlText + ' from APP_COMMONCONNECTION  
			inner join bik_node bn on bn.obj_id = convert(varchar(30),APP_COMMONCONNECTION.si_id) and bn.tree_id = ' + convert(varchar(10),@connectionTreeId);
		    
			exec(@sqlText);
			
			/*
			insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, app.SI_CONNECTION_TYPE, app.SI_PROVIDER_CAPTION, app.SI_CUBE_CAPTION, app.SI_CUID, app.SI_CREATION_TIME, app.SI_UPDATE_TS, app.SI_OWNER, app.SI_NETWORK_LAYER, app.SI_MD_USERNAME 
			from APP_COMMONCONNECTION app 
			inner join bik_node bn on bn.obj_id = convert(varchar(30),app.si_id) and tree_id = @connectionTreeId
			*/
	end
end;
go


exec sp_update_version '1.7.z8.15', '1.7.z8.16';
go