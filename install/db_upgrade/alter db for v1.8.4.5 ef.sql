exec sp_check_version '1.8.4.5';
GO

UPDATE [dbo].[bik_right_role]
SET rank = 30
WHERE code = 'NonPublicUser'
GO

UPDATE [dbo].[bik_right_role]
SET rank = 55
WHERE code = 'Creator'
GO

ALTER TABLE [dbo].[bik_custom_right_role_user_entry] ADD rights_processor VARCHAR(5)
GO

DELETE [dbo].[bik_node_action_in_custom_right_role]
WHERE role_id = (
		SELECT id
		FROM bik_custom_right_role
		WHERE code = 'NonPublicUser'
		)
GO

SET ANSI_NULLS ON
GO

SET IDENTITY_INSERT [dbo].[bik_node_action_in_custom_right_role] ON
GO

DECLARE @id AS INT;
DECLARE @nonPublicUserId AS INT = (
		SELECT id
		FROM [dbo].[bik_custom_right_role]
		WHERE code = 'NonPublicUser'
		);

DECLARE cur CURSOR LOCAL
FOR
SELECT action_id
FROM [dbo].[bik_node_action_in_custom_right_role]
WHERE role_id = (
		SELECT id
		FROM bik_custom_right_role
		WHERE code = 'RegularUser'
		)

DECLARE @nextId AS INT

OPEN cur

FETCH NEXT
FROM cur
INTO @id

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @nextId = max(id)
	FROM [dbo].[bik_node_action_in_custom_right_role]

	INSERT INTO [dbo].[bik_node_action_in_custom_right_role] (
		id
		,action_id
		,role_id
		)
	VALUES (
		@nextId + 1
		,@id
		,@nonPublicUserId
		)

	FETCH NEXT
	FROM cur
	INTO @id
END

CLOSE cur

DEALLOCATE cur
GO

SET IDENTITY_INSERT [dbo].[bik_node_action_in_custom_right_role] OFF
GO

exec sp_update_version '1.8.4.5'
	,'1.8.4.6';
GO


