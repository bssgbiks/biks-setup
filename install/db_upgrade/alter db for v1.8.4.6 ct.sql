﻿exec sp_check_version '1.8.4.6';
go

if not exists (select 1 from bik_app_prop where name='useProgramerEditMode' )
insert into bik_app_prop(name, val) values ('useProgramerEditMode', 'false')
else 
update bik_app_prop set val='false' where name='useProgramerEditMode' 

exec sp_update_version '1.8.4.6', '1.8.4.7';
go

