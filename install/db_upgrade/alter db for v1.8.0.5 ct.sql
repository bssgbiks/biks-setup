﻿exec sp_check_version '1.8.0.5';
go

if not exists (select 1 from sys.columns where object_id=object_id(N'bik_joined_obj_attribute') and name='in_drools')
alter table bik_joined_obj_attribute add in_drools int default 0;
go

exec sp_update_version '1.8.0.5', '1.8.0.6';
go
