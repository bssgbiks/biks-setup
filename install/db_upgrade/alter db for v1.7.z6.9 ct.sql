exec sp_check_version '1.7.z6.9';
go

--------------------------
if not exists(select 1 from sys.columns where name = N'max_file_size' and object_id = object_id(N'bik_file_system'))
begin
	alter table bik_file_system add max_file_size numeric(10,2);   
end
go

if not exists(select 1 from sys.columns where name = N'file_name_patterns' and object_id = object_id(N'bik_file_system'))
begin
	alter table bik_file_system add file_name_patterns varchar(max);   
end
go

if not exists(select 1 from bik_node_kind where code='RawFile')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('RawFile', 'Plik', 'raw_file', 'Documents', 0, 6, 0)
end
go

if not exists(select 1 from sys.tables where object_id = object_id(N'aaa_file_system_info'))
begin
	create table aaa_file_system_info(
		target_file_path varchar(max),
		obj_id varchar(max),
		file_size bigint,
		last_modified bigint,
		download_link varchar(max),
		file_name varchar(max)
	);
end
go

if not exists(select 1 from sys.tables where object_id = object_id(N'bik_file_system_info'))
begin
	create table bik_file_system_info(
		node_id int foreign key references bik_node(id) not null unique,
		target_file_path varchar(max) not null,		
		file_size bigint not null,
		last_modified bigint not null,
		download_link varchar(max),
		file_name varchar(max) not null
	);
end
go
--------------------------

exec sp_update_version '1.7.z6.9', '1.7.z6.10'
go