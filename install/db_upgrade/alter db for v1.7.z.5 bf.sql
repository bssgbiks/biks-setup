﻿exec sp_check_version '1.7.z.5';
go

alter table bik_lisa_logs alter column info varchar(max) null;

if not exists (select * from bik_app_prop where name = 'isAlternativeJoinedPanelViewEnabled')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('isAlternativeJoinedPanelViewEnabled', 'false', 1)
end;
go
 
exec sp_update_version '1.7.z.5', '1.7.z.6';
go
