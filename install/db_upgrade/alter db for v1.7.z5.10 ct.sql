exec sp_check_version '1.7.z5.10';
go

if not exists (select 1 from bik_data_source_def where description = 'JDBC') 
begin
	insert into bik_data_source_def(description) values('JDBC');
end;

--update bik_app_prop set val= 'MS SQL,DQM,Profile,Oracle,PostgreSQL,Active Directory,JDBC' where name='availableConnectors';

create table bik_jdbc(
   id int not null IDENTITY(1,1) primary key,
   name varchar(800) not null,
   jdbc_url varchar(256),
   db_user varchar(256),
   db_password varchar(256),
   driver_name varchar(256),
   query varchar(8000),
   tree_id int foreign key references bik_tree(id),
   source_id int foreign key references bik_data_source_def(id),
   is_active int not null default(0)
);

exec sp_update_version '1.7.z5.10', '1.7.z5.11';
go 