﻿exec sp_check_version '1.7.z10.1';
go

---ukrywanie, pokazywaie drzewa---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_show_or_hide_tree]'))
drop procedure [dbo].[api_show_or_hide_tree]
go

create procedure [dbo].[api_show_or_hide_tree](@tree_code varchar(255), @is_hidden int)
as
begin
	update bik_tree set is_hidden = @is_hidden where code = @tree_code;
end
go

---lista drzew---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_tree_list]'))
drop procedure [dbo].[api_tree_list]
go

create procedure [dbo].[api_tree_list]
as 
begin 
	select code, name, is_hidden from bik_tree
end
go

---lista obiektow dla drzewa---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_tree_objects_list]'))
drop procedure [dbo].[api_tree_objects_list]
go

create procedure [dbo].[api_tree_objects_list](@tree_code varchar(255))
as 
begin 
	select bn.name, bn.id, bn.parent_node_id as parent_id, dbo.fn_get_branch_path(bn.id) as path, bn.descr 
	from bik_node bn join bik_tree bt on bn.tree_id = bt.id 
	where bt.code = @tree_code and bn.is_deleted = 0
end
go

---lista typow wezlow---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_node_kind_list]'))
drop procedure [dbo].[api_node_kind_list]
go

create procedure [dbo].[api_node_kind_list]
as
begin
	select code, caption as name from bik_node_kind 
end
go

---usun wezel---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_delete_node]'))
drop procedure [dbo].[api_delete_node]
go

create procedure [dbo].[api_delete_node](@id int)
as
begin
	declare @branch_ids varchar(1000) = (select branch_ids from bik_node where id = @id)
	update bik_node set is_deleted = 1 where branch_ids like @branch_ids + '%'
end
go

---edytuj wezel---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_edit_node]'))
drop procedure [dbo].[api_edit_node]
go

create procedure  [dbo].[api_edit_node](@id int, @name varchar(4000), @descr varchar(max))
as
begin
	if (@name is not null)
	update bik_node set name = @name, descr = @descr where id = @id and is_deleted = 0
	else update bik_node set descr = @descr where id = @id and is_deleted = 0
end
go

---dodaj wezel---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_add_node]'))
drop procedure [dbo].[api_add_node]
go

create procedure [dbo].[api_add_node](@tree_code varchar(255), @parent_id int, @name varchar(4000), @descr varchar(max), @node_kind_code varchar(255))
as
begin
	declare @node_kind_id int = (select id from bik_node_kind where code = @node_kind_code)
	declare @tree_id int = (select id from bik_tree where code = @tree_code)
	
	if @parent_id is null or exists (select 1 from bik_node where id = @parent_id and tree_id = @tree_id and is_deleted = 0)
	begin 
		begin tran
		insert into bik_node(parent_node_id, node_kind_id, name, tree_id, descr) 
		values(@parent_id, @node_kind_id, @name, @tree_id, @descr)
	
		declare @node_id int = scope_identity()
		declare @nodeFilter varchar(max) = 'n.id in (' + cast(@node_id as varchar(max)) + ')'
		exec sp_node_init_branch_id @tree_id, @node_id
		exec sp_verticalize_node_attrs_inner @nodeFilter
		commit
	end
end
go

---lista powiazan---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_list_joined_objs]'))
drop procedure [dbo].[api_list_joined_objs]
go

create procedure [dbo].[api_list_joined_objs](@node_id int)
as 
begin
	select bn.name, dbo.fn_get_branch_path(bn.id) as path, bn.descr 
	from bik_node bn join bik_joined_objs bjo on bn.id = bjo.src_node_id 
	where bjo.src_node_id = @node_id and bn.is_deleted = 0
end
go

---dodaj powiazanie---
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_add_joined_obj]'))
drop procedure [dbo].[api_add_joined_obj]
go

create procedure [dbo].[api_add_joined_obj](@src_node_id int, @dst_node_id int, @bidirection int)
as 
begin
	insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_To_Descendants)
    select @src_node_id, @dst_node_id, 0, 0
    where not exists (select 1 from bik_joined_objs
    where src_node_id = @src_node_id and dst_node_id = @dst_node_id)
    
    if (@bidirection > 0) 
	insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_To_Descendants)
    select @dst_node_id, @src_node_id, 0, 0
    where not exists (select 1 from bik_joined_objs
    where src_node_id = @dst_node_id and dst_node_id = @src_node_id)
end
go

---Usun powiazanie
if exists (select 1 from sys.objects where object_id = object_id(N'[dbo].[api_delete_joined_obj]'))
drop procedure [dbo].[api_delete_joined_obj]
go

create procedure [dbo].[api_delete_joined_obj](@src_node_id int, @dst_node_id int, @bidirection int)
as 
begin
	delete bik_joined_objs  
    where src_node_id = @src_node_id and dst_node_id = @dst_node_id
    
    if (@bidirection > 0) 
	delete bik_joined_objs
    where src_node_id = @dst_node_id and dst_node_id = @src_node_id
end
go

exec sp_update_version '1.7.z10.1', '1.7.z10.2';
go