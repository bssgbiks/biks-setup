﻿exec sp_check_version '1.8.2.17';
go

declare @treeOfTrees int = (select id from bik_tree where code='TreeOfTrees')

declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Wzorzec', @metaCategory, 'hyperlinkInMono', null, 0, 0
 
declare @connectors varchar(max) = (select top 1 val from bik_app_prop where name='availableConnectors')
if not exists (select 1 from dbo.fn_split_by_sep(@connectors, ',', 7) where str='DQM')
begin
	update bik_node set is_deleted = 1 where tree_id=@treeOfTrees and obj_id in ('admin:dqm', 'dqm', '#admin:dqm:config', '#admin:dqm:logs', '#admin:dqm:schedule')
end
 
if not exists (select 1 from bik_app_prop where name='enableAttributeUsagePage' and (val='1' or val='true'))
update bik_node set is_deleted = 1 where tree_id=@treeOfTrees and obj_id in ('#admin:dict:attrUsage')

if not exists (select 1 from bik_node_kind where code = 'metaBiksMenuGroup')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksMenuGroup', 'Grupa', 'oraclePackage', 'metaBIKSMenuConf', 1)
else update bik_node_kind set caption='Grupa', icon_name='oraclePackage', tree_kind='metaBIKSMenuConf', is_folder=1 where code='metaBiksMenuGroup'
declare @menuGroupNk int = (select id from bik_node_kind where code='metaBiksMenuGroup')

if not exists (select 1 from bik_node_kind where code = 'metaBiksMenuItem')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksMenuItem', 'Drzewo', 'dqmtestsuccess', 'metaBIKSMenuConf', 1)
else update bik_node_kind set caption='Drzewo', icon_name='dqmtestsuccess', tree_kind='metaBIKSMenuConf', is_folder=0 where code='metaBiksMenuItem'
declare @menuItemNk int = (select id from bik_node_kind where code='metaBiksMenuItem')
 
exec sp_add_attr_def 'metaBIKS.Typ drzewa', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_2_node_kind 'metaBiksMenuItem', 'metaBIKS.Typ drzewa', null, 1, null, null

if not exists (select 1 from bik_tree_kind where code='metaBIKSMenuConf')
insert into bik_tree_kind(code, caption, branch_node_kind_id, leaf_node_kind_id)
values ('metaBIKSMenuConf', 'Struktura drzew', @menuGroupNk, @menuItemNk)
else
update bik_tree_kind set caption='Struktura drzew', branch_node_kind_id=@menuGroupNk, leaf_node_kind_id=@menuItemNk, is_deleted=0, allow_linking=0
where code='metaBIKSMenuConf'
declare @menuConfTk int = (select id from bik_tree_kind where code='metaBIKSMenuConf')

if not exists (select 1 from bik_node_kind_4_tree_kind where tree_kind_id=@menuConfTk and src_node_kind_id is null and dst_node_kind_id=@menuGroupNk and relation_type is null)
insert into bik_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type, is_deleted)
values (@menuConfTk, null, @menuGroupNk, null, 0)
else 
update bik_node_kind_4_tree_kind set is_deleted=0
where tree_kind_id=@menuConfTk and src_node_kind_id is null and dst_node_kind_id=@menuGroupNk and relation_type is null

if not exists (select 1 from bik_node_kind_4_tree_kind where tree_kind_id=@menuConfTk and src_node_kind_id is null and dst_node_kind_id=@menuItemNk and relation_type is null)
insert into bik_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type, is_deleted)
values (@menuConfTk, null, @menuItemNk, null, 0)
else 
update bik_node_kind_4_tree_kind set is_deleted=0
where tree_kind_id=@menuConfTk and src_node_kind_id is null and dst_node_kind_id=@menuItemNk and relation_type is null

if not exists (select 1 from bik_node_kind_4_tree_kind where tree_kind_id=@menuConfTk and src_node_kind_id=@menuItemNk and dst_node_kind_id is null and relation_type is null)
insert into bik_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type, is_deleted)
values (@menuConfTk, @menuItemNk, null, null, 0)
else 
update bik_node_kind_4_tree_kind set is_deleted=0
where tree_kind_id=@menuConfTk and src_node_kind_id=@menuItemNk and dst_node_kind_id is null and relation_type is null
 
if not exists (select 1 from bik_node_kind_4_tree_kind where tree_kind_id=@menuConfTk and src_node_kind_id=@menuGroupNk and dst_node_kind_id=@menuGroupNk and relation_type='Dziecko')
insert into bik_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type, is_deleted)
values (@menuConfTk, @menuGroupNk, @menuGroupNk, 'Dziecko', 0)
else 
update bik_node_kind_4_tree_kind set is_deleted=0
where tree_kind_id=@menuConfTk and src_node_kind_id=@menuGroupNk and dst_node_kind_id=@menuGroupNk and relation_type='Dziecko'
 
if not exists (select 1 from bik_node_kind_4_tree_kind where tree_kind_id=@menuConfTk and src_node_kind_id=@menuGroupNk and dst_node_kind_id=@menuItemNk and relation_type='Dziecko')
insert into bik_node_kind_4_tree_kind(tree_kind_id, src_node_kind_id, dst_node_kind_id, relation_type, is_deleted)
values (@menuConfTk, @menuGroupNk, @menuItemNk, 'Dziecko', 0)
else 
update bik_node_kind_4_tree_kind set is_deleted=0
where tree_kind_id=@menuConfTk and src_node_kind_id=@menuGroupNk and dst_node_kind_id=@menuItemNk and relation_type='Dziecko'

if not exists (select 1 from bik_tree where code='MenuConfiguration' and tree_kind='metaBIKSMenuConf')
insert into bik_tree(name, node_kind_id, code, tree_kind, is_auto_obj_id)
values ('Zarządzanie menu', null, 'MenuConfiguration', 'metaBIKSMenuConf', 1)
else 
update bik_tree set name='Zarządzanie menu', node_kind_id=null, is_hidden=0, is_in_ranking=0, branch_level_for_statistics=0, is_built_in=0 where code='MenuConfiguration' and tree_kind='metaBIKSMenuConf'

if not exists (select 1 from bik_app_prop where name='menuConfigTree')
insert into bik_app_prop(name, val) values('menuConfigTree', 'MenuConfiguration')
else 
update bik_app_prop set val='MenuConfiguration' where name='menuConfigTree'

declare @menuConfTree int = (select id from bik_tree where code='MenuConfiguration')

exec sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex;
exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

declare @menuCode varchar(255) = '$' + (select code from bik_tree where id = @menuConfTree)

if not exists (select 1 from bik_node where tree_id=@treeOfTrees and obj_id=@menuCode)
begin
	exec sp_add_menu_node 'Admin', '@', @menuCode
	declare @menuNodeId int = (select id from bik_node where tree_id = @treeOfTrees and obj_id = @menuCode)
	exec sp_set_node_visual_order_ex @menuNodeId, 9999, 1
	exec sp_node_init_branch_id @treeOfTrees, @menuNodeId
end 
 
 
exec sp_update_version '1.8.2.17', '1.8.3';
go


 