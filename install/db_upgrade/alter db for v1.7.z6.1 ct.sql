exec sp_check_version '1.7.z6.1';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------
if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[dbo].[bik_search_cache]') and type in (N'U'))
begin
	create table bik_search_cache (
		ticket varchar(256),
		node_id int foreign key references bik_node(id),
		appearance varchar(max)
	);
end;
go
------------


exec sp_update_version '1.7.z6.1', '1.7.z6.2';
go