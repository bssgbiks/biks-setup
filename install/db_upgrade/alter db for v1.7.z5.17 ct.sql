﻿exec sp_check_version '1.7.z5.17';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if not exists (select 1 from bik_tree where code = 'AttributeUsage')
begin
	insert into bik_tree(name, code, tree_kind, is_hidden)
	values('Użycia atrybutów', 'AttributeUsage', 'Metadata', 0);
	
	exec dbo.sp_add_menu_node 'admin:stat', 'Użycie atrybutów', '#admin:dict:attrUsage';
	
	insert into bik_translation(code, txt, lang, kind)
	values('#admin:dict:attrUsage', 'Użycie atrybutów', 'pl', 'node');
	insert into bik_translation(code, txt, lang, kind)
	values('#admin:dict:attrUsage', 'Attributes usage', 'en', 'node');
	insert into bik_translation(code, txt, lang, kind)
	values('AttributeUsage', 'Użycie atrybutów', 'pl', 'tree');
	insert into bik_translation(code, txt, lang, kind)
	values('AttributeUsage', 'Attributes usage', 'en', 'tree');
	
	--insert into bik_data_source_def(description) values('Attribute usage');
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
	values('AttributeCategory', 'Grupa atrybutów', 'folder', 'Metadata', 1);
	
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
	values('AttributeSingleValue', 'Wartość atrybutu', 'measure', 'Metadata', 0);
	
	declare @treeOfTreesId int = dbo.fn_tree_id_by_code('TreeOfTrees');
    exec sp_node_init_branch_id @treeOfTreesId, null;
    
    if not exists (select 1 from bik_node_action where code = '#admin:dict:attrUsage')
    begin 
       insert into bik_node_action(code) values ('#admin:dict:attrUsage');
    end;
    
    declare @actionId int = (select id from bik_node_action where code = '#admin:dict:attrUsage');
    declare @roleId int = (select id from bik_custom_right_role where code = 'Administrator');
    if @roleId is not null and not exists (select 1 from bik_node_action_in_custom_right_role where action_id = @actionId and role_id = @roleId)
    begin 
		insert into bik_node_action_in_custom_right_role(action_id,role_id) values (@actionId, @roleId);
    end;
    exec sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex;
end;
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z5.17', '1.7.z5.18';
go