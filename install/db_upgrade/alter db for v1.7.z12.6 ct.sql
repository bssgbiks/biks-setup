﻿exec sp_check_version '1.7.z12.6';
go

if object_id('bik_system_user_in_group_prepared') is null 
create table bik_system_user_in_group_prepared(
	id int identity(1,1) primary key,
	user_id int references bik_system_user(id),
	group_id int references bik_system_user_group(id),
	unique (user_id, group_id)
)   
go

if object_id('dbo.sp_recalculate_System_User_In_Group_Prepared', 'P') is not null
drop procedure dbo.sp_recalculate_System_User_In_Group_Prepared
go

create procedure dbo.sp_recalculate_System_User_In_Group_Prepared as
begin
	truncate table bik_system_user_in_group_prepared;
	insert into bik_system_user_in_group_prepared(user_id, group_id)
	select su.id, x.group_id 
	from bik_system_user su cross apply fn_get_system_user_groups(su.id) x
end;
go

exec sp_recalculate_System_User_In_Group_Prepared
go

if object_id('dbo.sp_recalculate_Custom_Right_Role_Action_Branch_Grants', 'P') is not null
drop procedure dbo.sp_recalculate_Custom_Right_Role_Action_Branch_Grants
go

create procedure dbo.sp_recalculate_Custom_Right_Role_Action_Branch_Grants as
begin
	if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
	begin
		return;
	end;
  
	declare @role_on_tree table(role_id int, tree_id int)
    insert into @role_on_tree 
    select distinct crr.id as role_id, t.id as tree_id
	from
		bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
		substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
		substring(x.str, 1, 1) <> '@' and t.code = x.str where t.is_hidden=0; 

	declare @user_role_on_tree_node table (user_id int, role_id int, tree_id int, node_id int);
	insert into @user_role_on_tree_node(user_id, role_id, tree_id, node_id)
	select user_id, role_id, tree_id, node_id from bik_custom_right_role_user_entry where user_id is not null
	union  
	select user_id, right_id as role_id, tree_id, null as node_id from bik_user_right 
	order by tree_id DESC, node_id;
	
	insert into @user_role_on_tree_node(user_id, role_id, tree_id, node_id)
	select urotn.user_id as user_id, urotn.role_id as role_id , rot.tree_id as tree_id, null as node_id 
	from @user_role_on_tree_node urotn join @role_on_tree rot on urotn.role_id=rot.role_id
	where urotn.tree_id is null group by urotn.user_id, urotn.role_id, rot.tree_id, node_id;

	delete from @user_role_on_tree_node where tree_id is null

	declare @group_role_on_tree_node table (group_id int, role_id int, tree_id int, node_id int);
	insert into @group_role_on_tree_node
	select group_id, role_id, tree_id, node_id from bik_custom_right_role_user_entry where user_id is null;
	
	insert into @group_role_on_tree_node(group_id, role_id, tree_id, node_id)
	select grotn.group_id as group_id, grotn.role_id as role_id , rot.tree_id as tree_id, null as node_id 
	from @group_role_on_tree_node grotn join @role_on_tree rot on grotn.role_id=rot.role_id
	where grotn.tree_id is null group by grotn.group_id, grotn.role_id, rot.tree_id, node_id;
	
	delete from @group_role_on_tree_node where tree_id is null
	
	insert into @user_role_on_tree_node(user_id, role_id, tree_id, node_id)
	select x.user_id, grotn.role_id, grotn.tree_id, grotn.node_id
	from bik_system_user_in_group_prepared x join @group_role_on_tree_node grotn on x.group_id=grotn.group_id
	--select count(1) from @user_role_on_tree_node
	--select * from bik_system_user su cross apply fn_get_system_user_groups(su.id) x join bik_system_user_group sg on sg.id=x.group_id where su.id =18
	--select * from @user_role_on_tree_node order by tree_id 
	--Liczymy tabele bik_custom_right_role_action_branch_grant
	truncate table bik_custom_right_role_action_branch_grant;
	insert into bik_custom_right_role_action_branch_grant(user_id, tree_id, action_id, branch_ids) 
	select urotn.user_id, urotn.tree_id, naicrr.action_id, case when urotn.node_id is null then '' else bn.branch_ids end as branch_ids
	from @user_role_on_tree_node urotn join bik_node_action_in_custom_right_role naicrr on urotn.role_id=naicrr.role_id  
	left join bik_node bn on urotn.node_id=bn.id
	group by urotn.user_id, urotn.tree_id, naicrr.action_id, urotn.node_id, branch_ids
end
go

exec sp_update_version '1.7.z12.6', '1.7.z12.7';
go