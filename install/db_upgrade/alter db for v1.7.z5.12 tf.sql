exec sp_check_version '1.7.z5.12';
go


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

if object_id('log_msg', 'u') is null
  create table log_msg (id int not null identity primary key, msg varchar(max) null, date_added datetime default getdate());
go

-------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_log_msg]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_log_msg]
go

create procedure [dbo].[sp_log_msg] @msg varchar(max) as
begin
  set nocount on;
  print @msg;
  insert into log_msg (msg) values (@msg);
end;
go


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_delete_rows_marked_for_delete_packed]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_delete_rows_marked_for_delete_packed]
go

create procedure [dbo].[sp_delete_rows_marked_for_delete_packed] @pack_size int = 10000, @diag_level int = 1 as
begin
  set nocount on;

  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_marked_for_delete_packed: start'; exec sp_log_msg @log_msg;
  end;
  
  declare @create_idxes int = 1, @drop_marker_columns_when_done int = 1;

  declare @rm4d_col_name sysname = '__row_marked_for_delete__';

  declare @tdtns_names table (table_name sysname not null primary key);

  insert into @tdtns_names (table_name)
  select o.name from sys.objects o inner join sys.columns c on o.object_id = c.object_id
  where o.type = 'u' and o.is_ms_shipped = 0 and c.name = @rm4d_col_name;

  if not exists (select 1 from @tdtns_names) begin
    set @log_msg = 'there is no table with column ' + @rm4d_col_name + ', nothing to do'; exec sp_log_msg @log_msg;
    return;
  end;

  declare @sql_txt nvarchar(max) = null;

  if @create_idxes <> 0 begin

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end +
      'create index idx_' + substring(table_name + '_' + @rm4d_col_name, 1, 128-36-4-1) + '_' + replace(newid(), '-', '_') + ' on ' +
        table_name + ' (' + @rm4d_col_name + ')'
    from 
      @tdtns_names tt
    where not exists (
      select 1 
      from 
        sys.indexes ind inner join 
        sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
        sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
        sys.tables t on ind.object_id = t.object_id 
      where 
        t.is_ms_shipped = 0 
        and col.name = @rm4d_col_name
        and t.name = tt.table_name
        and ic.index_column_id = 1
    );

    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': after create indexes for initial tables'; exec sp_log_msg @log_msg;
    end;
  end;

  set @sql_txt = null;

  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ' union all ' end +
    'select ' + quotename(table_name, '''') + ' as table_name, (select count(1) from ' + quotename(table_name) + ' where ' + @rm4d_col_name + ' = 1) as marked_count'
  from @tdtns_names;

  set @sql_txt = 'insert into #tdtns (table_name, marked_count) ' + @sql_txt;

  create table #tdtns (table_name sysname not null primary key, marked_count int not null);

  exec(@sql_txt);

  declare @tab_cnt_with_no_rows_to_del int;

  if @diag_level > 0 begin
    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end + '  ' + table_name + ': ' + cast(marked_count as varchar(20)) from #tdtns where marked_count > 0 order by marked_count desc, table_name;

    set @tab_cnt_with_no_rows_to_del = (select count(1) from #tdtns where marked_count = 0);

    set @log_msg = cast(sysdatetime() as varchar(23)) + ': initial list of tables with rows to delete:
' + coalesce(@sql_txt, '  <none>') + '
number of tables with column ' + @rm4d_col_name + ' but without rows to delete: ' + cast(@tab_cnt_with_no_rows_to_del as varchar(20)); exec sp_log_msg @log_msg;
  end;

  /*

  z kolejki we� tabel� z wierszami do usuni�cia (t1) - czyli tabela t1 wylatuje z kolejki
  dla ka�dej tabeli t2, kt�ra ma klucz obcy do t1:
    je�eli w tabeli t2 brakuje kolumny @rm4d_col_name - dodaj tak� kolumn�
    update warto�ci @rm4d_col_name na 1 dla wierszy t2 powi�zanych z wierszami t1, kt�re b�d� usuni�te z t1, ale nie s� jeszcze oznaczone w t2 jako do usuni�cia
    je�eli zaktualizowano jakiekolwiek wiersze (@@rowcount > 0), to dodaj tabel� t2 do kolejki (o ile nie ma jej w kolejce)

  */

  create table #tdtns_queue (table_name sysname not null primary key);

  insert into #tdtns_queue select table_name from #tdtns where marked_count > 0;

  declare @rc int;

  declare @t1 sysname;

  while 1 = 1 begin
    set @t1 = (select top 1 table_name from #tdtns_queue);
    if @t1 is null break;

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': starting propagation of row marking for table: ' + @t1; exec sp_log_msg @log_msg;
    end;

    delete from #tdtns_queue where table_name = @t1;

    declare @cur cursor;

    --if @diag_level > 0 select table_name, name, referencedcolumn from vw_ww_foreignkeys where ReferencedTableName = @t1;

    set @cur = cursor for select table_name, name, referencedcolumn from vw_ww_foreignkeys where ReferencedTableName = @t1;

    open @cur;

    declare @t2 sysname, @fk_col sysname, @pk_col sysname;

    fetch next from @cur into @t2, @fk_col, @pk_col;

    while @@fetch_status = 0 begin
      if @diag_level > 0 begin
        set @log_msg = cast(sysdatetime() as varchar(23)) + ': propagating row marking from table: ' + @t1 + ' (' + @pk_col + ') to table ' + @t2 + ' (' + @fk_col + ')'; exec sp_log_msg @log_msg;
      end;

      declare @must_create_idx int;

      if not exists (select 1 from sys.columns where object_id = object_id(@t2) and name = @rm4d_col_name) begin
        if @diag_level > 0 begin
          set @log_msg = cast(sysdatetime() as varchar(23)) + ': adding missing column ' + @rm4d_col_name + ' on table ' + @t2; exec sp_log_msg @log_msg;
        end;

        set @sql_txt = 'alter table ' + @t2 + ' add ' + @rm4d_col_name + ' int not null default 0;';
        exec(@sql_txt);

        set @must_create_idx = 1;
      end;
      else begin
        if @create_idxes <> 0
          set @must_create_idx = case when exists (select 1 
            from 
              sys.indexes ind inner join 
              sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
              sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
              sys.tables t on ind.object_id = t.object_id 
            where 
              t.is_ms_shipped = 0 
              and col.name = @rm4d_col_name
              and t.name = @t2
              and ic.index_column_id = 1) then 0 else 1 end;
      end;      

      if @create_idxes <> 0 and @must_create_idx <> 0 begin
        set @sql_txt = 'create index idx_' + substring(@t2 + '_' + @rm4d_col_name, 1, 128-36-4-1) + '_' + replace(newid(), '-', '_') + ' on ' +
          @t2 + ' (' + @rm4d_col_name + ')';

        exec(@sql_txt);

        if @diag_level > 0 begin
          set @log_msg = cast(sysdatetime() as varchar(23)) + ': after create index on column ' + @rm4d_col_name + ' of table ' + @t2; exec sp_log_msg @log_msg;
        end;
      end;

      if @diag_level > 0 begin
        set @log_msg = cast(sysdatetime() as varchar(23)) + ': updating column ' + @rm4d_col_name + ' of table ' + @t2; exec sp_log_msg @log_msg;
      end;

      set @sql_txt = 'update top (' + cast(@pack_size as varchar(20)) + ') t2 set ' + @rm4d_col_name + ' = 1 from ' + @t2 + ' t2 inner join ' + @t1 + ' t1 on t2.' + @fk_col + ' = t1.' + @pk_col + 
        ' where t2.' + @rm4d_col_name + ' = 0 and t1.' + @rm4d_col_name + ' = 1';

      declare @upd_iter_cnt int, @total_rc int;

      set @total_rc = 0;

      while 1 = 1 begin
        exec(@sql_txt);
        set @rc = @@rowcount;
        set @total_rc += @rc;
        if @rc < @pack_size break;
      end;

      if @diag_level > 0 begin
        set @log_msg = cast(sysdatetime() as varchar(23)) + ': finished updating column ' + @rm4d_col_name + ' of table ' + @t2 + ', affected rows: ' + 
        cast(@total_rc as varchar(20)); exec sp_log_msg @log_msg;
      end;

      if @rc > 0 begin
        if not exists (select 1 from #tdtns_queue where table_name = @t2)
          insert into #tdtns_queue (table_name) values (@t2);
      end;

      if not exists (select 1 from #tdtns where table_name = @t2)
        insert into #tdtns (table_name, marked_count) values (@t2, @rc);
      else
        update #tdtns set marked_count = marked_count + @rc;

      fetch next from @cur into @t2, @fk_col, @pk_col;
    end;
    
    close @cur;
  end;

  if @diag_level > 0 begin
    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end + '  ' + table_name + ': ' + cast(marked_count as varchar(20)) from #tdtns where marked_count > 0 order by marked_count desc, table_name;

    set @tab_cnt_with_no_rows_to_del = (select count(1) from #tdtns where marked_count = 0);

    set @log_msg = cast(sysdatetime() as varchar(23)) + ': final list of tables with rows to delete:
' + coalesce(@sql_txt, '  <none>') + '
number of tables with column ' + @rm4d_col_name + ' but without rows to delete: ' + cast(@tab_cnt_with_no_rows_to_del as varchar(20)); exec sp_log_msg @log_msg;
  end;

  --select * from vw_ww_foreignkeys where ReferencedTableName in (select table_name from #tdtns);

  declare @sql_txt_enable_fks nvarchar(max) = null;

  set @sql_txt = null;

  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'alter table ' + table_name + ' nocheck constraint ' + ForeignKey_Name,
    @sql_txt_enable_fks = case when @sql_txt_enable_fks is null then '' else @sql_txt_enable_fks + ';
  ' end + 'alter table ' + table_name + ' with check check constraint ' + ForeignKey_Name
  from vw_ww_foreignkeys where ReferencedTableName in (select table_name from #tdtns where marked_count > 0);

  if @sql_txt is not null exec(@sql_txt);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': after disable foreign keys referencing tables with rows to delete'; exec sp_log_msg @log_msg;
  end;

  set @sql_txt = null;

  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'while 1 = 1 begin delete top (' + cast(@pack_size as varchar(20)) + ') from ' + table_name + ' where ' + @rm4d_col_name + ' = 1; 
set @rc = @@rowcount; if @rc < ' + cast(@pack_size as varchar(20)) + ' break; end;' +
    case when @diag_level > 0 then '
set @log_msg = cast(sysdatetime() as varchar(23)) + '': after deleting rows from table ' + table_name + '''; exec sp_log_msg @log_msg;' else '' end
  from #tdtns where marked_count > 0;
  
  if @sql_txt is not null begin
    set @sql_txt = 'declare @rc int, @log_msg varchar(max); ' + @sql_txt;

    exec(@sql_txt);
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': after deleting rows from all tables'; exec sp_log_msg @log_msg;
  end;

  if @sql_txt_enable_fks is not null exec(@sql_txt_enable_fks);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': after enabling foreign keys referencing tables with deleted rows'; exec sp_log_msg @log_msg;
  end;

  if @drop_marker_columns_when_done <> 0 begin

    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end + 'drop index ' + idx_name + ' on ' + table_name
    from (
      select distinct t.name as table_name, ind.name as idx_name
      from 
        sys.indexes ind 
        inner join sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
        inner join sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id 
        inner join sys.tables t on ind.object_id = t.object_id 
      where 
        t.is_ms_shipped = 0 
        and col.name = @rm4d_col_name
    ) x;

    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': after drop indexes on column ' + @rm4d_col_name + ' on all tables'; exec sp_log_msg @log_msg;
    end;

    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
    ' end + 'alter table ' + t.name + ' drop constraint ' + df.name
    from sys.default_constraints df
    inner join sys.tables t on df.parent_object_id = t.object_id
    inner join sys.columns c on df.parent_object_id = c.object_id and df.parent_column_id = c.column_id
    where c.name = @rm4d_col_name;

    --set @log_msg = coalesce(@sql_txt, '<no constraints to drop>');
    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': after drop constraints on column ' + @rm4d_col_name + ' of all tables'; exec sp_log_msg @log_msg;
    end;

    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
  ' end + 'alter table ' + table_name + ' drop column ' + @rm4d_col_name
    from #tdtns;

    --set @log_msg = coalesce(@sql_txt, '<no columns to drop>');
    if @sql_txt is not null exec(@sql_txt);
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_marked_for_delete_packed: end'; exec sp_log_msg @log_msg;
  end;
end;
go


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_delete_rows_from_bik_node_packed]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_delete_rows_from_bik_node_packed]
go

create procedure [dbo].[sp_delete_rows_from_bik_node_packed] @pack_size int = 10000, @diag_level int = 1 as
begin
  set nocount on;

  declare @create_idxes int = 1;

  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_from_bik_node_packed: start, pack size: ' + cast(@pack_size as varchar(20)); exec sp_log_msg @log_msg;
  end;

  declare @has_trigger int = 0;

  if exists (select 1 from sys.triggers tr inner join sys.tables t on t.object_id = tr.parent_id
    where tr.is_disabled = 0 and t.name = 'bik_node' and tr.name = 'trg_bik_node_chunking') begin
    set @has_trigger = 1;
    exec('disable trigger trg_bik_node_chunking on bik_node');  

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': trigger trg_bik_node_chunking disabled'; exec sp_log_msg @log_msg;
    end;
  end;

  if not exists (select 1 from sys.columns where object_id = object_id('bik_node') and name = '__row_marked_for_delete__') begin
    exec('alter table bik_node add __row_marked_for_delete__ int not null default 0');

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': column __row_marked_for_delete__ added to table bik_node'; exec sp_log_msg @log_msg;
    end;
  end;  
 
  declare @sql_txt nvarchar(max);

  if @create_idxes <> 0 begin
    if not exists (
      select 1 
      from 
        sys.indexes ind inner join 
        sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
        sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
        sys.tables t on ind.object_id = t.object_id 
      where 
        t.is_ms_shipped = 0 
        and col.name = '__row_marked_for_delete__'
        and t.name = 'bik_node'
        and ic.index_column_id = 1
    ) begin
      set @sql_txt = 'create index idx_bik_node___row_marked_for_delete__' + replace(newid(), '-', '_') + ' on bik_node (__row_marked_for_delete__)';
      exec (@sql_txt);

        if @diag_level > 0 begin
          set @log_msg = cast(sysdatetime() as varchar(23)) + ': index on bik_node (__row_marked_for_delete__) created'; exec sp_log_msg @log_msg;
        end;
    end;
  end;

  set @sql_txt = 'update top (' + cast(@pack_size as varchar(20)) + ') bik_node set __row_marked_for_delete__ = 1 where is_deleted <> 0 and __row_marked_for_delete__ = 0';

  declare @rc int;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': starting to mark bik_node rows'; exec sp_log_msg @log_msg;
  end;

  declare @total_marked_count int = 0;

  while 1 = 1 begin
    exec(@sql_txt);

    set @rc = @@rowcount;

    set @total_marked_count += @rc;

    if @rc < @pack_size break;
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': marked bik_node rows: ' + cast(@total_marked_count as varchar(20)) + ', before delete'; exec sp_log_msg @log_msg;
  end;

  /* -- nie usuwaj z modrzewia
  declare @tree_of_trees_id int = (select id from bik_tree where code = 'TreeOfTrees')
  set @sql_txt = 'update bik_node set __row_marked_for_delete__ = 0 where tree_id = ' + cast(@tree_of_trees_id as varchar(20));
  --print @sql_txt;
  exec (@sql_txt);
  */

  exec sp_delete_rows_marked_for_delete_packed @pack_size, @diag_level;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': finished delete rows from bik_node and referencing tables'; exec sp_log_msg @log_msg;
  end;

  if @has_trigger = 1 begin
    exec('enable trigger trg_bik_node_chunking on bik_node');  
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_from_bik_node_packed: end'; exec sp_log_msg @log_msg;
  end;
end;
go


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------


if not exists (select * from bik_data_source_def where description = 'File System')
begin
	insert into bik_data_source_def(description) values ('File System')
end;
go

if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' -- PB
begin
	if not exists (select * from bik_app_prop where name = 'availableConnectors' and val like '%File System%')
	update bik_app_prop set val = val + ',File System' where name = 'availableConnectors'
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_jdbc_source') and type in (N'U'))
	create table bik_jdbc_source (
		id int not null primary key identity(1,1),
		name varchar(512) not null,
		driver_name varchar(512) not null,
		jdbc_url_template varchar(max) null
	);
go

if not exists(select * from bik_jdbc_source where name = 'SQL Server')
begin
	insert into bik_jdbc_source(name, driver_name, jdbc_url_template)
	values('SQL Server', 'com.microsoft.sqlserver.jdbc.SQLServerDriver', 'jdbc:sqlserver://HOST\INSTANCE[:PORT];databaseName=DATABASE'),
	('Oracle','oracle.jdbc.driver.OracleDriver','jdbc:oracle:thin:@//HOST[:PORT]/SERVICE'),
	('MySQL','com.mysql.jdbc.Driver','jdbc:mysql://HOST/DATABASE'),
	('PostreSQL','org.postgresql.Driver','jdbc:postgresql://HOST/DATABASE'),
	('DB2','com.ibm.db2.jcc.DB2Driver','jdbc:db2://HOST/DATABASE'),
	('Teradata','com.teradata.jdbc.TeraDriver','jdbc:teradata://HOST');
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_jdbc_source]') and name = N'idx_bik_jdbc_source_name')
	create unique index idx_bik_jdbc_source_name on bik_jdbc_source(name);
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_jdbc') and name = 'jdbc_source_id')
begin
	exec('alter table bik_jdbc add jdbc_source_id int null references bik_jdbc_source(id)');
	
	exec('update bik_jdbc set jdbc_source_id = src.id from bik_jdbc as tgt inner join bik_jdbc_source as src on tgt.driver_name = src.driver_name');
	
	exec('delete from bik_jdbc where jdbc_source_id is null');
	
	alter table bik_jdbc alter column jdbc_source_id int not null;
	
	alter table bik_jdbc drop column driver_name;
	
	declare @fk_name varchar(512) = (select cstr.name from sys.tables as tbl inner join sys.foreign_keys as cstr on cstr.parent_object_id=tbl.object_id
		inner join sys.foreign_key_columns as fk on fk.constraint_object_id = cstr.object_id
		inner join sys.columns as cfk on fk.parent_column_id = cfk.column_id and fk.parent_object_id = cfk.object_id
		where tbl.name = 'bik_jdbc' and cfk.name = 'source_id');
		
	exec('alter table dbo.bik_jdbc drop constraint ' + @fk_name);
	
	alter table bik_jdbc drop column source_id;
end;
go


if not exists(select * from sys.objects where object_id = object_id(N'bik_file_system') and type in (N'U'))
	create table bik_file_system (
		id int not null primary key identity(1,1),
		name varchar(512) not null,
		path varchar(max) null,
		domain varchar(max) null,
		[user] varchar(max) null,
		[password] varchar(max) null,
		is_active int not null default(0),
		download_files int not null default(0),
		tree_id int null references bik_tree(id)
	);
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_file_system]') and name = N'idx_bik_file_system_name')
	create unique index idx_bik_file_system_name on bik_file_system(name);
go

update bik_app_prop set val = 'true' where name = 'insertStatisticsForAllNodes'

------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------


exec sp_update_version '1.7.z5.12', '1.7.z5.13';
go
