﻿exec sp_check_version '1.7.z12.2';
go

declare @lisa_tree_id int = (select id from bik_tree where code='DictionaryDWH')
declare @instance_id int = (select MIN(id) from bik_lisa_instance)

delete from bik_lisa_dict_instance where node_id in (select id from bik_node where obj_id='DWHDictionaryRoot');

update bik_node set obj_id=CAST(@instance_id as varchar)+'.'+obj_id 
where tree_id=@lisa_tree_id and node_kind_id in (select id from bik_node_kind where code in ('DWHCategorization', 'DWHCategory'))
and (SUBSTRING(obj_id, 0, CHARINDEX('.', obj_id)) like '%[^0-9]%' or not exists(select id from bik_lisa_instance where id=CAST(SUBSTRING(obj_id, 0, CHARINDEX('.', obj_id)) as int)))

exec sp_update_version '1.7.z12.2', '1.7.z12.3';
go