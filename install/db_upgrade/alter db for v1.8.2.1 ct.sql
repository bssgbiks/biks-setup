﻿exec sp_check_version '1.8.2.1';
go
 
delete from bik_attr_def where name='metaBIKS.metaBIKS.Wyświetlaj jako liczbę'
declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')

exec sp_add_attr_def 'metaBIKS.Wyświetlaj jako liczbę', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_2_node_kind 'metaBiksAttributeDef', 'metaBIKS.Wyświetlaj jako liczbę', null, 1, null, null

exec sp_update_version '1.8.2.1', '1.8.2.2';


