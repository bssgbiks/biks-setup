﻿exec sp_check_version '1.8.0.7';
go

if object_id('bik_tree_import_log') is null
create table bik_tree_import_log (
	tree_id int,
	description varchar(max),
	start_time datetime default(getdate()) 
)
go

exec sp_update_version '1.8.0.7', '1.8.0.8';
go