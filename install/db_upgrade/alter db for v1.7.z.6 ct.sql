﻿exec sp_check_version '1.7.z.6';
go

update bik_help set caption = 'Editing joined objects' where help_key = '#EditnigJoinedObj' and lang = 'en'
update bik_help set caption = 'Tutorial' where help_key = '#Tutorial' and lang = 'en'

exec sp_update_version '1.7.z.6', '1.7.z.7';
go
