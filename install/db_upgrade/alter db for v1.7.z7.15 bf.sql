﻿exec sp_check_version '1.7.z7.15';
go

--Ulubione i aktualności
if not exists (select * from bik_app_prop where name = 'emailScheduleTitle')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('emailScheduleTitle', 'Aktualności i zmiany w ulubionych', 1)
end
go

if not exists (select * from bik_app_prop where name = 'emailScheduleBody')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('emailScheduleBody', 
	'<div style="color:#094f8b">Witaj %{name}%, <br/><br/>
	<h2 style="color:#094f8b">Aktualności</h2>%{bodyNews}%
	<h2 style="color:#094f8b">Ulubione </h2>%{bodyFvs}%
	</div><br/><br/>
	<p style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Ten e-mail został wygenerowany automatycznie. Prosimy na niego nie odpowiadać. </p>
	<p style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Jeśli nie chcesz otrzymywać powiadomień wejdź na %{page}% i odznacz opcję w edycji danych.</p>'
	, 1)
end
go
--Aktualności
if not exists (select * from bik_app_prop where name = 'emailAdhocTitle')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('emailAdhocTitle', 'Aktualności', 1)
end
go

if not exists (select * from bik_app_prop where name = 'emailAdhocBody')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('emailAdhocBody', '<div style="color:#094f8b">Witaj %{name}%, <br/><br/>	
	W systemie BIKS została dodana nowa aktualność</i>
	</br></br>
	<h2 style="color:#094f8b">%{newsTitle}%</h2>
	<div>
	%{newsBody}%</div>
	</div><br/><br/>
	<p style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Ten e-mail został wygenerowany automatycznie. Prosimy na niego nie odpowiadać. </p>
	<p style="font-size: 12px;color: #b9b9b9;line-height: 1.5">Jeśli nie chcesz otrzymywać powiadomień wejdź na %{page}% i odznacz opcję w edycji danych.</p>'
	, 1)
end
go


if not exists(select 1 from sys.columns where name = N'is_send_mails' and object_id = object_id(N'bik_system_user'))
begin
	alter table bik_system_user add is_send_mails int not null default 1;
end
go

update bik_system_user set is_send_mails = 0;
go

exec sp_update_version '1.7.z7.15', '1.7.z7.16';
go
