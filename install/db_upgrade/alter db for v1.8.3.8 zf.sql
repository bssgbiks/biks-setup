exec sp_check_version '1.8.3.8';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'extension_attribute1')
	alter table bik_active_directory add extension_attribute1 varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'extension_attribute2')
	alter table bik_active_directory add extension_attribute2 varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'extension_attribute3')
	alter table bik_active_directory add extension_attribute3 varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'extension_attribute4')
	alter table bik_active_directory add extension_attribute4 varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'extension_attribute5')
	alter table bik_active_directory add extension_attribute5 varchar(max) null;
go

exec sp_update_version '1.8.3.8', '1.8.3.9';
go
