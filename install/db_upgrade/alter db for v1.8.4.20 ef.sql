﻿exec sp_check_version '1.8.4.20';
go

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideSearchTab'
	,'true'
	,1
	)


exec sp_update_version '1.8.4.20', '1.8.4.21';
go
