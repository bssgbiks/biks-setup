﻿exec sp_check_version '1.7.z9.3';

--vw_biks_statistic - przeglądane nody

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_biks_statistic]'))
DROP VIEW [dbo].[vw_biks_statistic]
GO

create view vw_biks_statistic as
   select 
		dbo.fn_get_menu_path_by_tree_code(bt.code)as menu_path,
		dbo.fn_get_node_name_path(bn.id) as object_path,
		bn.name as object_name,
		bt.name as tree_name,
		bnk.caption as kind_name,
		bsu.login_name as user_name,
		event_datetime
	from bik_statistic_ext bse 
	left join bik_node bn on bse.clicked_node_id=bn.id
	left join bik_tree bt on bse.tree_id=bt.id
	left join bik_system_user bsu on bsu.id=bse.user_id
	left join bik_node_kind bnk on bnk.id=bn.node_kind_id
go

/*vw_biks_statistic_search - wyszukiwania*/

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_biks_statistic_search]'))
DROP VIEW [dbo].[vw_biks_statistic_search]
GO

create view vw_biks_statistic_search as
	select 
		parametr as search_text,
		login_name as user_name,
		event_datetime 
	from bik_statistic bs 
    left join bik_system_user bsu on bsu.id=bs.user_id
    where counter_name='newSearch'
go

/*
 vw_biks_statistic_daily_user_login - Logowania uzytkowników do systemu (jedno dziennie)
 */
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_biks_statistic_daily_user_login]'))
DROP VIEW [dbo].[vw_biks_statistic_daily_user_login]
GO

create view vw_biks_statistic_daily_user_login as 
    select 
		login_name as user_name,
		event_datetime 
	from bik_statistic bs 
    left join bik_system_user bsu on bsu.id=bs.user_id
    where counter_name='userLogin'
go

/*
 vw_biks_statistic_data_load - Logi z zasilania (podobne do tego co widoczne na Admin > Zasilania > Logi)
 */
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_biks_statistic_data_load]'))
DROP VIEW [dbo].[vw_biks_statistic_data_load]
GO

create view vw_biks_statistic_data_load as
	select
		def.description as system,
		log.server_name as server,
		log.start_time,
		log.finish_time,
		DATEDIFF(ss,log.start_time,log.finish_time) as duration_in_sec,
		log.status_description as description
	from bik_data_load_log as log
	inner join bik_data_source_def as def
	on def.id = log.data_source_type_id
go	

-- pierwsze logowania do BIKS - data utworzenia konta w BIKS per login
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_biks_statistic_first_login]'))
DROP VIEW [dbo].[vw_biks_statistic_first_login]
GO

create view vw_biks_statistic_first_login as 
    select bsu.login_name as user_name,
	coalesce(bsu.date_added, bs.event_datetime) as create_datetime
	from bik_system_user bsu 
    left join bik_statistic bs on bsu.id = bs.user_id and bs.counter_name='userLoginOnce'
go
-- select * from vw_biks_statistic_first_login

exec sp_update_version '1.7.z9.3', '1.7.z9.4';
go
