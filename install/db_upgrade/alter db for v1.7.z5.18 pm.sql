exec sp_check_version '1.7.z5.18';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


if not exists(select 1 from syscolumns sc where id = object_id('bik_crr_label') and name = 'is_default_for_new_node')
  alter table bik_crr_label add is_default_for_new_node int not null default 0;
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_set_crr_labels_on_node]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_set_crr_labels_on_node]
go
		
create procedure [dbo].[sp_set_crr_labels_on_node](@labels varchar(max), @node_id int, @diag_level int = 0) as
begin
	set nocount on;

  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: start'; exec sp_log_msg @log_msg;
  end;

  declare @current_labels table (name varchar(100) not null primary key, is_inherited tinyint not null);

  insert into @current_labels (name, is_inherited)  
  select name, max(is_inherited)
  from
  (
  select case when str like '%*' then substring(str, 1, len(str) - 1) else str end as name, case when str like '%*' then 1 else 0 end as is_inherited
  from --dbo.fn_split_by_sep('PLK*, CP, CP, PLK', ',', 7) -- 
    dbo.fn_split_by_sep(dbo.fn_get_crr_labels_for_node(@node_id), ',', 7)
  ) x
  group by name;
  
  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: @current_labels filled'; exec sp_log_msg @log_msg;
  end;

  --select * from @current_labels;

  --select * from dbo.fn_split_by_sep('PLK*, CP', ',', 7)
  
  declare @expected_labels table (name varchar(100) not null primary key);

  insert into @expected_labels (name)  
  select distinct case when str like '%*' then substring(str, 1, len(str) - 1) else str end as name
  from --dbo.fn_split_by_sep('PLK*, CP, CP, PLK', ',', 7) -- 
    dbo.fn_split_by_sep(@labels, ',', 7);
  
  --select * from @expected_labels;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: @expected_labels filled'; exec sp_log_msg @log_msg;
  end;

  declare @label_mods table (name varchar(100) not null primary key, will_add tinyint not null);
  declare @mods_cnt int;

  insert into @label_mods (name, will_add)
  select coalesce(cl.name, el.name), case when cl.name is null then 1 else 0 end from
    @current_labels cl full outer join @expected_labels el on cl.name = el.name
  where
    (cl.name is not null and cl.is_inherited = 0 and el.name is null) -- aktualna jest niedziedziczona i ma jej nie być
    or cl.name is null -- aktualnie nie ma, a ma być
  ;

  set @mods_cnt = @@rowcount;

  --select * from @label_mods;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: @label_mods filled'; exec sp_log_msg @log_msg;
  end;

  if @mods_cnt = 0 return;

  delete crrue
  from bik_custom_right_role_user_entry crrue inner join bik_crr_label crrl on crrue.group_id = crrl.group_id and crrue.role_id = crrl.role_id
    inner join @label_mods lm on crrl.name = lm.name
  where crrue.node_id = @node_id and lm.will_add = 0;

  declare @tree_id int = (select tree_id from bik_node where id = @node_id);

  insert into bik_custom_right_role_user_entry (user_id, role_id, tree_id, node_id, group_id)
  select null, crrl.role_id, @tree_id, @node_id, crrl.group_id
  from  
    bik_crr_label crrl inner join @label_mods lm on crrl.name = lm.name
  where lm.will_add <> 0;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: bik_custom_right_role_user_entry updated'; exec sp_log_msg @log_msg;
  end;

  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: after sp_recalculate_Custom_Right_Role_Action_Branch_Grants, all done'; exec sp_log_msg @log_msg;
  end;
end;
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_apply_default_crr_labels_on_node_modified_by_user]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_apply_default_crr_labels_on_node_modified_by_user]
go
		
create procedure [dbo].[sp_apply_default_crr_labels_on_node_modified_by_user](@node_id int, @user_id int, @is_new_node tinyint) as
begin
	set nocount on;

  if @is_new_node = 0 return;

  if not exists (select 1 from bik_crr_label where is_default_for_new_node <> 0) return;

  create table #crr_labs_member_of_groups (group_id int not null primary key);
  create table #crr_labs_parent_groups (group_id int not null primary key);

  declare @tmp_groups table (group_id int not null primary key);

  declare @new_grp_cnt int;

  insert into #crr_labs_parent_groups (group_id)
  select group_id from bik_system_user_in_group where user_id = @user_id and is_deleted = 0;

  set @new_grp_cnt = @@rowcount;

  while @new_grp_cnt > 0 begin
    insert into #crr_labs_member_of_groups (group_id) select group_id from #crr_labs_parent_groups;

    delete from @tmp_groups;

    insert into @tmp_groups (group_id)
    select gig.group_id from bik_system_group_in_group gig inner join #crr_labs_parent_groups pg on gig.member_group_id = pg.group_id
      left join #crr_labs_member_of_groups mog on mog.group_id = gig.group_id
    where gig.is_deleted = 0 and mog.group_id is null;

    truncate table #crr_labs_parent_groups;

    insert into #crr_labs_parent_groups (group_id)
    select group_id from @tmp_groups;

    set @new_grp_cnt = @@rowcount;   
  end;

  declare @labels varchar(max) = dbo.fn_get_crr_labels_for_node(@node_id);

  select @labels = @labels + ',' + l.name 
  from bik_crr_label l inner join #crr_labs_member_of_groups mog on l.group_id = mog.group_id
  where l.is_default_for_new_node <> 0;

  exec sp_set_crr_labels_on_node @labels, @node_id;  
end;
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if not exists (select 1 from bik_node_action where code = '+EditCrrLabels')
  insert into bik_node_action (code) values ('+EditCrrLabels');
go

declare @act_id int = (select id from bik_node_action where code = '+EditCrrLabels');

insert into bik_node_action_in_custom_right_role
select @act_id, crr.id
from bik_custom_right_role crr
where crr.code <> 'RegularUser' and not exists (select 1 from bik_node_action_in_custom_right_role where role_id = crr.id and action_id = @act_id);
go

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


exec sp_update_version '1.7.z5.18', '1.7.z5.19';
go
