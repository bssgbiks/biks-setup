﻿exec sp_check_version '1.8.5.2';
GO

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideBlogsTab'
	,'true'
	,1
	)

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideUsersTab'
	,'true'
	,1
	)

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideUserRolesTab'
	,'true'
	,1
	)

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideGrantRightsBtn'
	,'true'
	,1
	)
	
exec sp_update_version '1.8.5.2'
	,'1.8.5.3';
GO


