﻿exec sp_check_version '1.7.z8.5';
go

if not exists (select * from bik_app_prop where name = 'allowLinkingInDocumentation')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('allowLinkingInDocumentation', 'false', 1)
end
go

 if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
			or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' 
			
begin
	update bik_app_prop set val='true' where name= 'allowLinkingInDocumentation';

end
go

exec sp_update_version '1.7.z8.5', '1.7.z8.6';
go