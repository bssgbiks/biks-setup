exec sp_check_version '1.7.z6';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


if object_id('[dbo].[fn_get_crr_labels_for_node]') is not null
  drop function [dbo].[fn_get_crr_labels_for_node]
go

create function [dbo].[fn_get_crr_labels_for_node] (@node_id int)
returns varchar(max)
as
begin
  declare @tree_id int, @branch_ids varchar(1000);

  select @tree_id = tree_id, @branch_ids = branch_ids from bik_node where id = @node_id;

  declare @res varchar(max) = '', @sep varchar(max) = ', ';

  select @res = @res + @sep + name + case when is_inherited = 0 then '' else '*' end
  from (
    select l.name as name, max(case when @branch_ids = n.branch_ids then 0 else 1 end) as is_inherited
    from bik_custom_right_role_user_entry crrue 
      inner join bik_crr_label l on crrue.role_id = l.role_id and crrue.group_id = l.group_id
      left join bik_node n on n.id = crrue.node_id
    where crrue.tree_id = @tree_id and crrue.user_id is null and (n.id is null or @branch_ids like n.branch_ids + '%')
    group by l.name
  ) x
  order by name;

  return substring(@res, datalength(@sep) + 1, len(@res));
end;
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if object_id (N'dbo.fn_get_system_user_group_paths', N'tf') is not null
    drop function dbo.fn_get_system_user_group_paths;
go

create function dbo.fn_get_system_user_group_paths(@group_id int)
returns @res table (path varchar(max) not null, branch_ids varchar(max) not null)
as
begin
  declare @tab table (group_id int not null, path varchar(max) not null, branch_ids varchar(max) not null);
  declare @tab2 table (group_id int not null, path varchar(max) not null, branch_ids varchar(max) not null);

  insert into @tab (group_id, path, branch_ids) select id, name, cast(id as varchar(20)) from bik_system_user_group where id = @group_id;

  declare @rc int;

  while 1 = 1 begin

    delete from @tab2;

    insert into @res (path, branch_ids)
    select t.path, t.branch_ids
    from @tab t left join bik_system_group_in_group gig on t.group_id = gig.member_group_id
    where gig.id is null;

    delete from @tab
    where group_id not in (select member_group_id from bik_system_group_in_group);

    insert into @tab2 (group_id, path, branch_ids)
    select sug.id, sug.name + ' » ' + t.path, cast(sug.id as varchar(20)) + '|' + t.branch_ids
    from @tab t left join (bik_system_group_in_group gig
      inner join bik_system_user_group sug on sug.id = gig.group_id) on t.group_id = gig.member_group_id;

    set @rc = @@rowcount;

    if @rc = 0 break;  

    delete from @tab;

    insert into @tab (group_id, path, branch_ids) select group_id, path, branch_ids from @tab2;

  end;

  insert into @res (path, branch_ids) select path, branch_ids from @tab;

  return;
end;
go


-------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if object_id (N'dbo.fn_get_system_user_groups', N'tf') is not null
    drop function dbo.fn_get_system_user_groups;
go

create function dbo.fn_get_system_user_groups(@user_id int)
returns @tab table (group_id int not null primary key)
as
begin
  declare @parent_groups table (group_id int not null primary key);

  declare @tmp_groups table (group_id int not null primary key);

  declare @new_grp_cnt int;

  insert into @parent_groups (group_id)
  select group_id from bik_system_user_in_group where user_id = @user_id and is_deleted = 0;

  set @new_grp_cnt = @@rowcount;

  while @new_grp_cnt > 0 begin
    insert into @tab (group_id) select group_id from @parent_groups;

    delete from @tmp_groups;

    insert into @tmp_groups (group_id)
    select distinct gig.group_id from bik_system_group_in_group gig inner join @parent_groups pg on gig.member_group_id = pg.group_id
      left join @tab mog on mog.group_id = gig.group_id
    where gig.is_deleted = 0 and mog.group_id is null;

    delete from @parent_groups;

    insert into @parent_groups (group_id)
    select group_id from @tmp_groups;

    set @new_grp_cnt = @@rowcount;   
  end;
     
  return;
end;
go


------------------------------------------------
-------------------------------------------------
-------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_apply_default_crr_labels_on_node_modified_by_user]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_apply_default_crr_labels_on_node_modified_by_user]
go
		
create procedure [dbo].[sp_apply_default_crr_labels_on_node_modified_by_user](@node_id int, @user_id int, @is_new_node tinyint) as
begin
	set nocount on;

  if @is_new_node = 0 return;

  if not exists (select 1 from bik_crr_label where is_default_for_new_node <> 0) return;

  create table #crr_labs_member_of_groups (group_id int not null primary key);
  create table #crr_labs_parent_groups (group_id int not null primary key);

  declare @tmp_groups table (group_id int not null primary key);

  declare @new_grp_cnt int;

  insert into #crr_labs_parent_groups (group_id)
  select group_id from bik_system_user_in_group where user_id = @user_id and is_deleted = 0;

  set @new_grp_cnt = @@rowcount;

  while @new_grp_cnt > 0 begin
    insert into #crr_labs_member_of_groups (group_id) select group_id from #crr_labs_parent_groups;

    delete from @tmp_groups;

    insert into @tmp_groups (group_id)
    select distinct gig.group_id from bik_system_group_in_group gig inner join #crr_labs_parent_groups pg on gig.member_group_id = pg.group_id
      left join #crr_labs_member_of_groups mog on mog.group_id = gig.group_id
    where gig.is_deleted = 0 and mog.group_id is null;

    truncate table #crr_labs_parent_groups;

    insert into #crr_labs_parent_groups (group_id)
    select group_id from @tmp_groups;

    set @new_grp_cnt = @@rowcount;   
  end;

  declare @labels varchar(max) = dbo.fn_get_crr_labels_for_node(@node_id);

  select @labels = @labels + ',' + l.name 
  from bik_crr_label l inner join #crr_labs_member_of_groups mog on l.group_id = mog.group_id
  where l.is_default_for_new_node <> 0;

  exec sp_set_crr_labels_on_node @labels, @node_id;  
end;
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


exec sp_update_version '1.7.z6', '1.7.z6.1';
go
