﻿exec sp_check_version '1.8.4.0';
go
 
alter table bik_node_kind add default('Document') for uploadable_children_kinds

update bik_node_kind set uploadable_children_kinds = 'Document' where uploadable_children_kinds is null

exec sp_update_version '1.8.4.0', '1.8.4.1';
go


