﻿exec sp_check_version '1.7.z5.3';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_system_user') and name = 'sso_login')
	alter table bik_system_user add sso_login varchar(256) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_system_user') and name = 'sso_domain')
	alter table bik_system_user add sso_domain varchar(256) null;
go

declare @uq_name varchar(512) = (select top 1 name from sys.objects where type = 'UQ' AND OBJECT_NAME(parent_object_id) = N'bik_system_user');
if @uq_name is not null
begin 
	exec('alter table dbo.bik_system_user drop constraint ' + @uq_name);
end
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_system_user]') and name = N'idx_bik_system_user_login_name')
	create unique index idx_bik_system_user_login_name on bik_system_user (login_name);
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_system_user]') and name = N'idx_bik_system_user_sso_login_sso_domain')
	create index idx_bik_system_user_sso_login_sso_domain on bik_system_user (sso_login, sso_domain);
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory_group') and name = 'parent_distinguished_name')
	alter table bik_active_directory_group add parent_distinguished_name varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_system_user_group') and name = 'distinguished_name')
	alter table bik_system_user_group add distinguished_name varchar(900) null;
go

declare @uq_name varchar(512) = (select top 1 name from sys.objects where type = 'UQ' AND OBJECT_NAME(parent_object_id) = N'bik_system_user_group');
if @uq_name is not null
begin 
	exec('alter table dbo.bik_system_user_group drop constraint ' + @uq_name);
end
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_system_user_group]') and name = N'idx_bik_system_user_group_distinguished_name')
	create unique index idx_bik_system_user_group_distinguished_name on bik_system_user_group (distinguished_name);
go

-- fix na statystyki
delete from bik_statistic_dict where id in (
select dic.id
from bik_statistic_dict dic 
left join bik_tree as bt on bt.code = dic.counter_name
where counter_name not in ('newSearch', 'userLogin', 'userLoginOnce') and bt.id is null)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_system_user_group') and name = 'scc_root_id')
  alter table bik_system_user_group add scc_root_id int null references bik_system_user_group (id);
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_calc_itscc_for_sugs]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_calc_itscc_for_sugs]
go

create procedure [dbo].[sp_calc_itscc_for_sugs] @diag_level int = 0 as
begin
  set nocount on;

  declare @start_time datetime = getdate();

  --declare @diag_level int = 0;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': [sp_calc_itscc_for_sugs] start';

  declare @idx int = 1;

  create table #stack (i int not null identity primary key, w_id int null, v_id int not null, f int not null, unique (w_id, v_id, f));

  --insert into #stack (w_id, v_id, f) select null, node_id, 0 from itscc_graph_node;
  insert into #stack (w_id, v_id, f) select null, id, 0 from bik_system_user_group where is_deleted = 0;

  create table #node_state (node_id int not null primary key, state int not null, idx int not null, lowlink int not null);

  create index idx_node_state_idx on #node_state (idx, state);

  insert into #node_state (node_id, state, idx, lowlink) select id, 0, 0, 0 from bik_system_user_group where is_deleted = 0;

  declare @pass int = 0;

  while 1 = 1
  begin
    if @diag_level > 1
      print '
    ----------------------------- pass #' + cast(@pass as varchar(20)) + ' -----------------------------

    ';
    if @diag_level > 2
    begin
      select * from #stack order by i;
      select * from #node_state order by node_id;
    end;

    declare @w_id int, @v_id int, @f int;
    declare @v_state int, @v_idx int, @v_lowlink int;
    
    declare @max_i int;
    set @max_i = null;
    select top 1 @max_i = i, @w_id = w_id, @v_id = v_id, @f = f from #stack order by i desc;
    if @max_i is null break;
    
    delete from #stack where i = @max_i;
    
    select @v_state = state, @v_idx = idx, @v_lowlink = lowlink from #node_state where node_id = @v_id;

    if @diag_level > 1
    print '@w_id = ' + coalesce(@w_id, '<null>') + ', @v_id = ' + @v_id + ', @f = ' + cast(@f as varchar(20)) +
      ', @v_state = ' + cast(@v_state as varchar(20)) + ', @v_idx = ' + cast(@v_idx as varchar(20)) + ', @v_lowlink = ' + cast(@v_lowlink as varchar(20));

    /*if @diag_level > 0
    begin
      declare @real_stack_size int = (select count(*) from #stack);
      if @stack_size <> @real_stack_size begin
        print '@stack_size = ' + cast(@stack_size as varchar(20)) + ', @real_stack_size = ' + cast(@real_stack_size as varchar(20));
        break;
      end;
    end;*/

    if @f = 0
    begin
      if @v_state <> 0 -- = 2
      begin
        continue;
      end;
      
      insert into #stack (w_id, v_id, f) values (@w_id, @v_id, 2);
      
      insert into #stack (w_id, v_id, f) select @v_id, e.member_group_id, 0
      from bik_system_group_in_group e inner join #node_state s on e.member_group_id = s.node_id
      where e.group_id = @v_id and e.is_deleted = 0 and s.state = 0;

      declare @lowlink int;
      
      select @lowlink = min(s.idx)
      from bik_system_group_in_group e inner join #node_state s on e.member_group_id = s.node_id
      where e.group_id = @v_id and e.is_deleted = 0 and s.state = 1;

      if @lowlink is null set @lowlink = @idx;

      update #node_state set state = 1, idx = @idx, lowlink = @lowlink where node_id = @v_id;
      
      set @idx += 1;
            
    end else if @f = 2
    begin
      if @v_idx = @v_lowlink
        update #node_state set state = 2, lowlink = @v_idx where idx >= @v_idx and state = 1;
      
      if @w_id is not null
        update #node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;        
    end;

    set @pass += 1;
    
    --if @pass > 10 break;
    
    -- exec sp_calc_itscc_for_sugs;

    --if @pass > 5000 set @diag_level = 1;
  end;

  declare @final_stack_size int = (select count(*) from #stack);

  if @final_stack_size > 0 print 'stack is not empty!!! stack size: ' + cast(@final_stack_size as varchar(20));

  if @diag_level > 0 
  begin
    declare @end_time datetime = getdate();

    declare @node_cnt int = (select count(*) from bik_system_user_group where is_deleted = 0);
    declare @edge_cnt int = (select count(*) from bik_system_group_in_group where is_deleted = 0);

    declare @ms_taken int = datediff(ms, @start_time, @end_time);

    print cast(sysdatetime() as varchar(23)) + ': [sp_calc_itscc_for_sugs] finished after ' + cast(@pass as varchar(20)) + ' passes, time taken: ' 
    + cast(@ms_taken as varchar(30)) + ' ms, nodes: ' + cast(@node_cnt as varchar(20)) + ', edges: ' + cast(@edge_cnt as varchar(20)) +
    ', passes per (n+e) = ' + cast(cast(@pass as float) / (@node_cnt + @edge_cnt) as varchar(20)) + ', time per (n+e) = ' + 
    cast(cast(@ms_taken as float) / (@node_cnt + @edge_cnt) as varchar(20));
  end;
  
  --select * from #node_state;
  
  update sug
  set sug.scc_root_id = ps.node_id
  from bik_system_user_group sug      -- użyty left join spowoduje przypisanie scc_root_id = null dla grup z is_deleted <> 0
  left join (#node_state ns inner join #node_state ps on ns.lowlink = ps.idx) on sug.id = ns.node_id;
end;
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]
go

create procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] @diag_level int = 0 as
begin
  set nocount on
  
  --declare @diag_level int = 1
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: start!'

  if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
  begin
    if @diag_level > 0 print 'bik_app_prop[customRightRolesTreeSelector] is empty, nothing to do';
  
    return;
  end;
  
  -- 1. 
  -- zakładamy, że zostały zidentyfikowane silnie powiązane komponenty (strongly connected components, scc)
  -- i poprawnie wyliczone jest bik_system_user_group.scc_root_id
  -- jeżeli dana grupa nie tworzy cyklu z innymi, to jest ona jedynym elementem swojego scc, więc 
  -- bik_system_user_group.scc_root_id = bik_system_user_group.id
  
  select distinct scc_root_id as scc_id
  into #rcrrabg_scc
  from bik_system_user_group where is_deleted = 0;
  
  select distinct pg.scc_root_id as parent_scc_id, cg.scc_root_id as child_scc_id
  into #rcrrabg_rel
  from bik_system_group_in_group gig inner join bik_system_user_group pg on gig.group_id = pg.id
  inner join bik_system_user_group cg on gig.member_group_id = cg.id
  where gig.is_deleted = 0 and pg.is_deleted = 0 and cg.is_deleted = 0 and pg.scc_root_id <> cg.scc_root_id;
    
  truncate table bik_custom_right_role_action_branch_grant;

  -- specjalna akcja widoczności w drzewie na zakładkach - ShowInTree
  declare @show_in_tree_act_id int = (select id from bik_node_action where code = 'ShowInTree');

  declare @trees_for_roles_0 table (role_id int not null, tree_id int not null, unique (role_id, tree_id));

  insert into @trees_for_roles_0 (role_id, tree_id)
  select distinct crr.id as role_id, t.id as tree_id
  from
    bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str;  

  -- select * from bik_custom_right_role_user_entry
  
  if @diag_level > 1 select * from @trees_for_roles_0;
  
  declare @right_for_group_flat table (role_id int not null, group_id int not null, tree_id int null, node_id int null,
    unique(group_id, role_id, tree_id, node_id));

  declare @groups_to_process table (group_id int not null primary key);

  declare @processed_groups table (group_id int not null primary key);

  declare @unprocessed_groups table (group_id int not null primary key);
  
  insert into @unprocessed_groups (group_id) --select id from bik_system_user_group;
  select scc_id from #rcrrabg_scc;

  insert into @groups_to_process (group_id)
  /*select id
  from bik_system_user_group
  where id not in (select member_group_id from bik_system_group_in_group);*/
  select scc_id from #rcrrabg_scc where scc_id not in (select child_scc_id from #rcrrabg_rel);
  
  select scc_id, scc_id as ancestor_scc_id
  into #rcrrabg_ancestors_flat
  from #rcrrabg_scc;
  
  while exists (select 1 from @groups_to_process)
  begin
    if @diag_level > 0
    begin
      declare @gtpstr varchar(max) = null;
      
      select @gtpstr = case when @gtpstr is null then '' else @gtpstr + ',' end + cast(group_id as varchar(20))
      from @groups_to_process;
      
      print 'groups to process: ' + @gtpstr;    
    end;

    insert into #rcrrabg_ancestors_flat (scc_id, ancestor_scc_id)
    select distinct gtp.group_id, af.ancestor_scc_id
    from @groups_to_process gtp inner join #rcrrabg_rel r on gtp.group_id = r.child_scc_id
    inner join #rcrrabg_ancestors_flat af on r.parent_scc_id = af.scc_id;
    
    insert into @processed_groups (group_id) select group_id from @groups_to_process;

    delete from @unprocessed_groups where group_id in (select group_id from @groups_to_process);

    delete from @groups_to_process;
         
    insert into @groups_to_process (group_id)
    select group_id
    from @unprocessed_groups
    where group_id not in 
      --(select member_group_id from bik_system_group_in_group where group_id in (select group_id from @unprocessed_groups));
      (select child_scc_id from #rcrrabg_rel where parent_scc_id in (select group_id from @unprocessed_groups));
  end;

  if @diag_level > 0 and exists(select 1 from @unprocessed_groups)
  begin
    declare @ugstr varchar(max) = null;
    
    select @ugstr = case when @ugstr is null then '' else @ugstr + ',' end + cast(group_id as varchar(20))
    from @unprocessed_groups;
    
    print 'nothing new to process, but there are unprocessed groups: ' + @ugstr;    
  end;

  -- poniższe nie wyłapuje sytuacji gdy istnieje wpis nadrzędny i podrzędny 
  -- (w sensie węzłów w tym samym drzewie, dla tej samej roli) - wrzucane są oba wpisy
  insert into @right_for_group_flat (role_id, group_id, tree_id, node_id)
  select distinct crrue.role_id, cg.id, crrue.tree_id, crrue.node_id
  from #rcrrabg_ancestors_flat af inner join bik_system_user_group ag on af.ancestor_scc_id = ag.scc_root_id
  inner join bik_custom_right_role_user_entry crrue on crrue.group_id = ag.id
  inner join bik_system_user_group cg on af.scc_id = cg.scc_root_id
  where ag.is_deleted = 0;
    
  declare @right_for_user_flat table (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));

  insert into @right_for_user_flat (user_id, role_id, tree_id, node_id)
  select distinct user_id, role_id, tree_id, node_id
  from
    (select uig.user_id, rfgf.role_id, rfgf.tree_id, rfgf.node_id
     from bik_system_user_in_group uig inner join @right_for_group_flat rfgf on uig.group_id = rfgf.group_id
     union
     select user_id, role_id, tree_id, node_id
     from bik_custom_right_role_user_entry crrue where user_id is not null) x;

  if @diag_level > 1 select * from bik_custom_right_role_user_entry;

  if @diag_level > 1 select *
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id;      

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct @show_in_tree_act_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    not exists -- nie dorzucamy wpisu, jeżeli występuje dla niego wpis nadrzędny (na wyższym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          @right_for_user_flat crrue2 left join
          @trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct naicrr.action_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    @right_for_user_flat crrue inner join bik_node_action_in_custom_right_role naicrr on crrue.role_id = naicrr.role_id left join
    @trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    naicrr.action_id <> @show_in_tree_act_id and
    not exists -- nie dorzucamy wpisu, jeżeli występuje dla niego wpis nadrzędny (na wyższym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          @right_for_user_flat crrue2 inner join bik_node_action_in_custom_right_role naicrr2 on crrue2.role_id = naicrr2.role_id left join
          @trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where naicrr2.action_id = naicrr.action_id and crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: end'
end;
go


------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------
------------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_fix_system_users_and_groups_from_ad]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_fix_system_users_and_groups_from_ad]
go

create procedure [dbo].[sp_fix_system_users_and_groups_from_ad](@domain varchar(512)) as
begin
	set nocount on

	-- bik_system_user_group
	merge into bik_system_user_group as sug
	using (select grr.s_a_m_account_name, grr.domain, grr.distinguished_name from bik_active_directory_group as grr where grr.domain = @domain
			union all select ouu.name, ouu.domain, ouu.distinguished_name from bik_active_directory_ou as ouu where ouu.domain = @domain) as adg 
		on sug.distinguished_name = adg.distinguished_name
	when matched then
	update set is_deleted = 0
	when not matched by target and adg.domain = @domain then
	insert (name, domain, distinguished_name) values (adg.s_a_m_account_name, adg.domain, adg.distinguished_name)
	when not matched by source and sug.domain = @domain then
	update set is_deleted = 1;

	-- bik_system_user_in_group
	update bik_system_user_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_user_in_group as uig
	using (select usr.id as usr_id, sug.id as gr_id from bik_active_directory as ad 
		cross apply dbo.fn_split_by_sep(ad.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as sug on sp.str = sug.distinguished_name
		inner join bik_system_user as usr on usr.login_name = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain) as x on uig.user_id = x.usr_id and uig.group_id = x.gr_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (user_id, group_id) values (x.usr_id, x.gr_id);

	-- bik_system_group_in_group
	update bik_system_group_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain) or member_group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_group_in_group as gig
	using(select par.id as par_id, gr.id as child_id
		from bik_active_directory_ou as ou
		inner join bik_system_user_group as gr on gr.distinguished_name = ou.distinguished_name
		inner join bik_system_user_group as par on par.distinguished_name = ou.parent_distinguished_name
		where ou.domain = @domain
		union all 
		select ugr.id, cgr.id from bik_active_directory_group as gr
		cross apply dbo.fn_split_by_sep(gr.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as ugr on sp.str = ugr.distinguished_name
		inner join bik_system_user_group as cgr on gr.distinguished_name = cgr.distinguished_name
		where gr.domain = @domain
		union all 
		select chilgr.id, pargr.id from bik_active_directory_group as grp
		inner join bik_system_user_group as pargr on grp.parent_distinguished_name = pargr.distinguished_name
		inner join bik_system_user_group as chilgr on grp.distinguished_name = chilgr.distinguished_name
		where grp.domain = @domain) as x on x.par_id = gig.group_id and x.child_id = gig.member_group_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (group_id, member_group_id) values (x.par_id, x.child_id);

end;
go


--------------------
--------------------

declare @biks_id varchar(100) = case 
  when (select count(*) from bik_system_user where login_name in ('pals', 'Admin')) = 2 then '5005832A-6D1D-4F70-BB6D-32146BF003AA'
  when (select count(*) from bik_system_user where login_name in ('mmokwa', 'rkomorowski')) = 2 then 'B424EC5D-2D08-45AE-874D-A4E95FA61880'
  when (select count(*) from bik_system_user where login_name in ('grzegorz.szalachwij', 'jerzy.paryska')) = 2 then '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2'
  else '' end;
  
if not exists(select * from bik_app_prop where name = 'biks_id')
	insert into bik_app_prop (name, val) values ('biks_id', @biks_id);
go

if not exists(select * from bik_app_prop where name = 'biks_instance_id')
	insert into bik_app_prop (name, val) values ('biks_instance_id', newid());
go

if not exists(select * from bik_app_prop where name = 'isMultiDomainMode')
	insert into bik_app_prop (name, val) values ('isMultiDomainMode', 'false');
go

-----------
-----------

-- poprzednia wersja w pliku: alter db for v1.6.z9.8 tf.sql
-- fix na domain z AD
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	declare @fixedOptNodeFilter varchar(max), @baseFixedOptNodeFilter varchar(max);
	if @optNodeFilter is null
	begin
		set @baseFixedOptNodeFilter = '(1 = 1)';
	end
	else
	begin
		set @baseFixedOptNodeFilter = '(' + @optNodeFilter + ')';
	end;

	-- SAP BO
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select report_tree_id from bik_sapbo_server union all select universe_tree_id from bik_sapbo_server union all select connection_tree_id from bik_sapbo_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @fixedOptNodeFilter
	
	declare @bo_extradata_sql varchar(max) = '(select node_id, author, owner, cuid, guid, ruid, convert(varchar(24),created,120) as created_time, convert(varchar(24),modified,120) as modified_time, convert(varchar(24),last_run_time,120) as last_run_time from bik_sapbo_extradata)';
	exec sp_verticalize_node_attrs_one_metadata_table @bo_extradata_sql, 'node_id', 'author, owner, cuid, guid, ruid, created_time, modified_time, last_run_time', null, @fixedOptNodeFilter
	
	declare @schedule_sql varchar(max) = '(select node_id, owner as schedule_owner, convert(varchar(24),creation_time,120) as creation_time, convert(varchar(24),nextruntime,120) as nextruntime, convert(varchar(24),expire,120) as expire, destination, format, parameters, destination_email_to from bik_sapbo_schedule)';
	exec sp_verticalize_node_attrs_one_metadata_table @schedule_sql, 'node_id', 'schedule_owner, destination, creation_time, nextruntime, expire, format, parameters, destination_email_to', null, @fixedOptNodeFilter
	
	declare @olap_sql varchar(max) = '(select node_id, type, provider_caption, cube_caption, cuid as olap_cuid, convert(varchar(24),created,120) as created, owner as olap_owner, convert(varchar(24),modified,120) as modified from bik_sapbo_olap_connection)';
	exec sp_verticalize_node_attrs_one_metadata_table @olap_sql, 'node_id', 'type, provider_caption, cube_caption, olap_cuid, created, olap_owner, modified', null, @fixedOptNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @fixedOptNodeFilter
	
	-- Bazy danych (Oracle, Postgres, ...)
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select distinct tree_id from bik_db_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_db_definition', 'object_node_id', 'definition', 'definition_sql', @fixedOptNodeFilter
	
	-- MS SQL
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''MSSQL''))';
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @fixedOptNodeFilter
	-- Usuwanie starych ex propsów
	delete from bik_searchable_attr_val where attr_id in 
	(select att.id from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null)

	delete from bik_searchable_attr from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null
	--
	exec sp_verticalize_node_attrs_ex_props @fixedOptNodeFilter
	  
	-- DQC
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''DQC''))';
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,logging_details, benchmark_definition, results_object, additional_information', null, @fixedOptNodeFilter
	
	-- AD
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''UserRoles''))';
	declare @ad_src_sql varchar(max) = '(select coalesce(bu.email, ad.mail) as email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name, ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.company,
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = coalesce(ad.domain + ''\'', '''') + ad.s_a_m_account_name or ad.s_a_m_account_name = bsu.sso_login and ad.domain = bsu.sso_domain))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name, postal_code, manager, description, department, title, mobile, company, display_name', null, @fixedOptNodeFilter
	
	-- SAP BW query extradata
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code in (''BWProviders'', ''BWReports'')))';
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner as sapbw_owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, sapbw_owner, last_edit', null, @fixedOptNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @fixedOptNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name = bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @fixedOptNodeFilter
	
	-- Profile
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''Profile''))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_item_extradata', 'item_node_id', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ1, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_file_extradata', 'item_node_id', 'libs, fid, global, akey1, akey2, akey3, akey4, akey5, akey6, akey7, del, syssn, netloc, parfid, mplctdd, dftdes, dftord, dfthdr, dftdes1, ltd, usr, filetyp, exist, extendlength, fsn, fdoc, qid1, acckeys, val4ext, predaen, screen, rflag, dflag, udacc, udfile, fpn, udpre, udpost, publish, zrsfile, glref, rectyp, ptruser, ptrtld, log, archfiles, archkey, ptrtim, ptruseru, ptrtldu, ptrtimu, listdft, listreq, des', null, @fixedOptNodeFilter

end;
go



------------
------------
-- poprzednia wersja w pliku: alter db for v1.6.y4 tf.sql
-- fix na domain z AD
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_confluence_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].[sp_insert_into_bik_joined_objs_confluence_connections]
go

create procedure [dbo].[sp_insert_into_bik_joined_objs_confluence_connections]
as
begin

	declare @roleForNodeId int = (select id from bik_role_for_node where code = 'AuthorOfObject')
    declare @roleNode int = (select node_id from bik_role_for_node where id = @roleForNodeId)
    declare @userNodeKindId int = dbo.fn_node_kind_id_by_code('User')
    declare @userTreeId int = dbo.fn_tree_id_by_code('UserRoles')

    insert into bik_user_in_node (user_id, node_id, role_for_node_id, inherit_to_descendants, is_auxiliary, is_built_in)
    select us.id as user_node_id, ex.node_id as element_node_id, @roleForNodeId, 0, 0, 1
    from bik_confluence_extradata ex
    inner join bik_system_user as sus on ex.author = sus.sso_login
    inner join bik_user as us on sus.user_id = us.id
    left join bik_user_in_node as uin on uin.user_id = us.id and uin.node_id = ex.node_id and uin.role_for_node_id = @roleForNodeId
    where uin.id is null

    insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id)
    select distinct @roleNode, @userNodeKindId, ubn.name, @userTreeId, ubn.id
    from bik_confluence_extradata ex
    inner join bik_system_user as sus on ex.author = sus.sso_login
    inner join bik_user as us on sus.user_id = us.id
    inner join bik_node as ubn on ubn.id = us.node_id and ubn.is_deleted = 0
    left join bik_node as bn on bn.parent_node_id = @roleNode and bn.linked_node_id = ubn.id and bn.is_deleted = 0
    where bn.id is null

    exec sp_node_init_branch_id  null, @roleNode

end;
go

exec sp_update_version '1.7.z5.3', '1.7.z5.4';
go