﻿exec sp_check_version '1.7.z8.2';
go


merge into bik_app_prop ap 
using (values ('emailScheduleTime', '9:00')) as v(name, val) on ap.name = v.name
when matched then update set ap.val = v.val
when not matched by target then insert (name, val) values (v.name, v.val);
go


exec sp_update_version '1.7.z8.2', '1.7.z8.3';
go
