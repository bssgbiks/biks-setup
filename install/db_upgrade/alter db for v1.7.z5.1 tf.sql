﻿exec sp_check_version '1.7.z5.1';
go

--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------
-- tabela do testu 11 PB
if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_def3000tr_profile_file') and type in (N'U'))
	create table bik_dqm_def3000tr_profile_file (
		id bigint not null primary key identity(1,1),
		file_name varchar(512) null,
		is_def_file int not null default(0),
		ztjd date not null,
		tamt decimal(17,2) not null,
		custacc varchar(100) not null,
		custad1 varchar(255) null,
		custad2 varchar(255) null,
		custad3 varchar(255) null,
		custad4 varchar(255) null,
		benacc varchar(100) not null,
		benad1 varchar(255) null,
		benad2 varchar(255) null,
		benad3 varchar(255) null,
		benad4 varchar(255) null,
		ref1 varchar(255) null,
		ref2 varchar(255) null,
		ref3 varchar(255) null,
		ref4 varchar(255) null
	);
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_user_group') and type in (N'U'))
	create table bik_system_user_group (
		id int not null primary key identity(1,1),
		name varchar(512) not null,
		domain varchar(512) null,
		is_built_in int not null default(1),
		is_deleted tinyint not null default(0),
        unique (domain, name)   
	);
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_group_in_group') and type in (N'U'))
	create table bik_system_group_in_group (
		id int not null primary key identity(1,1),
		group_id int not null references bik_system_user_group(id),
		member_group_id int not null references bik_system_user_group(id),
		is_deleted tinyint not null default(0),
        unique (group_id, member_group_id),        
        unique (member_group_id, group_id) 
	);
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_system_user_in_group') and type in (N'U'))
	create table bik_system_user_in_group (
		id int not null primary key identity(1,1),
		user_id int not null references bik_system_user(id),
		group_id int not null references bik_system_user_group(id),
		is_deleted tinyint not null default(0),
        unique (group_id, user_id),        
        unique (user_id, group_id)  
	);
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'domain')
	alter table bik_active_directory add domain varchar(512) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'logon_count')
	alter table bik_active_directory add logon_count varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'memberof')
	alter table bik_active_directory add memberof varchar(max) null;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'is_deleted')
	alter table bik_active_directory add is_deleted tinyint not null default(0);
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_active_directory_group') and type in (N'U'))
	create table bik_active_directory_group (
		id int not null primary key identity(1,1),
		domain varchar(512) not null,
		s_a_m_account_name varchar(512) not null,
		s_a_m_account_type varchar(512) null,
		distinguished_name varchar(max) null,
		description varchar(max) null,
		mail varchar(max) null,
		member varchar(max) null,
		memberof varchar(max) null,
		group_type varchar(max) null,
		objectclass varchar(max) null,
		objectcategory varchar(max) null,
		is_deleted tinyint not null default(0)
	);
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_active_directory_ou') and type in (N'U'))
	create table bik_active_directory_ou (
		id int not null primary key identity(1,1),
		domain varchar(512) not null,
		name varchar(max) not null,
		distinguished_name varchar(max) not null,
		parent_distinguished_name varchar(max) null,
		description varchar(max) null,
		objectclass varchar(max) null,
		objectcategory varchar(max) null,
		is_deleted tinyint not null default(0)
	);
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_custom_right_role_user_entry') and name = 'group_id')
	alter table bik_custom_right_role_user_entry add group_id int null references bik_system_user_group (id);
go


alter table bik_custom_right_role_user_entry alter column user_id int null;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_s_a_m_account_name')
begin
	drop index [idx_bik_active_directory_s_a_m_account_name] ON [dbo].[bik_active_directory];
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_s_a_m_account_name_domain')
begin
	create unique index idx_bik_active_directory_s_a_m_account_name_domain on bik_active_directory (s_a_m_account_name, domain);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_domain_s_a_m_account_name')
begin
	create unique index idx_bik_active_directory_domain_s_a_m_account_name on bik_active_directory (domain, s_a_m_account_name);
end;
go


--------------------------------------------------------------
--------------------------------------------------------------
--------------------------------------------------------------

exec sp_update_version '1.7.z5.1', '1.7.z5.2';
go
