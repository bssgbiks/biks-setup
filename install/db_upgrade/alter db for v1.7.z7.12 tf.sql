﻿exec sp_check_version '1.7.z7.12';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

-- ostatni skrypt w pliku: alter db for v1.7.z5.9 tf.sql 
-- fix na duplikaty - na is_deleted
if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_fix_system_users_and_groups_from_ad]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_fix_system_users_and_groups_from_ad]
go

create procedure [dbo].[sp_fix_system_users_and_groups_from_ad](@domain varchar(512)) as
begin
	set nocount on

	-- bik_system_user_group
	merge into bik_system_user_group as sug
	using (select grr.s_a_m_account_name, grr.domain, grr.distinguished_name from bik_active_directory_group as grr where grr.domain = @domain and grr.is_deleted = 0
			union --all 
			select ouu.name, ouu.domain, ouu.distinguished_name from bik_active_directory_ou as ouu where ouu.domain = @domain and ouu.is_deleted = 0) as adg 
		on sug.distinguished_name = adg.distinguished_name
	when matched then
	update set is_deleted = 0
	when not matched by target and adg.domain = @domain then
	insert (name, domain, distinguished_name) values (adg.s_a_m_account_name, adg.domain, adg.distinguished_name)
	when not matched by source and sug.domain = @domain then
	update set is_deleted = 1;

	-- bik_system_user_in_group
	update bik_system_user_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_user_in_group as uig
	using (select usr.id as usr_id, sug.id as gr_id from bik_active_directory as ad 
		cross apply dbo.fn_split_by_sep(ad.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as sug on sp.str = sug.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain and ad.is_deleted = 0
		union --all
		select usr.id, gr.id from bik_active_directory as ad 
		inner join bik_system_user_group as gr on ad.parent_distinguished_name = gr.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain and ad.is_deleted = 0) as x on uig.user_id = x.usr_id and uig.group_id = x.gr_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (user_id, group_id) values (x.usr_id, x.gr_id);

	-- bik_system_group_in_group
	update bik_system_group_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain) or member_group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_group_in_group as gig
	using(select par.id as par_id, gr.id as child_id
		from bik_active_directory_ou as ou
		inner join bik_system_user_group as gr on gr.distinguished_name = ou.distinguished_name
		inner join bik_system_user_group as par on par.distinguished_name = ou.parent_distinguished_name
		where ou.domain = @domain and ou.is_deleted = 0
		union --all 
		select ugr.id, cgr.id from bik_active_directory_group as gr
		cross apply dbo.fn_split_by_sep(gr.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as ugr on sp.str = ugr.distinguished_name
		inner join bik_system_user_group as cgr on gr.distinguished_name = cgr.distinguished_name
		where gr.domain = @domain and gr.is_deleted = 0
		union --all 
		select pargr.id, chilgr.id from bik_active_directory_group as grp
		inner join bik_system_user_group as pargr on grp.parent_distinguished_name = pargr.distinguished_name
		inner join bik_system_user_group as chilgr on grp.distinguished_name = chilgr.distinguished_name
		where grp.domain = @domain and grp.is_deleted = 0) as x on x.par_id = gig.group_id and x.child_id = gig.member_group_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (group_id, member_group_id) values (x.par_id, x.child_id);

end;
go

declare @treeOdTree int = (select id from bik_tree where code = 'TreeOfTrees');

-- Dodanie BI Admin
if not exists(select * from bik_node where tree_id = @treeOdTree and obj_id = 'admin:biadmin')
begin
	exec sp_add_menu_node 'admin', 'BI Admin', 'admin:biadmin'
	exec sp_add_menu_node 'admin:biadmin', 'Zarządzanie instancjami', '#admin:biadmin:instanceManager'
	exec sp_add_menu_node 'admin:biadmin', 'Dane połącznia', '#admin:biadmin:cfg'
	
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:biadmin:instanceManager', 'Instance manager', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:biadmin:cfg', 'Connection properties', 'en', 'node')
	
	insert into bik_node_action(code)
	values ('#admin:biadmin:instanceManager')
	
	insert into bik_node_action(code)
	values ('#admin:biadmin:cfg')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:biadmin:instanceManager'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:biadmin:cfg'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	
	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
	
	insert into bik_admin(param, value) values ('biadmin.isActive', '0')
	insert into bik_admin(param, value) values ('biadmin.host', '')
	insert into bik_admin(param, value) values ('biadmin.user', '')
	insert into bik_admin(param, value) values ('biadmin.password', '')
end;

exec sp_node_init_branch_id  @treeOdTree, null

if not exists (select * from bik_data_source_def where description = 'BIAdmin')
begin
	insert into bik_data_source_def(description) values ('BIAdmin')
end;

if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
begin
	if not exists (select * from bik_app_prop where name = 'availableConnectors' and val like '%BIAdmin%')
	begin
		update bik_app_prop set val = val + ',BIAdmin' where name = 'availableConnectors'
	end
end


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

-- poprzednia wersja w "alter db for v1.1.7.8 bf.sql"
create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania
  declare @sub int = 1
  declare @calc_fvs_change_ex int = 1;
  declare @row_cnt int
  declare @log_msg varchar(max);

  if @sub > 0 begin	
    create table #updateTable (node_id int, branch_ids varchar(max) collate DATABASE_DEFAULT, act int, parent_node_id int);	
  end
  -- diag
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': start, @source=' + @source; exec sp_log_msg @log_msg; end;

  --------------
  -- 1. rozbijamy nazwy atrybutów - nazwy kolumn w źródle i nazwy właściwe (opcjonalne)
  --------------

  --declare @ attrPropNamesTab table (idx int primary key, name sysname not null)
  create table #vnaot_attrPropNamesTab (idx int primary key, name sysname collate DATABASE_DEFAULT not null);

  insert into #vnaot_attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  /*declare @ attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )*/

  create table #vnaot_attrNamesTab (name sysname collate DATABASE_DEFAULT not null primary key, 
    proper_name varchar(255) collate DATABASE_DEFAULT not null, search_weight int not null,
    attr_id int null
  );
  
  insert into #vnaot_attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join #vnaot_attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybutów i node_kindy, teraz uzupełniamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupełnieniem searchable attrs'; exec sp_log_msg @log_msg; end;
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from #vnaot_attrNamesTab ant
  where ant.attr_id is null  
  
  update #vnaot_attrNamesTab set attr_id = a.id
  from #vnaot_attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.proper_name
  
  --------------
  -- 3. wrzucamy wartości atrybutów do tabeli pomocniczej
  --------------

  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do #vnaot_attrValsTab'; exec sp_log_msg @log_msg; end;
  
  /*declare @ attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id))
  */

  create table #vnaot_attrValsTab (attr_id int not null, node_id int not null, value varchar(max) collate DATABASE_DEFAULT null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id), unique (node_id, attr_id));
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from #vnaot_attrNamesTab
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed open attrCur'; exec sp_log_msg @log_msg; end;

  if @diags_level > 0 begin
    select name, --attr_type, 
    proper_name, search_weight, attr_id from #vnaot_attrNamesTab
  end

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'; exec sp_log_msg @log_msg; end;

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed pętlą'; exec sp_log_msg @log_msg; end;

  while @@fetch_status = 0
  begin
    if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': start pętli dla pola: ' + @attrName + ' (' + @attrProperName + ')'; exec sp_log_msg @log_msg; end;
    
    set @attrValSql = 
    'select attr_id, node_id, /*case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end*/ value, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x where not (is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''')'
    if @diags_level > 0 begin set @log_msg = 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql; exec sp_log_msg @log_msg; end;
  
    insert into #vnaot_attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed końcem iteracji pętli dla pola: ' + @attrName; exec sp_log_msg @log_msg; end;
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyciągniętych wartości
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from #vnaot_attrValsTab)
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': za pętlą, kursor zamknięty, liczba wartości razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'; exec sp_log_msg @log_msg;
  end
  
  declare @attrIdsStr varchar(max) = ''
  update #vnaot_attrNamesTab set @attrIdsStr = @attrIdsStr + ',' + cast(attr_id as varchar(20))
  set @attrIdsStr = substring(@attrIdsStr, 2, len(@attrIdsStr))
  
  declare @attrValsInRangeSql varchar(max) = 'select av.id, av.node_id, av.attr_id 
  from bik_searchable_attr_val av' + case when @optNodeFilter is null then '' else ' inner join bik_node n on n.id = av.node_id and ' + @optNodeFilter end +
  ' where av.attr_id in (' + @attrIdsStr + ')'
  
  --declare @ attrValsInRange table (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id))
  create table #vnaot_attrValsInRange (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id));

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed insert into #vnaot_attrValsInRange, sql=' + @attrValsInRangeSql; exec sp_log_msg @log_msg;
  end 
  
  insert into #vnaot_attrValsInRange (id, node_id, attr_id)
  exec(@attrValsInRangeSql)
  
  declare @delCnt int
/*
  delete from bik_searchable_attr_val
  from #vnaot_attrValsTab avt 
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
*/

  --update bik_app_prop set val = '50' where name = 'suggestSimilar.maxResults'
  declare @register_bik_node_changes int = 0;

  if coalesce((select val from bik_app_prop where name = 'suggestSimilar.maxResults'), '0') <> '0'
    and exists (select 1 from bik_app_prop where name = 'indexing.fullData' and val in ('inProgress', 'done'))
    set @register_bik_node_changes = 1;

  --select @register_bik_node_changes

  if @register_bik_node_changes <> 0 begin

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': wrzucanie zmian do bik_node_change_detail'; exec sp_log_msg @log_msg;
    end 

    declare @change_id bigint;

    insert into bik_node_change (date_added) values (getdate());

    select @change_id = scope_identity();

    insert into bik_node_change_detail (change_id, node_id)
    select distinct @change_id, coalesce(av0.node_id, av.node_id)
    from 
      bik_searchable_attr_val av0 inner join #vnaot_attrValsInRange avir on av0.id = avir.id
      full outer join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
    where av0.id is null or av.node_id is null or av0.value <> av.value or av0.tree_id <> av.tree_id or av0.node_kind_id <> av.node_kind_id;
  end;


  if @calc_fvs_change_ex <> 0 begin

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_del'; exec sp_log_msg @log_msg;
    end; 

    declare @cfce_ids_str varchar(max);

    create table #cfce_del (node_id int not null primary key, branch_ids varchar(1000) collate DATABASE_DEFAULT null, unique (branch_ids, node_id));

    insert into #cfce_del (node_id, branch_ids)
    select id, branch_ids from bik_node where id in (select node_id from #vnaot_attrValsInRange) and is_deleted <> 0;

    set @row_cnt = @@rowcount;

    if @diags_level > 0 begin
      set @cfce_ids_str = '';
      select @cfce_ids_str = @cfce_ids_str + '|' + cast(node_id as varchar(20)) from #cfce_del;
      set @cfce_ids_str = case when @cfce_ids_str = '' then '' else stuff(@cfce_ids_str, 1, 1, '') end;
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_ins, @row_cnt= ' + cast(@row_cnt as varchar(20)) + ', @cfce_ids_str=' + @cfce_ids_str; exec sp_log_msg @log_msg;
    end; 

    create table #cfce_ins (node_id int not null primary key, branch_ids varchar(1000) collate DATABASE_DEFAULT null, unique (branch_ids, node_id));
    
    insert into #cfce_ins (node_id, branch_ids)
    select id, branch_ids from bik_node where id in (select nv.node_id from #vnaot_attrValsTab nv)
      and id not in (select av.node_id from bik_searchable_attr_val av) and is_deleted = 0;    

    set @row_cnt = @@rowcount;

    if @diags_level > 0 begin
      set @cfce_ids_str = '';
      select @cfce_ids_str = @cfce_ids_str + '|' + cast(node_id as varchar(20)) from #cfce_ins;
      set @cfce_ids_str = case when @cfce_ids_str = '' then '' else stuff(@cfce_ids_str, 1, 1, '') end;
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_upd, @row_cnt= ' + cast(@row_cnt as varchar(20)) + ', @cfce_ids_str=' + @cfce_ids_str; exec sp_log_msg @log_msg;
    end; 

    create table #cfce_upd (id int not null identity primary key, node_id int not null, 
      branch_ids varchar(1000) collate DATABASE_DEFAULT null, attr_id int not null, unique (node_id, attr_id), unique (node_id, branch_ids, attr_id));

    insert into #cfce_upd (node_id, branch_ids, attr_id)
    select coalesce(ir.node_id, nv.node_id), coalesce(n.branch_ids, nn.branch_ids), coalesce(ir.attr_id, nv.attr_id)
    from 
      #vnaot_attrValsInRange ir inner join bik_node n on ir.node_id = n.id and n.is_deleted = 0 
      inner join bik_searchable_attr_val av on ir.node_id = av.node_id and ir.attr_id = av.attr_id
      full outer join (#vnaot_attrValsTab nv inner join bik_node nn on nn.id = nv.node_id) on ir.node_id = nv.node_id and ir.attr_id = nv.attr_id
      left join #cfce_ins ins on ins.node_id = nv.node_id
    where coalesce(av.value, '') <> coalesce(nv.value, '') and ins.node_id is null;

    set @row_cnt = @@rowcount;

    if @diags_level > 0 begin
      set @cfce_ids_str = '';
      select @cfce_ids_str = @cfce_ids_str + '|' + cast(node_id as varchar(20)) from #cfce_upd;
      set @cfce_ids_str = case when @cfce_ids_str = '' then '' else stuff(@cfce_ids_str, 1, 1, '') end;
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_upd_agg, @row_cnt= ' + cast(@row_cnt as varchar(20)) + ', @cfce_ids_str=' + @cfce_ids_str; exec sp_log_msg @log_msg;
    end; 

    create table #cfce_upd_agg (node_id int not null primary key, branch_ids varchar(1000) collate DATABASE_DEFAULT null, attr_ids varchar(max) not null);
    
    insert into #cfce_upd_agg (node_id, branch_ids, attr_ids)
    select node_id, branch_ids, stuff((select '|' + cast(attr_id as varchar(20)) from #cfce_upd u where u.node_id = x.node_id for xml path ('')), 1, 1, '')
    from (select distinct node_id, branch_ids from #cfce_upd) x;

    -- nowe
    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupełnianiem bik_object_in_fvs_change_ex - nowe'; exec sp_log_msg @log_msg;
    end; 

    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.user_id, f.id, i.node_id from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
        inner join #cfce_ins i on i.branch_ids like n.branch_ids + '%'
    ) as c (user_id, fvs_id, node_id) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status)
      values (c.node_id, c.fvs_id, 1)
    when matched then
      update set ex.node_status = 1, ex.changed_attrs = '', ex.joined_obj_ids_added = '', ex.joined_obj_ids_deleted = '', 
        ex.new_comment_cnt = 0, ex.vote_val_delta = 0, ex.vote_cnt_delta = 0, ex.dqc_test_fail_cnt = 0
    ;    

    -- zmienione
    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupełnianiem bik_object_in_fvs_change_ex - zmienione'; exec sp_log_msg @log_msg;
    end; 

    declare @now datetime = getdate();

    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.user_id, f.id, i.node_id, i.attr_ids from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
        inner join #cfce_upd_agg i on i.branch_ids like n.branch_ids + '%'
    ) as c (user_id, fvs_id, node_id, attr_ids) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status, changed_attrs)
      values (c.node_id, c.fvs_id, 0, c.attr_ids)
    when matched and ex.node_status = 0 or (ex.node_status = 1 and datediff(mi, ex.date_added, @now) > 30) then 
      update set ex.changed_attrs = dbo.fn_add_items(ex.changed_attrs, c.attr_ids, '|', 7)
    ;    
    
    -- usunięte
    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupełnianiem bik_object_in_fvs_change_ex - zmienione'; exec sp_log_msg @log_msg;
    end; 

    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.user_id, f.id, i.node_id from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
        inner join #cfce_del i on i.branch_ids like n.branch_ids + '%'
    ) as c (user_id, fvs_id, node_id) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status)
      values (c.node_id, c.fvs_id, -1)
    when matched then
      update set ex.node_status = -1, ex.changed_attrs = '', ex.joined_obj_ids_added = '', ex.joined_obj_ids_deleted = '', 
        ex.new_comment_cnt = 0, ex.vote_val_delta = 0, ex.vote_cnt_delta = 0, ex.dqc_test_fail_cnt = 0
    ;    

  end;
 
  if @sub > 0 begin
  ------------- 
	--Subskrypcje- usunięte 
	-------------
	-- gdy usuwa się atrybut (wartość na null lub pustą) to wrzucamy do #updateTable
	-- node_id, 0
	-- możliwe duplikaty
	 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje usuniete'; exec sp_log_msg @log_msg; end;
	insert into #updateTable (node_id, act) 
	select bik_searchable_attr_val.node_id, 0
	from bik_searchable_attr_val join #vnaot_attrValsInRange avir on  bik_searchable_attr_val.id = avir.id left join #vnaot_attrValsTab av 
	on avir.attr_id = av.attr_id and avir.node_id = av.node_id
	where bik_searchable_attr_val.id = avir.id and av.node_id is null
	
  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ':Usunięte insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
	-------------
  end
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed właściwym delete'; exec sp_log_msg @log_msg;
  end 
  
  delete from bik_searchable_attr_val
  from #vnaot_attrValsInRange avir left join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where bik_searchable_attr_val.id = avir.id and av.node_id is null
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'; exec sp_log_msg @log_msg;
  end
  
  if @sub > 0 begin
	--Subskrypcje -- zaktualizowane
	-------------
	-- znów dorzucamy z act = 0
	-- node_id, 0 -- gdy zmieniła się wartość któregoś atrybutu (musiał już być wcześniej dodany taki atr.)
	-- możliwe duplikaty
	
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje aktualizacja'; exec sp_log_msg @log_msg; end;

	insert into #updateTable (node_id, act) 
	select avt.node_id, 0
	from #vnaot_attrValsTab avt join bik_searchable_attr_val on bik_searchable_attr_val.node_id = avt.node_id 
	     and bik_searchable_attr_val.attr_id = avt.attr_id
	where --ww: zbędne: avt.value is not null and
	    --ww: zbędne: avt.node_id not in(select node_id from #updateTable) and
	(bik_searchable_attr_val.value <> avt.value
    --ww: poniższe do zastanowienia
    or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id)

  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:Zaktualizowane insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
  
	-------------
	end
		
  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight,
    tree_id = avt.tree_id , node_kind_id = avt.node_kind_id
  from #vnaot_attrValsTab avt
  where --ww: zbędne: avt.value is not null and 
    bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight
     or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id )
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'; exec sp_log_msg @log_msg;
  end
  
  --------------
  -- 6. dorzucenie nowych wartości
  --------------
  
  if @sub > 0 begin
  ------------- 
	--Subskrypcje -- nowe lub zaktualizowana
	-------------
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa'; exec sp_log_msg @log_msg; end;
	declare @newAttrId int 
	select @newAttrId=id from bik_searchable_attr where name='name'

	-- nie było wartości, a teraz się pojawiła dla jakiegoś atrybutu
	-- node_id, 0 lub 1 -> 1 gdy to atrybut name się właśnie pojawił
	insert into #updateTable (node_id, act) 
	select avt.node_id, --max(
	case when avt.attr_id = @newAttrId then 1 else 0 end--) 
	as act
	from #vnaot_attrValsTab avt left join bik_searchable_attr_val av on
	avt.attr_id = av.attr_id and avt.node_id = av.node_id
	where av.id is null 
	--ww: zbędne: and avt.value is not null
	--ww: zbędne, i tak są duplikaty
	-- and avt.node_id not in (select node_id from #updateTable)
	--group by avt.node_id

 select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:Nowe lub zaktualizowana insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa koniec'; exec sp_log_msg @log_msg; end;
	-------------
	end
	
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from #vnaot_attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'; exec sp_log_msg @log_msg;
  end
  
  
  -- select * from #vnaot_attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --begin set @log_msg = @sql
  
  --select * from #vnaot_attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
  
   
    ------------- 
	--Subskrypcje
	------------- 
	if @sub > 0 begin
	
 if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :przed subskrypcją: '; exec sp_log_msg @log_msg;
  end

    select ut.node_id, ut.node_id as parent_node_id, max(ut.act) as act
    into #updateTableNoDupl
    from #updateTable ut
	group by ut.node_id

  	--update #updateTable set branch_ids=(select branch_ids from bik_node where id=#updateTable.node_id),parent_node_id=node_id
  	
  	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:update #updateTable set branch_ids, row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
  
	drop table #updateTable  
  
	declare @cnt int
	set @cnt=1
	
	while @cnt>0 begin
	
	insert into bik_object_in_fvs_change (node_id, fvs_id, action_type) 
	select ub.node_id, fvs.id, ub.act
	from #updateTableNoDupl ub join bik_object_in_fvs fvs on ub.parent_node_id = fvs.node_id
	left join bik_object_in_fvs_change fc on fvs.id = fc.fvs_id and ub.node_id = fc.node_id
	where fc.id is null
	
	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' insert into bik_object_in_fvs_change(node_id,fvs_id,action_type) , row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end

	update #updateTableNoDupl
    --set parent_node_id = bn.parent_node_id
    set parent_node_id = (select parent_node_id from bik_node where id = #updateTableNoDupl.parent_node_id)
	--from bik_node bn
	--where bn.id = #updateTableNoDupl.parent_node_id
	
select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' update #updateTable, row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end

	delete from #updateTableNoDupl where parent_node_id is null

  select @row_cnt = @@rowcount

	select @cnt=count(*) from #updateTableNoDupl

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:na końcu pętli, ile zostało: ' + cast(@cnt as varchar(20))+ ' po delete na #updateTableNoDupl, usuniętych row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
    end

	end
	
	drop table #updateTableNoDupl
	
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:po pętli, KONIEC'; exec sp_log_msg @log_msg;
  end

    end
end
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.12', '1.7.z7.13';
go
