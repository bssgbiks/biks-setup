﻿---UWAGA: Tylko raz wykonujemy ten skrypt-------------
exec sp_check_version '1.7.z12.7';
go

if not exists (select 1 from bik_app_prop where name='useHashedPassword') 
insert into bik_app_prop(name, val, is_editable) values ('useHashedPassword', '0', 1)
go

--if exists (select 1 from bik_app_prop where name='useHashedPassword' and val='1')
--update bik_system_user set password=hashbytes('SHA1', password) where password is not null

exec sp_update_version '1.7.z12.7', '1.7.z12.8';
go