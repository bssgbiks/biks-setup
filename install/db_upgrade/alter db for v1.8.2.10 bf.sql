﻿exec sp_check_version '1.8.2.10';
go


update bik_attribute set value_opt='metaBiksAttributeDef' where attr_def_id=
(select id from bik_attr_def where name='metaBIKS.Wzorzec') and node_kind_id=(select id from bik_node_kind where code='metaBiksNodeKind4TreeKind')

update bik_attribute set value_opt='metaBiksBuiltInNodeKind|metaBiksNodeKind' where attr_def_id=
(select id from bik_attr_def where name='metaBIKS.Wzorzec') and node_kind_id=(select id from bik_node_kind where code='metaBiksNodeKindInRelation')

update bik_attribute set value_opt='metaBiksAttributeDef' where attr_def_id=
(select id from bik_attr_def where name='metaBIKS.Wzorzec') and node_kind_id=(select id from bik_node_kind where code='metaBiksAttribute4NodeKind')


exec sp_update_version '1.8.2.10', '1.8.2.11';
go
 