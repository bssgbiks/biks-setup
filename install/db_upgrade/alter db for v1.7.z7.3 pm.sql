exec sp_check_version '1.7.z7.3';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

-- select * from bik_app_prop where name like 'indexing.%'

delete 
-- select *
from bik_app_prop where name like 'suggestSimilar.%';

insert into bik_app_prop (name, val, is_editable) values 
('suggestSimilar.maxResults', '0', 1),
('suggestSimilar.minScore', '0.01', 1),
('suggestSimilar.dupBuff', '20', 1);
go


if object_id('bik_node_change') is null
create table bik_node_change (
  id bigint not null identity primary key,
  date_added datetime not null default getdate()
);
go

-- drop table bik_node_change_detail
if object_id('bik_node_change_detail') is null
create table bik_node_change_detail (
  id bigint not null identity primary key,
  change_id bigint not null foreign key references bik_node_change (id),
  node_id int not null foreign key references bik_node (id)
);
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_node_change_detail]') and name = N'idx_bik_node_change_detail_cn')
create unique index idx_bik_node_change_detail_cn on bik_node_change_detail (change_id, node_id);
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_node_change_detail]') and name = N'idx_bik_node_change_detail_nc')
create unique index idx_bik_node_change_detail_nc on bik_node_change_detail (node_id, change_id);
go

/*

delete from bik_node_change_detail
delete from bik_node_change

select * from bik_node_change
select * from bik_node_change_detail

select * from bik_node where id in (2971347)

*/


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

-- poprzednia wersja w "alter db for v1.1.7.8 bf.sql"
create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania
  declare @sub int = 1
  declare @row_cnt int
  

  if @sub > 0 begin	
    create table #updateTable (node_id int, branch_ids varchar(max), act int, parent_node_id int);	
  end
  -- diag
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start, @source=' + @source

  --------------
  -- 1. rozbijamy nazwy atrybut�w - nazwy kolumn w �r�dle i nazwy w�a�ciwe (opcjonalne)
  --------------

  declare @attrPropNamesTab table (idx int primary key, name sysname not null)
  
  insert into @attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  declare @attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )
  
  insert into @attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join @attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybut�w i node_kindy, teraz uzupe�niamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed uzupe�nieniem searchable attrs'
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from @attrNamesTab ant
  where ant.attr_id is null  
  
  update @attrNamesTab set attr_id = a.id
  from @attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.proper_name
  
  --------------
  -- 3. wrzucamy warto�ci atrybut�w do tabeli pomocniczej
  --------------

  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do @attrValsTab'
  
  declare @attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id))
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from @attrNamesTab
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed open attrCur'

  if @diags_level > 0 begin
    select name, --attr_type, 
    proper_name, search_weight, attr_id from @attrNamesTab
  end

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed p�tl�'

  while @@fetch_status = 0
  begin
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': start p�tli dla pola: ' + @attrName + ' (' + @attrProperName + ')'
    
    set @attrValSql = 
    'select attr_id, node_id, /*case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end*/ value, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x where not (is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''')'
    if @diags_level > 0 print 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql
  
    insert into @attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed ko�cem iteracji p�tli dla pola: ' + @attrName
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyci�gni�tych warto�ci
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from @attrValsTab)
    print cast(sysdatetime() as varchar(23)) + ': za p�tl�, kursor zamkni�ty, liczba warto�ci razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'
  end
  
  declare @attrIdsStr varchar(max) = ''
  update @attrNamesTab set @attrIdsStr = @attrIdsStr + ',' + cast(attr_id as varchar(20))
  set @attrIdsStr = substring(@attrIdsStr, 2, len(@attrIdsStr))
  
  declare @attrValsInRangeSql varchar(max) = 'select av.id, av.node_id, av.attr_id 
  from bik_searchable_attr_val av' + case when @optNodeFilter is null then '' else ' inner join bik_node n on n.id = av.node_id and ' + @optNodeFilter end +
  ' where av.attr_id in (' + @attrIdsStr + ')'
  
  declare @attrValsInRange table (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id))
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': przed insert into @attrValsInRange, sql=' + @attrValsInRangeSql
  end 
  
  insert into @attrValsInRange (id, node_id, attr_id)
  exec(@attrValsInRangeSql)
  
  declare @delCnt int
/*
  delete from bik_searchable_attr_val
  from @attrValsTab avt 
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
*/
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': przed w�a�ciwym delete'
  end 
   
  declare @change_id bigint;

  insert into bik_node_change (date_added) values (getdate());

  select @change_id = scope_identity();

  insert into bik_node_change_detail (change_id, node_id)
  select distinct @change_id, coalesce(av0.node_id, av.node_id)
  from 
    bik_searchable_attr_val av0 inner join @attrValsInRange avir on av0.id = avir.id
    full outer join @attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where av0.id is null or av.node_id is null or av0.value <> av.value or av0.tree_id <> av.tree_id or av0.node_kind_id <> av.node_kind_id;

  if @sub > 0 begin
  ------------- 
	--Subskrypcje- usuni�te 
	-------------
	-- gdy usuwa si� atrybut (warto�� na null lub pust�) to wrzucamy do #updateTable
	-- node_id, 0
	-- mo�liwe duplikaty
	 if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': subskrypcje usuniete'
	insert into #updateTable (node_id, act) 
	select bik_searchable_attr_val.node_id, 0
	from bik_searchable_attr_val join @attrValsInRange avir on  bik_searchable_attr_val.id = avir.id left join @attrValsTab av 
	on avir.attr_id = av.attr_id and avir.node_id = av.node_id
	where bik_searchable_attr_val.id = avir.id and av.node_id is null
	
  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ':Usuni�te insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20))
  end
	-------------
  end
  
  delete from bik_searchable_attr_val
  from @attrValsInRange avir left join @attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where bik_searchable_attr_val.id = avir.id and av.node_id is null
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'
  end
  
  if @sub > 0 begin
	--Subskrypcje -- zaktualizowane
	-------------
	-- zn�w dorzucamy z act = 0
	-- node_id, 0 -- gdy zmieni�a si� warto�� kt�rego� atrybutu (musia� ju� by� wcze�niej dodany taki atr.)
	-- mo�liwe duplikaty
	
		 if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': subskrypcje aktualizacja'

	insert into #updateTable (node_id, act) 
	select avt.node_id, 0
	from @attrValsTab avt join bik_searchable_attr_val on bik_searchable_attr_val.node_id = avt.node_id 
	     and bik_searchable_attr_val.attr_id = avt.attr_id
	where --ww: zb�dne: avt.value is not null and
	    --ww: zb�dne: avt.node_id not in(select node_id from #updateTable) and
	(bik_searchable_attr_val.value <> avt.value
    --ww: poni�sze do zastanowienia
    or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id)

  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ' :sub:Zaktualizowane insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20))
  end
  
	-------------
	end
		
  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight,
    tree_id = avt.tree_id , node_kind_id = avt.node_kind_id
  from @attrValsTab avt
  where --ww: zb�dne: avt.value is not null and 
    bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight
     or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id )
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'
  end
  
  --------------
  -- 6. dorzucenie nowych warto�ci
  --------------
  
  if @sub > 0 begin
  ------------- 
	--Subskrypcje -- nowe lub zaktualizowana
	-------------
		 if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa'
	declare @newAttrId int 
	select @newAttrId=id from bik_searchable_attr where name='name'

	-- nie by�o warto�ci, a teraz si� pojawi�a dla jakiego� atrybutu
	-- node_id, 0 lub 1 -> 1 gdy to atrybut name si� w�a�nie pojawi�
	insert into #updateTable (node_id, act) 
	select avt.node_id, --max(
	case when avt.attr_id = @newAttrId then 1 else 0 end--) 
	as act
	from @attrValsTab avt left join bik_searchable_attr_val av on
	avt.attr_id = av.attr_id and avt.node_id = av.node_id
	where av.id is null 
	--ww: zb�dne: and avt.value is not null
	--ww: zb�dne, i tak s� duplikaty
	-- and avt.node_id not in (select node_id from #updateTable)
	--group by avt.node_id

 select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ' :sub:Nowe lub zaktualizowana insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20))
  end
		 if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa koniec'
	-------------
	end
	
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from @attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'
  end
  
  
  -- select * from @attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --print @sql
  
  --select * from @attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
  
   
    ------------- 
	--Subskrypcje
	------------- 
	if @sub > 0 begin
	
 if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ' :przed subskrypcj�: '
  end

    select ut.node_id, ut.node_id as parent_node_id, max(ut.act) as act
    into #updateTableNoDupl
    from #updateTable ut
	group by ut.node_id

  	--update #updateTable set branch_ids=(select branch_ids from bik_node where id=#updateTable.node_id),parent_node_id=node_id
  	
  	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ' :sub:update #updateTable set branch_ids, row cnt=' + cast(@row_cnt as varchar(20))
  end
  
	drop table #updateTable  
  
	declare @cnt int
	set @cnt=1
	
	while @cnt>0 begin
	
	insert into bik_object_in_fvs_change (node_id, fvs_id, action_type) 
	select ub.node_id, fvs.id, ub.act
	from #updateTableNoDupl ub join bik_object_in_fvs fvs on ub.parent_node_id = fvs.node_id
	left join bik_object_in_fvs_change fc on fvs.id = fc.fvs_id and ub.node_id = fc.node_id
	where fc.id is null
	
	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' insert into bik_object_in_fvs_change(node_id,fvs_id,action_type) , row cnt=' + cast(@row_cnt as varchar(20))
  end

	update #updateTableNoDupl
    --set parent_node_id = bn.parent_node_id
    set parent_node_id = (select parent_node_id from bik_node where id = #updateTableNoDupl.parent_node_id)
	--from bik_node bn
	--where bn.id = #updateTableNoDupl.parent_node_id
	
select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' update #updateTable, row cnt=' + cast(@row_cnt as varchar(20))
  end

	delete from #updateTableNoDupl where parent_node_id is null

  select @row_cnt = @@rowcount

	select @cnt=count(*) from #updateTableNoDupl

    if @diags_level > 0 begin
      print cast(sysdatetime() as varchar(23)) +' :sub:na ko�cu p�tli, ile zosta�o: ' + cast(@cnt as varchar(20))+ ' po delete na #updateTableNoDupl, usuni�tych row cnt=' + cast(@row_cnt as varchar(20))
    end

	end
	
	drop table #updateTableNoDupl
	
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) +' :sub:po p�tli, KONIEC'
  end

    end
end
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.3', '1.7.z7.4';
go
