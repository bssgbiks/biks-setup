﻿exec sp_check_version '1.8.4.16';
go

if not exists (select * from bik_admin where param='ad.filter')
     insert into bik_admin (param,value)
     values ('ad.filter','(&(objectCategory=group)(objectclass=group));')
else 
	update bik_admin set value='(&(objectCategory=group)(objectclass=group));' where param='ad.filter'

exec sp_update_version '1.8.4.16', '1.8.4.17';
go
