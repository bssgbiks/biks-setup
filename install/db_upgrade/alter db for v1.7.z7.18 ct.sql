﻿exec sp_check_version '1.7.z7.18';
go

if not exists (select 1 from sys.columns where name = N'is_versioned' and object_id = object_id(N'bik_lisa_extradata'))
begin
	alter table bik_lisa_extradata add is_versioned int
end;
go

exec sp_update_version '1.7.z7.18', '1.7.z7.19';
go