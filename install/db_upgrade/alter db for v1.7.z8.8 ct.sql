﻿exec sp_check_version '1.7.z8.8';
go

if not exists (select 1 from bik_node_action where code='LisaExport')
begin
	insert into bik_node_action(code) values('LisaExport')
end
go

declare @lisaExportActId int = (select id from bik_node_action where code='LisaExport')
delete from bik_node_action_in_custom_right_role where action_id = @lisaExportActId
insert into bik_node_action_in_custom_right_role(action_id, role_id)
select @lisaExportActId, id from bik_custom_right_role where code <> 'RegularUser'

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

exec sp_update_version '1.7.z8.8', '1.7.z8.9';
go