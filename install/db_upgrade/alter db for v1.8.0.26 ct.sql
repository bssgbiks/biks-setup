﻿exec sp_check_version '1.8.0.26';
go

if not exists (select * from sys.columns where object_id=object_id('bik_lisa_dict_column') and name='default_value')  
alter table bik_lisa_dict_column add default_value varchar(max);
go
 
exec sp_update_version '1.8.0.26', '1.8.0.27';
go