﻿exec sp_check_version '1.8.1.4';
go

 ALTER TABLE bik_node_kind_4_tree_kind ALTER COLUMN src_node_kind_id int NULL
    
insert into bik_node_kind_4_tree_kind(tree_kind_id,dst_node_kind_id,is_deleted)
select id,branch_node_kind_id,0 from bik_tree_kind btk where not exists 
(select * from bik_node_kind_4_tree_kind bnk where bnk.tree_kind_id=btk.id 
and bnk.dst_node_kind_id=btk.branch_node_kind_id and src_node_kind_id is null)

insert into bik_node_kind_4_tree_kind(tree_kind_id,src_node_kind_id ,dst_node_kind_id,relation_type,is_deleted)
select id,branch_node_kind_id,leaf_node_kind_id,'Dziecko',0 from bik_tree_kind btk where not exists 
(select * from bik_node_kind_4_tree_kind bnk where bnk.tree_kind_id=btk.id 
and bnk.src_node_kind_id=btk.branch_node_kind_id and bnk.dst_node_kind_id=btk.leaf_node_kind_id
and relation_type='Dziecko')

insert into bik_node_kind_4_tree_kind(tree_kind_id,src_node_kind_id ,dst_node_kind_id,relation_type,is_deleted)
select id,branch_node_kind_id,branch_node_kind_id,'Dziecko',0 from bik_tree_kind btk where not exists 
(select * from bik_node_kind_4_tree_kind bnk where bnk.tree_kind_id=btk.id 
and bnk.src_node_kind_id=btk.branch_node_kind_id and bnk.dst_node_kind_id=btk.branch_node_kind_id
and relation_type='Dziecko')


exec sp_update_version '1.8.1.4', '1.8.1.5';