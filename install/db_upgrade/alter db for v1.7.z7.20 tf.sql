﻿exec sp_check_version '1.7.z7.20';
go


-- poprzednia wersja w pliku: alter db for v1.7.z5.3 tf.sql
-- dodanie konektora profile
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_metadata]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_metadata
go

create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	declare @fixedOptNodeFilter varchar(max), @baseFixedOptNodeFilter varchar(max);
	if @optNodeFilter is null
	begin
		set @baseFixedOptNodeFilter = '(1 = 1)';
	end
	else
	begin
		set @baseFixedOptNodeFilter = '(' + @optNodeFilter + ')';
	end;

	-- SAP BO
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select report_tree_id from bik_sapbo_server union all select universe_tree_id from bik_sapbo_server union all select connection_tree_id from bik_sapbo_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @fixedOptNodeFilter
	
	declare @bo_extradata_sql varchar(max) = '(select node_id, author, owner, cuid, guid, ruid, convert(varchar(24),created,120) as created_time, convert(varchar(24),modified,120) as modified_time, convert(varchar(24),last_run_time,120) as last_run_time from bik_sapbo_extradata)';
	exec sp_verticalize_node_attrs_one_metadata_table @bo_extradata_sql, 'node_id', 'author, owner, cuid, guid, ruid, created_time, modified_time, last_run_time', null, @fixedOptNodeFilter
	
	declare @schedule_sql varchar(max) = '(select node_id, owner as schedule_owner, convert(varchar(24),creation_time,120) as creation_time, convert(varchar(24),nextruntime,120) as nextruntime, convert(varchar(24),expire,120) as expire, destination, format, parameters, destination_email_to from bik_sapbo_schedule)';
	exec sp_verticalize_node_attrs_one_metadata_table @schedule_sql, 'node_id', 'schedule_owner, destination, creation_time, nextruntime, expire, format, parameters, destination_email_to', null, @fixedOptNodeFilter
	
	declare @olap_sql varchar(max) = '(select node_id, type, provider_caption, cube_caption, cuid as olap_cuid, convert(varchar(24),created,120) as created, owner as olap_owner, convert(varchar(24),modified,120) as modified from bik_sapbo_olap_connection)';
	exec sp_verticalize_node_attrs_one_metadata_table @olap_sql, 'node_id', 'type, provider_caption, cube_caption, olap_cuid, created, olap_owner, modified', null, @fixedOptNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @fixedOptNodeFilter
	
	-- Bazy danych (Oracle, Postgres, ...)
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select distinct tree_id from bik_db_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_db_definition', 'object_node_id', 'definition', 'definition_sql', @fixedOptNodeFilter
	
	-- MS SQL
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''MSSQL''))';
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @fixedOptNodeFilter
	-- Usuwanie starych ex propsów
	delete from bik_searchable_attr_val where attr_id in 
	(select att.id from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null)

	delete from bik_searchable_attr from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null
	--
	exec sp_verticalize_node_attrs_ex_props @fixedOptNodeFilter
	  
	-- DQC
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''DQC''))';
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,logging_details, benchmark_definition, results_object, additional_information', null, @fixedOptNodeFilter
	
	-- AD
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''UserRoles''))';
	declare @ad_src_sql varchar(max) = '(select coalesce(bu.email, ad.mail) as email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name, ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.company,
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = coalesce(ad.domain + ''\'', '''') + ad.s_a_m_account_name or ad.s_a_m_account_name = bsu.sso_login and ad.domain = bsu.sso_domain))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name, postal_code, manager, description, department, title, mobile, company, display_name', null, @fixedOptNodeFilter
	
	-- SAP BW query extradata
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code in (''BWProviders'', ''BWReports'')))';
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner as sapbw_owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, sapbw_owner, last_edit', null, @fixedOptNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @fixedOptNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name = bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @fixedOptNodeFilter
	
	-- Profile
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''Profile''))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_item_extradata', 'item_node_id', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ1, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_file_extradata', 'item_node_id', 'libs, fid, global, akey1, akey2, akey3, akey4, akey5, akey6, akey7, del, syssn, netloc, parfid, mplctdd, dftdes, dftord, dfthdr, dftdes1, ltd, usr, filetyp, exist, extendlength, fsn, fdoc, qid1, acckeys, val4ext, predaen, screen, rflag, dflag, udacc, udfile, fpn, udpre, udpost, publish, zrsfile, glref, rectyp, ptruser, ptrtld, log, archfiles, archkey, ptrtim, ptruseru, ptrtldu, ptrtimu, listdft, listreq, des', null, @fixedOptNodeFilter

	-- File System
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code like ''FileSystem%''))';
	declare @fileSystemSQL varchar(max) = '(select bn.id, DATEADD(MILLISECOND, fs.last_modified % 1000, DATEADD(SECOND, fs.last_modified / 1000, ''19700101'')) as modified, fs.download_link as file_name, fs.target_file_path
		from bik_node as bn
		inner join bik_file_system_info as fs on bn.id = fs.node_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @fileSystemSQL, 'id', 'modified, file_name, target_file_path', null, @fixedOptNodeFilter
	
end;
go


exec sp_update_version '1.7.z7.20', '1.7.z8';
go