﻿exec sp_check_version '1.7.z3';
go

if not exists(select * from bik_app_prop where name = 'lameLoggersConfig')
begin
	insert into bik_app_prop(name, val, is_editable)
	values('lameLoggersConfig', '', 1)
end

exec sp_update_version '1.7.z3', '1.7.z4';
go