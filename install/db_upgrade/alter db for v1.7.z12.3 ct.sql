﻿exec sp_check_version '1.7.z12.3';
go

alter table bik_lisa_dict_instance add unique(node_id, instance_id)
go

update bn1 set bn1.obj_id=bn2.obj_id   
from bik_node bn1 join bik_node bn2 on bn1.linked_node_id is not null and bn1.linked_node_id=bn2.id
where bn1.node_kind_id in (select id from bik_node_kind where code in ('DWHDictionary', 'DWHCategorization', 'DWHCategory'))
 
merge bik_lisa_dict_instance di
using 
(select id as node_id, CAST(SUBSTRING(obj_id, 0, CHARINDEX('.', obj_id)) as int) as instance_id
	from bik_node where node_kind_id in (select id from bik_node_kind where code in ('DWHDictionary', 'DWHCategorization', 'DWHCategory'))) temp 
on temp.node_id = di.node_id
when NOT matched THEN 
insert (node_id, instance_id) values (node_id, instance_id);

exec sp_update_version '1.7.z12.3', '1.7.z12.4';
go
 