﻿exec sp_check_version '1.8.1.8';
go

if not exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind') and name='is_deleted')
alter table bik_node_kind add is_deleted int default 0
go

update bik_node_kind set is_deleted = 0 where is_deleted is null
update bik_node_kind_4_tree_kind set is_deleted = 0 where is_deleted is null
go

exec sp_update_version '1.8.1.8', '1.8.1.9';