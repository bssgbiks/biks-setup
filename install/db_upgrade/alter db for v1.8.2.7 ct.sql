﻿exec sp_check_version '1.8.2.7';
go

declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Typ relacji', @metaCategory, 'combobox', 'Powiązanie,Dowiązanie,Hyperlink,Dziecko', 0, 0 

delete from bik_attribute_linked 
where attribute_id in (select attr.id from bik_attribute attr join bik_attr_def adef on adef.id=attr.attr_def_id and adef.name='metaBIKS.Folder')

delete from bik_attribute 
where attr_def_id in (select id from bik_attr_def where name='metaBIKS.Folder')

delete from bik_attr_def where name='metaBIKS.Folder'

exec sp_add_attr_def 'metaBIKS.Typ w drzewie', @metaCategory, 'combobox', 'Główna gałąź+Gałąź,Główna gałąź+Liść,Gałąź,Liść', 0, 0

update bik_attribute_linked set value=replace(value, 'Główny gałąź', 'Główna gałąź')

exec sp_update_version '1.8.2.7', '1.8.2.8';
go
 