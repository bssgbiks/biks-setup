﻿exec sp_check_version '1.7.z6.6';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_delete_childless_folders]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_delete_childless_folders
go

create procedure [dbo].sp_delete_childless_folders
as
begin
	
	declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees');
	
	create table #candidates (id int not null primary key);
	
	insert into #candidates(id)
	select id
	from (
	select *, (select count(*) from bik_node where parent_node_id = bn.id) as childCount 
	from bik_node bn 
	where bn.tree_id = @treeOfTrees
	and substring(obj_id, 1, 1) not in ('#', '@', '$', '&')
	) x
	where x.childCount = 0;
	
	delete from bik_node_name_chunk where node_id in (select id from #candidates);
	
	delete from bik_node where id in (select id from #candidates);

end;
go


if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' -- PB
begin
	update bik_dqm_test set sql_text = 'select
		(select count(*) from %{hd}% as hd 
		 left join %{kre}% as kre on cast(hd.PRO_MODULO as varchar(max)) = kre.[LN.CID]
		 left join %{dep}% as dep on cast(hd.PRO_MODULO as varchar(max)) = dep.[DEP.CID]
		 where kre.[LN.CID] is not null or dep.[DEP.CID] is not null
	) as case_count, 
		(select count(*) from %{hd}% as hd 
		 left join %{kre}% as kre on cast(hd.PRO_MODULO as varchar(max)) = kre.[LN.CID]
		 left join %{dep}% as dep on cast(hd.PRO_MODULO as varchar(max)) = dep.[DEP.CID]
		 where (kre.[LN.CID] is not null or dep.[DEP.CID] is not null) and 
		 (kre.[LN.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_OTWARCIA, 120),'''') <> coalesce(kre.[LN.ODT],'''') or kre.[LN.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_ZAMKNIECIA, 120),'''') <> coalesce(kre.[LN.DTC],'''')
		 or kre.[LN.CID] is not null and coalesce(hd.PRO_STAT_WIND,'''') <> coalesce(kre.[LN.ZWINDSTS],'''') or kre.[LN.CID] is not null and coalesce(hd.KRE_LICZBA_DNI_ZADL,'''') <> coalesce(kre.[LN.DAYSDELQ],'''')
		 or kre.[LN.CID] is not null and coalesce(cast(hd.PRO_STATUS as varchar(max)),'''') <> coalesce(kre.[LN.STAT],'''') or kre.[LN.CID] is not null and coalesce(hd.KRE_KWOTA_ZALEG,'''') <> coalesce(kre.[LN.GTDUE],'''')
		 or kre.[LN.CID] is not null and coalesce(hd.KRE_DEFAULT,'''') <> coalesce(kre.[LN.ZMSRDFLG],'''') or kre.[LN.CID] is not null and coalesce(hd.KRE_RODZAJ_ZDARZENIA_DEFAULT,'''') <> coalesce(kre.[LN.ZMSRDTYP],'''')
		 or dep.[DEP.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_OTWARCIA, 120),'''') <> coalesce(dep.[DEP.ODT],'''') or dep.[DEP.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_ZAMKNIECIA, 120),'''') <> coalesce(dep.[DEP.DTC],'''')
		 or dep.[DEP.CID] is not null and coalesce(cast(hd.PRO_STATUS as varchar(max)),'''') <> coalesce(dep.[DEP.STAT],'''') or dep.[DEP.CID] is not null and coalesce(hd.IKO_DEFAULT,'''') <> coalesce(dep.[DEP.ZMSRDFLG],'''')
		 or dep.[DEP.CID] is not null and coalesce(hd.IKO_RODZAJ_ZDARZENIA_DEFAULT,'''') <> coalesce(dep.[DEP.ZMSRDTYP],''''))
	) as error_count', error_sql_text = 'select hd.PRO_MODULO as CID,
	hd.PRO_DATA_OTWARCIA,
	kre.[LN.ODT],
	case when kre.[LN.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_OTWARCIA, 120),'''') <> coalesce(kre.[LN.ODT],'''') then 1 else 0 end as LN_ODT_ER,
	dep.[DEP.ODT],
	case when dep.[DEP.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_OTWARCIA, 120),'''') <> coalesce(dep.[DEP.ODT],'''') then 1 else 0 end as DEP_ODT_ER,
	hd.PRO_DATA_ZAMKNIECIA,
	kre.[LN.DTC],
	case when kre.[LN.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_ZAMKNIECIA, 120),'''') <> coalesce(kre.[LN.DTC],'''') then 1 else 0 end as LN_DTC_ER,
	dep.[DEP.DTC],
	case when dep.[DEP.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_ZAMKNIECIA, 120),'''') <> coalesce(dep.[DEP.DTC],'''') then 1 else 0 end as DEP_DTC_ER,
	hd.PRO_STAT_WIND,
	kre.[LN.ZWINDSTS],
	case when kre.[LN.CID] is not null and coalesce(hd.PRO_STAT_WIND,'''') <> coalesce(kre.[LN.ZWINDSTS],'''') then 1 else 0 end as LN_ZWINDSTS_ER,
	kre.[LN.ZSPRWTFL],
	kre.[LN.ZFWID],
	kre.[LN.ZSPRZEDDT],
	kre.[LN.ZUMKW],
	hd.KRE_LICZBA_DNI_ZADL,
	kre.[LN.DAYSDELQ],
	case when kre.[LN.CID] is not null and coalesce(hd.KRE_LICZBA_DNI_ZADL,'''') <> coalesce(kre.[LN.DAYSDELQ],'''') then 1 else 0 end as LN_DAYSDELQ_ER,
	hd.PRO_STATUS,
	kre.[LN.STAT],
	case when kre.[LN.CID] is not null and coalesce(cast(hd.PRO_STATUS as varchar(max)),'''') <> coalesce(kre.[LN.STAT],'''') then 1 else 0 end as LN_STAT_ER,
	dep.[DEP.STAT],
	case when dep.[DEP.CID] is not null and coalesce(cast(hd.PRO_STATUS as varchar(max)),'''') <> coalesce(dep.[DEP.STAT],'''') then 1 else 0 end as DEP_STAT_ER,
	hd.KRE_KWOTA_ZALEG,
	kre.[LN.GTDUE],
	case when kre.[LN.CID] is not null and coalesce(hd.KRE_KWOTA_ZALEG,'''') <> coalesce(kre.[LN.GTDUE],'''') then 1 else 0 end as LN_GTDUE_ER,
	hd.KRE_DEFAULT,
	kre.[LN.ZMSRDFLG],
	case when kre.[LN.CID] is not null and coalesce(hd.KRE_DEFAULT,'''') <> coalesce(kre.[LN.ZMSRDFLG],'''') then 1 else 0 end as LN_ZMSRDFLG_ER,
	hd.KRE_RODZAJ_ZDARZENIA_DEFAULT,
	kre.[LN.ZMSRDTYP],
	case when kre.[LN.CID] is not null and coalesce(hd.KRE_RODZAJ_ZDARZENIA_DEFAULT,'''') <> coalesce(kre.[LN.ZMSRDTYP],'''') then 1 else 0 end as LN_ZMSRDTYP_ER,
	hd.IKO_DEFAULT,
	dep.[DEP.ZMSRDFLG],
	case when dep.[DEP.CID] is not null and coalesce(hd.IKO_DEFAULT,'''') <> coalesce(dep.[DEP.ZMSRDFLG],'''') then 1 else 0 end as DEP_ZMSRDFLG_ER,
	hd.IKO_RODZAJ_ZDARZENIA_DEFAULT,
	dep.[DEP.ZMSRDTYP],
	case when dep.[DEP.CID] is not null and coalesce(hd.IKO_RODZAJ_ZDARZENIA_DEFAULT,'''') <> coalesce(dep.[DEP.ZMSRDTYP],'''') then 1 else 0 end as DEP_ZMSRDTYP_ER
	from %{hd}% as hd 
	left join %{kre}% as kre on cast(hd.PRO_MODULO as varchar(max)) = kre.[LN.CID]
	left join %{dep}% as dep on cast(hd.PRO_MODULO as varchar(max)) = dep.[DEP.CID]
	where (kre.[LN.CID] is not null or dep.[DEP.CID] is not null) and 
	(kre.[LN.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_OTWARCIA, 120),'''') <> coalesce(kre.[LN.ODT],'''') or kre.[LN.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_ZAMKNIECIA, 120),'''') <> coalesce(kre.[LN.DTC],'''')
	or kre.[LN.CID] is not null and coalesce(hd.PRO_STAT_WIND,'''') <> coalesce(kre.[LN.ZWINDSTS],'''') or kre.[LN.CID] is not null and coalesce(hd.KRE_LICZBA_DNI_ZADL,'''') <> coalesce(kre.[LN.DAYSDELQ],'''')
	or kre.[LN.CID] is not null and coalesce(cast(hd.PRO_STATUS as varchar(max)),'''') <> coalesce(kre.[LN.STAT],'''') or kre.[LN.CID] is not null and coalesce(hd.KRE_KWOTA_ZALEG,'''') <> coalesce(kre.[LN.GTDUE],'''')
	or kre.[LN.CID] is not null and coalesce(hd.KRE_DEFAULT,'''') <> coalesce(kre.[LN.ZMSRDFLG],'''') or kre.[LN.CID] is not null and coalesce(hd.KRE_RODZAJ_ZDARZENIA_DEFAULT,'''') <> coalesce(kre.[LN.ZMSRDTYP],'''')
	or dep.[DEP.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_OTWARCIA, 120),'''') <> coalesce(dep.[DEP.ODT],'''') or dep.[DEP.CID] is not null and coalesce(convert(varchar(10), hd.PRO_DATA_ZAMKNIECIA, 120),'''') <> coalesce(dep.[DEP.DTC],'''')
	or dep.[DEP.CID] is not null and coalesce(cast(hd.PRO_STATUS as varchar(max)),'''') <> coalesce(dep.[DEP.STAT],'''') or dep.[DEP.CID] is not null and coalesce(hd.IKO_DEFAULT,'''') <> coalesce(dep.[DEP.ZMSRDFLG],'''')
	or dep.[DEP.CID] is not null and coalesce(hd.IKO_RODZAJ_ZDARZENIA_DEFAULT,'''') <> coalesce(dep.[DEP.ZMSRDTYP],''''))'
	where name = 'Poprawne_profile_klientów_HD_Profile - Ryzyko kredytowe'

	create table #tmp (id int not null primary key);

	insert into #tmp(id)
	select id from bik_dqm_request 
	where test_id in (select id from bik_dqm_test where name = 'Poprawne_profile_klientów_HD_Profile - Ryzyko kredytowe')

	delete from bik_dqm_request_error where request_id in (select id from #tmp)

	delete from bik_dqm_request
	where id in (select id from #tmp)

	drop table #tmp
end;
go

exec sp_update_version '1.7.z6.6', '1.7.z6.7';
go