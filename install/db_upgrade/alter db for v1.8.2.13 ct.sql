﻿exec sp_check_version '1.8.2.13';
go

if not exists (select 1 from sys.all_columns where object_id=object_id('bik_attribute') and name='visual_order')
alter table bik_attribute add visual_order int default 0
go
update bik_attribute set visual_order = 0 where visual_order is null

declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Kolejność wyświetlenia', @metaCategory, 'shortText', null, 1, 0

exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Kolejność wyświetlenia', '0', 1, null, null
exec sp_update_version '1.8.2.13', '1.8.2.14';
go

 