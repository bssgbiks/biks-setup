exec sp_check_version '1.7.z6.4';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_generic_insert_system_attr_values]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_generic_insert_system_attr_values]
go

create procedure [dbo].[sp_generic_insert_system_attr_values] (@treeCode varchar(max), @categoryName varchar(max)) as
begin
	set nocount on;
	
	--procka
	if not exists(select * from bik_attr_category where name = @categoryName and is_built_in = 1)
		insert into bik_attr_category(name, is_built_in) values (@categoryName, 1);

	declare @categoryId int = (select id from bik_attr_category where name = @categoryName and is_built_in = 1);

	-- merge na attr_def
	merge bik_attr_def as def
	using (select name from amg_attribute group by name) x on x.name = def.name
	when matched and def.is_built_in = 1 and def.attr_category_id = @categoryId then
	update set def.is_deleted = 0
	when not matched by target then
	insert (name, attr_category_id, is_built_in, type_attr) values (x.name, @categoryId, 1, 'shortText');

	-- merge na bik_attr_system
	merge bik_attr_system as asys
	using (select def.id as attr_id, bn.node_kind_id from amg_attribute as att
	inner join bik_attr_def as def on def.name = att.name and def.is_built_in = 1 and def.attr_category_id = @categoryId
	inner join bik_node as bn on bn.obj_id = att.obj_id
	inner join bik_tree as bt on bt.id = bn.tree_id
	where bt.code = @treeCode
	group by def.id, bn.node_kind_id) x on asys.node_kind_id = x.node_kind_id and x.attr_id = asys.attr_id
	when not matched by target then
	insert (node_kind_id, attr_id, is_visible) values (x.node_kind_id, x.attr_id, 1);

	-- merge na wartości 
	merge bik_attr_system_linked as lin
	using (
	select bsys.id as attr_id, bn.id as node_id, att.value from amg_attribute as att
	inner join bik_node as bn on bn.obj_id = att.obj_id
	inner join bik_tree as bt on bt.id = bn.tree_id
	inner join bik_attr_def as def on def.name = att.name and def.is_built_in = 1
	inner join bik_attr_system as bsys on bsys.node_kind_id = bn.node_kind_id and bsys.attr_id = def.id
	where bt.code = @treeCode
	) as x on lin.attr_system_id = x.attr_id and lin.node_id = x.node_id
	when matched then
	update set lin.is_deleted = 0, lin.value = x.value
	when not matched by target then
	insert (attr_system_id, node_id, value) values (x.attr_id, x.node_id, x.value)
	when not matched by source and lin.node_id in (select bn.id from bik_node as bn inner join bik_tree as bt on bt.id = bn.tree_id and bt.code = @treeCode) then
	update set lin.is_deleted = 1;

end;
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z6.4', '1.7.z6.5';
go

 