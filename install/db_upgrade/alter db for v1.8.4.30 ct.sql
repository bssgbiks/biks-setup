﻿exec sp_check_version '1.8.4.30';
go

update bik_attr_def set value_opt='hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,sql,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,ansiiText,longText,shortText' where name='metaBiks.Typ atrybutu'
update bik_attr_def set value_opt='hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,sql,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,ansiiText,longText,shortText' where name='metaBiks.Nadpisany typ atrybutu'

update bik_attribute set value_opt='hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,sql,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,ansiiText,longText,shortText,'
where attr_def_id in (select id from bik_attr_def where name in ('metaBiks.Typ atrybutu', 'metaBiks.Nadpisany typ atrybutu'))
and node_kind_id in (select id from bik_node_kind where code in ('metaBiksBuiltInNodeKind', 'metaBiksNodeKind'))
 
exec sp_update_version '1.8.4.30', '1.8.5.0';
go 
