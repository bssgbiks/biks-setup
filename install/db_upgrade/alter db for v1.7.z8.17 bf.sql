﻿exec sp_check_version '1.7.z8.17';
go

declare @treeOdTree int = (select id from bik_tree where code = 'TreeOfTrees');

-- DodanieAudytu
if exists(select * from bik_node where tree_id = @treeOdTree and obj_id = 'admin:biadmin')
begin
	
	if not exists(select * from bik_node where tree_id = @treeOdTree and obj_id = '#admin:biadmin:audyt')

	begin 
		exec sp_add_menu_node 'admin:biadmin', 'Audyt', '#admin:biadmin:audyt'
		
		insert into bik_translation(code, txt, lang, kind)
		values ('#admin:biadmin:audyt', 'Audyt', 'en', 'node')
		
		insert into bik_node_action(code)
		values ('#admin:biadmin:audyt')
		
		insert into bik_node_action_in_custom_right_role (action_id, role_id)
		select (select id from bik_node_action where code = '#admin:biadmin:audyt'), id 
		from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
		
		
		exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
		insert into bik_admin(param, value) values ('biadmin.audit.isActive', '0')
		insert into bik_admin(param, value) values ('biadmin.audit.server', '')
		insert into bik_admin(param, value) values ('biadmin.audit.instance', '')
		insert into bik_admin(param, value) values ('biadmin.audit.login', '')
		insert into bik_admin(param, value) values ('biadmin.audit.password', '')
		insert into bik_admin(param, value) values ('biadmin.audit.dbName', '')
	end;
	
end;	


exec sp_node_init_branch_id  @treeOdTree, null


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z8.17', '1.7.z8.18';
go
