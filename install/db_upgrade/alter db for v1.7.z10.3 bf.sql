﻿exec sp_check_version '1.7.z10.3';
go


if not exists (select 1 from sys.columns where name = N'value_opt_to' and object_id = object_id(N'bik_joined_obj_attribute'))
begin
	alter table bik_joined_obj_attribute add value_opt_to varchar(max) null
end;
go

if not exists (select 1 from bik_app_prop where name = 'disableImportExport')
	insert into bik_app_prop(name, val) values('disableImportExport', 'true');
go

exec sp_update_version '1.7.z10.3', '1.7.z10.4';
go