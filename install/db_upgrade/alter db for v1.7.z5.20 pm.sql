exec sp_check_version '1.7.z5.20';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
begin

	declare @roleId int;
	select @roleId=id from  bik_custom_right_role where code='RegularUser';

  if not exists(select 1 from bik_system_user_group where distinguished_name = 'DC=polsatc')
	begin
		insert into bik_system_user_group(name, domain, is_built_in, is_deleted, distinguished_name)
		values ('polsatc', 'polsatc', 1, 0, 'DC=polsatc');
		
		declare @cyfrowyId int = scope_identity();
		update bik_system_user_group set scc_root_id = @cyfrowyId where id = @cyfrowyId		
	end;

  declare @sug_id_plk int = (select id from bik_system_user_group where distinguished_name = 'DC=corp,DC=plusnet');
  declare @sug_id_cp int = (select id from bik_system_user_group where distinguished_name = 'DC=polsatc');

  truncate table bik_crr_label;

  if @sug_id_plk is not null
    insert into bik_crr_label (name, role_id, group_id, is_default_for_new_node) values ('PLK', @roleId, @sug_id_plk, 1);

  if @sug_id_cp is not null
    insert into bik_crr_label (name, role_id, group_id, is_default_for_new_node) values ('CP', @roleId, @sug_id_cp, 1);
    
  if not exists(select * from bik_app_prop where name = 'availableConnectors' and val like '%JDBC%')
	update bik_app_prop
	set val = val + ',JDBC'
	where name = 'availableConnectors';
    
  if not exists(select * from bik_app_prop where name = 'availableConnectors' and val like '%Confluence%')
	update bik_app_prop
	set val = val + ',Confluence'
	where name = 'availableConnectors';
	    
  if not exists(select * from bik_app_prop where name = 'availableConnectors' and val like '%MS SQL%')
	update bik_app_prop
	set val = val + ',MS SQL'
	where name = 'availableConnectors';	
end;
go

-- select * from bik_system_user_group
--select  *from bik_data_source_def

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


exec sp_update_version '1.7.z5.20', '1.7.z6';
go
