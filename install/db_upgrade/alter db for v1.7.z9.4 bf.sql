﻿exec sp_check_version '1.7.z9.4';


if object_id('bik_dqm_connection') is null
create table bik_dqm_connection(
	id int IDENTITY(1,1) NOT NULL,
	source_id int NOT NULL,
	name varchar(512) NOT NULL,
	server varchar(512) NULL,
	instance varchar(512) NULL, -- sid z oracla tez tu
	db_user varchar(512) NULL,
	db_password varchar(512) NULL,
	port int NULL,
	database_name varchar(max) NULL,
	is_deleted int not null);
	
go

if not exists (select * from bik_app_prop where name = 'useDqmConnectionMode')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('useDqmConnectionMode', 'true', 1)
end
go


if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' -- PB
begin
	update bik_app_prop set val='false' where name= 'useDqmConnectionMode';

end
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_dqm_test') and name = 'connection_id')
	alter table bik_dqm_test add connection_id int; 
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_dqm_test_multi') and name = 'connection_id')
	alter table bik_dqm_test_multi add connection_id int; 
go

exec sp_update_version '1.7.z9.4', '1.7.z9.5';
go
