﻿exec sp_check_version '1.8.0.11';
go

if not exists (select 1 from sys.columns where name='source_folder_path' and object_id=object_id('bik_plain_file'))
alter table bik_plain_file add source_folder_path varchar(max)
go

if OBJECT_ID (N'dbo.fn_node_id_by_tree_code_and_obj_id', N'FN') is not null
drop function dbo.fn_node_id_by_tree_code_and_obj_id;
go

create function [dbo].[fn_node_id_by_tree_code_and_obj_id](@tree_code varchar(256), @obj_id varchar(900))
returns int
as
begin
  return (select id from bik_node where tree_id = dbo.fn_tree_id_by_code(@tree_code) and ((obj_id is not null and obj_id = @obj_id) or (obj_id is null and dbo.fn_get_obj_id_by_node_name_path(id)=@obj_id)))
end
go

exec sp_update_version '1.8.0.11', '1.8.0.12';
go
