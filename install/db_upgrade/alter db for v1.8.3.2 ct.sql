﻿exec sp_check_version '1.8.3.2';
go
 
declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Może być pusty', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Może być pusty', null, 1, null, null

update bik_attr_def set name='metaBIKS.Pokaż na formatce nowego obiektu' where name='metaBIKS.Wymagany'
if not exists (select 1 from sys.all_columns where name='is_empty' and object_id=object_id('bik_attribute'))
alter table bik_attribute add is_empty int default 1
go
update bik_attribute set is_empty=1 where is_empty is null
update attr set is_empty=0
from bik_attribute attr join bik_attr_def adef on attr.attr_def_id = adef.id 
where adef.name like 'metaBIKS%'

exec sp_update_version '1.8.3.2', '1.8.3.3';
go