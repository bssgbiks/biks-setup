﻿exec sp_check_version '1.7.z8.11';
go

declare @treeOdTree int = (select id from bik_tree where code = 'TreeOfTrees');

-- Dodanie BI Admin - object & archive mng
if not exists(select * from bik_node where tree_id = @treeOdTree and obj_id = '#admin:biadmin:objectManager')
begin
	exec sp_add_menu_node 'admin:biadmin', 'Zarządzanie obiektami', '#admin:biadmin:objectManager'
	exec sp_add_menu_node 'admin:biadmin', 'Archiwizacja danych', '#admin:biadmin:archiveManager'
	
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:biadmin:objectManager', 'Object manager', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind)
	values ('#admin:biadmin:archiveManager', 'Archive manager', 'en', 'node')
	
	insert into bik_node_action(code)
	values ('#admin:biadmin:objectManager')
	
	insert into bik_node_action(code)
	values ('#admin:biadmin:archiveManager')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:biadmin:objectManager'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:biadmin:archiveManager'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	
	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

end;

exec sp_node_init_branch_id  @treeOdTree, null;

exec sp_update_version '1.7.z8.11', '1.7.z8.12';
go