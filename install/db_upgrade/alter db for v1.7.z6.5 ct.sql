exec sp_check_version '1.7.z6.5';
go

if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_add_attrs') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_add_attrs
go

create procedure dbo.sp_verticalize_node_attrs_add_attrs(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  --declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  --declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in=0 /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  --insert into @attr_names (attr_id, name, is_deleted)
  --exec(@attr_names_sql)
  
  --select * from @attr_names
  
  --declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  declare attr_names_cur cursor for select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in = 0 /*and is_deleted = 0 and exists (select 1 from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id where a.attr_def_id = ad.id and al.is_deleted = 0 and a.is_deleted = 0)*/
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

	--set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
  
  ------Dla metadata atrybutów---------------
  declare attr_names_cur cursor 
  for 
  select distinct ad.id,  ad.name, ad.is_deleted  from bik_attr_def ad 
  inner join bik_attr_system ats on ats.attr_id = ad.id 
  inner join bik_attr_system_linked al on al.attr_system_id = ats.id 
  where ad.is_built_in = 1  

  declare @attr_system_id int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_system_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_system_source_sql varchar(max) = '(select node_id, value from bik_attr_system_linked al inner join bik_attr_system ats on ats.id = al.attr_system_id inner join bik_attr_def adef on adef.id = ats.attr_id
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and ats.is_visible = 1 and ats.attr_id = ' + cast(@attr_system_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_system_source_sql

	--set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_metadata_table @attr_system_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_system_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur 
end
go

exec sp_update_version '1.7.z6.5', '1.7.z6.6';
go