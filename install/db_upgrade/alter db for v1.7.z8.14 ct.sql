﻿exec sp_check_version '1.7.z8.14';
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_admin_backup_log') and type in (N'U'))
	create table bik_admin_backup_log(
		id int not null primary key identity(1,1),
		start_time datetime,
		end_time datetime,
		job_cuid varchar(128),
		status varchar(max)
)	;
go

if not exists (select * from sys.objects where object_id = object_id(N'bik_admin_backup_file') and type in (N'U'))
	create table bik_admin_backup_file(
		id bigint not null primary key identity(1,1),
		backup_log_id int foreign key references bik_admin_backup_log(id),
		start_time datetime,
		end_time datetime,
		status varchar(max),
		file_name varchar(512),
		obj_id int,
		obj_cuid varchar(128),
		obj_name varchar(max),
		obj_last_modification datetime,
		obj_kind varchar(256)
	);
go

exec sp_update_version '1.7.z8.14', '1.7.z8.15';
go