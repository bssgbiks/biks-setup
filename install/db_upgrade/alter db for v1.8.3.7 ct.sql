﻿exec sp_check_version '1.8.3.7';
go
 
if not exists (select 1 from sys.all_columns where name='is_public' and object_id=object_id('bik_attribute'))
alter table bik_attribute add is_public int default 1
go
update bik_attribute set is_public=1 where is_public is null

declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Publiczny', @metaCategory, 'comboBooleanBox', 'Tak,Nie', 0, 0
exec sp_add_attr_2_node_kind 'metaBiksAttribute4NodeKind', 'metaBIKS.Publiczny', null, 1, null, null

if not exists (select 1 from bik_node_action where code='ShowNonPublicAttributes')
begin
	insert into bik_node_action(code) values('ShowNonPublicAttributes')
	insert into bik_node_action_in_custom_right_role(action_id, role_id)
	select na.id, crr.id from bik_node_action na, bik_custom_right_role crr where na.code='ShowNonPublicAttributes'
end
exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

exec sp_update_version '1.8.3.7', '1.8.3.8';
go

