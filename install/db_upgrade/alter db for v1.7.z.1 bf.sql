﻿exec sp_check_version '1.7.z.1';
go

if not exists (select * from bik_app_prop where name = 'customDialogPercentSize')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('customDialogPercentSize', '', 1)
end;
go

if not exists (select * from bik_app_prop where name = 'notExpandingJoinedObjsFooter')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('notExpandingJoinedObjsFooter', 'false', 1)
end;
go

exec sp_update_version '1.7.z.1', '1.7.z.2';
go
