﻿exec sp_check_version '1.8.3.5';
go
 
declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Treść pomocy dla autora', @metaCategory, 'longText', null, 0, 0
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Treść pomocy dla autora', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksBuiltInNodeKind', 'metaBIKS.Treść pomocy dla autora', null, 1, null, null

exec sp_add_attr_def 'metaBIKS.Treść pomocy dla innych', @metaCategory, 'longText', null, 0, 0
exec sp_add_attr_2_node_kind 'metaBiksNodeKind', 'metaBIKS.Treść pomocy dla innych', null, 1, null, null
exec sp_add_attr_2_node_kind 'metaBiksBuiltInNodeKind', 'metaBIKS.Treść pomocy dla innych', null, 1, null, null

exec sp_update_version '1.8.3.5', '1.8.3.6';
go