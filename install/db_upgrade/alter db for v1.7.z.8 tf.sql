﻿exec sp_check_version '1.7.z.8';
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_evaluation') and type in (N'U'))
begin
	create table bik_dqm_evaluation (
		id int primary key identity(1,1),
		node_id int not null references bik_node(id),
		group_node_id int not null references bik_node(id),
		date_from date not null,
		date_to date not null,
		leader_node_id int not null references bik_node(id),
		created_date date not null, 
		prev_quality_level varchar(512) null,
		actual_quality_level varchar(512) not null,
		significance varchar(512) not null,
		action_plan varchar(max) null,
		is_accepted int not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_report') and type in (N'U'))
begin
	create table bik_dqm_report (
		id int primary key identity(1,1),
		node_id int not null references bik_node(id),
		date_from date not null,
		date_to date not null
	);
end;
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_report_evaluation') and type in (N'U'))
begin
	create table bik_dqm_report_evaluation (
		id int primary key identity(1,1),
		report_node_id int not null references bik_node(id),
		evaluation_node_id int not null references bik_node(id)
	);
end;
go


if not exists(select 1 from bik_node_kind where code = 'DQMFolderRate')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMFolderRate', 'Folder ocen', 'postgresFolder', 'QualityMetadata', 0, 5, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMFolderCoordination', 'Folder koordynatora', 'dqmfoldercoordination', 'QualityMetadata', 0, 5, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMReport', 'Raport', 'oracleBIAnalysisAndReports', 'QualityMetadata', 0, 15, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMRate', 'Ocena', 'dqmrate', 'QualityMetadata', 0, 10, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMRateAccepted', 'Ocena zaakceptowana', 'dqmrateaccepted', 'QualityMetadata', 0, 10, 0)
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('DQMRateRejected', 'Ocena odrzucona', 'dqmraterejected', 'QualityMetadata', 0, 10, 0)
	
	insert into bik_icon(name)
	values('dqmrate'), ('dqmrateaccepted'), ('dqmraterejected'), ('dqmfoldercoordination')
end
go

if not exists(select * from bik_icon where name = 'paragraph')
begin
	insert into bik_icon(name)
	values('paragraph')
end;

update bik_icon set name = 'oracleFolder' where name = 'OracleFolder'

update bik_app_prop set val = '80;80' where name = 'customDialogPercentSize'
update bik_app_prop set val = 'true' where name = 'notExpandingJoinedObjsFooter'

exec sp_update_version '1.7.z.8', '1.7.z.9';
go
