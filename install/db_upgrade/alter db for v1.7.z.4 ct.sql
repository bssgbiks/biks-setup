exec sp_check_version '1.7.z.4';
go

if not exists(select 1 from bik_node_kind where code='OraclePackage') 
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank, is_leaf_for_statistic)
	values('OraclePackage', 'Pakiet Oracle', 'oraclePackage', 'Metadata', 0, 8, 0);
	
	insert into bik_translation(code, txt, lang, kind)
	values  ('OraclePackage', 'Oracle package', 'en', 'kind');
	
	insert into bik_icon(name)
	values  ('oraclePackage');
	
	exec sp_add_menu_node '&OracleOwner', '@', '&OraclePackage'
	
	declare @tree_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
	exec sp_node_init_branch_id @tree_id, null
end;
go

exec sp_update_version '1.7.z.4', '1.7.z.5';
go

