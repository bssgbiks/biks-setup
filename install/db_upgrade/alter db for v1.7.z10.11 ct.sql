exec sp_check_version '1.7.z10.11';
go 

/* Tylko dla PLK
if not exists (select 1 from bik_app_prop where name='enableNodeHistory')
insert into bik_app_prop(name,val) values('enableNodeHistory', 'false');
else update bik_app_prop set val='false' where name='enableNodeHistory'
go
*/

if not exists (select 1 from bik_app_prop where name='historyAggregateLevel')
insert into bik_app_prop(name,val) values('historyAggregateLevel', '1');
else update bik_app_prop set val='1' where name='historyAggregateLevel'
go

if object_id('bik_plain_file') is not null 
drop table bik_plain_file
go

create table bik_plain_file(
	id int primary key identity(1,1),
	source_file_path varchar(max),
	is_remote int default 0,	
	name varchar(max),
	domain varchar(max),
	username varchar(max),
	password varchar(max),
	tree_id int references bik_tree(id),
	is_active int default 0
)
go

if not exists (select 1 from bik_data_source_def where description='Plain File')
insert into bik_data_source_def(description) values('Plain File')
go

if object_id('sp_generic_insert_into_bik_node_ex') is not null 
drop procedure sp_generic_insert_into_bik_node_ex
go

create procedure sp_generic_insert_into_bik_node_ex(@treeCode varchar(max), @objId varchar(900), @forceDelete int)
as
begin
-- założenia:
	-- branch_ids są uzupełnione w bik_node i amg_node (@objId is not null)
	-- jeśli @objId is not null wtedy musi istniec ten obiekt w bik_node (inny mechanizm go wstawil wczesniej)
	-- 
	declare @treeId int = (select id from bik_tree where code = @treeCode);
	declare @parentNodeId int = (select id from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @branchIds varchar(900) = (select branch_ids from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @amgBranchIds varchar(900) = (select branch_ids from amg_node where obj_id = @objId);

	-- update
	update bik_node 
	set is_deleted = 0, name = gen.name, descr = gen.descr, visual_order = gen.visual_order, node_kind_id = bnk.id
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.obj_id = gen.obj_id
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @parentNodeId, bnk.id, gen.name, gen.descr, @treeId, gen.obj_id, gen.visual_order
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	left join (bik_node as bn inner join bik_tree as bt on bn.tree_id = bt.id and bn.tree_id = @treeId and (@branchIds is null or bn.branch_ids like @branchIds + '%')) on bn.obj_id = gen.obj_id
	where bn.id is null
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	order by gen.obj_id
	
	exec sp_node_init_branch_id @treeId, @parentNodeId;

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join amg_node as gen on bik_node.obj_id = gen.obj_id
	inner join bik_node as bk on bk.obj_id = gen.parent_obj_id
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @treeId
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	if (@forceDelete > 0) begin
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join amg_node as gen on bik_node.obj_id = gen.obj_id 
		and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	where gen.name is null
	and bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node as bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @treeId
	and (@branchIds is null or bn2.branch_ids like @branchIds + '%')
	and bn2.is_deleted = 1
	end;

	exec sp_node_init_branch_id @treeId, null
end;
go

if object_id('sp_generic_insert_into_bik_node') is not null 
drop procedure sp_generic_insert_into_bik_node
go

create procedure sp_generic_insert_into_bik_node(@treeCode varchar(max), @objId varchar(900))
as
begin
	exec sp_generic_insert_into_bik_node_ex @treeCode, @objId, 1;
end;
go

exec sp_update_version '1.7.z10.11', '1.7.z10.12';
go
