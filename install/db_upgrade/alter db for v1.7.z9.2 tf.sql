﻿exec sp_check_version '1.7.z9.2';

if not exists(select * from bik_app_prop where name = 'purgeWebiForReportSDK')
	insert into bik_app_prop(name, val) values ('purgeWebiForReportSDK', 'false')
go

exec sp_update_version '1.7.z9.2', '1.7.z9.3';
go
