﻿exec sp_check_version '1.8.4.7';
go

/****** Object:  StoredProcedure [dbo].[sp_fix_system_users_and_groups_from_ad]    Script Date: 25.06.2018 21:13:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[sp_fix_system_users_and_groups_from_ad](@domain varchar(512)) as
begin
	set nocount on

	-- bik_system_user_group
	merge into bik_system_user_group as sug
	using (select grr.s_a_m_account_name, grr.domain, grr.distinguished_name from bik_active_directory_group as grr where grr.domain = @domain and grr.is_deleted = 0
			union --all 
			select ouu.name, ouu.domain, ouu.distinguished_name from bik_active_directory_ou as ouu where ouu.domain = @domain and ouu.is_deleted = 0) as adg 
		on sug.distinguished_name = adg.distinguished_name
	when matched then
	update set is_deleted = 0
	when not matched by target and adg.domain = @domain then
	insert (name, domain, distinguished_name) values (adg.s_a_m_account_name, adg.domain, adg.distinguished_name)
	when not matched by source and sug.domain = @domain then
	update set is_deleted = 1;

	-- bik_system_user_in_group
	update bik_system_user_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_user_in_group as uig
	using (select usr.id as usr_id, sug.id as gr_id from bik_active_directory as ad 
		cross apply dbo.fn_split_by_sep(ad.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as sug on sp.str = sug.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain and ad.is_deleted = 0
		union --all
		select usr.id, gr.id from bik_active_directory as ad 
		inner join bik_system_user_group as gr on ad.parent_distinguished_name = gr.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain and ad.is_deleted = 0) as x on uig.user_id = x.usr_id and uig.group_id = x.gr_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (user_id, group_id) values (x.usr_id, x.gr_id);

	-- bik_system_group_in_group
	update bik_system_group_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain) or member_group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_group_in_group as gig
	using(select par.id as par_id, gr.id as child_id
		from bik_active_directory_ou as ou
		inner join bik_system_user_group as gr on gr.distinguished_name = ou.distinguished_name
		inner join bik_system_user_group as par on par.distinguished_name = ou.parent_distinguished_name
		where ou.domain = @domain and ou.is_deleted = 0
		union --all 
		select ugr.id, cgr.id from bik_active_directory_group as gr
		cross apply dbo.fn_split_by_sep(gr.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as ugr on sp.str = ugr.distinguished_name
		inner join bik_system_user_group as cgr on gr.distinguished_name = cgr.distinguished_name
		where gr.domain = @domain and gr.is_deleted = 0
		union --all 
		select pargr.id, chilgr.id from bik_active_directory_group as grp
		inner join bik_system_user_group as pargr on grp.parent_distinguished_name = pargr.distinguished_name
		inner join bik_system_user_group as chilgr on grp.distinguished_name = chilgr.distinguished_name
		where grp.domain = @domain and grp.is_deleted = 0) as x on x.par_id = gig.group_id and x.child_id = gig.member_group_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (group_id, member_group_id) values (x.par_id, x.child_id);

	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
	
end;
go
--zaczyna się druga funkcja
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]    Script Date: 25.06.2018 21:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] @diag_level int = 0 as
begin
  set nocount on
  
  --declare @diag_level int = 1
  
  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: start!'; exec sp_log_msg @log_msg;
  end;

  if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
  begin
    if @diag_level > 0 begin
      set @log_msg = 'bik_app_prop[customRightRolesTreeSelector] is empty, nothing to do'; exec sp_log_msg @log_msg;
    end;

    return;
  end;
  
  -- 1. 
  -- zakładamy, że zostały zidentyfikowane silnie powiązane komponenty (strongly connected components, scc)
  -- i poprawnie wyliczone jest bik_system_user_group.scc_root_id
  -- jeżeli dana grupa nie tworzy cyklu z innymi, to jest ona jedynym elementem swojego scc, więc 
  -- bik_system_user_group.scc_root_id = bik_system_user_group.id
  
  select distinct scc_root_id as scc_id
  into #rcrrabg_scc
  from bik_system_user_group where is_deleted = 0;
  
  select distinct pg.scc_root_id as parent_scc_id, cg.scc_root_id as child_scc_id
  into #rcrrabg_rel
  from bik_system_group_in_group gig inner join bik_system_user_group pg on gig.group_id = pg.id
  inner join bik_system_user_group cg on gig.member_group_id = cg.id
  where gig.is_deleted = 0 and pg.is_deleted = 0 and cg.is_deleted = 0 and pg.scc_root_id <> cg.scc_root_id;
    
  truncate table bik_custom_right_role_action_branch_grant;

  -- specjalna akcja widoczności w drzewie na zakładkach - ShowInTree
  declare @show_in_tree_act_id int = (select id from bik_node_action where code = 'ShowInTree');

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##rcrrabg_trees_for_roles_0'))
    drop table ##rcrrabg_trees_for_roles_0;

  create table ##rcrrabg_trees_for_roles_0 (role_id int not null, tree_id int not null, unique (role_id, tree_id));

  /*
  declare ##rcrrabg_trees_for_roles_0 table (role_id int not null, tree_id int not null, unique (role_id, tree_id));
  */

  insert into ##rcrrabg_trees_for_roles_0 (role_id, tree_id)
  select distinct crr.id as role_id, t.id as tree_id
  from
    bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str;  

  -- select * from bik_custom_right_role_user_entry
  
  if @diag_level > 1 select * from ##rcrrabg_trees_for_roles_0;
  
  declare @right_for_group_flat table (role_id int not null, group_id int not null, tree_id int null, node_id int null,
    unique(group_id, role_id, tree_id, node_id));

  declare @groups_to_process table (group_id int not null primary key);

  declare @processed_groups table (group_id int not null primary key);

  declare @unprocessed_groups table (group_id int not null primary key);
  
  insert into @unprocessed_groups (group_id) --select id from bik_system_user_group;
  select scc_id from #rcrrabg_scc;

  insert into @groups_to_process (group_id)
  /*select id
  from bik_system_user_group
  where id not in (select member_group_id from bik_system_group_in_group);*/
  select scc_id from #rcrrabg_scc where scc_id not in (select child_scc_id from #rcrrabg_rel);
  
  select scc_id, scc_id as ancestor_scc_id
  into #rcrrabg_ancestors_flat
  from #rcrrabg_scc;
  
  while exists (select 1 from @groups_to_process)
  begin
    if @diag_level > 0
    begin
      declare @gtpstr varchar(max) = null;
      
      select @gtpstr = case when @gtpstr is null then '' else @gtpstr + ',' end + cast(group_id as varchar(20))
      from @groups_to_process;
      
      set @log_msg = 'groups to process: ' + @gtpstr; exec sp_log_msg @log_msg;   
    end;

    insert into #rcrrabg_ancestors_flat (scc_id, ancestor_scc_id)
    select distinct gtp.group_id, af.ancestor_scc_id
    from @groups_to_process gtp inner join #rcrrabg_rel r on gtp.group_id = r.child_scc_id
    inner join #rcrrabg_ancestors_flat af on r.parent_scc_id = af.scc_id;
    
    insert into @processed_groups (group_id) select group_id from @groups_to_process;

    delete from @unprocessed_groups where group_id in (select group_id from @groups_to_process);

    delete from @groups_to_process;
         
    insert into @groups_to_process (group_id)
    select group_id
    from @unprocessed_groups
    where group_id not in 
      --(select member_group_id from bik_system_group_in_group where group_id in (select group_id from @unprocessed_groups));
      (select child_scc_id from #rcrrabg_rel where parent_scc_id in (select group_id from @unprocessed_groups));
  end;

  if @diag_level > 0 and exists(select 1 from @unprocessed_groups)
  begin
    declare @ugstr varchar(max) = null;
    
    select @ugstr = case when @ugstr is null then '' else @ugstr + ',' end + cast(group_id as varchar(20))
    from @unprocessed_groups;
    
    set @log_msg = 'nothing new to process, but there are unprocessed groups: ' + @ugstr; exec sp_log_msg @log_msg;    
  end;

  -- poniższe nie wyłapuje sytuacji gdy istnieje wpis nadrzędny i podrzędny 
  -- (w sensie węzłów w tym samym drzewie, dla tej samej roli) - wrzucane są oba wpisy
  insert into @right_for_group_flat (role_id, group_id, tree_id, node_id)
  select distinct crrue.role_id, cg.id, crrue.tree_id, crrue.node_id
  from #rcrrabg_ancestors_flat af inner join bik_system_user_group ag on af.ancestor_scc_id = ag.scc_root_id
  inner join bik_custom_right_role_user_entry crrue on crrue.group_id = ag.id
  inner join bik_system_user_group cg on af.scc_id = cg.scc_root_id
  where ag.is_deleted = 0;

  if exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##rcrrabg_right_for_user_flat'))
    drop table ##rcrrabg_right_for_user_flat;

  create table ##rcrrabg_right_for_user_flat (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));
    
  /*
  declare ##rcrrabg_right_for_user_flat table (role_id int not null, user_id int not null, tree_id int null, node_id int null,
    unique(user_id, role_id, tree_id, node_id));
  */

  insert into ##rcrrabg_right_for_user_flat (user_id, role_id, tree_id, node_id)
  select distinct user_id, role_id, tree_id, node_id
  from
    (select uig.user_id, rfgf.role_id, rfgf.tree_id, rfgf.node_id
     from bik_system_user_in_group uig inner join @right_for_group_flat rfgf on uig.group_id = rfgf.group_id
	 and uig.is_deleted = 0
     union
     select user_id, role_id, tree_id, node_id
     from bik_custom_right_role_user_entry crrue where user_id is not null) x;

  if @diag_level > 1 select * from bik_custom_right_role_user_entry;

  if @diag_level > 1 select *
  from --bik_custom_right_role_user_entry 
    ##rcrrabg_right_for_user_flat crrue left join
    ##rcrrabg_trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id;      

  if @diag_level > 1 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: before semi-final insert'; exec sp_log_msg @log_msg;
  end;

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct @show_in_tree_act_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    ##rcrrabg_right_for_user_flat crrue left join
    ##rcrrabg_trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    not exists -- nie dorzucamy wpisu, jeżeli występuje dla niego wpis nadrzędny (na wyższym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          ##rcrrabg_right_for_user_flat crrue2 left join
          ##rcrrabg_trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );

  if @diag_level > 1 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: before final insert'; exec sp_log_msg @log_msg;
  end;

  insert into bik_custom_right_role_action_branch_grant (action_id, user_id, tree_id, branch_ids)
  select distinct naicrr.action_id, crrue.user_id, coalesce(crrue.tree_id, rts.tree_id) as tree_id, --crrue.node_id,
    coalesce(n.branch_ids, '') as branch_ids
  from --bik_custom_right_role_user_entry 
    ##rcrrabg_right_for_user_flat crrue inner join bik_node_action_in_custom_right_role naicrr on crrue.role_id = naicrr.role_id left join
    ##rcrrabg_trees_for_roles_0 rts on crrue.tree_id is null and rts.role_id = crrue.role_id
  left join bik_node n on n.id = crrue.node_id
  where
    naicrr.action_id <> @show_in_tree_act_id and
    not exists -- nie dorzucamy wpisu, jeżeli występuje dla niego wpis nadrzędny (na wyższym poziomie w drzewie)
    (
      select --
        1
        --crrue2.user_id, coalesce(crrue2.tree_id, rts2.tree_id) as tree_id, crrue2.node_id, n2.branch_ids
        from --bik_custom_right_role_user_entry 
          ##rcrrabg_right_for_user_flat crrue2 inner join bik_node_action_in_custom_right_role naicrr2 on crrue2.role_id = naicrr2.role_id left join
          ##rcrrabg_trees_for_roles_0 rts2 on crrue2.tree_id is null and rts2.role_id = crrue2.role_id
      left join bik_node n2 on n2.id = crrue2.node_id
      where naicrr2.action_id = naicrr.action_id and crrue2.user_id = crrue.user_id and coalesce(n.branch_ids, '') like coalesce(n2.branch_ids, '') + '_%'
        and coalesce(crrue.tree_id, rts.tree_id) = coalesce(crrue2.tree_id, rts2.tree_id)
        --and (coalesce(n.id, -1) <> coalesce(n2.id, -1))
    );
  
  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_recalculate_Custom_Right_Role_Action_Branch_Grants: end'; exec sp_log_msg @log_msg;
  end;
end;
go
exec sp_update_version '1.8.4.7', '1.8.4.8';
go