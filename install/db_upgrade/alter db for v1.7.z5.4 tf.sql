exec sp_check_version '1.7.z5.4';
go

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


if  exists (select * from sys.objects where object_id = object_id(N'[dbo].[fn_index_info]') and type in (N'fn', N'if', N'tf', N'fs', N'ft'))
drop function [dbo].[fn_index_info]
go

create function [dbo].[fn_index_info] (@table_name_patt varchar(max))
returns @tab table (table_name sysname not null, index_name sysname not null, cols varchar(max) not null, include_cols varchar(max) null, 
  idx_level tinyint not null, has_identity_col tinyint not null, has_fk_col tinyint not null)
as
begin

  declare @fk_cols table (table_name sysname not null, col_name sysname not null, unique (table_name, col_name));
  
  insert into @fk_cols
  select distinct table_name, name from dbo.vw_ww_foreignkeys where Table_Name like @table_name_patt;

  declare @cur cursor;

  set @cur = cursor for 
  select 
       tablename = t.name,
       indexname = ind.name,
       --indexid = ind.index_id,
       --columnid = ic.index_column_id,
       columnname = col.name,
       --ind.*,
       --ic.*,
       --col.*
       col.is_identity,
       ind.is_primary_key,
       ind.is_unique,
       ind.is_unique_constraint,
       ic.is_included_column
  from 
       sys.indexes ind 
  inner join 
       sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
  inner join 
       sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id 
  inner join 
       sys.tables t on ind.object_id = t.object_id 
  where 
       --ind.is_primary_key = 1
       --and ind.is_unique = 0 
       --and ind.is_unique_constraint = 0 
       -- and 
       t.is_ms_shipped = 0 
       --and t.name = 'bik_fts_stemmed_word'
       and t.name like @table_name_patt
       --and (ind.is_unique = 1 or ind.is_unique_constraint = 1)
  order by 
       t.name, ind.name, ind.index_id, ic.index_column_id 
  ;

  open @cur;

  declare @table_name sysname, @index_name sysname, @col_name sysname, @is_identity tinyint,
    @is_primary_key tinyint,
    @is_unique tinyint,
    @is_unique_constraint tinyint, @is_included_column tinyint;
  
  declare @is_fk_col tinyint, @prev_table_name sysname = '', @prev_index_name sysname = '', @idx_col_names varchar(max) = null,
    @include_cols varchar(max) = null, @idx_level tinyint = null, @has_identity_col tinyint = null,
    @has_fk_col tinyint = null;

  fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
    @is_unique, @is_unique_constraint, @is_included_column;

  while @@fetch_status = 0
  begin
    if @prev_table_name <> @table_name or @prev_index_name <> @index_name
    begin
      if @idx_col_names is not null
      begin
        --print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
        insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
        values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
      end;
            
      select @idx_col_names = null, @include_cols = null, @has_identity_col = 0, @has_fk_col = 0;
      
      set @idx_level = case when @is_primary_key <> 0 then 3
                            when @is_unique_constraint <> 0 then 2
                            when @is_unique <> 0 then 1
                            else 0 end;
    end;
    
    if @is_included_column = 0
    begin   
      set @idx_col_names = case when @idx_col_names is null then '' else @idx_col_names + ', ' end + @col_name;

      set @is_fk_col = case when exists(select 1 from @fk_cols where table_name = @table_name and col_name = @col_name) then 1 else 0 end;
      
      if @is_fk_col <> 0 set @has_fk_col = 1;
      
      if @is_identity <> 0 set @has_identity_col = 1;    
    end
    else
      set @include_cols = case when @include_cols is null then '' else @include_cols + ', ' end + @col_name;
    
    select @prev_table_name = @table_name, @prev_index_name = @index_name;

    fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
      @is_unique, @is_unique_constraint, @is_included_column;
  end;

  --if @idx_col_names <> '' print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
  insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
  values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
  
  return;
end;
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_add_fk_if_missing]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_add_fk_if_missing]
go

create procedure [dbo].[sp_add_fk_if_missing] @on_table_name sysname, @on_column_name sysname, @referenced_table_name sysname, @diag_level int = 1 as
begin
  set nocount on;

  declare @fk_name varchar(max) = (select top 1 foreignkey_name from vw_ww_foreignkeys where Table_Name = @on_table_name and Name = @on_column_name and ReferencedTableName = @referenced_table_name);

  if @fk_name is not null begin
    if @diag_level > 0 begin
	  print 'table ' + @on_table_name + ' already has FK on column ' + @on_column_name + ' referencing table ' + @referenced_table_name + ', FK name: ' + @fk_name;
	end;
    return;
  end;

  set @fk_name = 'fk_' + @on_table_name + '_' + @on_column_name;

  if len(@fk_name) > 128 or exists (select 1 from vw_ww_foreignkeys where foreignkey_name = @fk_name) begin
    set @fk_name = substring(@fk_name, 1, 128 - 37) + '_' + replace(newid(), '-', '_');
  end;

  declare @pk_column_name sysname = (select cols from dbo.fn_index_info(replace(replace(@referenced_table_name, '_', '[_]'), '%', '[%]')) 
    where idx_level = 3 and charindex(',', cols) = 0);
	
  declare @sql_txt varchar(max) = 'alter table ' + @on_table_name + ' add constraint ' + @fk_name + ' foreign key (' + @on_column_name + ') references ' +
    @referenced_table_name + ' (' + @pk_column_name + ')';

  exec(@sql_txt);
 
  if @diag_level > 0 begin
    print 'FK on table ' + @on_table_name + ' column ' + @on_column_name + ' referencing table ' + @referenced_table_name + ' column ' + @pk_column_name + ' created, FK name: ' + @fk_name;
  end;
end;
go


-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------



if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_node_name_chunk' and Name = 'node_id' and ReferencedTableName = 'bik_node')
alter table bik_node_name_chunk add constraint fk_bik_node_name_chunk_node_id foreign key (node_id) references bik_node (id);
go


if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_node_name_chunk' and Name = 'tree_id' and ReferencedTableName = 'bik_tree')
alter table bik_node_name_chunk add constraint fk_bik_node_name_chunk_tree_id foreign key (tree_id) references bik_tree (id);
go



if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_tree' and Name = 'node_kind_id' and ReferencedTableName = 'bik_node_kind')
alter table bik_tree add constraint fk_bik_tree_node_kind foreign key (node_kind_id) references bik_node_kind (id);
go



-- bik_searchable_attr_val	node_kind_id
if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_searchable_attr_val' and Name = 'node_kind_id' and ReferencedTableName = 'bik_node_kind')
alter table bik_searchable_attr_val add constraint fk_bik_searchable_attr_val_node_kind foreign key (node_kind_id) references bik_node_kind (id);
go



-- bik_searchable_attr_val	tree_id
if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_searchable_attr_val' and Name = 'tree_id' and ReferencedTableName = 'bik_tree')
alter table bik_searchable_attr_val add constraint fk_bik_searchable_attr_val_tree foreign key (tree_id) references bik_tree (id);
go




-- bik_tree_kind	branch_node_kind_id
if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_tree_kind' and Name = 'branch_node_kind_id' and ReferencedTableName = 'bik_node_kind')
alter table bik_tree_kind add constraint fk_bik_tree_kind_branch_node_kind foreign key (branch_node_kind_id) references bik_node_kind (id);
go

-- bik_tree_kind	leaf_node_kind_id
if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_tree_kind' and Name = 'leaf_node_kind_id' and ReferencedTableName = 'bik_node_kind')
alter table bik_tree_kind add constraint fk_bik_tree_kind_leaf_node_kind foreign key (leaf_node_kind_id) references bik_node_kind (id);
go



-- bik_object_in_fvs	user_id
delete from bik_object_in_fvs_change
where fvs_id in (select f.id
  from bik_object_in_fvs f left join bik_system_user s on f.user_id = s.id
  where s.id is null);
go

delete from f
--select *
from bik_object_in_fvs f left join bik_system_user s on f.user_id = s.id
where s.id is null;
go

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_object_in_fvs' and Name = 'user_id' and ReferencedTableName = 'bik_system_user')
alter table bik_object_in_fvs add constraint fk_bik_object_in_fvs_system_user foreign key (user_id) references bik_system_user (id);
go


-- bik_object_in_history	user_id

delete from f
--select * 
from bik_object_in_history f left join bik_system_user s on f.user_id = s.id
where s.id is null;
go

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_object_in_history' and Name = 'user_id' and ReferencedTableName = 'bik_system_user')
alter table bik_object_in_history add constraint fk_bik_object_in_history_system_user foreign key (user_id) references bik_system_user (id);
go


-- select * from sys.objects where name like '%fake%'

if object_id('bik_teradata_fake', 'u') is not null
  drop table bik_teradata_fake;
go

if object_id('bik_teradata_index_fake', 'u') is not null
  drop table bik_teradata_index_fake;
go


-- select * from vw_ww_foreignkeys where ForeignKey_Name = 'fk_bik_home_page_hint_node' and table_name = 'bik_object_in_history'

/*
-- to tylko dlatego, �e mi si� wcze�niej r�ka lekko omskn�a
if exists (select 1 from vw_ww_foreignkeys where ForeignKey_Name = 'fk_bik_home_page_hint_node' and table_name = 'bik_object_in_history')
  alter table bik_object_in_history drop constraint fk_bik_home_page_hint_node;
 go

select * from vw_ww_foreignkeys where table_name = 'bik_home_page_hint'

*/



delete f
--select * 
from bik_home_page_hint f left join bik_node s on f.node_id = s.id
where s.id is null;
go


if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_home_page_hint' and Name = 'node_id' and ReferencedTableName = 'bik_node')
alter table bik_home_page_hint add constraint fk_bik_home_page_hint_node foreign key (node_id) references bik_node (id);
go



-- bik_news	only_for_user_id

delete from f
--select * 
from bik_news f left join bik_system_user s on f.only_for_user_id = s.id
where f.only_for_user_id is not null and s.id is null;
go

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_news' and Name = 'only_for_user_id' and ReferencedTableName = 'bik_system_user')
alter table bik_news add constraint fk_bik_news_system_user foreign key (only_for_user_id) references bik_system_user (id);
go


-- bik_statistic_ext	grouping_node_id

delete from f
--select * 
from bik_statistic_ext f left join bik_node s on f.grouping_node_id = s.id
where f.grouping_node_id is not null and s.id is null;
go

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_statistic_ext' and Name = 'grouping_node_id' and ReferencedTableName = 'bik_node')
alter table bik_statistic_ext add constraint fk_bik_statistic_ext_node foreign key (grouping_node_id) references bik_node (id);
go




-- bik_erwin_shape	data_load_log_id

-- select * from vw_ww_foreignkeys where name = 'data_load_log_id'

-- select * from bik_erwin_shape
if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_erwin_shape' and Name = 'data_load_log_id' and ReferencedTableName = 'bik_data_load_log')
alter table bik_erwin_shape add constraint fk_bik_erwin_shape_data_load_log foreign key (data_load_log_id) references bik_data_load_log (id);
go


-- bik_erwin_table	data_load_log_id

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_erwin_table' and Name = 'data_load_log_id' and ReferencedTableName = 'bik_data_load_log')
alter table bik_erwin_table add constraint fk_bik_erwin_table_data_load_log foreign key (data_load_log_id) references bik_data_load_log (id);
go



-- bik_erwin_table_column	data_load_log_id

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_erwin_table_column' and Name = 'data_load_log_id' and ReferencedTableName = 'bik_data_load_log')
alter table bik_erwin_table_column add constraint fk_bik_erwin_table_column_data_load_log foreign key (data_load_log_id) references bik_data_load_log (id);
go



-- bik_lisa_dict_column	column_id
-- select * from bik_lisa_dict_column


-- bik_lisa_dict_column_lookup	column_id

-- select * from bik_lisa_dict_column_lookup

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_lisa_dict_column_lookup' and Name = 'column_id' and ReferencedTableName = 'bik_lisa_dict_column')
alter table bik_lisa_dict_column_lookup add constraint fk_bik_lisa_dict_column_lookup_lisa_dict_column foreign key (column_id) references bik_lisa_dict_column (id);
go



-- bik_sapbo_object_table	object_node_id

-- select * from bik_sapbo_object_table f left join bik_node n on f.object_node_id = n.id where n.id is null and f.object_node_id is not null

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_sapbo_object_table' and Name = 'object_node_id' and ReferencedTableName = 'bik_node')
alter table bik_sapbo_object_table add constraint fk_bik_sapbo_object_table_node foreign key (object_node_id) references bik_node (id);
go


-- bik_sapbo_object_table	table_node_id

-- select * from bik_sapbo_object_table f left join bik_node n on f.table_node_id = n.id where n.id is null and f.table_node_id is not null

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_sapbo_object_table' and Name = 'table_node_id' and ReferencedTableName = 'bik_node')
alter table bik_sapbo_object_table add constraint fk_bik_sapbo_object_table_table_node foreign key (table_node_id) references bik_node (id);
go



if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_sapbo_object_table' and Name = 'table_node_id' and ReferencedTableName = 'bik_node')
alter table bik_sapbo_object_table add constraint fk_bik_sapbo_object_table_table_node foreign key (table_node_id) references bik_node (id);
go

-- select * from bik_confluence_missing_joined_node

-- bik_confluence_missing_joined_node	conf_node_id
-- bik_confluence_missing_joined_node	dst_node_id
-- bik_confluence_missing_joined_node	opt_dst_subnode_parent_id

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_confluence_missing_joined_node' and Name = 'conf_node_id' and ReferencedTableName = 'bik_node')
alter table bik_confluence_missing_joined_node add constraint fk_bik_confluence_missing_joined_node_conf_node foreign key (conf_node_id) references bik_node (id);
go

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_confluence_missing_joined_node' and Name = 'dst_node_id' and ReferencedTableName = 'bik_node')
alter table bik_confluence_missing_joined_node add constraint fk_bik_confluence_missing_joined_node_dst_node foreign key (dst_node_id) references bik_node (id);
go

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_confluence_missing_joined_node' and Name = 'opt_dst_subnode_parent_id' and ReferencedTableName = 'bik_node')
alter table bik_confluence_missing_joined_node add constraint fk_bik_confluence_missing_joined_node_opt_dst_subnode_parent foreign key (opt_dst_subnode_parent_id) references bik_node (id);
go

-- bik_confluence_missing_joined_node	dst_tree_id

if not exists(select 1 from vw_ww_foreignkeys where Table_Name = 'bik_confluence_missing_joined_node' and Name = 'dst_tree_id' and ReferencedTableName = 'bik_tree')
alter table bik_confluence_missing_joined_node add constraint fk_bik_confluence_missing_joined_node_tree foreign key (dst_tree_id) references bik_tree (id);
go


-------------------------------------------------------
-------------------------------------------------------

declare @are_fts_tables_empty int = case when exists (select 1 from bik_fts_word) then 0 else 1 end;

if @are_fts_tables_empty <> 0 begin
exec('
if object_id(''bik_fts_word_stemming'', ''u'') is not null drop table bik_fts_word_stemming;

if object_id(''bik_fts_word_in_obj'', ''u'') is not null drop table bik_fts_word_in_obj;

if object_id(''bik_fts_word_in_obj_attr'', ''u'') is not null drop table bik_fts_word_in_obj_attr;

if object_id(''bik_fts_stemmed_word'', ''u'') is not null drop table bik_fts_stemmed_word;

if object_id(''bik_fts_attr'', ''u'') is not null drop table bik_fts_attr;

if object_id(''bik_fts_word'', ''u'') is not null drop table bik_fts_word;

if object_id(''bik_fts_processed_obj'', ''u'') is not null drop table bik_fts_processed_obj;
');
end;

--select 'drop table ' + name + ';' from sys.objects where name like 'bik_fts_%' and type = 'U'


if object_id('[bik_fts_word_stemming]', 'u') is null
CREATE TABLE [dbo].[bik_fts_word_stemming](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[word_id] [int] NOT NULL,
	[stemmed_word_id] [int] NOT NULL,
	[origin_flag] [int] NOT NULL,
);
GO



if object_id('[bik_fts_word_in_obj_attr]', 'u') is null
CREATE TABLE [dbo].[bik_fts_word_in_obj_attr](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[obj_id] [int] NOT NULL,
	[attr_id] [int] NOT NULL,
	[word_id] [int] NOT NULL,
	[original_cnt] [int] NOT NULL,
	[synonym_cnt] [int] NOT NULL,
	[dictstemmed_cnt] [int] NOT NULL,
	[algostemmed_cnt] [int] NOT NULL,
	[accentfree_cnt] [int] NOT NULL,
    UNIQUE ([obj_id], [attr_id], [word_id])
)
GO


if object_id('[bik_fts_word_in_obj]', 'u') is null
CREATE TABLE [dbo].[bik_fts_word_in_obj](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[obj_id] [int] NOT NULL,
	[word_id] [int] NOT NULL,
	[origin_flag] [int] NOT NULL,
	[occurence_cnt] [int] NOT NULL,
	[attr_cnt] [int] NOT NULL,
	[max_weight] [decimal](9, 8) NOT NULL,
	UNIQUE ([obj_id], [word_id])
)
GO



if object_id('[bik_fts_word]', 'u') is null
CREATE TABLE [dbo].[bik_fts_word](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[word] [varchar](800) NOT NULL,
	UNIQUE ([word])
)
GO


if object_id('[bik_fts_stemmed_word]', 'u') is null
CREATE TABLE [dbo].[bik_fts_stemmed_word](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[word] [varchar](800) NOT NULL,
	[weight] [decimal](9, 8) NOT NULL DEFAULT ((1.0))
)
GO


if object_id('[bik_fts_processed_obj]', 'u') is null
CREATE TABLE [dbo].[bik_fts_processed_obj](
	[id] [int] NOT NULL primary key,
	[finished] [tinyint] NOT NULL CONSTRAINT [DF_bik_fts_processed_obj_finished]  DEFAULT ((0))
)
GO


if object_id('[bik_fts_attr]', 'u') is null
CREATE TABLE [dbo].[bik_fts_attr](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[name] [varchar](800) NOT NULL,
	[weight] [decimal](9, 8) NOT NULL,
	UNIQUE ([name])
)
GO


exec sp_add_fk_if_missing 'bik_fts_processed_obj', 'id', 'bik_node';

-- bik_fts_word_in_obj_attr	attr_id
exec sp_add_fk_if_missing 'bik_fts_word_in_obj_attr', 'attr_id', 'bik_fts_attr';

-- bik_fts_word_in_obj_attr	obj_id
exec sp_add_fk_if_missing 'bik_fts_word_in_obj_attr', 'obj_id', 'bik_fts_processed_obj';

-- bik_fts_word_in_obj_attr	word_id
exec sp_add_fk_if_missing 'bik_fts_word_in_obj_attr', 'word_id', 'bik_fts_word';

-- bik_fts_word_stemming	stemmed_word_id
exec sp_add_fk_if_missing 'bik_fts_word_stemming', 'stemmed_word_id', 'bik_fts_stemmed_word';

-- bik_fts_word_stemming	word_id
exec sp_add_fk_if_missing 'bik_fts_word_stemming', 'word_id', 'bik_fts_word';


exec sp_add_fk_if_missing 'bik_fts_word_in_obj', 'word_id', 'bik_fts_word';

exec sp_add_fk_if_missing 'bik_fts_word_in_obj', 'obj_id', 'bik_fts_processed_obj';
go

-- TF - dodaje moje FK
exec sp_add_fk_if_missing 'bik_dqm_log', 'multi_source_id', 'bik_dqm_test_multi';
exec sp_add_fk_if_missing 'bik_dqm_schedule', 'multi_source_id', 'bik_dqm_test_multi';
exec sp_add_fk_if_missing 'bik_dqm_test', 'db_server_id', 'bik_db_server';
exec sp_add_fk_if_missing 'bik_dqm_test', 'error_db_server_id', 'bik_db_server';
exec sp_add_fk_if_missing 'bik_dqm_test_multi', 'db_server_id', 'bik_db_server';
go
exec sp_add_fk_if_missing 'aaa_ids_for_report', 'object_node_id', 'bik_node';
exec sp_add_fk_if_missing 'aaa_ids_for_report', 'query_node_id', 'bik_node';
exec sp_add_fk_if_missing 'aaa_query', 'report_node_id', 'bik_node';
go


if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
begin
		update bik_user set login_name_for_ad = 'polkomtel\' + login_name_for_ad where login_name_for_ad is null or login_name_for_ad not like 'polkomtel\%';
		update bik_system_user set sso_domain = 'polkomtel', sso_login = login_name where sso_domain is null;
		update bik_system_user set login_name = 'polkomtel\' + login_name where login_name not like 'polkomtel\%';
		update bik_active_directory set domain = 'polkomtel' where domain is null;
		update bik_app_prop set val = 'true' where name = 'isMultiDomainMode';

		if not exists(select * from bik_admin where param = 'ad.domains')
		begin
			insert into bik_admin(param, value) values ('ad.domains', 'polkomtel,polsatc')
		end
		else
		begin
			update bik_admin set value = 'polkomtel,polsatc' where param = 'ad.domains'
		end

		update bik_system_user
		set user_id = case when exists(select * from bik_system_user where user_id = us.id) then null else us.id end
		from bik_system_user as usy
		inner join bik_user as us on usy.login_name = us.login_name_for_ad
		inner join bik_node as bn on bn.id = us.node_id
		where usy.user_id is null and bn.is_deleted = 0

		declare @user int = (select id from bik_node_kind where code = 'User')
		declare @company int = (select id from bik_attr_def where name = 'ad_company' and is_built_in = 1)
		declare @mail int = (select id from bik_attr_def where name = 'ad_mail' and is_built_in = 1)
		declare @www int = (select id from bik_attr_def where name = 'Strona wwww' and is_built_in = 1)

		update bik_attr_system set is_visible = 1 where node_kind_id = @user and attr_id = @company
		update bik_attr_system set is_visible = 1 where node_kind_id = @user and attr_id = @mail
		update bik_attr_system set is_visible = 0 where node_kind_id = @user and attr_id = @www

		if not exists(select * from bik_translation where code = 'Strona wwww' and lang = 'pl')
		insert into bik_translation(code, txt, lang, kind)
		values ('Strona wwww', 'Strona www', 'pl', 'adef')

end else if (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' -- CP
begin
		update bik_user set login_name_for_ad = 'polsatc\' + login_name_for_ad where login_name_for_ad is null or login_name_for_ad not like 'polsatc\%';
		update bik_system_user set sso_domain = 'polsatc', sso_login = login_name where sso_domain is null;
		update bik_system_user set login_name = 'polsatc\' + login_name where login_name not like 'polsatc\%';
		update bik_active_directory set domain = 'polsatc' where domain is null;
		update bik_app_prop set val = 'true' where name = 'isMultiDomainMode';

		if not exists(select * from bik_admin where param = 'ad.domains')
		begin
			insert into bik_admin(param, value) values ('ad.domains', 'polkomtel,polsatc')
		end
		else
		begin
			update bik_admin set value = 'polkomtel,polsatc' where param = 'ad.domains'
		end

end
go

exec sp_update_version '1.7.z5.4', '1.7.z5.5';
go
