﻿exec sp_check_version '1.8.0.18';
go


if not exists (select 1 from sys.columns where object_id=object_id(N'bik_attribute') and name='is_required')
alter table bik_attribute add is_required int default 0;
go


if not exists (select 1 from sys.columns where object_id=object_id(N'bik_attribute') and name='is_visible')
alter table bik_attribute add is_visible int default 1;
go


update bik_attribute set is_required=0,is_visible=1;

exec sp_update_version '1.8.0.18', '1.8.0.19';
go
