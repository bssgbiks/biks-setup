﻿exec sp_check_version '1.8.4.10';
go

alter table bik_node_kind alter column tree_kind varchar(255) NULL
go

if object_id('sp_build_metamodel') is not null
drop procedure sp_build_metamodel
go

create procedure sp_build_metamodel(@deleteOldTree int)
as
begin
    truncate table amg_node
    truncate table amg_attribute

    declare @confTreeCode varchar(max)=(select val from bik_app_prop where name='metadataTree')
    declare @treeId int = (select id from bik_tree where code=@confTreeCode)
    declare @root varchar(max)

    set @root = 'Definicje atrybutów'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select name, 'metaBiksAttributesCatalog', null, @root+'|'+name+'|', @root+'|', 0
    from bik_attr_category where is_deleted=0 and is_built_in=0 and name not like 'metaBiks%'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select adef.name, 'metaBiksAttributeDef', null, @root+'|'+bac.name+'|'+adef.name+'|', @root+'|'+bac.name+'|', 0
    from bik_attr_def adef join bik_attr_category bac on adef.attr_category_id=bac.id
    where adef.is_deleted=0 and adef.is_built_in=0 and bac.is_deleted=0 and bac.is_built_in=0 and bac.name not like 'metaBiks%'

    --Meta atrybuty do definicji atrybutow
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista wartości', adef.value_opt from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy', adef.hint from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', adef.type_attr from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Używaj w systemie zarządzania regułami', case when adef.in_drools=1 then 'Tak' else 'Nie' end from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wyświetlaj jako liczbę', case when adef.display_as_number=1 then 'Tak' else 'Nie' end from bik_attr_def adef join amg_node a on a.name=adef.name
    where a.node_kind_code='metaBiksAttributeDef' and adef.is_built_in=0 and adef.is_deleted=0

    --Typ obiektu
    set @root = 'Definicje obiektów'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    declare @ids table(id int)
    insert into @ids
    select distinct branch_node_kind_id from bik_tree_kind where is_deleted=0 union
    select distinct leaf_node_kind_id from bik_tree_kind where is_deleted=0 union
    select distinct dst_node_kind_id from bik_node_kind_4_tree_kind where (src_node_kind_id is null or relation_type = 'Dziecko') and (is_deleted=0) union
    select distinct src_node_kind_id from bik_node_kind_4_tree_kind where (dst_node_kind_id is null or relation_type = 'Dziecko') and (is_deleted=0) union
    select distinct id from bik_node_kind where tree_kind is null or ltrim(tree_kind)=''
    delete from @ids where id in (select nk.id from bik_node_kind nk join @ids ids on ids.id=nk.id and nk.code like 'metaBiks%')

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select code, 'metaBiksNodeKind', null, @root+'|'+code+'|', @root+'|', 0 from bik_node_kind where id in (select id from @ids) and is_deleted=0 and code not like 'metaBiks%'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select code, 'metaBiksBuiltInNodeKind', null, @root+'|'+code+'|', @root+'|', 0 from bik_node_kind where id not in (select id from @ids) and is_deleted=0 and code not like 'metaBiks%'
    ---------
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Atrybuty dzieci w tabeli', nk.children_attr_names from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Dzieci w tabeli', nk.children_kinds from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Ikona', nk.icon_name from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu obiektu', nk.caption from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Rejestr', case when nk.is_registry=1 then 'Tak' else 'Nie' end from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla autora', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':author' and lang='pl'
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla innych', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':user' and lang='pl'
    where a.node_kind_code='metaBiksNodeKind' and nk.is_deleted=0 and nk.id in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'
    ------------
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu obiektu', nk.caption from bik_node_kind nk join amg_node a on a.name=nk.code
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla autora', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':author' and lang='pl'
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Treść pomocy dla innych', coalesce(h.text_help, '') from bik_node_kind nk join amg_node a on a.name=nk.code left join bik_help h on help_key=nk.code+':user' and lang='pl'
    where a.node_kind_code='metaBiksBuiltInNodeKind' and nk.is_deleted=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%'

    declare @attr table(node_kind_code varchar(max), attr_name varchar(max), category varchar(max), default_value varchar(max), is_required int, type_attr varchar(max), value_opt varchar(max), is_empty int, is_public int)
    insert into @attr(node_kind_code, attr_name, category, default_value, is_required, type_attr, value_opt, is_empty, is_public)
    select nk.code, adef.name, c.name, attr.default_value, attr.is_required, attr.type_attr, attr.value_opt, attr.is_empty, attr.is_public
    from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
    join bik_attr_def adef on adef.id=attr.attr_def_id
    join bik_attr_category c on adef.attr_category_id=c.id
    where attr.is_deleted=0 and adef.is_built_in=0 and nk.id in (select id from @ids) and nk.code not like 'metaBiks%'

    declare @attr_builtin table(node_kind_code varchar(max), attr_name varchar(max), category varchar(max), default_value varchar(max), is_required int, type_attr varchar(max), value_opt varchar(max), is_empty int, is_public int)
    insert into @attr(node_kind_code, attr_name, category, default_value, is_required, type_attr, value_opt, is_empty, is_public)
    select nk.code, adef.name, c.name, attr.default_value, attr.is_required, attr.type_attr, attr.value_opt, attr.is_empty, attr.is_public
    from bik_attribute attr join bik_node_kind nk on nk.id=attr.node_kind_id
    join bik_attr_def adef on adef.id=attr.attr_def_id
    join bik_attr_category c on adef.attr_category_id=c.id
    where attr.is_deleted=0 and adef.is_built_in=0 and nk.id not in (select id from @ids) and nk.is_deleted=0 and nk.code not like 'metaBiks%' and adef.name not like 'metaBiks%'

    --Atrybut dla typu obiektu dynamicznego
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select attr.attr_name, 'metaBiksAttribute4NodeKind', null, @root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|', @root+'|'+attr.node_kind_code+'|', 0
    from @attr attr

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista wartości', attr.value_opt from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', attr.type_attr from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Może być pusty', case when attr.is_empty=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pokaż na formatce nowego obiektu', case when attr.is_required=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Publiczny', case when attr.is_public=1 then 'Tak' else 'Nie' end from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wartość domyślna', attr.default_value from amg_node a join @attr attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    --Atrybut dla typu obiektu wbudowanego
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select attr.attr_name, 'metaBiksAttribute4NodeKind', null, @root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|', @root+'|'+attr.node_kind_code+'|', 0
    from @attr_builtin attr

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Lista wartości', attr.value_opt from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ atrybutu', attr.type_attr from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Może być pusty', case when attr.is_empty=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pokaż na formatce nowego obiektu', case when attr.is_required=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Publiczny', case when attr.is_public=1 then 'Tak' else 'Nie' end from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wartość domyślna', attr.default_value from amg_node a join @attr_builtin attr on a.obj_id=@root+'|'+attr.node_kind_code+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind'

    --Typ drzew
    set @root='Definicje drzew'
    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    values(@root, 'metaBiksDefinitionGroup', null, @root+'|', null, 0)

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select tk.code, 'metaBiksTreeKind', null, @root+'|'+tk.code+'|', @root+'|', 0
    from bik_tree_kind tk where tk.code not like 'metaBiks%' and tk.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Nazwa typu drzewa', tk.caption from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Pozwolić dowiązać', case when tk.allow_linking=1 then 'Tak' else 'Nie' end from amg_node a join bik_tree_kind tk on a.obj_id=@root+'|'+tk.code+'|'
    where a.node_kind_code='metaBiksTreeKind'

    declare @nkInTk table(tk_code varchar(max), src_code varchar(max), dst_code varchar(max), relation_type varchar(max))
    insert into @nkInTk(tk_code, src_code, dst_code, relation_type)
    select tk.code, snk.code, dnk.code, relation_type from bik_node_kind_4_tree_kind nk4tk
    join bik_tree_kind tk on tk.id=nk4tk.tree_kind_id
    left join bik_node_kind snk on snk.id=nk4tk.src_node_kind_id
    left join bik_node_kind dnk on dnk.id=nk4tk.dst_node_kind_id
    where nk4tk.is_deleted=0 and tk.code not like 'metaBiks%' and tk.is_deleted=0 and (snk.is_deleted is null or snk.is_deleted=0) and (dnk.is_deleted is null or dnk.is_deleted=0)
 	--select * from @nkInTk where tk_code='MM_Organizacja'

    declare @nk4tk table(tk_code varchar(max), nk_code varchar(max), type_in_tree varchar(max) default '')
    insert into @nk4tk(tk_code, nk_code)
    select tk_code, src_code as nk_code from @nkInTk where (src_code is not null and relation_type is null) or relation_type='Dziecko' union
    select tk_code, dst_code as nk_code from @nkInTk where (dst_code is not null and relation_type is null) or relation_type='Dziecko' union
    select tk_code, src_code as nk_code from @nkInTk where relation_type is not null and relation_type <> 'Dziecko'
	
	--select * from @nk4tk where tk_code='MM_Organizacja'

    update t1 set t1.type_in_tree='Główna gałąź+' from @nk4tk t1 where t1.nk_code in (select t2.dst_code from @nkInTk t2 where t2.tk_code=t1.tk_code and t2.src_code is null and t2.dst_code is not null and t2.relation_type is null)
    update t1 set t1.type_in_tree=t1.type_in_tree+'Gałąź+' from @nk4tk t1 where t1.nk_code in (select t2.src_code from @nkInTk t2 where t2.tk_code=t1.tk_code and t2.src_code is not null and t2.dst_code is not null and t2.relation_type='Dziecko')
    update t1 set t1.type_in_tree=t1.type_in_tree+'Liść+' from @nk4tk t1 where t1.nk_code in (select t2.src_code from @nkInTk t2 where t2.src_code is not null and t2.dst_code is null and t2.relation_type is null)
    update @nk4tk set type_in_tree=substring(type_in_tree, 0, len(type_in_tree)) where type_in_tree like '%+'

	--select * from @nk4tk where tk_code='MM_Organizacja'

    insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
    select nk4tk.nk_code, 'metaBiksNodeKind4TreeKind', null, @root+'|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|', @root+'|'+nk4tk.tk_code+'|', 0
    from @nk4tk as nk4tk

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Typ w drzewie', nk4tk.type_in_tree from @nk4tk nk4tk join amg_node a on a.obj_id= @root + '|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|'
    where a.node_kind_code='metaBiksNodeKind4TreeKind'

    declare @relation table(obj_id varchar(max), value varchar(max))
    insert into @relation(obj_id, value)
    select @root + '|'+nkInTk.tk_code+'|'+nkInTk.src_code+'|'+nkInTk.dst_code+'|', nkInTk.relation_type from @nkInTk nkInTk  
    where nkInTk.relation_type is not null

    insert into amg_attribute(obj_id, name, value)
    select obj_id, 'metaBiks.Typ relacji', stuff((select '+' + r2.value from @relation r2 where r1.obj_id = r2.obj_id for xml path ('')),1,1,'') from @relation r1 group by obj_id

	insert into amg_node(name, node_kind_code, descr, obj_id, parent_obj_id, visual_order)
	select x.str, 'metaBiksNodeKindInRelation', null, a.obj_id, substring(a.obj_id, 1, len(a.obj_id)-charindex('|',reverse(a.obj_id),2)+1), 0
    from amg_attribute as a cross apply fn_split_by_sep(a.obj_id, '|', 7) as x
	where a.name='metaBiks.Typ relacji' and x.idx=4

	--select 'przed build'
    exec sp_generic_insert_into_bik_node_ex @confTreeCode, null, @deleteOldTree;

    --Atrybuty Wzorzec
    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+attr.attr_name from amg_node a join @attr attr on a.obj_id='Definicje obiektów|'+attr.node_kind_code+'|'+attr.attr_name+'|' join bik_node bn on bn.obj_id='Definicje atrybutów|'+attr.category+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+attr.attr_name from amg_node a join @attr_builtin attr on a.obj_id='Definicje obiektów|'+attr.node_kind_code+'|'+attr.attr_name+'|' join bik_node bn on bn.obj_id='Definicje atrybutów|'+attr.category+'|'+attr.attr_name+'|'
    where a.node_kind_code='metaBiksAttribute4NodeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+nk_code from amg_node a join @nk4tk nk4tk on a.obj_id='Definicje drzew|'+nk4tk.tk_code+'|'+nk4tk.nk_code+'|' join bik_node bn on bn.obj_id='Definicje obiektów|'+nk4tk.nk_code+'|'
    where a.node_kind_code='metaBiksNodeKind4TreeKind' and bn.tree_id=@treeId and bn.is_deleted=0

    insert into amg_attribute(obj_id, name, value)
    select a.obj_id, 'metaBiks.Wzorzec', cast(bn.id as varchar(100))+'_'+@confTreeCode+','+x.str 
	from amg_attribute a cross apply dbo.fn_split_by_sep(a.obj_id, '|', 7) as x join bik_node bn on bn.obj_id='Definicje obiektów|'+x.str+'|'
    where a.name='metaBiks.Typ relacji' and bn.tree_id=@treeId and bn.is_deleted=0 and x.idx=4
    --
    merge bik_attribute_linked al
    using (
    select bn.id as node_id, attr.id as attr_id, a.value from bik_node bn join amg_attribute a on bn.obj_id=a.obj_id
    join bik_tree bt on bn.tree_id=bt.id
    join bik_attr_def adef on adef.name=a.name
    join bik_attribute attr on attr.node_kind_id=bn.node_kind_id and attr.attr_def_id=adef.id
    where bt.code=@confTreeCode
    ) x on x.node_id=al.node_id and x.attr_id=al.attribute_id
    when matched then
    update set al.value=x.value, al.is_deleted=0
    when not matched by target then
    insert (attribute_id, value, node_id, is_deleted) values (x.attr_id, x.value, x.node_id, 0)
    when not matched by source and al.node_id in (select bn.id from bik_node as bn inner join bik_tree as bt on bt.id = bn.tree_id and bt.code = @confTreeCode) then
    update set al.is_deleted=1
    ;
end
go

exec sp_update_version '1.8.4.10', '1.8.4.11';
go