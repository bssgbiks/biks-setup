exec sp_check_version '1.7.z6.3';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if not exists (select * from bik_jdbc_source where name = 'SAP HANA')
begin
	insert into bik_jdbc_source(name, driver_name, jdbc_url_template)
	values ('SAP HANA', 'com.sap.db.jdbc.Driver', 'jdbc:sap://HOST:PORT[/?<options>]')
end;

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_filter_users_group]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_filter_users_group]
go

create procedure [dbo].[sp_filter_users_group] (@txt varchar(max)) as
begin
	set nocount on;
	
	create table #res (id varchar(900) not null, unique(id));
	create table #tmp (id varchar(900) not null, unique(id));
	create table #candidate (id varchar(900) not null, unique(id));
	
	insert into #res(id)
	select x.branch_ids
	from bik_system_user_group as gr
	cross apply dbo.fn_get_system_user_group_paths(gr.id) x
	where gr.name like '%' + @txt + '%' and gr.is_deleted = 0
	
	insert into #candidate
	select id from (
	select distinct case when charindex('|', res.id) = 0 then null else substring(res.id, 1, dbo.fn_last_charindex('|', res.id, null)-1) end as id
	from #res as res) x
	where x.id is not null and x.id <> ''
	
	while exists(select * from #candidate)
	begin
		insert into #tmp(id)
		select id 
		from #candidate as can
		
		delete from #candidate
	
		insert into #res(id)
		select tmp.id 
		from #tmp as tmp
		left join #res as res on res.id = tmp.id
		where res.id is null and tmp.id is not null
		
		insert into #candidate(id)
		select id from (
			select distinct case when charindex('|', tmp.id) = 0 then null else substring(tmp.id, 1, dbo.fn_last_charindex('|', tmp.id, null)-1) end as id
			from #tmp as tmp
		) x 
		where x.id is not null and x.id <> ''
		
		delete from #tmp
	end;
	
	select x.id as obj_id, x.parent_id, x.path_id as id, gr.domain, gr.name, case when exists(select * from bik_system_group_in_group where group_id = gr.id and is_deleted = 0) then 0 else 1 end as has_no_children
	from (
		select res.id, case when charindex('|', res.id) = 0 then null else substring(res.id, 1, dbo.fn_last_charindex('|', res.id, null)-1) end as parent_id, 
		case when charindex('|', res.id) = 0 then res.id else substring(res.id, dbo.fn_last_charindex('|', res.id, null) + 1, len(res.id) - dbo.fn_last_charindex('|', res.id, null)) end as path_id
		from #res as res
	) x left join bik_system_user_group as gr on gr.id = x.path_id

end;
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('[bik_jdbc]') and name = 'query_joined')
begin
	alter table bik_jdbc add query_joined varchar(max) null;
end;
go

alter table bik_jdbc alter column query varchar(max) null;
go

if object_id('amg_attribute', 'u') is null
begin
	create table amg_attribute (
		id int not null identity(1,1) primary key,
		obj_id varchar(900) not null,
		name varchar(900) not null,
		value varchar(max) null
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[amg_attribute]') and name = N'uq_amg_attribute_obj_id_name')
begin
	create unique index uq_amg_attribute_obj_id_name on amg_attribute (obj_id, name);
end;
go

if object_id('bik_attr_system_linked', 'u') is null
begin
	create table bik_attr_system_linked (
		id int not null identity(1,1) primary key,
		attr_system_id int not null references bik_attr_system(id),
		node_id int not null references bik_node(id),
		value varchar(max) null,
		is_deleted int not null default(0)
	);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_attr_system_linked]') and name = N'idx_bik_attr_system_linked_a_n')
begin
	create unique index idx_bik_attr_system_linked_a_n on bik_attr_system_linked (attr_system_id, node_id);
end;
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z6.3', '1.7.z6.4';
go

 