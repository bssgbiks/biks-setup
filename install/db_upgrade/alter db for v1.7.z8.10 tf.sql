﻿exec sp_check_version '1.7.z8.10';
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_s_a_m_account_name_domain')
begin
	drop index [idx_bik_active_directory_s_a_m_account_name_domain] ON [dbo].[bik_active_directory];
end;
go

if exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_domain_s_a_m_account_name')
begin
	drop index [idx_bik_active_directory_domain_s_a_m_account_name] ON [dbo].[bik_active_directory];
end;
go

alter table bik_active_directory alter column domain varchar(250) null;
go

alter table bik_active_directory alter column url varchar(400) null;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_s_a_m_account_name_domain_url')
begin
	create unique index idx_bik_active_directory_s_a_m_account_name_domain_url on bik_active_directory (s_a_m_account_name, domain, url);
end;
go

if not exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_active_directory]') and name = N'idx_bik_active_directory_domain_s_a_m_account_name_url')
begin
	create unique index idx_bik_active_directory_domain_s_a_m_account_name_url on bik_active_directory (domain, s_a_m_account_name, url);
end;
go

exec sp_update_version '1.7.z8.10', '1.7.z8.11';
go