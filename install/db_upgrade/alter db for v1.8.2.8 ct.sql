﻿exec sp_check_version '1.8.2.8';
go

if not exists (select 1 from bik_node_kind where code = 'metaBiksBuiltInNodeKind')
insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
values ('metaBiksBuiltInNodeKind', 'Definicja wbudowanego typu obiektu ', 'key', 'metaBIKS', 1)
else update bik_node_kind set caption='Definicja wbudowanego typu obiektu', icon_name='key', tree_kind='metaBIKS', is_folder=1 where code='metaBiksBuiltInNodeKind' 

exec sp_add_attr_2_node_kind 'metaBiksBuiltInNodeKind', 'metaBIKS.Nazwa typu obiektu', null, 1, null, null

exec sp_update_version '1.8.2.8', '1.8.2.9';
go
 
 