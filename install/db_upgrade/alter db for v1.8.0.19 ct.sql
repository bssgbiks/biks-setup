﻿exec sp_check_version '1.8.0.19';
go

-------------------------------------------
if (object_id('bik_node_history') is null)
--szluzy do indeksowania i sledzenia zmian bik_node
create table bik_node_history (
	--id int identity(1,1),
	node_id int,
	name varchar(max) default null,
	descr varchar(max) default null,
	change_source varchar(max),
	modified_date datetime
)
go
--drop table bik_node_history

if (object_id('trg_bik_node_history') is not null)
drop trigger trg_bik_node_history
go

create trigger trg_bik_node_history on bik_node 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @node_status table (
		node_id int,
		old_is_deleted int,
		new_is_deleted int
	)
	insert into @node_status(node_id, old_is_deleted, new_is_deleted)
	select i.id, d.is_deleted, i.is_deleted
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id not in (select node_id from @node_status)
	
	--insert przez set is_deleted=0 a byl is_deleted=1
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id in (select node_id from @node_status where old_is_deleted = 1 and new_is_deleted = 0)

	insert into bik_node_history(node_id, name, descr, change_source, modified_date) 
	select i.id, i.name, i.descr, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select node_id from @node_status where old_is_deleted = 0 and new_is_deleted = 0)
	and (coalesce(i.name, '') <> coalesce(d.name, '') or coalesce(i.descr, '') <> coalesce(d.descr, ''))

	insert into bik_node_history(node_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select node_id from @node_status)

	insert into bik_node_history(node_id, change_source, modified_date) 
	select node_id, @change_source, @modified_date from @node_status where old_is_deleted = 0 and new_is_deleted = 1
end
go
-------------------------------------------
if (object_id('bik_attr_system_linked_history') is null)
create table bik_attr_system_linked_history (
	--id int identity(1,1),
	attr_system_linked_id int,
	value varchar(max) default null,
	change_source varchar(max),
	modified_date datetime
)
go

if (object_id('trg_bik_attr_system_linked_history') is not null)
drop trigger trg_bik_attr_system_linked_history
go

create trigger trg_bik_attr_system_linked_history on bik_attr_system_linked 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @attr_status table (
		attr_system_linked_id int,
		old_is_deleted int,
		new_is_deleted int
	)
	insert into @attr_status(attr_system_linked_id, old_is_deleted, new_is_deleted)
	select i.id, d.is_deleted, i.is_deleted
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_attr_system_linked_history(attr_system_linked_id, value, change_source, modified_date)
	select id, value, @change_source, @modified_date
	from inserted where id not in (select attr_system_linked_id from @attr_status)
	
	--insert przez set is_deleted=0 a byl is_deleted=1
	insert into bik_attr_system_linked_history(attr_system_linked_id, value, change_source, modified_date)
	select id, value, @change_source, @modified_date
	from inserted where id in (select attr_system_linked_id from @attr_status where old_is_deleted = 1 and new_is_deleted = 0)

	insert into bik_attr_system_linked_history(attr_system_linked_id, value, change_source, modified_date) 
	select i.id, i.value, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select attr_system_linked_id from @attr_status where old_is_deleted = 0 and new_is_deleted = 0)
	and (coalesce(i.value, '') <> coalesce(d.value, ''))

	insert into bik_attr_system_linked_history(attr_system_linked_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select attr_system_linked_id from @attr_status)

	insert into bik_attr_system_linked_history(attr_system_linked_id, change_source, modified_date) 
	select attr_system_linked_id, @change_source, @modified_date from @attr_status where old_is_deleted = 0 and new_is_deleted = 1
end
go

-------------------------------------------
if (object_id('bik_attribute_linked_history') is null)
create table bik_attribute_linked_history (
	--id int identity(1,1),
	attribute_linked_id int,
	value varchar(max) default null,
	change_source varchar(max),
	modified_date datetime
)
go

if (object_id('trg_bik_attribute_linked_history') is not null)
drop trigger trg_bik_attribute_linked_history
go

create trigger trg_bik_attribute_linked_history on bik_attribute_linked 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @attr_status table (
		attribute_linked_id int,
		old_is_deleted int,
		new_is_deleted int
	)
	insert into @attr_status(attribute_linked_id, old_is_deleted, new_is_deleted)
	select i.id, d.is_deleted, i.is_deleted
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_attribute_linked_history(attribute_linked_id, value, change_source, modified_date)
	select id, value, @change_source, @modified_date
	from inserted where id not in (select attribute_linked_id from @attr_status)
	
	--insert przez set is_deleted=0 a byl is_deleted=1
	insert into bik_attribute_linked_history(attribute_linked_id, value, change_source, modified_date)
	select id, value, @change_source, @modified_date
	from inserted where id in (select attribute_linked_id from @attr_status where old_is_deleted = 1 and new_is_deleted = 0)

	insert into bik_attribute_linked_history(attribute_linked_id, value, change_source, modified_date) 
	select i.id, i.value, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select attribute_linked_id from @attr_status where old_is_deleted = 0 and new_is_deleted = 0)
	and (coalesce(i.value, '') <> coalesce(d.value, ''))

	insert into bik_attribute_linked_history(attribute_linked_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select attribute_linked_id from @attr_status)

	insert into bik_attribute_linked_history(attribute_linked_id, change_source, modified_date) 
	select attribute_linked_id, @change_source, @modified_date from @attr_status where old_is_deleted = 0 and new_is_deleted = 1
end
go

-------------------------------------------
if (object_id('bik_joined_obj_attribute_linked_history') is null)
create table bik_joined_obj_attribute_linked_history (
	--id int identity(1,1),
	joined_obj_attribute_linked_id int,
	value varchar(max) default null,
	change_source varchar(max),
	modified_date datetime
)
go

if (object_id('trg_bik_joined_obj_attribute_linked_history') is not null)
drop trigger trg_bik_joined_obj_attribute_linked_history
go

create trigger trg_bik_joined_obj_attribute_linked_history on bik_joined_obj_attribute_linked
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @attr_status table (
		joined_obj_attribute_linked_id int
	)
	insert into @attr_status(joined_obj_attribute_linked_id)
	select i.id
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_joined_obj_attribute_linked_history(joined_obj_attribute_linked_id, value, change_source, modified_date)
	select id, value, @change_source, @modified_date
	from inserted where id not in (select joined_obj_attribute_linked_id from @attr_status)
	
	insert into bik_joined_obj_attribute_linked_history(joined_obj_attribute_linked_id, value, change_source, modified_date) 
	select i.id, i.value, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select joined_obj_attribute_linked_id from @attr_status)
	and (coalesce(i.value, '') <> coalesce(d.value, ''))

	insert into bik_joined_obj_attribute_linked_history(joined_obj_attribute_linked_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select joined_obj_attribute_linked_id from @attr_status)
end
go

-------------------------------------------
if (object_id('bik_attachment_history') is null)
create table bik_attachment_history (
	--id int identity(1,1),
	attachment_id int,
	href varchar(max) default null,
	change_source varchar(max),
	modified_date datetime
)
go

if (object_id('trg_bik_attachment_history') is not null)
drop trigger trg_bik_attachment_history
go

create trigger trg_bik_attachment_history on bik_attachment 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @attachment_status table (
		attachment_id int
	)
	insert into @attachment_status(attachment_id)
	select i.id
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_attachment_history(attachment_id, href, change_source, modified_date)
	select id, href, @change_source, @modified_date
	from inserted where id not in (select attachment_id from @attachment_status)
	
	insert into bik_attachment_history(attachment_id, href, change_source, modified_date) 
	select i.id, i.href, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select attachment_id from @attachment_status)
	and (coalesce(i.href, '') <> coalesce(d.href, ''))

	insert into bik_attachment_history(attachment_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select attachment_id from @attachment_status)
end
go

-------------------------------------------
if not exists (select 1 from sys.all_columns where object_id=object_id('bik_file_system_info') and name = 'id')
alter table bik_file_system_info add id int identity(1,1)
go

if (object_id('bik_file_system_info_history') is null)
create table bik_file_system_info_history (
	--id int identity(1,1),
	file_system_info_id int,
	href varchar(max) default null,
	change_source varchar(max),
	modified_date datetime
)
go

if (object_id('trg_bik_file_system_info_history') is not null)
drop trigger trg_bik_file_system_info_history
go

create trigger trg_bik_file_system_info_history on bik_file_system_info 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @attachment_status table (
		attachment_id int
	)
	insert into @attachment_status(attachment_id)
	select i.id
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_file_system_info_history(file_system_info_id, href, change_source, modified_date)
	select id, download_link, @change_source, @modified_date
	from inserted where id not in (select attachment_id from @attachment_status)
	
	insert into bik_file_system_info_history(file_system_info_id, href, change_source, modified_date) 
	select i.id, i.download_link, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select attachment_id from @attachment_status)
	and (coalesce(i.download_link, '') <> coalesce(d.download_link, ''))

	insert into bik_file_system_info_history(file_system_info_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select attachment_id from @attachment_status)
end
go

if not exists (select 1 from bik_app_prop where name = 'indexing.lastIndexedDate')
insert into bik_app_prop(name, val) values ('indexing.lastIndexedDate', '')
else update bik_app_prop set val = '' where name = 'indexing.lastIndexedDate'

declare @treeOdTree int = (select id from bik_tree where code = 'TreeOfTrees');

if not exists(select * from bik_node where tree_id = @treeOdTree and obj_id = '#SearchPage')
begin 
	exec sp_add_menu_node null, 'Szukaj', '#SearchPage'
  
	insert into bik_translation(code, txt, lang, kind)
	values ('#SearchPage', 'Search', 'en', 'node')
end

update bik_node set is_deleted=1 where tree_id=@treeOdTree and obj_id='#NewSearch'

--TO-DO: przeniesc zawartosci z bik_node_change_detail_value do bik_node_history
if object_id('bik_node_name_chunk') is not null 
drop table bik_node_name_chunk
go

if object_id('trg_bik_node_chunking') is not null
drop trigger trg_bik_node_chunking

exec sp_update_version '1.8.0.19', '1.8.0.20';
go