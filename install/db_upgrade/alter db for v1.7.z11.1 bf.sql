exec sp_check_version '1.7.z11.1';
go

update bik_app_prop set val='1' where name='disableBuiltInRoles';
	go

exec sp_update_version '1.7.z11.1', '1.7.z11.2';
go