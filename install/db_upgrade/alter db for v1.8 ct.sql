﻿exec sp_check_version '1.8';
go

declare @useHashPassword varchar(max)=(select val from bik_app_prop where name='useHashPassword');
if (@useHashPassword is null) set @useHashPassword='0'

declare @password varchar(max)='123' 
if (@useHashPassword='1') set @password=hashbytes('SHA1', @password)

if not exists (select 1 from bik_system_user where login_name='bik_artificial_admin')
insert into bik_system_user(login_name, password, is_disabled, user_id, name, avatar, sso_login, sso_domain, is_send_mails)
values('bik_artificial_admin', @password, 0, null, 'BIKS artficial admin', null, null, null, 1)
else 
update bik_system_user 
set password=@password, is_disabled=0, user_id=null, name='BIKS artficial admin', avatar=null, sso_login=null, sso_domain=null, is_send_mails=1
where login_name='bik_artificial_admin';

declare @userId int=(select id from bik_system_user where login_name='bik_artificial_admin')
delete from bik_user_right where user_id = @userId
insert into bik_user_right (user_id, right_id, tree_id) values(@userId, (select id from bik_right_role where code = 'Administrator'), null)

delete from bik_custom_right_role_user_entry where user_id = @userId;
insert into bik_custom_right_role_user_entry (user_id, role_id, tree_id, node_id)
values(@userId, (select id from bik_custom_right_role where code = 'Administrator'), null, null)

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

exec sp_recalculate_Custom_Right_Role_RutsEx @userId;
 
exec sp_update_version '1.8', '1.8.0.1';
go
