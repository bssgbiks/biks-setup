﻿exec sp_check_version '1.7.z6.10';
go

--------------------------
--------------------------


declare @treeOdTree int = dbo.fn_tree_id_by_code('TreeOfTrees');
if exists(select * from bik_node where tree_id = @treeOdTree and obj_id = '$TeradataDataModel')
and not exists (select * from bik_node where tree_id = @treeOdTree and obj_id = '&TeradataServer$TeradataDataModel')
begin
	exec sp_add_menu_node '$TeradataDataModel', '@', '&TeradataServer$TeradataDataModel'
	exec sp_add_menu_node '&TeradataServer$TeradataDataModel', '@', '&TeradataArea$TeradataDataModel'
	exec sp_add_menu_node '&TeradataArea$TeradataDataModel', '@', '&TeradataTable$TeradataDataModel'
	exec sp_add_menu_node '&TeradataTable$TeradataDataModel', '@', '&TeradataColumn$TeradataDataModel'
	exec sp_add_menu_node '&TeradataTable$TeradataDataModel', '@', '&TeradataColumnPK$TeradataDataModel'
	exec sp_add_menu_node '&TeradataTable$TeradataDataModel', '@', '&TeradataColumnIDX$TeradataDataModel'
end;
go


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_teradata_model_connections]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_teradata_model_connections
go

create procedure sp_insert_into_bik_joined_objs_teradata_model_connections as
begin

	-------------- Teradata Table <-> Teradata Data Model Table
	exec sp_prepare_bik_joined_objs_tmp
	
	declare @modelTreeId int = dbo.fn_tree_id_by_code('TeradataDataModel');
	declare @teradataTreeId int = dbo.fn_tree_id_by_code('Teradata');

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct bn.id, tera.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0 and bn.name = mode.name
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' and tera.is_deleted = 0
	where mode.type = 'TeradataTable'

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct tera.id, bn.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0 and bn.name = mode.name
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' and tera.is_deleted = 0
	where mode.type = 'TeradataTable'

	-- TF: na razie nie potrzebne są powiązania między kolumnami - komentuje
	/*
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct bn.id, tera.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' + rtrim(mode.name) + '|' and tera.is_deleted = 0
	where mode.type in('TeradataColumn', 'TeradataColumnIDX', 'TeradataColumnPK')

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct tera.id, bn.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' + rtrim(mode.name) + '|' and tera.is_deleted = 0
	where mode.type in('TeradataColumn', 'TeradataColumnIDX', 'TeradataColumnPK')
	*/
	
	exec sp_move_bik_joined_objs_tmp '@TeradataDataModel', '@Teradata'

end;
go


-- poprzednia wersja w pliku: alter db for v1.7.z5.19 tf.sql
-- dodanie połączeń między DQC a Teradata Model
if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_insert_into_bik_joined_objs_connections_for_dqc]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_insert_into_bik_joined_objs_connections_for_dqc
go

create procedure sp_insert_into_bik_joined_objs_connections_for_dqc as
begin

	-------------- Test <-> Teradata
	exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct test.id, ter.id, 1, 0
	from bik_dqc_test_parameters dtp
	join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
	join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct ter.id, test.id, 1, 0
	from bik_dqc_test_parameters dtp
	join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
	join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null

	exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Teradata'

	----------------- Test <-> BO, TeradataModel

	create table #tmp_jo (teradata int, dqc int);
	create table #tmp_to_insert (src int, dst int);

	declare @teradataTree int = dbo.fn_tree_id_by_code('Teradata');
	declare @dqcTree int = dbo.fn_tree_id_by_code('DQC');

	insert into #tmp_jo(teradata, dqc)
	select src.id, dst.id 
	from bik_joined_objs as jo
	join bik_node as src on src.id = jo.src_node_id
	join bik_node as dst on dst.id = jo.dst_node_id
	where src.tree_id = @teradataTree and src.is_deleted = 0 and src.linked_node_id is null
	and dst.tree_id = @dqcTree and dst.is_deleted = 0 and dst.linked_node_id is null
	and jo.type = 1 -- tylko automatyczne

	insert into #tmp_to_insert(src,dst)
	select distinct bn.id, tmp.dqc 
	from #tmp_jo as tmp
	join bik_joined_objs as jo on tmp.teradata = jo.dst_node_id 
	join bik_node as bn on bn.id = jo.src_node_id
	where bn.is_deleted = 0 and linked_node_id is null and bn.id <> tmp.dqc
	and bn.tree_id in (select report_tree_id from bik_sapbo_server where is_deleted = 0 
					   union all 
					   select universe_tree_id from bik_sapbo_server where is_deleted = 0 
					   union all 
					   select connection_tree_id from bik_sapbo_server where is_deleted = 0
					   union all 
					   select id from bik_tree where code = 'TeradataDataModel')
	and jo.type = 1 -- tylko automatyczne

	exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select src, dst, 1, 0
	from #tmp_to_insert as tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select dst, src, 1, 0
	from #tmp_to_insert as tmp
	
	exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Reports, @ObjectUniverses, @Connections, @TeradataDataModel'

end;
go

--------------------------
--------------------------

exec sp_update_version '1.7.z6.10', '1.7.z7'
go