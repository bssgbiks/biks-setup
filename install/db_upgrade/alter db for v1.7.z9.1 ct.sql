﻿exec sp_check_version '1.7.z9.1';

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_admin_backup_file') and name = 'file_size')
	alter table bik_admin_backup_file add file_size int null;
go

exec sp_update_version '1.7.z9.1', '1.7.z9.2';
go
