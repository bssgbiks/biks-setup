exec sp_check_version '1.7.z10.12';
go 
/* dla PB */

	if ((select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  begin
	/* dodanie wszystkich drzew do szablonu zwyklego uzytkownika */
	insert into bik_custom_right_role_rut_entry (tree_id,node_id,is_deleted)
	select bt.id,null,0 from bik_tree bt where is_hidden=0 and not exists(select * from bik_custom_right_role_rut_entry where bt.id=tree_id)
	and code not in ('DQM','DQMDokumentacja')
	exec sp_recalculate_Custom_Right_Role_Ruts;
	
	/* włączenie Lucene  i wyszukiwania po zawartości dokumentów */
	
	merge into bik_app_prop ap 
	using (values ('indexing.fullData', 'none')) as v(name, val) on ap.name = v.name
	when matched then update set ap.val = v.val
	when not matched by target then insert (name, val) values (v.name, v.val);
	
	
	update bik_app_prop set val = 'false' where name = 'useFullTextIndex'
	--update bik_app_prop set val = 'none' where name = 'indexing.fullData'
	update bik_app_prop set val = 'txt|log|sql|csv' where name = 'fileContentsExtractor.plainTextExts'
	update bik_app_prop set val = '*' where name = 'fileContentsExtractor.tikaExts'
	update bik_app_prop set val = '10000000' where name = 'fileContentsExtractor.maxFileSize'
	update bik_app_prop set val = '100000.0' where name = 'fileContentsExtractor.fileContentsBoost'

	end
	
exec sp_update_version '1.7.z10.12', '1.7.z11';
go