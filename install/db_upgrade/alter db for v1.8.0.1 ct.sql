﻿exec sp_check_version '1.8.0.1';
go

--Dodanie Drools
declare @treeOfTrees int = (select id from bik_tree where code = 'TreeOfTrees');
declare @parent_node_id int = dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', 'admin')

if not exists (select 1 from bik_node where tree_id=@treeOfTrees and obj_id='#admin:drools:logs') 
begin
	exec sp_add_menu_node 'admin:drools', 'Logi', '#admin:drools:logs'
	
	insert into bik_translation(code, txt, lang, kind) values ('#admin:drools:logs', 'Logs', 'en', 'node')
	
	insert into bik_node_action(code) values ('#admin:drools:logs')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:drools:logs'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	
	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants
end

update bik_node set visual_order=9999 where obj_id='#admin:drools:cfg' and tree_id=@treeOfTrees

exec sp_node_init_branch_id  @treeOfTrees, null

if not exists(select * from sys.objects where object_id = object_id(N'bik_drools_log') and type in (N'U'))
	create table bik_drools_log(
		id int not null primary key identity(1,1),
		start_time datetime default sysdatetime(),
		msg varchar(max)
)	;
go

--Nie uzywane, skasujemy
delete from bik_app_prop where name='lastCheckedChangeIdByDrools'
delete from bik_admin where param='drools.inspectedTreeIds'

if not exists (select 1 from sys.columns where object_id=object_id(N'bik_attr_def') and name='in_drools')
alter table bik_attr_def add in_drools int default 0
go

 
exec sp_update_version '1.8.0.1', '1.8.0.2';
go