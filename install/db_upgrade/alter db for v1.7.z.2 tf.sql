﻿exec sp_check_version '1.7.z.2';
go

-- poprzednia wersja w pliku: alter db for v1.6.z9.20 bf.sql
-- fix na collate DATABASE_DEFAULT
if  exists (select * from sys.objects where object_id = object_id('[dbo].[sp_get_all_statistics_ext_dict]') and type in ('p', 'pc'))
drop procedure [dbo].[sp_get_all_statistics_ext_dict]
go
 
create procedure [dbo].[sp_get_all_statistics_ext_dict](@lang varchar(3)) as
begin
	 set nocount on
	 
    create table #tmp
    (
            counter_name varchar(255) collate DATABASE_DEFAULT,
            description varchar(255) collate DATABASE_DEFAULT,
            best_parameter varchar(255) collate DATABASE_DEFAULT,
            day_cnt int,
            week_cnt int,
            thirty_cnt int,
            all_cnt int,
            total int
    )

    declare @treeId int
    declare @counterName varchar(255)
    declare @description varchar(255)
    declare @bestParameter varchar(255)
    declare @cnt1 int, @cnt7 int, @cnt30 int, @cntAll int, @total int 

    declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.is_hidden = 0

    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
					select @bestParameter = '', @cnt1 = 0, @cnt7 = 0, @cnt30 = 0, @cntAll = 0, @total = 0;
                    with table_stat
                    as
                    (
                            select bse.grouping_node_id, bn.name, count(*) as all_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
                            from bik_statistic_ext bse
                            left join bik_node bn on bn.id = bse.grouping_node_id
                            where bse.tree_id = @treeId
                            group by bse.tree_id, bse.grouping_node_id, bn.name
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc
                    --select top 1 @bestParameter=name, @cnt1=day_cnt, @cnt7=week_cnt, @cnt30=thirty_cnt, @cntAll=all_cnt 
                    --,@total=(select sum( all_cnt) from table_stat) 
                    --from table_stat order by all_cnt desc			

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs


	declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.id is null


    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
					select @bestParameter = '', @cnt1 = 0, @cnt7 = 0, @cnt30 = 0, @cntAll = 0, @total = 0;
                    with table_stat
                    as
                    (
							select bs.parametr as name,
							count(*) as all_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
							from 
							bik_statistic bs 
							left join bik_tree bt on bs.counter_name = bt.code  
							where 
							bs.counter_name = @counterName and (bt.id is null or bt.is_hidden = 0)
							group by bs.parametr 
                            
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs
    
    select counter_name,coalesce(txt, description) as description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total
	from #tmp left join bik_translation bt on (#tmp.counter_name=bt.code and lang=@lang and (kind='tree' or kind='stat'))
	
	drop table #tmp;
end
go

-- poprzednia wersja w "alter db for v1.6.z6.2 tf.sql"
-- dodanie is_deleted attrsów z pusta wartością
if exists (select * from sys.objects where object_id = OBJECT_ID(N'dbo.sp_verticalize_node_attrs_add_attrs') and type in (N'P', N'PC'))
drop procedure dbo.sp_verticalize_node_attrs_add_attrs
go

create procedure dbo.sp_verticalize_node_attrs_add_attrs(@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  --declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  --declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in=0 /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  --insert into @attr_names (attr_id, name, is_deleted)
  --exec(@attr_names_sql)
  
  --select * from @attr_names
  
  --declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  declare attr_names_cur cursor for select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in = 0 /*and is_deleted = 0 and exists (select 1 from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id where a.attr_def_id = ad.id and al.is_deleted = 0 and a.is_deleted = 0)*/
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

	--set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
end
go


exec sp_verticalize_node_attrs_add_attrs null;


exec sp_update_version '1.7.z.2', '1.7.z.3';
go
