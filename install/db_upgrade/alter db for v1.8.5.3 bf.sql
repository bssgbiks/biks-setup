﻿exec sp_check_version '1.8.5.3';
GO

if not exists (select * from bik_node_action where code = 'AddAnyNodeForKind')
begin
insert into bik_node_action (code) values ('AddAnyNodeForKind')

declare @actionId int;
select  @actionId=id from bik_node_action where code='AddAnyNodeForKind'

insert into bik_node_action_in_custom_right_role (action_id,role_id)
select distinct @actionId,role_id from bik_node_action_in_custom_right_role
where action_id = (select id from bik_node_action where code='AddAnyNode')

end

if not exists (select * from bik_node_action where code = 'AddCustomTreeNode')
begin

insert into bik_node_action (code) values ('AddCustomTreeNode')
--declare @actionId int;
select  @actionId=id from bik_node_action where code='AddCustomTreeNode'

insert into bik_node_action_in_custom_right_role (action_id,role_id)
select distinct @actionId,role_id from bik_node_action_in_custom_right_role
where action_id = (select id from bik_node_action where code='AddAnyNode')

end

if not exists (select * from bik_node_action where code = 'AddEvent')
begin

insert into bik_node_action (code) values ('AddEvent')
--declare @actionId int;
select  @actionId=id from bik_node_action where code='AddEvent'

insert into bik_node_action_in_custom_right_role (action_id,role_id)
select distinct @actionId,role_id from bik_node_action_in_custom_right_role
where action_id = (select id from bik_node_action where code='AddAnyNode')

end


exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
	
	
exec sp_update_version '1.8.5.3'
	,'1.8.5.4';
GO


