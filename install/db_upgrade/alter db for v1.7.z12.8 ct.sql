﻿exec sp_check_version '1.7.z12.8';
go
 
--Usuwamy bo nie jest uzywana
if object_id(N'sp_insert_into_bik_node_oracle_extradata') is not null 
drop procedure sp_insert_into_bik_node_oracle_extradata
go

if object_id(N'sp_teradata_init_branch_names') is not null 
drop procedure sp_teradata_init_branch_names
go

exec sp_update_version '1.7.z12.8', '1.7.z12.9';
go