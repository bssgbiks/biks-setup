﻿exec sp_check_version '1.7.z7.2';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

update bik_node_action set code = 'DQMDownloadXLSX' where code = 'DQMDownloadReport'

if not exists (select * from bik_node_action where code = 'DQMAddDataErrorIssue')
begin
	insert into bik_node_action(code) values ('DQMAddDataErrorIssue')
	
	if exists (select * from bik_custom_right_role where code = 'DqmEditor')
	insert into bik_node_action_in_custom_right_role(action_id, role_id)
	values ((select id from bik_node_action where code = 'DQMAddDataErrorIssue'), (select id from bik_custom_right_role where code = 'DqmEditor'))
end;

if not exists (select * from bik_node_action where code = 'DQMEditDataErrorIssue')
begin
	insert into bik_node_action(code) values ('DQMEditDataErrorIssue')
	
	if exists (select * from bik_custom_right_role where code = 'DqmEditor')
	insert into bik_node_action_in_custom_right_role(action_id, role_id)
	values ((select id from bik_node_action where code = 'DQMEditDataErrorIssue'), (select id from bik_custom_right_role where code = 'DqmEditor'))
end;

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.2', '1.7.z7.3';
go
