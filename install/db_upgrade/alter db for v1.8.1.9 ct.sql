﻿exec sp_check_version '1.8.1.9';
go

if not exists (select 1 from bik_tree where code='metaBIKS@tmp')
insert into bik_tree(name, node_kind_id, code, tree_kind, is_hidden, is_auto_obj_id) 
values('metaBIKS@tmp', null, 'metaBIKS@tmp', 'metaBIKS', 1, 1)

exec sp_update_version '1.8.1.9', '1.8.1.10';
