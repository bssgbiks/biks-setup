﻿exec sp_check_version '1.7.z10';


if not exists (select * from bik_app_prop where name = 'enableAttributeJoinedObjs')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('enableAttributeJoinedObjs', 'false', 1)
end
go


if object_id('bik_joined_obj_attribute', 'u') is null
begin
	create table bik_joined_obj_attribute(
	id int not null identity(1,1) primary key,
	name varchar (255) not null,
	type varchar (30) not null ,
	value_opt varchar (max) null);
end;
go


if object_id('bik_joined_obj_attribute_linked', 'u') is null
begin
	create table bik_joined_obj_attribute_linked(
	id int not null identity(1,1) primary key,
	joined_obj_id int not null references bik_joined_objs(id),
	attribute_id int not null references bik_joined_obj_attribute(id),
	value  varchar (max) null,
	is_main smallint not null default (0));
	end;
go



exec sp_update_version '1.7.z10', '1.7.z10.1';
go
