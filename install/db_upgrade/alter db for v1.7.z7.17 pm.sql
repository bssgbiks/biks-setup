﻿exec sp_check_version '1.7.z7.17';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs
go

create procedure [dbo].[sp_verticalize_node_attrs](@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @has_fulltext_catalog int = (select case when exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') then 1 else 0 end has_fulltext_catalog);
  
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
    exec('drop fulltext index on bik_searchable_attr_val')
    --exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING MANUAL')
  end
  
  exec sp_verticalize_node_attrs_inner @optNodeFilter

  if @has_fulltext_catalog = 1 begin
    if not exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
      declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
      exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
    end
  
    if (select change_tracking_state from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) <> 'A' begin
      exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO')
    end
  end;
end
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_manage_biks_fts_catalog]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_manage_biks_fts_catalog
go

create procedure [dbo].[sp_manage_biks_fts_catalog](@drop_it int)
as
begin
  set nocount on

  declare @has_fulltext_catalog int = (select case when exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') then 1 else 0 end has_fulltext_catalog);
  
  print 'sp_manage_biks_fts_catalog(' + cast(@drop_it as varchar(20)) + '), @has_fulltext_catalog=' + cast(@has_fulltext_catalog as varchar(20));

  if @has_fulltext_catalog = 0 and @drop_it <> 0 begin
    print 'sp_manage_biks_fts_catalog: done, no fulltext catalog, nothing to drop';

    return; -- nie ma katalogu, nie ma co usuwać ;-)
  end;

  if @drop_it <> 0 begin
    print '  drop (if exists)';
     
    if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
      print '  drop fulltext index on bik_searchable_attr_val';
      exec('drop fulltext index on bik_searchable_attr_val')
    end;
    if exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') begin
      print '  drop fulltext CATALOG BIKS_FTS_Catalog';
      exec('DROP FULLTEXT CATALOG BIKS_FTS_Catalog');
    end;
  end 
  else
  begin
    print '  create (if not exists)';
    if not exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') begin
      print '  create fulltext CATALOG BIKS_FTS_Catalog';
      exec('CREATE FULLTEXT CATALOG BIKS_FTS_Catalog AS DEFAULT');
    end;

    if not exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
      print '  CREATE FULLTEXT INDEX ON bik_searchable_attr_val';
      declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
      exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
    end
  
    if (select change_tracking_state from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) <> 'A' begin
      print '  ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO';
      exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO')
    end
  end;

  print 'sp_manage_biks_fts_catalog: done';
end;
go


/*

exec sp_manage_biks_fts_catalog 1; -- skasuj fts microsoftu

exec sp_manage_biks_fts_catalog 0; -- przywróć fts microsoftu

*/


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

delete from bik_app_prop where name like 'fileContentsExtractor.%';

insert into bik_app_prop (name, val) values
  ('fileContentsExtractor.plainTextExts', 'txt|log|sql|csv'), -- rozszerzenia oddzielamy | (bar-em)
  ('fileContentsExtractor.tikaExts', '*'),  -- wszystkie inne rozszerzenia
  ('fileContentsExtractor.maxFileSize', '0'), -- nie będzie działać, bo zero!
--  ('fileContentsExtractor.maxFileSize', '10000000'), -- 10 mb
  ('fileContentsExtractor.fileContentsBoost', '100000.0'); -- float
go

-- update bik_app_prop set val = '10' where name = 'suggestSimilar.maxResults'

-- update bik_app_prop set val = 'false' where name = 'useFullTextIndex'

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


update bik_app_prop set val = 'none' where name = 'indexing.fullData';
go


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------


exec sp_update_version '1.7.z7.17', '1.7.z7.18';
go
