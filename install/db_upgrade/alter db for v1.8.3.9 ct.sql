﻿exec sp_check_version '1.8.3.9';
go
 
if not exists (select 1 from sys.all_columns where name='folder_path' and object_id=object_id('bik_jdbc'))
alter table bik_jdbc add folder_path varchar(max)
go

if not exists (select 1 from sys.all_columns where name='adhoc_sql' and object_id=object_id('bik_jdbc'))
alter table bik_jdbc add adhoc_sql varchar(max)
go

exec sp_update_version '1.8.3.9', '1.8.3.10';
go


