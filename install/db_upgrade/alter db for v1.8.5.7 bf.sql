﻿exec sp_check_version '1.8.5.7';
go

if not exists(select * from sys.objects where object_id = object_id(N'bik_mail_log') and type in (N'U'))
	create table bik_mail_log(
		id int not null primary key identity(1,1),
		start_time datetime default sysdatetime(),
		msg varchar(max)
)	;
go

declare @treeOfTrees int = (select id from bik_tree where code = 'TreeOfTrees');
if not exists (select 1 from bik_node where tree_id = @treeOfTrees and obj_id='admin:email')
begin
	exec sp_add_menu_node 'admin', 'E-mail', 'admin:email'
	exec sp_add_menu_node 'admin:email', 'Logi', '#admin:email:logs'
	
	if not exists(select 1 from bik_translation where code ='admin:email')
	begin
	insert into bik_translation(code, txt, lang, kind) values ('admin:email', 'E-mail', 'en', 'node')
	end
	if not exists(select 1 from bik_translation where code ='#admin:email:logs')
	begin
	insert into bik_translation(code, txt, lang, kind) values ('#admin:email:logs', 'Logs', 'en', 'node')
	end
	
	if not exists(select 1 from bik_node_action where code='#admin:email:logs')
	begin
	insert into bik_node_action(code) values ('#admin:email:logs')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:email:logs'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	end
	--exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants
end

exec sp_node_init_branch_id  @treeOfTrees, null

--declare @treeOfTrees int = (select id from bik_tree where code = 'TreeOfTrees');
if not exists (select 1 from bik_node where tree_id = @treeOfTrees and obj_id='#admin:dict:bikAppProps')
begin
	--exec sp_add_menu_node 'admin', 'app', 'admin:app'
	exec sp_add_menu_node 'admin:dict', 'Konfiguracja App Prop', '#admin:dict:bikAppProps'
	if not exists(select 1 from bik_translation where code ='#admin:dict:bikAppProps')
	begin
	insert into bik_translation(code, txt, lang, kind) values ('#admin:dict:bikAppProps', 'Configuration', 'en', 'node')
	end
	if not exists(select 1 from bik_node_action where code='#admin:dict:bikAppProps')
	begin
	insert into bik_node_action(code) values ('#admin:dict:bikAppProps')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:dict:bikAppProps'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	end
	--exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants
end

exec sp_node_init_branch_id  @treeOfTrees, null
exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants
 
exec sp_update_version '1.8.5.7','1.8.5.8';
go
