exec sp_check_version '1.8.4.15';
GO

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'allowedFileExtensions'
	,'.pdf;.jpeg;.gif;.jpg;.png;.txt'
	,1
	)

exec sp_update_version '1.8.4.15'
	,'1.8.4.16';
GO

