﻿exec sp_check_version '1.8.5.14';
go

if not exists (select 1 from bik_app_prop where name='expandSelectedNodesInHyperlinkAttribute' )
insert into bik_app_prop(name, val) values ('expandSelectedNodesInHyperlinkAttribute', 'false')
else 
update bik_app_prop set val='false' where name='expandSelectedNodesInHyperlinkAttribute'
go 

exec sp_update_version '1.8.5.14','1.8.5.15';
go

