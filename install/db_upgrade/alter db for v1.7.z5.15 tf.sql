﻿exec sp_check_version '1.7.z5.15';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if not exists(select * from bik_app_prop where name = 'hideSocialUserInfoOnAdminUsersTab')
	insert into bik_app_prop(name, val,is_editable) values ('hideSocialUserInfoOnAdminUsersTab', 'false', 1)
go

if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' -- PLK
begin
	update bik_app_prop set val = 'true' where name = 'hideSocialUserInfoOnAdminUsersTab'
end;
go

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_filter_users_group]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_filter_users_group]
go

create procedure [dbo].[sp_filter_users_group] (@txt varchar(max)) as
begin
	set nocount on;

	create table #grIds (id int not null, parent int null, unique(id, parent));
	create table #candidate (id int not null);
	create table #tmp (id int not null, parent int null, unique(id, parent));

	insert into #grIds(id, parent)
	select gr.id, gig.group_id
	from bik_system_user_group as gr
	left join bik_system_group_in_group as gig on gr.id = gig.member_group_id and gig.is_deleted = 0
	where gr.name like '%' + @txt + '%' and gr.is_deleted = 0

	insert into #candidate
	select distinct parent from #grIds where parent is not null

	--select * from @candidate

	while exists(select * from #candidate)
	begin
		insert into #tmp(id, parent)
		select can.id, gig.group_id from #candidate as can
		left join bik_system_group_in_group as gig on can.id = gig.member_group_id and gig.is_deleted = 0

		insert into #grIds(id, parent)
		select distinct can.id, can.parent from #tmp as can
		where not exists(select * from #grIds as old where can.id = old.id and can.parent = old.parent or can.id = old.id and old.parent is null and can.parent is null)

		delete from #candidate

		insert into #candidate(id)
		select distinct parent from #tmp where parent is not null

		delete from #tmp
	end;

	-- usuwanie blednych grup, czyli takich, których parentem jest null, a grupa nie jest domeną
	-- po usunięciu takiej grupy - rekurencyjne przejście po grupach, aby odrzucić dla nieh podgrupy
	insert into #tmp(id, parent)
	select gr.id, gr.parent 
	from #grIds as gr 
	inner join bik_system_user_group as sgu on sgu.id = gr.id 
	where gr.parent is null and sgu.distinguished_name not like 'DC=%'


	while exists(select  * from #tmp)
	begin
		delete from #grIds from #grIds as gr 
		where exists (select * from #tmp as old where gr.id = old.id and gr.parent = old.parent or gr.id = old.id and old.parent is null and gr.parent is null)

		delete from #tmp

		insert into #tmp
		select gr.id, gr.parent 
		from #grIds as gr
		left join #grIds as par on par.id = gr.parent
		where gr.parent is not null and par.id is null
	end;

	select res.id, res.parent as parentId, gr.name, gr.domain, case when exists(select * from bik_system_group_in_group where group_id = gr.id and is_deleted = 0) then 0 else 1 end as has_no_children
	from #grIds as res
	inner join bik_system_user_group as gr on gr.id = res.id and gr.is_deleted = 0

end;
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z5.15', '1.7.z5.16';
go
