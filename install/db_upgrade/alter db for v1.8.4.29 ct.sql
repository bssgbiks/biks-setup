﻿exec sp_check_version '1.8.4.29';
go

update bik_schedule set interval=interval*1440
update bik_dqm_schedule set interval=interval*1440
exec sp_update_version '1.8.4.29', '1.8.4.30';
go 