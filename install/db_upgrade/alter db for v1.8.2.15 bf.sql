﻿exec sp_check_version '1.8.2.15';
go

update bik_attr_def set value_opt='shortText,longText,ansiiText,hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject'
where name='metaBIKS.Typ atrybutu' or name='metaBIKS.Nadpisany typ atrybutu'

update bik_attr_def set type_attr='hyperlinkInMulti' where type_attr='hyperlinkIn'
update bik_attribute set type_attr='hyperlinkInMulti' where type_attr='hyperlinkIn'

update bik_attribute_linked set value='hyperlinkInMulti'where value like 'hyperlinkIn'
and attribute_id in (select id from bik_attribute where attr_def_id in (select id from bik_attr_def where name='metaBIKS.Typ atrybutu'))

 
go
exec sp_update_version '1.8.2.15', '1.8.2.16';
go
