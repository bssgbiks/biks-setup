exec sp_check_version '1.7.z5.9';
go

if not exists(select 1 from syscolumns sc where id = OBJECT_ID('bik_active_directory') and name = 'parent_distinguished_name')
	alter table bik_active_directory add parent_distinguished_name varchar(max) null;
go


-- ostatni skrypt w pliku: alter db for v1.7.z5.3 tf.sql 
-- fix na sso_login i dobrego parenta w group_in_group
if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_fix_system_users_and_groups_from_ad]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_fix_system_users_and_groups_from_ad]
go

create procedure [dbo].[sp_fix_system_users_and_groups_from_ad](@domain varchar(512)) as
begin
	set nocount on

	-- bik_system_user_group
	merge into bik_system_user_group as sug
	using (select grr.s_a_m_account_name, grr.domain, grr.distinguished_name from bik_active_directory_group as grr where grr.domain = @domain
			union all select ouu.name, ouu.domain, ouu.distinguished_name from bik_active_directory_ou as ouu where ouu.domain = @domain) as adg 
		on sug.distinguished_name = adg.distinguished_name
	when matched then
	update set is_deleted = 0
	when not matched by target and adg.domain = @domain then
	insert (name, domain, distinguished_name) values (adg.s_a_m_account_name, adg.domain, adg.distinguished_name)
	when not matched by source and sug.domain = @domain then
	update set is_deleted = 1;

	-- bik_system_user_in_group
	update bik_system_user_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_user_in_group as uig
	using (select usr.id as usr_id, sug.id as gr_id from bik_active_directory as ad 
		cross apply dbo.fn_split_by_sep(ad.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as sug on sp.str = sug.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain
		union all
		select usr.id, gr.id from bik_active_directory as ad 
		inner join bik_system_user_group as gr on ad.parent_distinguished_name = gr.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain) as x on uig.user_id = x.usr_id and uig.group_id = x.gr_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (user_id, group_id) values (x.usr_id, x.gr_id);

	-- bik_system_group_in_group
	update bik_system_group_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain) or member_group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_group_in_group as gig
	using(select par.id as par_id, gr.id as child_id
		from bik_active_directory_ou as ou
		inner join bik_system_user_group as gr on gr.distinguished_name = ou.distinguished_name
		inner join bik_system_user_group as par on par.distinguished_name = ou.parent_distinguished_name
		where ou.domain = @domain
		union all 
		select ugr.id, cgr.id from bik_active_directory_group as gr
		cross apply dbo.fn_split_by_sep(gr.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as ugr on sp.str = ugr.distinguished_name
		inner join bik_system_user_group as cgr on gr.distinguished_name = cgr.distinguished_name
		where gr.domain = @domain
		union all 
		select pargr.id, chilgr.id from bik_active_directory_group as grp
		inner join bik_system_user_group as pargr on grp.parent_distinguished_name = pargr.distinguished_name
		inner join bik_system_user_group as chilgr on grp.distinguished_name = chilgr.distinguished_name
		where grp.domain = @domain) as x on x.par_id = gig.group_id and x.child_id = gig.member_group_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (group_id, member_group_id) values (x.par_id, x.child_id);

end;
go

exec sp_update_version '1.7.z5.9', '1.7.z5.10';
go
