﻿exec sp_check_version '1.7.z8.4';
go

if not exists (select * from bik_app_prop where name = 'showSuggestedElements')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('showSuggestedElements', 'true', 1)
end
go

if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' -- PB
begin
	update bik_app_prop set val='false' where name= 'showSuggestedElements';

end
go

declare @default_constraint_name sysname
select @default_constraint_name = object_name(cdefault)
from syscolumns
where id = object_id('bik_system_user')
and name = 'is_send_mails'
if @default_constraint_name is not null
exec ('alter table ' + 'bik_system_user' + ' drop constraint ' + @default_constraint_name);

alter table bik_system_user add  default 0 for is_send_mails;
go


exec sp_update_version '1.7.z8.4', '1.7.z8.5';
go