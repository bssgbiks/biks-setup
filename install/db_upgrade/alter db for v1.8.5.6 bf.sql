﻿exec sp_check_version '1.8.5.6';
go
--hyperlinkInMono,hyperlinkInMulti,hyperlinkOut,data,combobox,checkbox,comboBooleanBox,calculation,javascript,selectOneJoinedObject,ansiiText,longText,shortText,

if not exists (select * from bik_attr_def where name in ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu') and value_opt like '%permissions%') 
update bik_attr_def set value_opt=value_opt+',permissions' where name in ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu')

if not exists (select * from bik_attribute where attr_def_id in (select id from bik_attr_def where name in
 ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu'))
 and value_opt like '%permissions%'  and value_opt like '%hyperlinkInMulti%') 
 update bik_attribute set value_opt=value_opt+'permissions,' where attr_def_id in (select id from bik_attr_def where name in
 ('metaBiks.Typ atrybutu','metaBiks.Nadpisany typ atrybutu')) and value_opt like '%hyperlinkInMulti%'




if not exists (select * from bik_app_prop where name='availableConnectors' and val like '%BIKS SQL%') 
update bik_app_prop set val=val+',BIKS SQL' where name='availableConnectors'

if  exists (select * from bik_app_prop where name='availableConnectors' and val like '%Permissions%') 
update bik_app_prop set val=REPLACE(val,',Permissions','') where name='availableConnectors';


if not exists(select * from sys.objects where object_id = object_id(N'bik_any_sql') and type in (N'U'))
begin
create table bik_any_sql(
	id int not null IDENTITY(1,1) primary key,
	name varchar(800) not null,
	query varchar(max),
	  is_active int not null default(0)
);
end;
if not exists (select * from bik_Any_sql where name='Uprawnienia')
insert into bik_any_sql (name,query) values 
('Uprawnienia','delete from bik_custom_right_role_user_entry where rights_processor=''sql''
	declare @roleId int = (select id from bik_custom_right_role where code=''Author'')
	
	insert into bik_custom_right_role_user_entry(user_id,role_id,tree_id,node_id,rights_processor)	
	select distinct
		bsu.id
		,@roleId
		,x.tree_id
		,x.node_id
		,''sql'' 
	from bik_system_user bsu 
	join bik_user bu on bsu.user_id=bu.id
	join (
		SELECT bal.node_id,bal.attribute_id,bal.value,bn.tree_id,
		SUBSTRING(str,0, CHARINDEX(''_'',str,0))as user_node_id FROM bik_attribute_linked  bal
		cross apply dbo.fn_split_by_sep(bal.value, ''|'', 7) as sp 
		join bik_attribute ba on bal.attribute_id=ba.id
		join bik_attr_def bad on bad.id=ba.attr_def_id 
		join bik_node bn on bn.id=bal.node_id
		where ba.is_deleted=0 and bad.is_deleted=0 
		and coalesce(ba.type_attr,bad.type_Attr) =''permissions''
		
		)x on x.user_node_id=bu.node_id
	
	
	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants');
-- ograniczenia do nazw atrybutów można dodać po ''permissions''
--and bad.name in (''Nazwy atrybutow po przecinku'',''np Właściciel'',''np Opiekun'')
delete from bik_admin where param like 'permissions.%'

delete from bik_data_load_log_details where log_id in (select id from bik_data_load_log where data_source_type_id in (select id from bik_data_source_def where description='Permissions'))
delete from bik_data_load_log where data_source_type_id in (select id from bik_data_source_def where description='Permissions')
delete from bik_schedule where source_id in (select id from bik_data_source_def where description='Permissions')
delete from bik_data_source_def where description='Permissions'

if not exists( select * from bik_data_source_def where description = 'BIKS SQL' )  
  begin
	insert into bik_data_source_def(description)
	values ('BIKS SQL')    
	
    declare @sourceId int, @priority int;
    select @sourceId = id from bik_data_source_def where description = 'BIKS SQL'

	insert into bik_schedule(source_id, hour, minute, interval, is_schedule, priority,instance_name)
	values(@sourceId, 0, 0, 1, 0, 11,'Uprawnienia')
  end

exec sp_update_version '1.8.5.6','1.8.5.7';
go
