﻿exec sp_check_version '1.8.0.28';
go

if not exists (select 1 from sys.all_columns where object_id=object_id('bik_attribute') and name='type_attr')
begin
	alter table bik_attribute add type_attr varchar(max) 
end
if not exists (select 1 from sys.all_columns where object_id=object_id('bik_attribute') and name='value_opt')
begin
	alter table bik_attribute add value_opt varchar(max) 
end
go

update attr set attr.type_attr = ad.type_attr, attr.value_opt=ad.value_opt from bik_attribute attr join bik_attr_def ad on ad.id = attr.attr_def_id

if not exists (select 1 from sys.objects where object_id=object_id('bik_allowed_joined_objs')  )
create table bik_allowed_joined_objs(
	id int not null identity(1,1),
	src_node_kind_id int not null,
	dst_node_kind_id int not null,
	is_deleted int default 0,
	constraint pk_bik_allowed_joined_objs primary key (id),
	constraint fk_src_node_kind foreign key (src_node_kind_id) references bik_node_kind(id),
	constraint fk_dst_node_kind foreign key (dst_node_kind_id) references bik_node_kind(id)
)
go

update bik_node_kind set caption = caption+'-'+code where not(caption like ('%'+code))
exec sp_update_version '1.8.0.28', '1.8.1';

