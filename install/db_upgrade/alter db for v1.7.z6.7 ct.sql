﻿exec sp_check_version '1.7.z6.7';
go
------------------------
if not exists (select 1 from sys.objects where object_id = OBJECT_ID(N'[dbo].[bik_lisa_auth]') and type in (N'U'))
begin
	create table bik_lisa_auth (
		node_id int foreign key references bik_node(id),
		login_mode int,
		teradata_login varchar(128),
		teradata_pwd varchar(128)
	)
end
go

if not exists (select 1 from bik_node_kind where code='TeradataFolder')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder)
	values('TeradataFolder', 'Folder słowników', 'folder', 'Dictionary', 1)
end
go 

if not exists (select 1 from bik_node_action where code='LisaAddFolder')
begin
	insert into bik_node_action(code) values('LisaAddFolder')
end
go

if not exists (select 1 from bik_node_action where code='EditLisaDict')
begin
	insert into bik_node_action(code) values('EditLisaDict')
end
go

declare @addFolderActId int = (select id from bik_node_action where code='LisaAddFolder')
delete from bik_node_action_in_custom_right_role where action_id = @addFolderActId
insert into bik_node_action_in_custom_right_role(action_id, role_id)
select @addFolderActId, id from bik_custom_right_role where code <> 'RegularUser'

declare @editLisaDictActId int = (select id from bik_node_action where code='EditLisaDict')
delete from bik_node_action_in_custom_right_role where action_id = @editLisaDictActId
insert into bik_node_action_in_custom_right_role(action_id, role_id)
select @editLisaDictActId, id from bik_custom_right_role where code <> 'RegularUser'

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
---------------------------------------------------------------------
exec sp_update_version '1.7.z6.7', '1.7.z6.8'
go