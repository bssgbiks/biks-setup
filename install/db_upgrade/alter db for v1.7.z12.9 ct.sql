﻿exec sp_check_version '1.7.z12.9';
go
 
if object_id(N'fn_get_obj_id_by_node_name_path') is not null
drop function dbo.fn_get_obj_id_by_node_name_path
go

create function dbo.fn_get_obj_id_by_node_name_path(@nodeId int) 
returns varchar(max) as
begin
	declare @obj_id varchar(max) = dbo.fn_get_node_name_path(@nodeId);
	return replace(@obj_id, ' » ', '|') 
end
go

if object_id(N'sp_delete_node_history_by_node_ids') is not null
drop procedure dbo.sp_delete_node_history_by_node_ids
go

create procedure [dbo].[sp_delete_node_history_by_node_ids](@node_ids bik_unique_not_null_id_table_type READONLY)
as
begin
	declare @change_ids bik_unique_not_null_id_table_type;
    insert into @change_ids(id)
    select distinct change_id from bik_node_change_detail where node_id in (
    select id from @node_ids)
    
    delete from bik_node_change_detail_value where change_detail_id in (select id from bik_node_change_detail where node_id in (select id from @node_ids))
    delete from bik_node_change_detail where node_id in (select id from @node_ids)
    --delete from bik_node_change where id in (select id from @change_ids)
end;
go

exec sp_update_version '1.7.z12.9', '1.7.z12.10';
go