﻿exec sp_check_version '1.8.0.8';
go

if object_id('bik_tree_import_status') is null
create table bik_tree_import_status (
	tree_id int,
	import_status int
)
go

exec sp_update_version '1.8.0.8', '1.8.0.9';
go
