﻿exec sp_check_version '1.8.4.1'
GO

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideTabsFromBarForMM'
	,'true'
	,1
	)
GO

DELETE
FROM bik_node_action_in_custom_right_role
WHERE action_id = (
		SELECT id
		FROM bik_node_action
		WHERE code = 'ShowNonPublicAttributes'
		)
	AND role_id = (
		SELECT id
		FROM bik_custom_right_role
		WHERE code = 'RegularUser'
		)

exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
GO

exec sp_update_version '1.8.4.1'
	,'1.8.4.2';
GO


