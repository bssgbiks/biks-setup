﻿exec sp_check_version '1.7.z7';
go

--------------------------
--------------------------

if not exists (select * from bik_app_prop where name = 'dqmEvaluationFirstEndDate')
begin
	insert into bik_app_prop (name, val, is_editable) 
	values ('dqmEvaluationFirstEndDate', '2015-12-31', 1)
end
go

--------------------------
--------------------------

exec sp_update_version '1.7.z7', '1.7.z7.1'
go