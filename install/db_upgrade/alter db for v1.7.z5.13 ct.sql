exec sp_check_version '1.7.z5.13';
if not exists (select 1 from bik_node_kind where code = 'DQMErrorRegister') 
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank) 
	values ('DQMErrorRegister', 'Rejestr błędów', 'postgresFolder', 'QualityMetadata', 0, 5);
end;

if not exists (select 1 from bik_node_kind where code = 'DQMOpenedDataErrorIssue')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank) 
	values ('DQMOpenedDataErrorIssue', 'Błąd otwarty', 'dqmOpenIssue', 'QualityMetadata', 0, 5);
end;

if not exists (select 1 from bik_node_kind where code = 'DQMClosedDataErrorIssue')
begin
	insert into bik_node_kind(code, caption, icon_name, tree_kind, is_folder, search_rank) 
	values ('DQMClosedDataErrorIssue', 'Błąd zamknięty', 'dqmCloseIssue', 'QualityMetadata', 0, 5);
end;

declare @nodeKindId int = (select id from bik_node_kind where code = 'DQMErrorRegister');
declare @dqmTreeId int = (select id from bik_tree where code = 'DQMDokumentacja');
update bik_node set node_kind_id = @nodeKindId where tree_id = @dqmTreeId and name = 'Rejestr błędów';

if not exists(select * from sys.objects where object_id = object_id(N'bik_dqm_data_error_issue') and type in (N'U'))
	create table bik_dqm_data_error_issue (
		id int not null identity(1,1) primary key,
		node_id int foreign key references bik_node(id),
		group_node_id int foreign key references bik_node(id),
		leader_node_id int foreign key references bik_node(id),
		error_status int,
		created_date datetime,
		action_plan varchar(max) 
	);
go

exec sp_update_version '1.7.z5.13', '1.7.z5.14';
go