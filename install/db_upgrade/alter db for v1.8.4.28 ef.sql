﻿exec sp_check_version '1.8.4.28';
GO

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideRightsBtn'
	,'true'
	,1
	)

INSERT INTO bik_app_prop (
	name
	,val
	,is_editable
	)
VALUES (
	'hideHelpForAttr'
	,'true'
	,1
	)

exec sp_update_version '1.8.4.28'
	,'1.8.4.29';
GO


