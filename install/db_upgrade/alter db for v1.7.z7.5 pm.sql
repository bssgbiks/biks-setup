exec sp_check_version '1.7.z7.5';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_verticalize_node_attrs_one_table]') and type in (N'P', N'PC'))
drop procedure [dbo].sp_verticalize_node_attrs_one_table
go

-- poprzednia wersja w "alter db for v1.1.7.8 bf.sql"
create procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania
  declare @sub int = 1
  declare @row_cnt int
  declare @log_msg varchar(max);

  if @sub > 0 begin	
    create table #updateTable (node_id int, branch_ids varchar(max), act int, parent_node_id int);	
  end
  -- diag
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': start, @source=' + @source; exec sp_log_msg @log_msg; end;

  --------------
  -- 1. rozbijamy nazwy atrybut�w - nazwy kolumn w �r�dle i nazwy w�a�ciwe (opcjonalne)
  --------------

  --declare @ attrPropNamesTab table (idx int primary key, name sysname not null)
  create table #vnaot_attrPropNamesTab (idx int primary key, name sysname not null);

  insert into #vnaot_attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  /*declare @ attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )*/

  create table #vnaot_attrNamesTab (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  );
  
  insert into #vnaot_attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join #vnaot_attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)

  --------------
  -- 2. znamy nazwy atrybut�w i node_kindy, teraz uzupe�niamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupe�nieniem searchable attrs'; exec sp_log_msg @log_msg; end;
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from #vnaot_attrNamesTab ant
  where ant.attr_id is null  
  
  update #vnaot_attrNamesTab set attr_id = a.id
  from #vnaot_attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.proper_name
  
  --------------
  -- 3. wrzucamy warto�ci atrybut�w do tabeli pomocniczej
  --------------

  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do #vnaot_attrValsTab'; exec sp_log_msg @log_msg; end;
  
  /*declare @ attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id))
  */

  create table #vnaot_attrValsTab (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id));
  
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from #vnaot_attrNamesTab
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed open attrCur'; exec sp_log_msg @log_msg; end;

  if @diags_level > 0 begin
    select name, --attr_type, 
    proper_name, search_weight, attr_id from #vnaot_attrNamesTab
  end

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'; exec sp_log_msg @log_msg; end;

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed p�tl�'; exec sp_log_msg @log_msg; end;

  while @@fetch_status = 0
  begin
    if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': start p�tli dla pola: ' + @attrName + ' (' + @attrProperName + ')'; exec sp_log_msg @log_msg; end;
    
    set @attrValSql = 
    'select attr_id, node_id, /*case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end*/ value, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x where not (is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''')'
    if @diags_level > 0 begin set @log_msg = 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql; exec sp_log_msg @log_msg; end;
  
    insert into #vnaot_attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed ko�cem iteracji p�tli dla pola: ' + @attrName; exec sp_log_msg @log_msg; end;
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wyci�gni�tych warto�ci
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from #vnaot_attrValsTab)
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': za p�tl�, kursor zamkni�ty, liczba warto�ci razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'; exec sp_log_msg @log_msg;
  end
  
  declare @attrIdsStr varchar(max) = ''
  update #vnaot_attrNamesTab set @attrIdsStr = @attrIdsStr + ',' + cast(attr_id as varchar(20))
  set @attrIdsStr = substring(@attrIdsStr, 2, len(@attrIdsStr))
  
  declare @attrValsInRangeSql varchar(max) = 'select av.id, av.node_id, av.attr_id 
  from bik_searchable_attr_val av' + case when @optNodeFilter is null then '' else ' inner join bik_node n on n.id = av.node_id and ' + @optNodeFilter end +
  ' where av.attr_id in (' + @attrIdsStr + ')'
  
  --declare @ attrValsInRange table (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id))
  create table #vnaot_attrValsInRange (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id));

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed insert into #vnaot_attrValsInRange, sql=' + @attrValsInRangeSql; exec sp_log_msg @log_msg;
  end 
  
  insert into #vnaot_attrValsInRange (id, node_id, attr_id)
  exec(@attrValsInRangeSql)
  
  declare @delCnt int
/*
  delete from bik_searchable_attr_val
  from #vnaot_attrValsTab avt 
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
*/

  --update bik_app_prop set val = '50' where name = 'suggestSimilar.maxResults'
  declare @register_bik_node_changes int = 0;

  if coalesce((select val from bik_app_prop where name = 'suggestSimilar.maxResults'), '0') <> '0'
    and exists (select 1 from bik_app_prop where name = 'indexing.fullData' and val in ('inProgress', 'done'))
    set @register_bik_node_changes = 1;

  --select @register_bik_node_changes

  if @register_bik_node_changes <> 0 begin

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': wrzucanie zmian do bik_node_change_detail'; exec sp_log_msg @log_msg;
    end 

    declare @change_id bigint;

    insert into bik_node_change (date_added) values (getdate());

    select @change_id = scope_identity();

    insert into bik_node_change_detail (change_id, node_id)
    select distinct @change_id, coalesce(av0.node_id, av.node_id)
    from 
      bik_searchable_attr_val av0 inner join #vnaot_attrValsInRange avir on av0.id = avir.id
      full outer join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
    where av0.id is null or av.node_id is null or av0.value <> av.value or av0.tree_id <> av.tree_id or av0.node_kind_id <> av.node_kind_id;
  end;

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed w�a�ciwym delete'; exec sp_log_msg @log_msg;
  end 
   
  if @sub > 0 begin
  ------------- 
	--Subskrypcje- usuni�te 
	-------------
	-- gdy usuwa si� atrybut (warto�� na null lub pust�) to wrzucamy do #updateTable
	-- node_id, 0
	-- mo�liwe duplikaty
	 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje usuniete'; exec sp_log_msg @log_msg; end;
	insert into #updateTable (node_id, act) 
	select bik_searchable_attr_val.node_id, 0
	from bik_searchable_attr_val join #vnaot_attrValsInRange avir on  bik_searchable_attr_val.id = avir.id left join #vnaot_attrValsTab av 
	on avir.attr_id = av.attr_id and avir.node_id = av.node_id
	where bik_searchable_attr_val.id = avir.id and av.node_id is null
	
  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ':Usuni�te insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
	-------------
  end
  
  delete from bik_searchable_attr_val
  from #vnaot_attrValsInRange avir left join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where bik_searchable_attr_val.id = avir.id and av.node_id is null
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'; exec sp_log_msg @log_msg;
  end
  
  if @sub > 0 begin
	--Subskrypcje -- zaktualizowane
	-------------
	-- zn�w dorzucamy z act = 0
	-- node_id, 0 -- gdy zmieni�a si� warto�� kt�rego� atrybutu (musia� ju� by� wcze�niej dodany taki atr.)
	-- mo�liwe duplikaty
	
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje aktualizacja'; exec sp_log_msg @log_msg; end;

	insert into #updateTable (node_id, act) 
	select avt.node_id, 0
	from #vnaot_attrValsTab avt join bik_searchable_attr_val on bik_searchable_attr_val.node_id = avt.node_id 
	     and bik_searchable_attr_val.attr_id = avt.attr_id
	where --ww: zb�dne: avt.value is not null and
	    --ww: zb�dne: avt.node_id not in(select node_id from #updateTable) and
	(bik_searchable_attr_val.value <> avt.value
    --ww: poni�sze do zastanowienia
    or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id)

  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:Zaktualizowane insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
  
	-------------
	end
		
  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight,
    tree_id = avt.tree_id , node_kind_id = avt.node_kind_id
  from #vnaot_attrValsTab avt
  where --ww: zb�dne: avt.value is not null and 
    bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight
     or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id )
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'; exec sp_log_msg @log_msg;
  end
  
  --------------
  -- 6. dorzucenie nowych warto�ci
  --------------
  
  if @sub > 0 begin
  ------------- 
	--Subskrypcje -- nowe lub zaktualizowana
	-------------
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa'; exec sp_log_msg @log_msg; end;
	declare @newAttrId int 
	select @newAttrId=id from bik_searchable_attr where name='name'

	-- nie by�o warto�ci, a teraz si� pojawi�a dla jakiego� atrybutu
	-- node_id, 0 lub 1 -> 1 gdy to atrybut name si� w�a�nie pojawi�
	insert into #updateTable (node_id, act) 
	select avt.node_id, --max(
	case when avt.attr_id = @newAttrId then 1 else 0 end--) 
	as act
	from #vnaot_attrValsTab avt left join bik_searchable_attr_val av on
	avt.attr_id = av.attr_id and avt.node_id = av.node_id
	where av.id is null 
	--ww: zb�dne: and avt.value is not null
	--ww: zb�dne, i tak s� duplikaty
	-- and avt.node_id not in (select node_id from #updateTable)
	--group by avt.node_id

 select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:Nowe lub zaktualizowana insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa koniec'; exec sp_log_msg @log_msg; end;
	-------------
	end
	
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from #vnaot_attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'; exec sp_log_msg @log_msg;
  end
  
  
  -- select * from #vnaot_attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --begin set @log_msg = @sql
  
  --select * from #vnaot_attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
  
   
    ------------- 
	--Subskrypcje
	------------- 
	if @sub > 0 begin
	
 if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :przed subskrypcj�: '; exec sp_log_msg @log_msg;
  end

    select ut.node_id, ut.node_id as parent_node_id, max(ut.act) as act
    into #updateTableNoDupl
    from #updateTable ut
	group by ut.node_id

  	--update #updateTable set branch_ids=(select branch_ids from bik_node where id=#updateTable.node_id),parent_node_id=node_id
  	
  	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:update #updateTable set branch_ids, row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
  
	drop table #updateTable  
  
	declare @cnt int
	set @cnt=1
	
	while @cnt>0 begin
	
	insert into bik_object_in_fvs_change (node_id, fvs_id, action_type) 
	select ub.node_id, fvs.id, ub.act
	from #updateTableNoDupl ub join bik_object_in_fvs fvs on ub.parent_node_id = fvs.node_id
	left join bik_object_in_fvs_change fc on fvs.id = fc.fvs_id and ub.node_id = fc.node_id
	where fc.id is null
	
	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' insert into bik_object_in_fvs_change(node_id,fvs_id,action_type) , row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end

	update #updateTableNoDupl
    --set parent_node_id = bn.parent_node_id
    set parent_node_id = (select parent_node_id from bik_node where id = #updateTableNoDupl.parent_node_id)
	--from bik_node bn
	--where bn.id = #updateTableNoDupl.parent_node_id
	
select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' update #updateTable, row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end

	delete from #updateTableNoDupl where parent_node_id is null

  select @row_cnt = @@rowcount

	select @cnt=count(*) from #updateTableNoDupl

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:na ko�cu p�tli, ile zosta�o: ' + cast(@cnt as varchar(20))+ ' po delete na #updateTableNoDupl, usuni�tych row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
    end

	end
	
	drop table #updateTableNoDupl
	
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:po p�tli, KONIEC'; exec sp_log_msg @log_msg;
  end

    end
end
go


/*

------------ badanie post�p�w wykonania ---------

with s as
(
select *
from log_msg lm
where --date_added > '2016.01.12 20:50'
  id >= 2047
  --id >= 740 and id < 2047
)
select
*, datediff(s, (select top 1 date_added from s where id < lm.id order by id desc), date_added) as duration_s,
  datediff(s, (select top 1 date_added from s where id < lm.id order by id), date_added) as total_duration_s
from s lm
order by lm.id

*/


-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z7.5', '1.7.z7.6';
go
