﻿exec sp_check_version '1.8.1.2';
go

if not exists (select 1 from bik_app_prop where name='jasperServer.reportEndPoint')
insert into bik_app_prop(name, val) values('jasperServer.reportEndPoint', '');

if not exists (select 1 from bik_app_prop where name='jasperServer.username')
insert into bik_app_prop(name, val) values('jasperServer.username', '');

if not exists (select 1 from bik_app_prop where name='jasperServer.password')
insert into bik_app_prop(name, val) values('jasperServer.password', '');

exec sp_update_version '1.8.1.2', '1.8.1.3';
