﻿exec sp_check_version '1.8.4.27';
go

declare @metaTreeKindId int = (select id from bik_tree_kind where code='metaBiks')
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksDefinitionGroup', 'metaBiksAttributesCatalog', 'Dziecko'
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksDefinitionGroup', 'metaBiksNodeKind', 'Dziecko'
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksDefinitionGroup', 'metaBiksTreeKind', 'Dziecko'

exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksAttributesCatalog', 'metaBiksAttributeDef', 'Dziecko'

exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksNodeKind', 'metaBiksAttribute4NodeKind', 'Dziecko'
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksBuiltInNodeKind', 'metaBiksAttribute4NodeKind', 'Dziecko'

exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksTreeKind', 'metaBiksNodeKind4TreeKind', 'Dziecko'
exec sp_add_node_kind_relation @metaTreeKindId, 'metaBiksNodeKind4TreeKind', 'metaBiksNodeKindInRelation', 'Dziecko'

exec sp_update_version '1.8.4.27', '1.8.4.28';
go 