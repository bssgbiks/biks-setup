﻿exec sp_check_version '1.8.3';
go

declare @treeOfTrees int = (select id from bik_tree where code='TreeOfTrees')
exec sp_node_init_branch_id @treeOfTrees, null

declare @rc int = 1
while @rc > 0 begin

update bn2 set is_deleted=1
from bik_node bn1 join bik_node bn2 on bn1.id=bn2.parent_node_id
where bn1.is_deleted=1 and bn2.is_deleted=0

set @rc = @@rowcount
end

update bik_node_kind set caption='Zakładka' where code='metaBiksMenuGroup'
update bik_node_kind set icon_name='bwarea' where code='metaBiksMenuItem'
update bik_node_kind set icon_name='SASLevel' where code='metaBiksMenuGroup'
 
exec sp_update_version '1.8.3', '1.8.3.1';
go