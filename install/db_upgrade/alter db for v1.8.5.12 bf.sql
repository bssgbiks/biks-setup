﻿exec sp_check_version '1.8.5.12';
go

if (object_id('trg_bik_node_history') is not null)
drop trigger trg_bik_node_history
go

create trigger trg_bik_node_history on bik_node 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @node_status table (
		node_id int,
		old_is_deleted int,
		new_is_deleted int
	)
	insert into @node_status(node_id, old_is_deleted, new_is_deleted)
	select i.id, d.is_deleted, i.is_deleted
	from inserted i join deleted d on i.id = d.id
 
	--fizycznie insert
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id not in (select node_id from @node_status)
	
	--insert przez set is_deleted=0 a byl is_deleted=1
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id in (select node_id from @node_status where old_is_deleted = 1 and new_is_deleted = 0)

	insert into bik_node_history(node_id, name, descr, change_source, modified_date) 
	select i.id, i.name, i.descr, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select node_id from @node_status where old_is_deleted = 0 and new_is_deleted = 0)
	and (coalesce(i.name, '') <> coalesce(d.name, '') or coalesce(i.descr, '') <> coalesce(d.descr, ''))

	insert into bik_node_history(node_id,name, change_source, modified_date)
	select id,'Obiekt usunięty', @change_source, @modified_date
	from deleted where id not in (select node_id from @node_status)
end
go


exec sp_update_version '1.8.5.12','1.8.5.13';
go

