﻿exec sp_check_version '1.7.z4';
go

if not exists (select * from bik_app_prop where name = 'dqmEvaluationStartDate')
begin
	insert into bik_app_prop(name, val, is_editable)
	values ('dqmEvaluationStartDate', '2015-01-01', 1)
end;
go

exec sp_update_version '1.7.z4', '1.7.z5';
go
