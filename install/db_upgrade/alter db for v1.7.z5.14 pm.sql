exec sp_check_version '1.7.z5.14';
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recreate_fn_is_custom_right_role_tree]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recreate_fn_is_custom_right_role_tree]
go

create procedure [dbo].[sp_recreate_fn_is_custom_right_role_tree] as
begin
	set nocount on;

  if object_id (N'dbo.fn_is_custom_right_role_tree', N'fn') is not null
    exec('drop function dbo.fn_is_custom_right_role_tree');

  declare @customRightRolesTreeSelector varchar(max) = (select val from bik_app_prop where name = 'customRightRolesTreeSelector');

  declare @sql_txt nvarchar(max) = null;

  if coalesce(@customRightRolesTreeSelector, '') = '' begin
    set @sql_txt = '1';
  end else begin

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ',' end + cast(tree_id as varchar(20))
    from (
    select distinct t.id as tree_id
    from dbo.fn_split_by_sep(@customRightRolesTreeSelector, ',', 7) x inner join bik_tree t on --t.code = x.str
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str
    ) x;

    set @sql_txt = 'case when @tree_id in (' + @sql_txt + ') then 1 else 0 end';
  end;

  set @sql_txt = 'create function dbo.fn_is_custom_right_role_tree(@tree_id int) returns int as begin return ' + @sql_txt + '; end';
  
  --print @sql_txt;
  exec(@sql_txt);
end;
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex]
go
		
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex] as
begin
	set nocount on;

  exec sp_recalculate_Custom_Right_Role_Tree_Selectors;

  exec sp_recreate_fn_is_custom_right_role_tree;

  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
end;
go

-----------------------------------------------------
-----------------------------------------------------
-----------------------------------------------------

exec sp_update_version '1.7.z5.14', '1.7.z5.15';
go
