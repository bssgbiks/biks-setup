﻿exec sp_check_version '1.8.5.13';
go

if (object_id('trg_bik_node_history') is not null)
drop trigger trg_bik_node_history
go

create trigger trg_bik_node_history on bik_node 
for insert, delete, update
as
begin
	set nocount on

	declare @log_level int = 1
	declare @tree_id int= (select id from bik_tree where code='Teradata')

	declare @change_source varchar(max) = (select coalesce(coalesce(su.name, su.login_name), ds.description) from bik_spid_source ss left join bik_system_user su on ss.user_id = su.id left join bik_data_source_def ds on ss.data_source_def_id = ds.id where ss.spid = @@spid)
	declare @modified_date datetime = sysdatetime()

	declare @node_status table (
		node_id int,
		old_is_deleted int,
		new_is_deleted int
	)
	insert into @node_status(node_id, old_is_deleted, new_is_deleted)
	select i.id, d.is_deleted, i.is_deleted
	from inserted i join deleted d on i.id = d.id 
	where i.tree_id!=@tree_id  and d.tree_id!=@tree_id 
 
	--fizycznie insert
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id not in (select node_id from @node_status)
	and tree_id!=@tree_id 
	
	--insert przez set is_deleted=0 a byl is_deleted=1
	insert into bik_node_history(node_id, name, descr, change_source, modified_date)
	select id, name, descr, @change_source, @modified_date
	from inserted where id in (select node_id from @node_status where old_is_deleted = 1 and new_is_deleted = 0)
	and tree_id!=@tree_id 

	insert into bik_node_history(node_id, name, descr, change_source, modified_date) 
	select i.id, i.name, i.descr, @change_source, @modified_date
	from inserted i join deleted d on i.id = d.id
	where i.id in (select node_id from @node_status where old_is_deleted = 0 and new_is_deleted = 0)
	and (coalesce(i.name, '') <> coalesce(d.name, '') or coalesce(i.descr, '') <> coalesce(d.descr, ''))
	and  i.tree_id!=@tree_id  and d.tree_id!=@tree_id 

	insert into bik_node_history(node_id, change_source, modified_date)
	select id, @change_source, @modified_date
	from deleted where id not in (select node_id from @node_status)
	and tree_id!=@tree_id 

	insert into bik_node_history(node_id,name, change_source, modified_date) 
	select node_id,'Obiekt usunięty', @change_source, @modified_date from @node_status where old_is_deleted = 0 and new_is_deleted = 1
end
go


if object_id('sp_generic_insert_into_bik_node') is not null 
drop procedure sp_generic_insert_into_bik_node
go

create procedure [dbo].[sp_generic_insert_into_bik_node](@treeCode varchar(max), @objId varchar(900), @serverObjId varchar(900))
as
begin
	-- założenia:
	-- branch_ids są uzupełnione w bik_node i amg_node (@objId is not null)
	-- jeśli @objId is not null wtedy musi istniec ten obiekt w bik_node (inny mechanizm go wstawil wczesniej)
	-- 
	declare @loglevel int = 0;
	declare @treeId int = (select id from bik_tree where code = @treeCode);
	--if (@forceDelete > 0 and @serverObjId is not null ) 
--	update bik_node set is_deleted=1 where tree_id=@treeId  and obj_id != @serverObjId and obj_id like @serverObjId+'%'
--	else if (@forceDelete > 0) update bik_node set is_deleted=1 where tree_id=@treeId
	
	exec sp_node_init_branch_id @treeId, null;
	
	declare @parentNodeId int = (select id from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @branchIds varchar(900) = (select branch_ids from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @amgBranchIds varchar(900) = (select branch_ids from amg_node where obj_id = @objId);

	if (@loglevel > 0) print 'Updating'
	-- update
	update bik_node 
	set is_deleted = 0, name = gen.name, descr = gen.descr, visual_order = gen.visual_order, node_kind_id = bnk.id
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.obj_id = gen.obj_id
	and bik_node.linked_node_id is null

	if (@loglevel > 0) print 'Insert'
	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @parentNodeId, bnk.id, gen.name, gen.descr, @treeId, gen.obj_id, gen.visual_order
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	left join (bik_node as bn inner join bik_tree as bt on bn.tree_id = bt.id and bn.tree_id = @treeId and (@branchIds is null or bn.branch_ids like @branchIds + '%')) on bn.obj_id = gen.obj_id
	where bn.id is null
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	order by gen.obj_id
	
	exec sp_node_init_branch_id @treeId, null;	

	if (@loglevel > 0) print 'Update parent'
	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join amg_node as gen on bik_node.obj_id = gen.obj_id
	inner join bik_node as bk on bk.obj_id = gen.parent_obj_id
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @treeId
	and bk.is_deleted = 0
	and bk.linked_node_id is null
	
	exec sp_node_init_branch_id @treeId, null;

	if (@loglevel > 0) print 'Delete'
	-- usunięcie zbędnych
	--if (@forceDelete > 0) begin
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join amg_node as gen on bik_node.obj_id = gen.obj_id 
		and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	where gen.name is null
	and bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bik_node.obj_id != @serverObjId and bik_node.obj_id like @serverObjId+'%'
	
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node as bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @treeId
	and (@branchIds is null or bn2.branch_ids like @branchIds + '%')
	and bn2.is_deleted = 1
	
	exec sp_node_init_branch_id @treeId, null
end;
go

exec sp_update_version '1.8.5.13','1.8.5.14';
go

