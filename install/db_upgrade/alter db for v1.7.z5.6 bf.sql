﻿exec sp_check_version '1.7.z5.6';
go

/* ***********
	1.Przełącznik w app_props czy stare role mają być widoczne
***********/
	merge into bik_app_prop ap 
	using (values ('disableBuiltInRoles', '0')) as v(name, val) on ap.name = v.name
	when matched then update set ap.val = v.val
	when not matched by target then insert (name, val) values (v.name, v.val);
	go
	
	if ((select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
	  or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' ) begin
	  update bik_app_prop set val='1' where name='disableBuiltInRoles';
	end;


/* ***********
	2.Aktualizacja AKCJI
***********/

	insert into bik_node_action (code)
	select x.code
	from
	  (values ('#admin:dict:attrDefs'),
	('#admin:dict:attrHints'),
	('#admin:dict:statistics'),
	('#admin:dict:sysUsers'),
	('#admin:dict:trees'),
	('#admin:dict:userstatistics'),
	('#admin:lisa:connection'),
	('#admin:lisa:logs'),
	('#admin:lisa:metadata'),
	('#admin:load:cfg'),
	('#admin:load:logs'),
	('#admin:load:runManual'),
	('#admin:load:schedule'),
	('#ExportImportTree'),
	('#HelpEditor'),
	('#NewsEditor'),
	('#OtherActionsSysAdmin'),
	('#OtherActionsAppAdmin'),
	('#ShowRefreshPageInConfluence'),
	('AddAnyNode'),
	('AddBlogEntry'),
	('AddBranch'),
	('AddComment'),
	('AddCustomTreeNode:dtk_MechanizmKontrolny'),
	('AddCustomTreeNode:dtk_Regula'),
	('AddDefinition'),
	('AddDocument'),
	('AddJoinedObjs'),
	('AddLeaf'),
	('AddLisaCategorization'),
	('AddLisaCategory'),
	('AddObjectFromConfluence'),
	('AddObjectToConfluence'),
	('AddQuestionAndAnswer'),
	('AddRole'),
	('AddUser'),
	('AddVersionDefinition'),
	('ChangeToBranch'),
	('ChangeToLeaf'),
	('Copy'),
	('CopyJojnedObj'),
	('Cut'),
	('Delete'),
	('DeleteRole'),
	('DelLisaCategory'),
	('DownloadDocument'),
	('DownloadErrorDqmData'),
	('DQMAddEvaluation'),
	('DQMAddReport'),
	('DQMAddTest'),
	('DQMCheckRate'),
	('DQMDeleteTest'),
	('DQMDownloadReport'),
	('DQMEditEvaluation'),
	('DQMEditTest'),
	('DQMRefreshReport'),
	('EditAnyComment'),
	('EditBlogEntry'),
	('EditDefinition'),
	('EditDescription'),
	('EditNodeDetails'),
	('EditUser'),
	('JumpToLinkedNode'),
	('LinkNode'),
	('LinkUser'),
	('LinkUserToRole'),
	('LinkUserToRoleForBlogs'),
	('MoveInTree'),
	('MoveLisaCategory'),
	('NewNestableParent'),
	('Paste'),
	('Rename'),
	('RenameRole'),
	('ResetVisualOrder'),
	('ShowAnticipatedObjLinkDialog'),
	('ShowDateAndAuthor'),
	('ShowInTree'),
	('ShowRefreshPageInConfluence'),
	('VoteForUsefulness')
	) x(code) left join bik_node_action na on x.code = na.code
	where na.code is null;
	go

/* ************
	DLA CP i Pol
*************/
	if ((select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880') 
	begin
		
		/*Widoczność zakładek*/
		update bik_app_prop set val='true' where  name='disableGuestMode' ;
/* ************
	3.Nowe role 
*************/
--- RegularUser 

		merge into bik_custom_right_role ap 
		using (select 'RegularUser',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
		if not exists (select * from bik_translation where code='AppAdmin' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('RegularUser','Zwykły użytkownik','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('RegularUser','User','en','crr');
		end;
		
--- Administrator
		merge into bik_custom_right_role ap 
		using (select 'Administrator',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		

		if not exists (select * from bik_translation where code='Administrator' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('Administrator','Administrator','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('Administrator','Admin','en','crr');
		end		
		
--- Admin Aplikacji
		merge into bik_custom_right_role ap 
		using (select 'AppAdmin',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
		if not exists (select * from bik_translation where code='AppAdmin' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('AppAdmin','Administrator aplikacji','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('AppAdmin','App Admin','en','crr');
		end;

--- Ekspert CP

		merge into bik_custom_right_role ap 
		using (select 'ExpertCP',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);

		if not exists (select * from bik_translation where code='ExpertCP' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('ExpertCP','Ekspert Cyfrowy Polsat','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('ExpertCP','Expert Cyfrowy Polsa','en','crr');
		end;
		
--- Ekspert Pol
		merge into bik_custom_right_role ap 
		using (select 'ExpertPol',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
		
		if not exists (select * from bik_translation where code='ExpertPol' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('ExpertPol','Ekspert Polkomtel','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('ExpertPol','Expert Polkomtel','en','crr');
		end;

--- Autor		
		merge into bik_custom_right_role ap 
		using (select 'Author',coalesce(max(visual_order),0)+1,'',1 from bik_custom_right_role) as v(code,visual_order,tree_selector,selector_mode) on ap.code = v.code
		when not matched by target then insert (code,visual_order,tree_selector,selector_mode) values (v.code,v.visual_order,v.tree_selector,v.selector_mode);
		
		
			if not exists (select * from bik_translation where code='Author' and kind='crr')
		begin
			insert into bik_translation (code, txt, lang,kind) values('Author','Autor','pl','crr');
			insert into bik_translation (code, txt, lang,kind) values('Author','Author','en','crr');
		end;
	end;
	go
---------------------------------------
/* *****
	4.AKTUALIZACJA procedury uzupelniajacej customRightRolesTreeSelector i treeSelectory dla ról Administrator, Admin Aplikacji i Autor
	wyzej
	*****/		
		
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_PB]') AND type in (N'P', N'PC'))
		DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_PB]
		GO

			
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL]') AND type in (N'P', N'PC'))
		DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL]
		GO
		
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]') AND type in (N'P', N'PC'))
		DROP PROCEDURE [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]
		GO

		
		create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_PB] as
		begin
		  set nocount on;
		  
		  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
		  if @regUserCrrId is null return;
		  
		  declare @tree_selector_codes_all varchar(max) = '';
		  declare @tree_selector_codes_ru varchar(max) = '';
		  
		  select 
			@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
			@tree_selector_codes_ru = case when code in ('DQM', 'DQMDokumentacja') then @tree_selector_codes_ru 
			  else case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code end 
		  from bik_tree where code not in (/*'DQM', 'DQMDokumentacja',*/ 'TreeOfTrees') and is_hidden = 0;
		  
		  -- select * from bik_custom_right_role
		  
		  update bik_app_prop set val = @tree_selector_codes_all where name = 'customRightRolesTreeSelector';
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
		  
		  -- select * from bik_app_prop  
		end;
		go


		create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL] as
		begin
		  set nocount on;
		  
		  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
		  if @regUserCrrId is null return;
		  
		  declare @tree_selector_codes_all varchar(max) = '';
		  declare @tree_selector_codes_ru varchar(max) = '';
		  
		  select 
			@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
			@tree_selector_codes_ru =  case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code  
		  from bik_tree where code not in ( 'TreeOfTrees') and is_hidden = 0;
		  
		  update bik_app_prop set val = @tree_selector_codes_all+','+ 'TreeOfTrees' where name = 'customRightRolesTreeSelector';
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_all+','+ 'TreeOfTrees' where code in('AppAdmin','Administrator','Author');
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
		end;
		go

		create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors] as
		begin
		  if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
			or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' 
			begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
			end else if (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA' begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_PB;
			end;
		end;
		go		
		/* tymczasowa procedura */
		if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_upgrade_role]') and type in (N'P', N'PC'))
			drop procedure [dbo].sp_upgrade_role
		go

		create procedure [dbo].[sp_upgrade_role](@cusromRoleCode  varchar(50),@oldRoleCode  varchar(50))
		as
		begin
			declare @cRoleId int;
			declare @oRoleId int;
			select @cRoleId=id from bik_custom_right_role where code=@cusromRoleCode;
			select @oRoleId=id from bik_right_role where code=@oldRoleCode;
			delete from bik_custom_right_role_user_entry where role_id=@cRoleId
			insert into bik_custom_right_role_user_entry(user_id,role_id,tree_id)
			select user_id,@cRoleId,null  from bik_user_right where right_id=@oRoleId;
		end;
		go
--------------------------------------------------------------------------------------------
	
	if ((select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
	or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880') 
		begin
/* *****
	5. Wykonanie procedury 
*****/	
		exec sp_recalculate_Custom_Right_Role_Tree_Selectors

/* *****
	6. Aktualizacja tree_selector dla Ekspertów
*****/	

		if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' begin 
			 update bik_custom_right_role set tree_selector = (select val from bik_app_prop where name = 'customRightRolesTreeSelector')
			 where code in('ExpertPol');
		end else begin
			 update bik_custom_right_role set tree_selector = (select val from bik_app_prop where name = 'customRightRolesTreeSelector')
			 where code in('ExpertCP');
		end;

/* *****
	7. Podpięcie akcji do ról
*****/	

		declare @roleId int;
--- Administrator
		select @roleId=id from  bik_custom_right_role where code='Administrator';
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action  bna where not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )
--- Admin Aplikacji
		select @roleId=id from  bik_custom_right_role where code='AppAdmin';
		insert into bik_node_action_in_custom_right_role(action_id,role_id)
		select id,@roleId from bik_node_action bna
		where code not in('#admin:load:logs', '#admin:load:runManual','#admin:load:schedule', '#admin:load:cfg','#admin:dict:statistics','#admin:dict:userstatistics','admin:dqm:logs'
		,'admin:lisa:metadata','#admin:lisa:connection','#admin:lisa:logs','#OtherActionsSysAdmin')
		and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) );		
--- Ekspert  Pol lub Cp		
		if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' begin 
			select @roleId=id from  bik_custom_right_role where code='ExpertPol';
			insert into bik_node_action_in_custom_right_role (action_id,role_id)
			select id,@roleId from bik_node_action bna where code not like '#%' and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )
		end else begin
			select @roleId=id from  bik_custom_right_role where code='ExpertCP';
			insert into bik_node_action_in_custom_right_role (action_id,role_id)
			select id,@roleId from bik_node_action bna where code not like '#%' and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )
		end;
--- Autor
		select @roleId=id from  bik_custom_right_role where code='Author';
		insert into bik_node_action_in_custom_right_role (action_id,role_id)
		select id,@roleId from bik_node_action bna where code not like '#%' and not exists (select  action_id,role_id from bik_node_action_in_custom_right_role 
		where  (action_id=bna.id  and role_id=@roleId) )

----------------------
/* *****
	8. Przepinanie starych ról na nowe: 
	(Nie usuwam starych!)
*****/	
--- Administrator
		exec sp_upgrade_role 'Administrator','Administrator';
--- Admin Aplikacji
		exec sp_upgrade_role 'AppAdmin','AppAdmin';

--- Ekspert Pol	
		if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' begin 
			exec sp_upgrade_role 'ExpertPol','Expert';
		end;

--- Ekspert CP	
		if (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' begin 
			exec sp_upgrade_role 'ExpertCp','Expert';
		end;

--- Autor
		declare @customRoleId int;
		select @customRoleId=id from bik_custom_right_role where code='Author';
		declare @odlRoleId int;
		select @odlRoleId=id from bik_right_role where code='Author';
		delete from bik_custom_right_role_user_entry where role_id=@customRoleId
		insert into bik_custom_right_role_user_entry(user_id,role_id,tree_id)
		select user_id,@customRoleId,tree_id  from bik_user_right where right_id=@odlRoleId 
	end;
	
	/*usunięcie tymczasowe procedury */
	
	if exists (select * from sys.objects where object_id = OBJECT_ID(N'[dbo].[sp_upgrade_role]') and type in (N'P', N'PC'))
			drop procedure [dbo].sp_upgrade_role
		go
exec sp_update_version '1.7.z5.6', '1.7.z5.7';
go
