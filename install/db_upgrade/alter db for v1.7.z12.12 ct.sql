﻿exec sp_check_version '1.7.z12.12';
go

insert into bik_app_prop(name, val) 
select 'useDrools', 'false' where not exists (select 1 from bik_app_prop where name='useDrools')

insert into bik_app_prop(name, val) 
select 'lastCheckedChangeIdByDrools', '0' where not exists (select 1 from bik_app_prop where name='lastCheckedChangeIdByDrools')

--Dodanie Drools
declare @treeOfTrees int = (select id from bik_tree where code = 'TreeOfTrees');
if not exists (select 1 from bik_node where tree_id = @treeOfTrees and obj_id='admin:drools')
begin
	exec sp_add_menu_node 'admin', 'Zarządzanie regułami', 'admin:drools'
	exec sp_add_menu_node 'admin:drools', 'Konfiguracje', '#admin:drools:cfg'
	
	insert into bik_translation(code, txt, lang, kind) values ('admin:drools', 'Rules management', 'en', 'node')
	insert into bik_translation(code, txt, lang, kind) values ('#admin:drools:cfg', 'Configurations', 'en', 'node')
	
	insert into bik_node_action(code) values ('#admin:drools:cfg')
	
	insert into bik_node_action_in_custom_right_role (action_id, role_id)
	select (select id from bik_node_action where code = '#admin:drools:cfg'), id 
	from bik_custom_right_role where code in ('Administrator', 'AppAdmin')
	
	exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants
end

exec sp_node_init_branch_id  @treeOfTrees, null
 
exec sp_update_version '1.7.z12.12', '1.8';
go