﻿exec sp_check_version '1.8.4.18';
go


if not exists (select * from bik_admin where param='ad.organizationalUnitFilter')
     insert into bik_admin (param,value)
     values ('ad.organizationalUnitFilter','(|(objectCategory=organizationalUnit)(objectclass=domain));')
else 
	update bik_admin set value='(|(objectCategory=organizationalUnit)(objectclass=domain));' where param='ad.organizationalUnitFilter'


if not exists (select * from bik_admin where param='ad.usersFilter')
     insert into bik_admin (param,value)
     values ('ad.usersFilter','(&(objectCategory=person)(objectclass=user)(!(objectclass=inetOrgPerson)));')
else 
	update bik_admin set value='(&(objectCategory=person)(objectclass=user)(!(objectclass=inetOrgPerson)));' where param='ad.usersFilter'

exec sp_update_version '1.8.4.18', '1.8.4.19';
go
