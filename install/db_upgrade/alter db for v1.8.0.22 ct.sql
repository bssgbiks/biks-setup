exec sp_check_version '1.8.0.22';
go

if not exists (select * from sys.columns where object_id=object_id('bik_lisa_dict_column') and name='is_latin')  
alter table bik_lisa_dict_column add is_latin int default 0;
go
 
exec sp_update_version '1.8.0.22', '1.8.0.23';
go
