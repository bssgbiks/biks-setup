﻿exec sp_check_version '1.8.2.3';
go
 
declare @metaCategory int = (select id from bik_attr_category where name='Metadata BIKS')
exec sp_add_attr_def 'metaBIKS.Typ relacji', @metaCategory, 'combobox', 'Powiązanie,Dowiązanie,Hyperlink,Dziecko', 0, 0 
exec sp_add_attr_def 'metaBIKS.Nazwa typu drzewa', @metaCategory, 'shortText', null, 0, 0
exec sp_add_attr_2_node_kind 'metaBiksTreeKind', 'metaBIKS.Nazwa typu drzewa', null, 1, null, null
update bik_attr_def set name='metaBIKS.Nazwa typu obiektu' where name='metaBIKS.Wyświetlona nazwa'

update attr set is_deleted = 1
from bik_attribute attr join bik_attr_def adef on attr.attr_def_id=adef.id join bik_node_kind nk on nk.id=attr.node_kind_id
where adef.name='metaBIKS.Nadpisany typ atrybutu' 
or (adef.name='metaBIKS.Lista wartości' and nk.code='metaBiksAttribute4NodeKind')
or (adef.name='metaBIKS.Nazwa typu obiektu' and nk.code='metaBiksTreeKind')
 


declare @attrId int = (select attr.id 
	from bik_attribute attr join bik_attr_def adef on adef.id = attr.attr_def_id 
	join bik_node_kind nk on nk.id=attr.node_kind_id
	where adef.name='metaBIKS.Nazwa typu drzewa' and nk.code='metaBiksTreeKind')
 
update al set attribute_id=@attrId
from bik_attribute_linked al join bik_attribute attr on al.attribute_id=attr.id join bik_attr_def adef on adef.id=attr.attr_def_id 
join bik_node bn on bn.id=al.node_id join bik_node_kind nk on nk.id=bn.node_kind_id
where adef.name='metaBIKS.Nazwa typu obiektu' and nk.code='metaBiksTreeKind'

update al set is_deleted = 1
from bik_attribute_linked al join bik_attribute attr on attr.id=al.attribute_id
where attr.is_deleted=1

exec sp_update_version '1.8.2.3', '1.8.2.4';
go