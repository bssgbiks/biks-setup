﻿exec sp_check_version '1.8.1.1';
go

if  not exists (select 1 from sys.all_columns where object_id=object_id('bik_node_kind_4_tree_kind') and name='is_deleted')
begin
alter table bik_node_kind_4_tree_kind add is_deleted int default 0;--not null
end
go

exec sp_update_version '1.8.1.1', '1.8.1.2';
