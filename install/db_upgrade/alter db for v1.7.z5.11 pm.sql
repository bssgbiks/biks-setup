exec sp_check_version '1.7.z5.11';
go

------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------


if object_id (N'dbo.fn_is_custom_right_role_tree', N'fn') is not null
    drop function dbo.fn_is_custom_right_role_tree;
go

create function dbo.fn_is_custom_right_role_tree(@tree_id int)
returns int
as
begin
  return case when @tree_id in (4,6,8,12,13,14,15,17,19,21,22,29,30,31,33,34,35,36,38,39) then 1 else 0 end;
end;
go

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recreate_fn_is_custom_right_role_tree]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recreate_fn_is_custom_right_role_tree]
go

		
create procedure [dbo].[sp_recreate_fn_is_custom_right_role_tree] as
begin
	set nocount on;
  
  if object_id (N'dbo.fn_is_custom_right_role_tree', N'fn') is not null
    exec('drop function dbo.fn_is_custom_right_role_tree');

  declare @customRightRolesTreeSelector varchar(max) = (select val from bik_app_prop where name = 'customRightRolesTreeSelector');

  declare @sql_txt nvarchar(max) = null;

  if coalesce(@customRightRolesTreeSelector, '') = '' begin
    set @sql_txt = '1';
  end else begin

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ',' end + cast(tree_id as varchar(20))
    from (
    select distinct t.id as tree_id
    from dbo.fn_split_by_sep(@customRightRolesTreeSelector, ',', 7) x inner join bik_tree t on t.code = x.str
    ) x;

    set @sql_txt = 'case when @tree_id in (' + @sql_txt + ') then 1 else 0 end';
  end;

  set @sql_txt = 'create function dbo.fn_is_custom_right_role_tree(@tree_id int) returns int as begin return ' + @sql_txt + '; end';
  
  --print @sql_txt;
  exec(@sql_txt);
end;
go

-- exec sp_recreate_fn_is_custom_right_role_tree;
-- exec sp_helptext 'dbo.fn_is_custom_right_role_tree'

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------


if object_id (N'dbo.fn_node_has_no_children', N'fn') is not null
    drop function dbo.fn_node_has_no_children;
go

create function dbo.fn_node_has_no_children(@node_id int, @action_id int, @user_id int)
returns int
as
begin
/*
  declare @crr_tree_ids table (tree_id int not null primary key);

  insert into @crr_tree_ids (tree_id)
  select distinct t.id 
  from dbo.fn_split_by_sep((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), ',', 7) x inner join bik_tree t on t.code = x.str;
*/

  return case when /*bn.disable_linked_subtree = 0 and*/ exists
        (select 1 from bik_node cbn 
--with (index (idx_bik_node_is_deleted_parent_node_id)) 
--with (index (idx_bik_node_parent_deleted_tree_linked_branch_x)) 

where is_deleted = 0 and parent_node_id = @node_id --coalesce(bn.linked_node_id, bn.id)
        
        
        
        --/*
and 
       
    
    
        
        ( --cbn.tree_id not in (select tree_id from @crr_tree_ids) --(4,6,8,12,13,14,15,17,19,21,22,29,30,31,33,34,35,36,38,39) 
          dbo.fn_is_custom_right_role_tree(cbn.tree_id) = 0
         or
        (
        cbn.linked_node_id is null and
        (
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = cbn.tree_id and cbn.branch_ids like nbr.branch_ids + '%')
        or
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = cbn.tree_id and nbr.branch_ids like cbn.branch_ids + '%')
        )
        ) or
        (
        cbn.linked_node_id is not null and
        (
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and lbn.branch_ids like nbr.branch_ids + '%'
        where lbn.id = cbn.linked_node_id)
        or
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and nbr.branch_ids like lbn.branch_ids + '%'
        where lbn.id = cbn.linked_node_id)
        )
        )
        )
    --*/

        
        )
        then 0 else 1 end;
end;
go


--------------------------------------------------
--------------------------------------------------
--------------------------------------------------


if object_id (N'dbo.fn_user_has_right_for_action_on_node', N'fn') is not null
    drop function dbo.fn_user_has_right_for_action_on_node;
go

create function dbo.fn_user_has_right_for_action_on_node(@user_id int, @action_id int, @tree_id int, @linked_node_id int, @branch_ids varchar(1000))
returns int
as
begin
  return case when dbo.fn_is_custom_right_role_tree(@tree_id) = 0
         or
        (
        @linked_node_id is null and
        (
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = @tree_id and @branch_ids like nbr.branch_ids + '%')
        or
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = @tree_id and nbr.branch_ids like @branch_ids + '%')
        )
        ) or
        (
        @linked_node_id is not null and
        (
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and lbn.branch_ids like nbr.branch_ids + '%'
        where lbn.id = @linked_node_id)
        or
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and nbr.branch_ids like lbn.branch_ids + '%'
        where lbn.id = @linked_node_id)
        )
        )        
  then 1 else 0 end;
end;
go


------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------

-- select * from dbo.fn_index_info('bik_node')


if  exists (select * from sys.indexes where object_id = object_id(N'[dbo].[bik_node]') and name = N'idx_bik_node_parent_deleted_tree_linked_branch')
	drop index idx_bik_node_parent_deleted_tree_linked_branch on bik_node;
go

create index idx_bik_node_parent_deleted_tree_linked_branch on [dbo].[bik_node] ([parent_node_id],[is_deleted])
  include ([tree_id],[linked_node_id],[branch_ids])
go


------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------


if exists (select * from sys.objects where object_id = object_id(N'[dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex]') and type in (N'p', N'pc'))
drop procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex]
go

		
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex] as
begin
	set nocount on;

  exec sp_recalculate_Custom_Right_Role_Tree_Selectors;

  exec sp_recreate_fn_is_custom_right_role_tree;
end;
go


------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------


exec [sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex];
go

-- exec sp_helptext 'dbo.fn_is_custom_right_role_tree'


------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------


if object_id (N'dbo.fn_is_unconnected_user_node_or_has_unconnected_user_subnode', N'fn') is not null
    drop function dbo.fn_is_unconnected_user_node_or_has_unconnected_user_subnode;
go

create function dbo.fn_is_unconnected_user_node_or_has_unconnected_user_subnode(@id int, @node_kind_id int, @linked_node_id int, @branch_ids varchar(1000), @tree_id int)
returns int
as
begin

  declare @user_node_kind_id int = (select id from bik_node_kind where code='User');
 
  return case when

 (
      @node_kind_id = @user_node_kind_id
  
      and @id not in (select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id) 

      and (@linked_node_id is null 

          or @linked_node_id not in(select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id)
      ) 
   ) 

   or @node_kind_id <> @user_node_kind_id and exists(

      select bndi.id from bik_node bndi /* !!! >> */ 
    --with (index (idx_bik_node_tree_id_is_deleted))
/*with (index (idx_bik_node_branch_ids))*/  /* << !!! */ inner join bik_node_kind bnkdi on bndi.node_kind_id=bnkdi.id 
      where bndi.branch_ids like @branch_ids + '%' 
        and bndi.id not in (select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id) 
        and (bndi.linked_node_id is null or bndi.linked_node_id not in(select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id)) 
        and bnkdi.code = 'User' and bndi.is_deleted = 0 

        and bndi.tree_id = @tree_id

   )
  then 1 else 0 end;

end;
go


/*
select * from bik_node 
where
  is_deleted = 0 and parent_node_id = 866095  and dbo.fn_is_unconnected_user_node_or_has_unconnected_user_subnode(id, node_kind_id, linked_node_id, branch_ids, tree_id) = 1;
*/


------------------------------------------------------------
------------------------------------------------------------
------------------------------------------------------------


exec sp_update_version '1.7.z5.11', '1.7.z5.12';
go
