﻿exec sp_check_version '1.8.0.27';
go

if not exists (select 1 from bik_app_prop where name = 'mustHaveSocialUser')
insert into bik_app_prop(name, val) values ('mustHaveSocialUser', '0')
else update bik_app_prop set val = '0' where name = 'mustHaveSocialUser'
 
exec sp_update_version '1.8.0.27', '1.8.0.28';
go