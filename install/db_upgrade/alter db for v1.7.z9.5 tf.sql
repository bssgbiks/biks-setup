﻿exec sp_check_version '1.7.z9.5';


if not exists(select * from bik_app_prop where name = 'typeMapping_TeradataDBMS')
begin
	insert into bik_app_prop(name, val) values ('typeMapping_TeradataDBMS', '  VARCHAR2=varchar(%{prec}%)  NVARCHAR2=nvarchar(%{prec}%)  NUMBER=<f:if cond="prec==0 || scale < 0">float</f:if><f:else>numeric(%{prec}%, %{scale}%)</f:else>  DATE=date  CHAR=char(%{prec}%)  NCHAR=nchar(%{prec}%)  LONG=VARCHAR(MAX)   RAW=VARBINARY(%{prec}%)   BLOB=VARBINARY(MAX)   CLOB=VARCHAR(MAX)   FLOAT=FLOAT  REAL=FLOAT   INT=NUMERIC(38)   BINARY_FLOAT=FLOAT   BINARY_DOUBLE=FLOAT(53)   BFILE=VARBINARY(MAX)   DATE=DATETIME   INTERVAL=DATETIME   LONG\u0020RAW=IMAGE   ROWID=CHAR(18)   TIMESTAMP=DATETIME   UROWID=CHAR(18)   ')
end
go   

exec sp_update_version '1.7.z9.5', '1.7.z10';
go
