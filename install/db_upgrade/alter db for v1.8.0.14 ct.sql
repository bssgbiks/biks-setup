﻿exec sp_check_version '1.8.0.14';
go

if OBJECT_ID (N'dbo.fn_tree_id_by_code', N'FN') is not null
drop function dbo.fn_tree_id_by_code;
go

create function dbo.fn_tree_id_by_code(@code varchar(256))
returns int
as
begin
  return (select id from bik_tree where code = @code and is_hidden=0)
end
go

if OBJECT_ID (N'dbo.fn_node_id_by_tree_code_and_obj_id', N'FN') is not null
drop function dbo.fn_node_id_by_tree_code_and_obj_id;
go

create function [dbo].[fn_node_id_by_tree_code_and_obj_id](@tree_code varchar(256), @obj_id varchar(900))
returns int
as
begin
  return (select id from bik_node where tree_id = dbo.fn_tree_id_by_code(@tree_code) and is_deleted = 0 and obj_id=@obj_id)
end
go

exec sp_update_version '1.8.0.14', '1.8.0.15';
go
