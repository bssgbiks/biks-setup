﻿/****** Object:  Table [dbo].[bik_attr_category]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attr_category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[is_deleted] [int] NOT NULL,
	[old_name] [varchar](255) NULL,
	[is_built_in] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_attr_category_name] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[is_built_in] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_attr_def]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attr_def](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[attr_category_id] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
	[is_built_in] [int] NOT NULL,
	[hint] [varchar](max) NULL,
	[type_attr] [varchar](30) NULL,
	[value_opt] [varchar](max) NULL,
	[display_as_number] [int] NOT NULL,
	[visual_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_attr_def_name] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[is_built_in] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_kind]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_kind](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](255) NOT NULL,
	[caption] [varchar](255) NOT NULL,
	[icon_name] [varchar](255) NULL,
	[tree_kind] [varchar](255) NOT NULL,
	[is_folder] [int] NOT NULL,
	[search_rank] [int] NOT NULL,
	[is_leaf_for_statistic] [int] NOT NULL,
	[children_kinds] [varchar](900) NULL,
	[children_attr_names] [varchar](max) NULL,
	[attr_calculating_types] [varchar](max) NULL,
	[attr_calculating_expressions] [varchar](max) NULL,
	[attr_dependent_nodes_sql] [varchar](max) NULL,
	[uploadable_children_kinds] [varchar](max) NULL,
	[generic_node_select] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_attribute]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attribute](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_kind_id] [int] NOT NULL,
	[attr_def_id] [int] NULL,
	[is_deleted] [int] NOT NULL,
	[default_value] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_attr_def_id_node_kind_id] UNIQUE NONCLUSTERED 
(
	[attr_def_id] ASC,
	[node_kind_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_split_by_sep]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- @trimOpt = 0 -> do not trim substr
--          = 1 -> do ltrim substr
--          = 2 -> do rtrim substr
--          = 3 -> do full trim substr
--          & 4 <> 0 -> skip empty substr
CREATE FUNCTION [dbo].[fn_split_by_sep] (@str varchar(max), @sep varchar(max), @trimOpt int)
RETURNS @tab TABLE (str varchar(max) not null, idx int not null primary key)
AS
BEGIN
  declare @pos int = 1;
  declare @len int = DATALENGTH(@str);
  declare @seplen int = DATALENGTH(@sep)
  declare @pos2 int
  declare @substr varchar(max)
  declare @idx int = 1
  
  while @pos <= @len begin
    set @pos2 = charindex(@sep, @str, @pos)
    
    if @pos2 = 0 set @pos2 = @len + 1
    
    set @substr = substring(@str, @pos, @pos2 - @pos)
    
    if @trimOpt & 1 <> 0 set @substr = ltrim(@substr)
    if @trimOpt & 2 <> 0 set @substr = rtrim(@substr)
    
    if @trimOpt & 4 = 0 or DATALENGTH(@substr) <> 0 -- @substr <> '' - to nie zadziała dla spacji
    begin
      insert into @tab (str, idx) values (@substr, @idx)
      set @idx = @idx + 1
    end
    
    set @pos = @pos2 + @seplen
  end

  RETURN;
END;
GO
/****** Object:  Table [dbo].[bik_tree]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_tree](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[node_kind_id] [int] NULL,
	[code] [varchar](255) NOT NULL,
	[tree_kind] [varchar](255) NULL,
	[is_hidden] [int] NOT NULL,
	[is_in_ranking] [int] NOT NULL,
	[branch_level_for_statistics] [int] NOT NULL,
	[is_built_in] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_bik_tree_code] ON [dbo].[bik_tree] 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'drzewko tabelka' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_tree'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'Drzewka' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_tree'
GO
/****** Object:  UserDefinedFunction [dbo].[fn_split_string]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_split_string] (@str varchar(max), @chunk_len int)
RETURNS @tab TABLE (chunk_str varchar(100))
WITH EXECUTE AS CALLER
AS
BEGIN
  declare @pos int = 1;
  declare @len int = len(@str);
  
  while @pos <= @len begin
    insert into @tab (chunk_str) values (substring(@str, @pos, @chunk_len));
    set @pos = @pos + 1;
  end

  RETURN;
END;
GO
/****** Object:  Table [dbo].[bik_node]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_node_id] [int] NULL,
	[node_kind_id] [int] NOT NULL,
	[name] [varchar](4000) NOT NULL,
	[tree_id] [int] NOT NULL,
	[linked_node_id] [int] NULL,
	[branch_ids] [varchar](1000) NULL,
	[obj_id] [varchar](1000) NULL,
	[descr] [varchar](max) NULL,
	[is_deleted] [int] NOT NULL,
	[is_built_in] [int] NOT NULL,
	[chunked_ver] [int] NOT NULL,
	[disable_linked_subtree] [int] NOT NULL,
	[search_rank] [int] NOT NULL,
	[vote_sum] [int] NOT NULL,
	[vote_cnt] [int] NOT NULL,
	[vote_val] [float] NOT NULL,
	[descr_plain] [varchar](max) NULL,
	[visual_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_branch_ids] ON [dbo].[bik_node] 
(
	[branch_ids] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_del_kind_tree_ln_dis_lin_st] ON [dbo].[bik_node] 
(
	[is_deleted] ASC,
	[node_kind_id] ASC,
	[tree_id] ASC
)
INCLUDE ( [linked_node_id],
[disable_linked_subtree]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_is_deleted_parent_node_id] ON [dbo].[bik_node] 
(
	[parent_node_id] ASC,
	[is_deleted] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_linked_node_id] ON [dbo].[bik_node] 
(
	[linked_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_parent_deleted_tree_linked_branch] ON [dbo].[bik_node] 
(
	[parent_node_id] ASC,
	[is_deleted] ASC
)
INCLUDE ( [tree_id],
[linked_node_id],
[branch_ids]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_parent_node_id] ON [dbo].[bik_node] 
(
	[parent_node_id] ASC,
	[tree_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_parent_node_id_linked_node_id_is_deleted] ON [dbo].[bik_node] 
(
	[parent_node_id] ASC,
	[linked_node_id] ASC,
	[is_deleted] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_tree_id_is_deleted] ON [dbo].[bik_node] 
(
	[tree_id] ASC,
	[is_deleted] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_tree_obj_id] ON [dbo].[bik_node] 
(
	[tree_id] ASC,
	[obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_node_kind_id_linked_node_id] ON [dbo].[bik_node] 
(
	[linked_node_id] ASC,
	[node_kind_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'drzewko ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_node', @level2type=N'COLUMN',@level2name=N'tree_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'opisek PR0 :D' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_node', @level2type=N'COLUMN',@level2name=N'descr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'główna tabelka biksa!!!' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_node'
GO
EXEC sys.sp_addextendedproperty @name=N'Type', @value=N'Główna tabela w BIKSie, tu jest wszystko, to nie ma nic :D' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_node'
GO
/****** Object:  Table [dbo].[bik_attr_system]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attr_system](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_kind_id] [int] NOT NULL,
	[attr_id] [int] NOT NULL,
	[is_visible] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_attr_system] UNIQUE NONCLUSTERED 
(
	[node_kind_id] ASC,
	[attr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_attr_system_linked]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attr_system_linked](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attr_system_id] [int] NOT NULL,
	[node_id] [int] NOT NULL,
	[value] [varchar](max) NULL,
	[is_deleted] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_attr_system_linked_a_n] ON [dbo].[bik_attr_system_linked] 
(
	[attr_system_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[log_msg]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[log_msg](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[msg] [varchar](max) NULL,
	[date_added] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_log_msg]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_log_msg] @msg varchar(max) as
begin
  set nocount on;
  print @msg;
  insert into log_msg (msg) values (@msg);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_add_items]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_add_items](@items1 varchar(max), @items2 varchar(max), @sep varchar(max), @trimOpt int)
returns varchar(max)
as
begin
  declare @tab table (str varchar(max) not null);

  insert into @tab (str)
  select
    str 
  from
    dbo.fn_split_by_sep(@items1, @sep, @trimOpt)
  union
  select
    str 
  from
    dbo.fn_split_by_sep(@items2, @sep, @trimOpt)
  ;

  declare @res varchar(max) = '';

  select @res = @res + @sep + str from @tab;

  return case when @res = '' then '' else stuff(@res, 1, datalength(@sep), '') end;
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_normalize_text_for_fts]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_normalize_text_for_fts](@txt varchar(max))
returns varchar(max)
as
begin
  if @txt is null return null

  set @txt = ltrim(rtrim(@txt))
  if @txt = '' return ''
  
  set @txt = replace(@txt, '_', ' ')
  set @txt = replace(@txt, '(', ' ')
  set @txt = replace(@txt, '/', ' ')
  set @txt = replace(@txt, ')', ' ')
  set @txt = replace(@txt, '\', ' ')
  set @txt = replace(@txt, '[', ' ')
  set @txt = replace(@txt, ']', ' ')
  set @txt = replace(@txt, '*', ' ')
  set @txt = replace(@txt, '"', ' ')
  set @txt = replace(@txt, '''', ' ')

  set @txt = ltrim(rtrim(@txt))
  --if @txt = '' set @txt = null
  
  return @txt
end
GO
/****** Object:  Table [dbo].[bik_searchable_attr]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_searchable_attr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[search_weight] [int] NOT NULL,
	[caption] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_searchable_attr_name] ON [dbo].[bik_searchable_attr] 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_change]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_change](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[date_added] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_app_prop]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_app_prop](
	[name] [varchar](255) NOT NULL,
	[val] [varchar](max) NOT NULL,
	[is_editable] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_app_prop_name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_change_detail]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_change_detail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[change_id] [bigint] NOT NULL,
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_node_change_detail_cn] ON [dbo].[bik_node_change_detail] 
(
	[change_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_node_change_detail_nc] ON [dbo].[bik_node_change_detail] 
(
	[node_id] ASC,
	[change_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_data_source_def]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_data_source_def](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [char](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_unique_description] UNIQUE NONCLUSTERED 
(
	[description] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_user]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NOT NULL,
	[email] [varchar](250) NULL,
	[phone_num] [varchar](200) NULL,
	[short_descr] [varchar](max) NULL,
	[node_id] [int] NULL,
	[login_name_for_ad] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_user_node_id] UNIQUE NONCLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_system_user]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_system_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[login_name] [varchar](250) NOT NULL,
	[password] [varchar](250) NULL,
	[is_disabled] [int] NOT NULL,
	[user_id] [int] NULL,
	[name] [varchar](255) NOT NULL,
	[avatar] [varchar](500) NULL,
	[date_added] [datetime] NULL,
	[sso_login] [varchar](256) NULL,
	[sso_domain] [varchar](256) NULL,
	[is_send_mails] [int] NOT NULL,
 CONSTRAINT [PK_bik_system_user_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_system_user_login_name] ON [dbo].[bik_system_user] 
(
	[login_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_system_user_sso_login_sso_domain] ON [dbo].[bik_system_user] 
(
	[sso_login] ASC,
	[sso_domain] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_unique_bik_system_user_user_id] ON [dbo].[bik_system_user] 
(
	[user_id] ASC
)
WHERE ([user_id] IS NOT NULL)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_change_detail_value]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_change_detail_value](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[change_detail_id] [bigint] NULL,
	[attr_id] [int] NULL,
	[old_value] [varchar](max) NULL,
	[new_value] [varchar](max) NULL,
	[user_id] [int] NULL,
	[data_source_def_id] [int] NULL,
	[spid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_object_in_fvs]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_object_in_fvs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_object_in_fvs] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_object_in_fvs_node_id_user_id] ON [dbo].[bik_object_in_fvs] 
(
	[node_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_object_in_fvs_change_ex]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_object_in_fvs_change_ex](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_added] [datetime] NOT NULL,
	[node_id] [int] NOT NULL,
	[fvs_id] [int] NOT NULL,
	[node_status] [int] NOT NULL,
	[changed_attrs] [varchar](max) NOT NULL,
	[joined_obj_ids_added] [varchar](max) NOT NULL,
	[joined_obj_ids_deleted] [varchar](max) NOT NULL,
	[new_comment_cnt] [int] NOT NULL,
	[vote_val_delta] [int] NOT NULL,
	[vote_cnt_delta] [int] NOT NULL,
	[dqc_test_fail_cnt] [int] NOT NULL,
	[cut_parent_id] [int] NULL,
	[is_pasted] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_searchable_attr_val]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_searchable_attr_val](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attr_id] [int] NOT NULL,
	[node_id] [int] NOT NULL,
	[value] [varchar](max) NOT NULL,
	[fixed_value] [varchar](max) NULL,
	[search_weight] [int] NOT NULL,
	[tree_id] [int] NOT NULL,
	[node_kind_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[attr_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_searchable_attr_val_nk_tr] ON [dbo].[bik_searchable_attr_val] 
(
	[node_kind_id] ASC,
	[tree_id] ASC
)
INCLUDE ( [id],
[attr_id],
[node_id],
[value]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_searchable_attr_val_node_attr_ids] ON [dbo].[bik_searchable_attr_val] 
(
	[node_id] ASC,
	[attr_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_spid_source]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_spid_source](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[spid] [int] NULL,
	[user_id] [int] NULL,
	[data_source_def_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_one_table]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- poprzednia wersja w "alter db for v1.7.z10.7 ct.sql"
-- naprawa błędu przy dodawaniu testu
CREATE procedure [dbo].[sp_verticalize_node_attrs_one_table](@source varchar(max), @nodeIdCol sysname, @cols varchar(max),
  @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
/*
  declare @source varchar(max) = '(select 3031527 as node_id,       '' as val)';
  declare @nodeIdCol sysname = 'node_id';
  declare @cols varchar(max) = 'val';
  declare @properNames varchar(max) = 'kkkk';
  declare @optNodeFilter varchar(max) = 'n.id = 3031527';
  */
  set nocount on
  
  declare @diags_level int = 0 -- 0 oznacza brak logowania
  declare @sub int = 1
  declare @calc_fvs_change_ex int = 1;
  declare @row_cnt int
  declare @log_msg varchar(max);

  if @sub > 0 begin	
    create table #updateTable (node_id int, branch_ids varchar(max) collate DATABASE_DEFAULT, act int, parent_node_id int);	
  end
  -- diag
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': start, @source=' + @source; exec sp_log_msg @log_msg; end;

  --------------
  -- 1. rozbijamy nazwy atrybutw - nazwy kolumn w r?dle i nazwy waciwe (opcjonalne)
  --------------

  --declare @ attrPropNamesTab table (idx int primary key, name sysname not null)
  create table #vnaot_attrPropNamesTab (idx int primary key, name sysname collate DATABASE_DEFAULT not null);

  insert into #vnaot_attrPropNamesTab (idx, name)
  select idx, str from dbo.fn_split_by_sep(@properNames, ',', 7)
  
  /*declare @ attrNamesTab table (name sysname not null primary key, 
    proper_name varchar(255) not null, search_weight int not null,
    attr_id int null
  )*/
  --select * from #vnaot_attrPropNamesTab;

  create table #vnaot_attrNamesTab (name sysname collate DATABASE_DEFAULT not null primary key, 
    proper_name varchar(255) collate DATABASE_DEFAULT not null, search_weight int not null,
    attr_id int null
  );
  
  insert into #vnaot_attrNamesTab (name, proper_name, search_weight,
    attr_id)
  select aaa.str, coalesce(apnt.name, aaa.str), coalesce(a.search_weight, 1),
    a.id
  from dbo.fn_split_by_sep(@cols, ',', 7) aaa left join #vnaot_attrPropNamesTab apnt on aaa.idx = apnt.idx
    left join 
      bik_searchable_attr a on a.name = coalesce(apnt.name, aaa.str)
  --select * from #vnaot_attrNamesTab;
  --------------
  -- 2. znamy nazwy atrybut?w i node_kindy, teraz uzupeniamy bik_attribute, ew. poprawiamy tam typy
  --------------
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupenieniem searchable attrs'; exec sp_log_msg @log_msg; end;
  
  insert into bik_searchable_attr (name, caption, search_weight)
  select ant.proper_name, ant.proper_name, 1
  from #vnaot_attrNamesTab ant
  where ant.attr_id is null  
  
  update #vnaot_attrNamesTab set attr_id = a.id
  from #vnaot_attrNamesTab aaa inner join bik_searchable_attr a on aaa.attr_id is null and a.name = aaa.proper_name
  --select * from #vnaot_attrNamesTab;
  --------------
  -- 3. wrzucamy warto??ci atrybutw do tabeli pomocniczej
  --------------

  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przygotowanie do wrzucania do #vnaot_attrValsTab'; exec sp_log_msg @log_msg; end;
  
  /*declare @ attrValsTab table (attr_id int not null, node_id int not null, value varchar(max) null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id))
  */

  create table #vnaot_attrValsTab (attr_id int not null, node_id int not null, value varchar(max) collate DATABASE_DEFAULT null, 
    --type int not null, 
    search_weight int not null, tree_id int not null, node_kind_id int not null, unique (attr_id, node_id), unique (node_id, attr_id));
 
  declare attrCur cursor for select name, --attr_type, 
  proper_name, search_weight, attr_id from #vnaot_attrNamesTab
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed open attrCur'; exec sp_log_msg @log_msg; end;

  if @diags_level > 0 begin
    select name, --attr_type, 
    proper_name, search_weight, attr_id from #vnaot_attrNamesTab
  end

  open attrCur
  
  declare @attrName sysname, @attrType int, @attrProperName varchar(255), @searchWeight int, @attr_id int
  declare @attrValSql varchar(max)
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed pierwszym fetch next from attrCur'; exec sp_log_msg @log_msg; end;

  fetch next from attrCur into @attrName, --@attrType, 
    @attrProperName, @searchWeight, @attr_id
  
  if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed p?tl'; exec sp_log_msg @log_msg; end;

  while @@fetch_status = 0
  begin
    if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': start p?tli dla pola: ' + @attrName + ' (' + @attrProperName + ')'; exec sp_log_msg @log_msg; end;
    
    set @attrValSql = 
    'select attr_id, node_id, /*case when is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''' then null else value end*/ value, search_weight,' +
    ' tree_id, node_kind_id from (' +
    'select ' + cast(@attr_id as varchar(20)) + ' as attr_id, x.' + @nodeIdCol + ' as node_id, x.' + @attrName + ' as value, ' + --', a.type, ' + 
    --cast(@searchWeight as varchar(20)) + ' * (n.search_rank + nk.search_rank) * 100 + n.vote_sum as search_weight, ' +
    'dbo.fn_calc_searchable_attr_val_weight(' + cast(@searchWeight as varchar(20)) + ', n.search_rank, nk.search_rank, n.vote_sum) as search_weight, ' + 
    + 'case when n.is_deleted = 0 and (n.linked_node_id is null or n.disable_linked_subtree <> 0) then 0 else 1 end is_deleted,' + 
      ' n.tree_id, n.node_kind_id' +
      ' from ' + @source + ' as x inner join bik_node n on n.id = x.' + @nodeIdCol +
      ' inner join bik_node_kind nk on n.node_kind_id = nk.id' +
      --' inner join bik_searchable_attr a on n.node_kind_id = a.node_kind_id and a.name = ''' + @attrProperName + '''' +
      --' where x.' + @attrName + ' is not null' +
      case when @optNodeFilter is null then '' else ' where ' + @optNodeFilter end +
      ' ) x where not (is_deleted = 1 or value is null or ltrim(rtrim(value)) = '''')'
    if @diags_level > 0 begin set @log_msg = 'sql for attr=' + @attrName + ' is: 
    ' + @attrValSql; exec sp_log_msg @log_msg; end;
  
    insert into #vnaot_attrValsTab (attr_id, node_id, value, --type, 
    search_weight, tree_id, node_kind_id)
    exec (@attrValSql)
  
    if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed ko?cem iteracji p?tli dla pola: ' + @attrName; exec sp_log_msg @log_msg; end;
    
    fetch next from attrCur into @attrName, --@attrType, 
      @attrProperName, @searchWeight, @attr_id
  end
    
  close attrCur
  
  deallocate attrCur
  
  --------------
  -- 5. aktualizacja bik_searchable_attr_val - na podstawie wycigni?tych wartoci
  --------------
  
  if @diags_level > 0 begin
    declare @avtCnt int = (select count(*) from #vnaot_attrValsTab)
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': za p?tl, kursor zamkni?ty, liczba wartoci razem: ' +
      cast(@avtCnt as varchar(20)) + ', przed delete'; exec sp_log_msg @log_msg;
  end
  
  declare @attrIdsStr varchar(max) = ''
  update #vnaot_attrNamesTab set @attrIdsStr = @attrIdsStr + ',' + cast(attr_id as varchar(20))
  set @attrIdsStr = substring(@attrIdsStr, 2, len(@attrIdsStr))
  
  declare @attrValsInRangeSql varchar(max) = 'select av.id, av.node_id, av.attr_id 
  from bik_searchable_attr_val av' + case when @optNodeFilter is null then '' else ' inner join bik_node n on n.id = av.node_id and ' + @optNodeFilter end +
  ' where av.attr_id in (' + @attrIdsStr + ')'
  
  --declare @ attrValsInRange table (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id))
  create table #vnaot_attrValsInRange (id int not null primary key, node_id int not null, attr_id int not null, unique (node_id, attr_id));

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed insert into #vnaot_attrValsInRange, sql=' + @attrValsInRangeSql; exec sp_log_msg @log_msg;
  end 
  
  insert into #vnaot_attrValsInRange (id, node_id, attr_id)
  exec(@attrValsInRangeSql)
  
  declare @delCnt int
/*
  delete from bik_searchable_attr_val
  from #vnaot_attrValsTab avt 
  where avt.value is null and bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id
*/

  --update bik_app_prop set val = '50' where name = 'suggestSimilar.maxResults'
  declare @register_bik_node_changes int = 0;

  if (coalesce((select val from bik_app_prop where name = 'suggestSimilar.maxResults'), '0') <> '0'
     or coalesce((select val from bik_app_prop where name = 'useFullTextIndex'), '0') in ('0', 'false'))
    and exists (select 1 from bik_app_prop where name = 'indexing.fullData' and val in ('inProgress', 'done'))
    set @register_bik_node_changes = 1;

  declare @enable_node_history int = 0;
  if (coalesce((select val from bik_app_prop where name = 'enableNodeHistory'), '0') <> '0')
  set @enable_node_history = 1;
  --select @register_bik_node_changes

  if @register_bik_node_changes <> 0 begin

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': wrzucanie zmian do bik_node_change_detail'; exec sp_log_msg @log_msg;
    end 

    declare @change_id bigint;

    insert into bik_node_change (date_added) values (getdate());
	
    select @change_id = scope_identity();
    
	declare attrChangeValCur cursor for select distinct coalesce(av0.node_id, av.node_id), coalesce(av.attr_id, av0.attr_id), av0.fixed_value as old_value, dbo.fn_normalize_text_for_fts(av.value) as new_value
    from 
      bik_searchable_attr_val av0 inner join #vnaot_attrValsInRange avir on av0.id = avir.id
      full outer join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
    where av0.id is null or av.node_id is null or av0.value <> av.value or av0.tree_id <> av.tree_id or av0.node_kind_id <> av.node_kind_id
      or av0.search_weight <> av.search_weight;
 
	open attrChangeValCur
  
	declare @node_id int, @old_value varchar(max), @new_value varchar(max);
	declare @change_detail_id bigint; 

	declare @user_id int = (select user_id from bik_spid_source where spid = @@spid);
	declare @data_source_def_id int = (select data_source_def_id from bik_spid_source where spid = @@spid);
	fetch next from attrChangeValCur into @node_id, @attr_id, @old_value, @new_value; 
    while @@fetch_status = 0
	begin
		print @node_id
		if not exists (select * from 	bik_node_change_detail where change_id=@change_id and node_id=@node_id)
		begin
		insert into bik_node_change_detail(change_id, node_id) values (@change_id, @node_id);
		
		select @change_detail_id = scope_identity();
		if @diags_level > 0 begin
			set @log_msg = cast(sysdatetime() as varchar(23)) + ': old_value = ' + @old_value + ', new_value = ' + @new_value; exec sp_log_msg @log_msg;
		end 

		if @enable_node_history <> 0 --and @node_id!= NULL
		insert into bik_node_change_detail_value(change_detail_id, attr_id, old_value, new_value, user_id, data_source_def_id)
		values (@change_detail_id, @attr_id, @old_value, @new_value, @user_id, @data_source_def_id);
		end
		fetch next from attrChangeValCur into @node_id, @attr_id, @old_value, @new_value; 
	end
    
	close attrChangeValCur
  
	deallocate attrChangeValCur

	/*
    insert into bik_node_change_detail (change_id, node_id)
    select distinct @change_id, coalesce(av0.node_id, av.node_id)
    from 
      bik_searchable_attr_val av0 inner join #vnaot_attrValsInRange avir on av0.id = avir.id
      full outer join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
    where av0.id is null or av.node_id is null or av0.value <> av.value or av0.tree_id <> av.tree_id or av0.node_kind_id <> av.node_kind_id
      or av0.search_weight <> av.search_weight; */
  end;


  if @calc_fvs_change_ex <> 0 begin

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_del'; exec sp_log_msg @log_msg;
    end; 

    declare @cfce_ids_str varchar(max);

    create table #cfce_del (node_id int not null primary key, branch_ids varchar(1000) collate DATABASE_DEFAULT null, unique (branch_ids, node_id));

    insert into #cfce_del (node_id, branch_ids)
    select id, branch_ids from bik_node where id in (select node_id from #vnaot_attrValsInRange) and is_deleted <> 0;

    set @row_cnt = @@rowcount;

    if @diags_level > 0 begin
      set @cfce_ids_str = '';
      select @cfce_ids_str = @cfce_ids_str + '|' + cast(node_id as varchar(20)) from #cfce_del;
      set @cfce_ids_str = case when @cfce_ids_str = '' then '' else stuff(@cfce_ids_str, 1, 1, '') end;
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_ins, @row_cnt= ' + cast(@row_cnt as varchar(20)) + ', @cfce_ids_str=' + @cfce_ids_str; exec sp_log_msg @log_msg;
    end; 

    create table #cfce_ins (node_id int not null primary key, branch_ids varchar(1000) collate DATABASE_DEFAULT null, unique (branch_ids, node_id));
    
    insert into #cfce_ins (node_id, branch_ids)
    select id, branch_ids from bik_node where id in (select nv.node_id from #vnaot_attrValsTab nv)
      and id not in (select av.node_id from bik_searchable_attr_val av) and is_deleted = 0;    

    set @row_cnt = @@rowcount;

    if @diags_level > 0 begin
      set @cfce_ids_str = '';
      select @cfce_ids_str = @cfce_ids_str + '|' + cast(node_id as varchar(20)) from #cfce_ins;
      set @cfce_ids_str = case when @cfce_ids_str = '' then '' else stuff(@cfce_ids_str, 1, 1, '') end;
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_upd, @row_cnt= ' + cast(@row_cnt as varchar(20)) + ', @cfce_ids_str=' + @cfce_ids_str; exec sp_log_msg @log_msg;
    end; 

    create table #cfce_upd (id int not null identity primary key, node_id int not null, 
      branch_ids varchar(1000) collate DATABASE_DEFAULT null, attr_id int not null, unique (node_id, attr_id), unique (node_id, branch_ids, attr_id));

    insert into #cfce_upd (node_id, branch_ids, attr_id)
    select coalesce(ir.node_id, nv.node_id), coalesce(n.branch_ids, nn.branch_ids), coalesce(ir.attr_id, nv.attr_id)
    from 
      #vnaot_attrValsInRange ir inner join bik_node n on ir.node_id = n.id and n.is_deleted = 0 
      inner join bik_searchable_attr_val av on ir.node_id = av.node_id and ir.attr_id = av.attr_id
      full outer join (#vnaot_attrValsTab nv inner join bik_node nn on nn.id = nv.node_id) on ir.node_id = nv.node_id and ir.attr_id = nv.attr_id
      left join #cfce_ins ins on ins.node_id = nv.node_id
    where coalesce(av.value, '') <> coalesce(nv.value, '') and ins.node_id is null;

    set @row_cnt = @@rowcount;

    if @diags_level > 0 begin
      set @cfce_ids_str = '';
      select @cfce_ids_str = @cfce_ids_str + '|' + cast(node_id as varchar(20)) from #cfce_upd;
      set @cfce_ids_str = case when @cfce_ids_str = '' then '' else stuff(@cfce_ids_str, 1, 1, '') end;
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed #cfce_upd_agg, @row_cnt= ' + cast(@row_cnt as varchar(20)) + ', @cfce_ids_str=' + @cfce_ids_str; exec sp_log_msg @log_msg;
    end; 

    create table #cfce_upd_agg (node_id int not null primary key, branch_ids varchar(1000) collate DATABASE_DEFAULT null, attr_ids varchar(max) collate DATABASE_DEFAULT not null);
    
    insert into #cfce_upd_agg (node_id, branch_ids, attr_ids)
    select node_id, branch_ids, stuff((select '|' + cast(attr_id as varchar(20)) from #cfce_upd u where u.node_id = x.node_id for xml path ('')), 1, 1, '')
    from (select distinct node_id, branch_ids from #cfce_upd) x;

    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': tworz? udawane bik_node - tylko dla fvs (id, branch_ids)'; exec sp_log_msg @log_msg;
    end; 

    create table #fake_bik_node_for_fvs (id int not null, branch_ids varchar(1000) collate DATABASE_DEFAULT, primary key (id, branch_ids));

    declare @dqc_tree_id int = (select id from bik_tree where code = 'DQC');
    with 
      cte0 as (
        select distinct fvs.node_id, n.branch_ids
        from bik_object_in_fvs fvs inner join bik_node n on fvs.node_id = n.id),
      cte1 (id, branch_ids) as (
        select distinct bids.node_id, n2.branch_ids 
        from bik_node n inner join cte0 bids on n.branch_ids like bids.branch_ids + '%'
          inner --loop 
          join bik_node n2 on n.linked_node_id = n2.id left join cte0 oo on oo.node_id = bids.node_id and oo.branch_ids = n2.branch_ids
        where n.is_deleted = 0 and n.linked_node_id is not null
          and n.tree_id = @dqc_tree_id and n2.tree_id = @dqc_tree_id and oo.node_id is null
        union all 
        select node_id, branch_ids from cte0
      )
    insert into #fake_bik_node_for_fvs (id, branch_ids)
    select id, branch_ids from cte1;

    -- nowe
    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupenianiem bik_object_in_fvs_change_ex - nowe'; exec sp_log_msg @log_msg;
    end; 

    with bik_node as (select id, branch_ids from #fake_bik_node_for_fvs)
    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.user_id, f.id, i.node_id from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
        inner join #cfce_ins i on i.branch_ids like n.branch_ids + '%'
    ) as c (user_id, fvs_id, node_id) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status)
      values (c.node_id, c.fvs_id, 1)
    when matched then
      update set ex.node_status = 1, ex.changed_attrs = '', ex.joined_obj_ids_added = '', ex.joined_obj_ids_deleted = '', 
        ex.new_comment_cnt = 0, ex.vote_val_delta = 0, ex.vote_cnt_delta = 0, ex.dqc_test_fail_cnt = 0
    ;    

    -- zmienione
    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupe?nianiem bik_object_in_fvs_change_ex - zmienione'; exec sp_log_msg @log_msg;
    end; 

    declare @now datetime = getdate();

    with bik_node as (select id, branch_ids from #fake_bik_node_for_fvs)
    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.user_id, f.id, i.node_id, i.attr_ids from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
        inner join #cfce_upd_agg i on i.branch_ids like n.branch_ids + '%'
    ) as c (user_id, fvs_id, node_id, attr_ids) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status, changed_attrs)
      values (c.node_id, c.fvs_id, 0, c.attr_ids)
    when matched and ex.node_status = 0 or (ex.node_status = 1 and datediff(mi, ex.date_added, @now) > 30) then 
      update set ex.changed_attrs = dbo.fn_add_items(ex.changed_attrs, c.attr_ids, '|', 7)
    ;    
    
    -- usunite
    if @diags_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed uzupenianiem bik_object_in_fvs_change_ex - zmienione'; exec sp_log_msg @log_msg;
    end; 

    with bik_node as (select id, branch_ids from #fake_bik_node_for_fvs)
    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.user_id, f.id, i.node_id from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
        inner join #cfce_del i on i.branch_ids like n.branch_ids + '%'
    ) as c (user_id, fvs_id, node_id) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status)
      values (c.node_id, c.fvs_id, -1)
    when matched then
      update set ex.node_status = -1, ex.changed_attrs = '', ex.joined_obj_ids_added = '', ex.joined_obj_ids_deleted = '', 
        ex.new_comment_cnt = 0, ex.vote_val_delta = 0, ex.vote_cnt_delta = 0, ex.dqc_test_fail_cnt = 0
    ;    

  end;
 
  if @sub > 0 begin
  ------------- 
	--Subskrypcje- usuni?te 
	-------------
	-- gdy usuwa si? atrybut (warto? na null lub pust) to wrzucamy do #updateTable
	-- node_id, 0
	-- mo?liwe duplikaty
	 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje usuniete'; exec sp_log_msg @log_msg; end;
	insert into #updateTable (node_id, act) 
	select bik_searchable_attr_val.node_id, 0
	from bik_searchable_attr_val join #vnaot_attrValsInRange avir on  bik_searchable_attr_val.id = avir.id left join #vnaot_attrValsTab av 
	on avir.attr_id = av.attr_id and avir.node_id = av.node_id
	where bik_searchable_attr_val.id = avir.id and av.node_id is null
	
  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ':Usunite insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
	-------------
  end
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': przed wa?ciwym delete'; exec sp_log_msg @log_msg;
  end 
   
  delete from bik_searchable_attr_val 
  from #vnaot_attrValsInRange avir left join #vnaot_attrValsTab av on avir.attr_id = av.attr_id and avir.node_id = av.node_id
  where bik_searchable_attr_val.id = avir.id and av.node_id is null
  
  set @delCnt = @@rowcount
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po delete, rowcnt=' + cast(@delCnt as varchar(20)) + ', przed update'; exec sp_log_msg @log_msg;
  end
  
  if @sub > 0 begin
	--Subskrypcje -- zaktualizowane
	-------------
	-- znw dorzucamy z act = 0
	-- node_id, 0 -- gdy zmienia si? warto? kt?rego atrybutu (musia ju?? by wczeniej dodany taki atr.)
	-- mo?liwe duplikaty
	
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje aktualizacja'; exec sp_log_msg @log_msg; end;

	insert into #updateTable (node_id, act) 
	select avt.node_id, 0
	from #vnaot_attrValsTab avt join bik_searchable_attr_val on bik_searchable_attr_val.node_id = avt.node_id 
	     and bik_searchable_attr_val.attr_id = avt.attr_id
	where --ww: zbdne: avt.value is not null and
	    --ww: zb?dne: avt.node_id not in(select node_id from #updateTable) and
	(bik_searchable_attr_val.value <> avt.value
    --ww: ponisze do zastanowienia
    or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id)

  select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:Zaktualizowane insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
  
	-------------
	end
		
  update bik_searchable_attr_val
  set value = avt.value, 
    fixed_value = case when bik_searchable_attr_val.value <> avt.value then dbo.fn_normalize_text_for_fts(avt.value) else fixed_value end,
    search_weight = avt.search_weight,
    tree_id = avt.tree_id , node_kind_id = avt.node_kind_id
  from #vnaot_attrValsTab avt
  where --ww: zb?dne: avt.value is not null and 
    bik_searchable_attr_val.node_id = avt.node_id and bik_searchable_attr_val.attr_id = avt.attr_id and
     (bik_searchable_attr_val.value <> avt.value or bik_searchable_attr_val.search_weight <> avt.search_weight
     or bik_searchable_attr_val.tree_id <> avt.tree_id or bik_searchable_attr_val.node_kind_id <> avt.node_kind_id )
  
  declare @updRowCnt int = @@rowcount
  
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po update, row count: ' +
      cast(@updRowCnt as varchar(20)) + ', przed insert'; exec sp_log_msg @log_msg;
  end
  
  --------------
  -- 6. dorzucenie nowych wartoci
  --------------
  
  if @sub > 0 begin
  ------------- 
	--Subskrypcje -- nowe lub zaktualizowana
	-------------
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa'; exec sp_log_msg @log_msg; end;
	declare @newAttrId int 
	select @newAttrId=id from bik_searchable_attr where name='name'

	-- nie by?o warto?ci, a teraz si pojawia dla jakiego? atrybutu
	-- node_id, 0 lub 1 -> 1 gdy to atrybut name si wa?nie pojawi?
	insert into #updateTable (node_id, act) 
	select avt.node_id, --max(
	case when avt.attr_id = @newAttrId then 1 else 0 end--) 
	as act
	from #vnaot_attrValsTab avt left join bik_searchable_attr_val av on
	avt.attr_id = av.attr_id and avt.node_id = av.node_id
	where av.id is null 
	--ww: zbdne: and avt.value is not null
	--ww: zb?dne, i tak s duplikaty
	-- and avt.node_id not in (select node_id from #updateTable)
	--group by avt.node_id

 select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:Nowe lub zaktualizowana insert into #updateTable (node_id,branch_ids,act), row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
		 if @diags_level > 0 begin set @log_msg = cast(sysdatetime() as varchar(23)) + ': subskrypcje nowa koniec'; exec sp_log_msg @log_msg; end;
	-------------
	end
	
  insert into bik_searchable_attr_val (node_id, attr_id, value, fixed_value, search_weight, tree_id, node_kind_id)
  select avt.node_id, avt.attr_id, avt.value, dbo.fn_normalize_text_for_fts(avt.value), avt.search_weight, avt.tree_id, avt.node_kind_id
  from #vnaot_attrValsTab avt left join bik_searchable_attr_val av on
  avt.attr_id = av.attr_id and avt.node_id = av.node_id
  where av.id is null and avt.value is not null

  declare @insRowCnt int = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': po insert, row count: ' +
      cast(@insRowCnt as varchar(20)) + ', KONIEC!'; exec sp_log_msg @log_msg;
  end
  
  
  -- select * from #vnaot_attrValsTab
  
  --declare @sql varchar(max) = 'select ' + @nodeIdCol + ', ' + @normalCols + ', ' + @identCols + ' from ' + @source
  --begin set @log_msg = @sql
  
  --select * from #vnaot_attrNamesTab
  --select * from @attrNodeKindsTab ankt inner join bik_node_kind nk on ankt.node_kind_id = nk.id
  
  	drop table #updateTable  
   	/* TF do usuniecia - rezygnacja z tabeli bik_object_in_fvs_change
    ------------- 
	--Subskrypcje
	------------- 
	if @sub > 0 begin
	
 if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :przed subskrypcj?: '; exec sp_log_msg @log_msg;
  end

    select ut.node_id, ut.node_id as parent_node_id, max(ut.act) as act
    into #updateTableNoDupl
    from #updateTable ut
	group by ut.node_id

  	--update #updateTable set branch_ids=(select branch_ids from bik_node where id=#updateTable.node_id),parent_node_id=node_id
  	
  	select @row_cnt = @@rowcount

  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ' :sub:update #updateTable set branch_ids, row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
  end
  

	
	drop table #updateTable  

	declare @cnt int
	set @cnt=1
	
	
	while @cnt>0 begin
	
			insert into bik_object_in_fvs_change (node_id, fvs_id, action_type) 
			select ub.node_id, fvs.id, ub.act
			from #updateTableNoDupl ub join bik_object_in_fvs fvs on ub.parent_node_id = fvs.node_id
			left join bik_object_in_fvs_change fc on fvs.id = fc.fvs_id and ub.node_id = fc.node_id
			where fc.id is null
			
			select @row_cnt = @@rowcount

		  if @diags_level > 0 begin
			set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' insert into bik_object_in_fvs_change(node_id,fvs_id,action_type) , row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
		  end

			update #updateTableNoDupl
			--set parent_node_id = bn.parent_node_id
			set parent_node_id = (select parent_node_id from bik_node where id = #updateTableNoDupl.parent_node_id)
			--from bik_node bn
			--where bn.id = #updateTableNoDupl.parent_node_id
			
		select @row_cnt = @@rowcount

		  if @diags_level > 0 begin
			set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:cnt '+cast(@cnt as varchar(20))+ ' update #updateTable, row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
		  end

			delete from #updateTableNoDupl where parent_node_id is null

		  select @row_cnt = @@rowcount

			select @cnt=count(*) from #updateTableNoDupl

			if @diags_level > 0 begin
			  set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:na kocu p?tli, ile zostao: ' + cast(@cnt as varchar(20))+ ' po delete na #updateTableNoDupl, usuni?tych row cnt=' + cast(@row_cnt as varchar(20)); exec sp_log_msg @log_msg;
			end

	end
	
	drop table #updateTableNoDupl
	
	
  if @diags_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) +' :sub:po p?tli, KONIEC'; exec sp_log_msg @log_msg;
  end

    end
    
    */
end
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_one_metadata_table]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_one_metadata_table](@source varchar(max), @nodeIdCol sysname, 
  @cols varchar(max), @properNames varchar(max), @optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_table @source, @nodeIdCol, @cols, @properNames, @optNodeFilter
end
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_add_attrs]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_add_attrs](@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @diags_level int = 0 -- 0 oznacza brak logowania

  --declare @attr_names table (attr_id int not null unique, name varchar(255) not null unique, is_deleted int not null)
  
  --declare @attr_names_sql varchar(max) = 'select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in=0 /*where ad.is_deleted = 0*/'
  
  /*
  if (@optNodeFilter is not null) begin
    set @attr_names_sql = @attr_names_sql + ' and ad.id in (select a.attr_def_id from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id
    inner join bik_node n on al.node_id = n.id where ' + @optNodeFilter + ')'
  end
  */
  
  --insert into @attr_names (attr_id, name, is_deleted)
  --exec(@attr_names_sql)
  
  --select * from @attr_names
  
  --declare attr_names_cur cursor for select attr_id, name, is_deleted from @attr_names
  declare attr_names_cur cursor for select ad.id, ad.name, ad.is_deleted from bik_attr_def ad where is_built_in = 0 /*and is_deleted = 0 and exists (select 1 from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id where a.attr_def_id = ad.id and al.is_deleted = 0 and a.is_deleted = 0)*/
  
  declare @attr_id int, @name varchar(255), @is_deleted int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_source_sql varchar(max) = '(select node_id, value from bik_attribute_linked al inner join bik_attribute a on al.attribute_id = a.id 
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and a.is_deleted = 0 and a.attr_def_id = ' + cast(@attr_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_source_sql

	--set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_table @attr_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur
  
  ------Dla metadata atrybutów---------------
  declare attr_names_cur cursor 
  for 
  select distinct ad.id,  ad.name, ad.is_deleted  from bik_attr_def ad 
  inner join bik_attr_system ats on ats.attr_id = ad.id 
  inner join bik_attr_system_linked al on al.attr_system_id = ats.id 
  where ad.is_built_in = 1  

  declare @attr_system_id int
  
  open attr_names_cur
  
  fetch next from attr_names_cur into @attr_system_id, @name, @is_deleted
  
  --return
  
  while @@fetch_status = 0 begin
    declare @attr_system_source_sql varchar(max) = '(select node_id, value from bik_attr_system_linked al inner join bik_attr_system ats on ats.id = al.attr_system_id inner join bik_attr_def adef on adef.id = ats.attr_id
    where ' + case when @is_deleted = 0 then '' else '1 = 0 and ' end + 'al.is_deleted = 0 and ats.is_visible = 1 and ats.attr_id = ' + cast(@attr_system_id as varchar(20)) + ')'
    
    if @diags_level > 0 print cast(sysdatetime() as varchar(23)) + ': before verticalize one, @name=' + @name + ', source_sql=' + @attr_system_source_sql

	--set @name = '$exprop_' + @name;
    exec sp_verticalize_node_attrs_one_metadata_table @attr_system_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from attr_names_cur into @attr_system_id, @name, @is_deleted
  end
  
  close attr_names_cur
  
  deallocate attr_names_cur 
end
GO
/****** Object:  Table [dbo].[bik_mssql_extended_properties]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_mssql_extended_properties](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NOT NULL,
	[node_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[value] [varchar](max) NULL,
	[database_name] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_mssql_extended_properties_node_id_name] UNIQUE NONCLUSTERED 
(
	[node_id] ASC,
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_ex_props]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_ex_props](@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare prop_names_cur cursor for select distinct name from bik_mssql_extended_properties

  declare @name varchar(512)
  
  open prop_names_cur
  
  fetch next from prop_names_cur into @name
  
  while @@fetch_status = 0 begin
    declare @prop_source_sql varchar(max) = '(select node_id, value from bik_mssql_extended_properties where name = '''+ cast(@name as varchar(20)) + ''')'
    
    set @name = '$exprop_' + @name;

    exec sp_verticalize_node_attrs_one_table @prop_source_sql, 'node_id', 'value', @name, @optNodeFilter    
  
    fetch next from prop_names_cur into @name
  end
  
  close prop_names_cur
  
  deallocate prop_names_cur
end
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_metadata]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_metadata](@optNodeFilter varchar(max))
as
begin
	declare @fixedOptNodeFilter varchar(max), @baseFixedOptNodeFilter varchar(max);
	if @optNodeFilter is null
	begin
		set @baseFixedOptNodeFilter = '(1 = 1)';
	end
	else
	begin
		set @baseFixedOptNodeFilter = '(' + @optNodeFilter + ')';
	end;

	-- SAP BO
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select report_tree_id from bik_sapbo_server union all select universe_tree_id from bik_sapbo_server union all select connection_tree_id from bik_sapbo_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_query', 'node_id', 'sql_text, filtr_text', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_connection', 'node_id', 'database_engine, database_source, connetion_networklayer_name, user_name, server', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_object', 'node_id', 'text_of_select, text_of_where', null, @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_sapbo_universe_table', 'node_id', 'sql_of_derived_table', null, @fixedOptNodeFilter
	
	declare @bo_extradata_sql varchar(max) = '(select node_id, author, owner, cuid, guid, ruid, convert(varchar(24),created,120) as created_time, convert(varchar(24),modified,120) as modified_time, convert(varchar(24),last_run_time,120) as last_run_time from bik_sapbo_extradata)';
	exec sp_verticalize_node_attrs_one_metadata_table @bo_extradata_sql, 'node_id', 'author, owner, cuid, guid, ruid, created_time, modified_time, last_run_time', null, @fixedOptNodeFilter
	
	declare @schedule_sql varchar(max) = '(select node_id, owner as schedule_owner, convert(varchar(24),creation_time,120) as creation_time, convert(varchar(24),nextruntime,120) as nextruntime, convert(varchar(24),expire,120) as expire, destination, format, parameters, destination_email_to from bik_sapbo_schedule)';
	exec sp_verticalize_node_attrs_one_metadata_table @schedule_sql, 'node_id', 'schedule_owner, destination, creation_time, nextruntime, expire, format, parameters, destination_email_to', null, @fixedOptNodeFilter
	
	declare @olap_sql varchar(max) = '(select node_id, type, provider_caption, cube_caption, cuid as olap_cuid, convert(varchar(24),created,120) as created, owner as olap_owner, convert(varchar(24),modified,120) as modified from bik_sapbo_olap_connection)';
	exec sp_verticalize_node_attrs_one_metadata_table @olap_sql, 'node_id', 'type, provider_caption, cube_caption, olap_cuid, created, olap_owner, modified', null, @fixedOptNodeFilter
	
	declare @obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as si_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''DataConnection'', ''Webi'', ''Flash'', ''CrystalReport'', ''Universe'', ''Excel'', ''FullClient'', ''Pdf'', ''Hyperlink'', ''Powerpoint'',
		''ReportFolder'', ''UniversesFolder'', ''ConnectionFolder'', ''ReportSchedule'', ''DSL.MetaDataFile'', ''CommonConnection'', ''CCIS.DataConnection''))'
	exec sp_verticalize_node_attrs_one_metadata_table @obj_id_sql, 'id', 'si_id', null, @fixedOptNodeFilter
	
	-- Bazy danych (Oracle, Postgres, ...)
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select distinct tree_id from bik_db_server))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_db_definition', 'object_node_id', 'definition', 'definition_sql', @fixedOptNodeFilter
	
	-- MS SQL
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''MSSQL''))';
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL')
	declare @mssql_sql varchar(max) = '(select bn.id, ms.definition_text from bik_mssql ms 
		inner join bik_node bn on bn.obj_id = ms.branch_names
		where bn.tree_id = ' +  cast(@mssql_tree_id as varchar(20)) + '
		and bn.is_deleted = 0
		and linked_node_id is null
		and ms.definition_text is not null)'
	exec sp_verticalize_node_attrs_one_metadata_table @mssql_sql, 'id', 'definition_text', null, @fixedOptNodeFilter
	-- Usuwanie starych ex propsów
	delete from bik_searchable_attr_val where attr_id in 
	(select att.id from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null)

	delete from bik_searchable_attr from bik_searchable_attr att 
	left join (select distinct name from bik_mssql_extended_properties prop) x on att.name = '$exprop_' + x.name
	where att.name like '$exprop_%' and x.name is null
	--
	exec sp_verticalize_node_attrs_ex_props @fixedOptNodeFilter
	  
	-- DQC
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''DQC''))';
	declare @tree_id int = (select id from bik_tree where code = 'DQC')
	declare @inact_nk_id int = (select id from bik_node_kind where code = 'DQCTestInactive'),
    @succ_nk_id int = (select id from bik_node_kind where code = 'DQCTestSuccess'),
    @fail_nk_id int = (select id from bik_node_kind where code = 'DQCTestFailed')

	declare @dqc_src_sql varchar(max) = '(select cast(dt.long_name as varchar(max)) as long_name, cast(dt.sampling_method as varchar(max)) as sampling_method, cast(dt.verified_attributes as varchar(max)) as verified_attributes,cast(dt.logging_details as varchar(max)) as logging_details, cast(dt.benchmark_definition as varchar(max)) as benchmark_definition, cast(dt.results_object as varchar(max)) as results_object, cast(dt.additional_information as varchar(max)) as additional_information, n.id as node_id
		from bik_dqc_test dt inner join bik_node n on dt.__obj_id = n.obj_id and n.node_kind_id in (' +
		cast(@inact_nk_id as varchar(20)) + ',' + cast(@succ_nk_id as varchar(20)) + ',' + cast(@fail_nk_id as varchar(20)) + ') and tree_id = ' + cast(@tree_id as varchar(20)) + '
		where n.linked_node_id is null and n.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @dqc_src_sql, 'node_id', 'long_name, sampling_method, verified_attributes,logging_details, benchmark_definition, results_object, additional_information', null, @fixedOptNodeFilter
	
	-- AD
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''UserRoles''))';
	declare @ad_src_sql varchar(max) = '(select coalesce(bu.email, ad.mail) as email, coalesce(bu.phone_num, ad.telephone_number) as phone_num,
		ad.physical_delivery_office_name, ad.postal_code, ad.manager, 
		ad.description, ad.department, ad.title, ad.mobile, 
		ad.company,
		ad.display_name, bu.node_id
		from bik_user bu
		join bik_node bn on bn.id = bu.node_id and bn.is_deleted = 0
		left join bik_system_user bsu on bsu.user_id = bu.id 
		left join bik_active_directory ad 
		on (bu.login_name_for_ad = coalesce(ad.domain + ''\'', '''') + ad.s_a_m_account_name or ad.s_a_m_account_name = bsu.sso_login and ad.domain = bsu.sso_domain))'
	exec sp_verticalize_node_attrs_one_metadata_table @ad_src_sql, 'node_id', 'email, phone_num, physical_delivery_office_name, postal_code, manager, description, department, title, mobile, company, display_name', null, @fixedOptNodeFilter
	
	-- SAP BW query extradata
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code in (''BWProviders'', ''BWReports'')))';
	declare @bwreports_id int = (select id from bik_tree where code = 'BWReports')
	declare @query_nk_id int = (select id from bik_node_kind where code = 'BWBEx')
	declare @sapbw_query_src_sql varchar(max) = '(select q.update_time, q.owner as sapbw_owner, q.last_edit, bn.id as node_id
		from bik_sapbw_query q inner join bik_node bn on q.obj_name = bn.obj_id and bn.node_kind_id = ' +
		cast(@query_nk_id as varchar(20)) + ' and bn.tree_id = ' + cast(@bwreports_id as varchar(20)) + '
		where bn.linked_node_id is null and bn.is_deleted = 0)'
	exec sp_verticalize_node_attrs_one_metadata_table @sapbw_query_src_sql, 'node_id', 'update_time, sapbw_owner, last_edit', null, @fixedOptNodeFilter
	
	-- SAP BW providers, query and areas - technical id
	declare @bw_obj_id_sql varchar(max) = '(select bn.id, bn.obj_id as technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWBEx'', ''BWArea'', ''BWMPRO'', ''BWCUBE'', ''BWISET'', ''BWODSO''))'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_obj_id_sql, 'id', 'technical_id', null, @fixedOptNodeFilter
	
	-- SAP BW objects - technical id
	declare @bw_objs_id_sql varchar(max) = '(select bn.id, obj.obj_name as obj_technical_id from bik_node bn
		inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
		inner join bik_sapbw_object obj on obj.provider + ''|'' + obj.obj_name = bn.obj_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null
		and bnk.code in (''BWUni'',''BWKyf'', ''BWTim'', ''BWDpa'', ''BWCha'')
		and obj.provider_parent is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @bw_objs_id_sql, 'id', 'obj_technical_id', null, @fixedOptNodeFilter
	
	-- Profile
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code = ''Profile''))';
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_item_extradata', 'item_node_id', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', 'libs, fid, di, nod, len, dft, dom, tbl, ptn, xpo, xpr, typ1, des, itp, min, max, dec, req, cmp, sfd, sfd1, sfd2, sfp, sft, siz, del, pos, rhd, srl, cnv, ltd, usr, mdd, val4ext, deprep, depostp, nullind, mddfid, validcmp', @fixedOptNodeFilter
	exec sp_verticalize_node_attrs_one_metadata_table 'bik_profile_file_extradata', 'item_node_id', 'libs, fid, global, akey1, akey2, akey3, akey4, akey5, akey6, akey7, del, syssn, netloc, parfid, mplctdd, dftdes, dftord, dfthdr, dftdes1, ltd, usr, filetyp, exist, extendlength, fsn, fdoc, qid1, acckeys, val4ext, predaen, screen, rflag, dflag, udacc, udfile, fpn, udpre, udpost, publish, zrsfile, glref, rectyp, ptruser, ptrtld, log, archfiles, archkey, ptrtim, ptruseru, ptrtldu, ptrtimu, listdft, listreq, des', null, @fixedOptNodeFilter

	-- File System
	set @fixedOptNodeFilter = @baseFixedOptNodeFilter + ' AND (n.tree_id in (select id from bik_tree where code like ''FileSystem%''))';
	declare @fileSystemSQL varchar(max) = '(select bn.id, DATEADD(MILLISECOND, fs.last_modified % 1000, DATEADD(SECOND, fs.last_modified / 1000, ''19700101'')) as modified, fs.download_link as file_name, fs.target_file_path
		from bik_node as bn
		inner join bik_file_system_info as fs on bn.id = fs.node_id
		where bn.is_deleted = 0
		and bn.linked_node_id is null)'
	exec sp_verticalize_node_attrs_one_metadata_table @fileSystemSQL, 'id', 'modified, file_name, target_file_path', null, @fixedOptNodeFilter
	
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_inner]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_inner](@optNodeFilter varchar(max))
as
begin
  exec sp_verticalize_node_attrs_one_table '(select id, name /*+ ''uqaz''*/ as name, coalesce(descr_plain, descr) as descr from bik_node)', 'id', 'name, descr', null, @optNodeFilter

  exec sp_verticalize_node_attrs_add_attrs @optNodeFilter

  exec sp_verticalize_node_attrs_metadata @optNodeFilter
end
GO
/****** Object:  StoredProcedure [dbo].[sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind](@sql_to_select_node_ids varchar(max), @opt_dependent_node_ids_before_change varchar(max)) as
begin
  set nocount on;

  declare @diags_level int = 0; -- 0 oznacza brak logowania, 1 - standardowe, 2 oznacza większe - z selectami

  if @diags_level > 0 begin
	  print cast(sysdatetime() as varchar(23)) + ': @sql_to_select_node_ids=' + coalesce(@sql_to_select_node_ids, '<null>');
  end;  

  declare @nksql nvarchar(max) = 'set @node_kind_id_ret = (select top 1 node_kind_id from bik_node where id in (' + @sql_to_select_node_ids + '))';

  --declare @node_kind_id int = (select node_kind_id from bik_node where id = @node_id);
  declare @node_kind_id int;
  
  exec sp_executesql @nksql, N'@node_kind_id_ret int output', @node_kind_id_ret=@node_kind_id output;

  if @diags_level > 0 begin
	  print cast(sysdatetime() as varchar(23)) + ': @node_kind_id=' + coalesce(cast(@node_kind_id as varchar(20)), '<null>');
  end;  

  if @node_kind_id is null return;  -- nie ma nic do roboty, ten select nic nie zwrócił (żadnych node'ów)

  --declare @sep_pos int = charindex(':::', @formula);
  
  -- declare @cols_def varchar(max) = rtrim(ltrim(substring(@formula, 1, @sep_pos - 1)));
  declare @cols_def varchar(max), @formula_expr varchar(max), @attr_dependent_nodes_sql varchar(max);
  
  select @cols_def = rtrim(ltrim(attr_calculating_types)), @formula_expr = rtrim(ltrim(attr_calculating_expressions)), @attr_dependent_nodes_sql = rtrim(ltrim(attr_dependent_nodes_sql))
  from bik_node_kind where id = @node_kind_id;

  if (@formula_expr is null or @formula_expr = '') and (@attr_dependent_nodes_sql is null or @attr_dependent_nodes_sql = '')
    return;
  
  declare @temp_tab_name_base sysname = '##biks_calc_formula_on_attr_vals__' + replace(convert(varchar(36),NEWID()), '-', '_');
  --declare @temp_tab_name_src sysname = case when @cols_def is null or @cols_def = '' then null else @temp_tab_name_base + '_src' end;
  if @cols_def = '' set @cols_def = null;
  
  declare @temp_tab_name_src sysname = @temp_tab_name_base + '_src';

  declare @create_temp_tab_sql varchar(max) = 'create table ' + @temp_tab_name_src + ' (node_id int' + case when @cols_def is null then '' else ', ' + @cols_def end + ')';
  
  --print @create_temp_tab_sql;
  exec(@create_temp_tab_sql);

  --declare @insert_node_id_sql varchar(max) = 'insert into ' + @temp_tab_name_src + ' (node_id) values (' + cast(@node_id as varchar(20)) + ')';
  declare @insert_node_id_sql varchar(max) = 'insert into ' + @temp_tab_name_src + ' (node_id) ' + @sql_to_select_node_ids;
  exec(@insert_node_id_sql)
	   
  if @cols_def is not null
  begin	  
	  -- declare @formula_expr varchar(max) = ltrim(rtrim(substring(@formula, @sep_pos + 3, len(@formula))));
	  -- declare @formula_expr varchar(max) = ltrim(rtrim((select attr_calculating_expressions from bik_node_kind where id = @node_kind_id)));
	   
	  declare @temp_tab_id int = object_id('tempdb..' + @temp_tab_name_src);
	 
	  declare @tempdb_schema_name sysname = (select ss.name from tempdb.sys.objects so inner join tempdb.sys.schemas ss on so.schema_id = ss.schema_id
		where so.object_id = @temp_tab_id);

	  --declare @cnt int = 0;
	 
	  declare @col_name sysname, @type_name varchar(max);  
	 
	  declare cur cursor for 
	  SELECT 
			column_name, 
			data_type + case data_type
				when 'sql_variant' then ''
				when 'text' then ''
				when 'ntext' then ''
				when 'xml' then ''
				when 'decimal' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
				when 'numeric' then '(' + cast(numeric_precision as varchar) + ', ' + cast(numeric_scale as varchar) + ')'
				else coalesce('('+case when character_maximum_length = -1 then 'max' else cast(character_maximum_length as varchar) end +')','') end        
		   from tempdb.information_schema.columns where table_name = @temp_tab_name_src and table_schema = @tempdb_schema_name
		   order by ordinal_position;

	  open cur;

	  fetch next from cur into @col_name, @type_name;

	  while @@fetch_status = 0
	  begin    
		declare @col_name_quoted varchar(max) = quotename(@col_name);
		declare @val_expr varchar(max) = 'cast(value as ' + @type_name + ')';
	    
		-- tu można zmienić @val_expr na inną konwersję w zależności od @type_name
    if charindex('numeric', @type_name) <> 0 -- poprawka dla typu numeric
	  begin
			set @val_expr = 'cast(replace(value,'','', ''.'') as ' + @type_name + ')';
	  end;				
		
		--print @col_name_quoted + ' ' + @type_name;

		declare @conv_one_val_sql varchar(max);
	    
		declare @attribute_id int = (select a.id from bik_attr_def ad inner join bik_attribute a on ad.id = a.attr_def_id where ad.name = @col_name and a.node_kind_id = @node_kind_id);

		--declare @select_val_conv_sql varchar(max) = 'select ' + @val_expr + ' from bik_attribute_linked where node_id = ' + cast(@node_id as varchar(10)) + ' and attribute_id = ' + cast(@attribute_id as varchar(10));	    
		--set @conv_one_val_sql = 'update ' + @temp_tab_name_src + ' set ' + @col_name_quoted + ' = (' + @select_val_conv_sql + ')';

		declare @select_val_conv_sql varchar(max) = 'select ' + @val_expr + ' from bik_attribute_linked where node_id = ' + @temp_tab_name_src + '.node_id and attribute_id = ' + cast(@attribute_id as varchar(10));	    
		set @conv_one_val_sql = 'update ' + @temp_tab_name_src + ' set ' + @col_name_quoted + ' = (' + @select_val_conv_sql + ')';
	    
		--print @conv_one_val_sql;
	    
		exec(@conv_one_val_sql);
	      
		--set @cnt = @cnt + 1;
		fetch next from cur into @col_name, @type_name;
	  end;
	 
	  close cur;
	  deallocate cur;
  end;

  declare @print_sql varchar(max);
  if @diags_level > 1 begin
    set @print_sql = 'select * from ' + @temp_tab_name_src;
    exec(@print_sql);
  end;
  
  if @formula_expr is not null and @formula_expr <> ''
  begin	 
    declare @temp_tab_name_dst sysname = @temp_tab_name_base + '_dst';
    
    --declare @select_from_temp_sql varchar(max) = 'select node_id, ' + @formula_expr + ' into ' + @temp_tab_name_dst + case when @temp_tab_name_src is null then '' else ' from ' + @temp_tab_name_src end;
    declare @select_from_temp_sql varchar(max);
    
    if (substring(ltrim(@formula_expr), 1, 6) = 'select')
      --set @select_from_temp_sql = replace(replace(@formula_expr, '${src_vals}$', @temp_tab_name_src), '${dst_vals}$', @temp_tab_name_dst)
      set @select_from_temp_sql = 'select * into ' + @temp_tab_name_dst + ' from (' + replace(@formula_expr, '${src_vals}$', @temp_tab_name_src) + ') x'
    else
      set @select_from_temp_sql = 'select node_id, ' + @formula_expr + ' into ' + @temp_tab_name_dst + ' from ' + @temp_tab_name_src + ' as x';
    
    if @diags_level > 0 begin
	    print cast(sysdatetime() as varchar(23)) + ': @select_from_temp_sql=' + @select_from_temp_sql;
    end;  
      
    exec(@select_from_temp_sql);
    
    if @diags_level > 0 begin
	    print cast(sysdatetime() as varchar(23)) + ': after exec @select_from_temp_sql';
    end;  

    if @diags_level > 1 begin
      set @print_sql = 'select * from ' + @temp_tab_name_dst;
      exec(@print_sql);
    end;

    declare @temp_tab_id_dst int = object_id('tempdb..' + @temp_tab_name_dst);
   
    declare @tempdb_schema_name_dst sysname = (select ss.name from tempdb.sys.objects so inner join tempdb.sys.schemas ss on so.schema_id = ss.schema_id
	  where so.object_id = @temp_tab_id_dst);

    --declare @cnt int = 0;
   
    declare @col_name_dst sysname, @type_name_dst varchar(max);  
   
    declare cur_dst cursor for 
    SELECT 
		  column_name, 
		  data_type
	     from tempdb.information_schema.columns where table_name = @temp_tab_name_dst and table_schema = @tempdb_schema_name_dst
	     order by ordinal_position;

    open cur_dst;

    fetch next from cur_dst into @col_name_dst, @type_name_dst;

    while @@fetch_status = 0
    begin
      declare @attr_def_id_dst int = (select id from bik_attr_def where name = @col_name_dst);
  	
      declare @attribute_id_dst int = (select id from bik_attribute where node_kind_id = @node_kind_id and attr_def_id = @attr_def_id_dst);     

	  declare @convert_dst_expr nvarchar(max) = 'cast(x.' + quotename(@col_name_dst) + ' as varchar(max))';
	  -- tu opcjonalnie inne konwersje zależnie od typu @type_name_dst
      
      declare @merge_sql varchar(max) = 'MERGE INTO bik_attribute_linked as av USING ' + @temp_tab_name_dst + ' as x' +
        ' ON x.node_id = av.node_id and av.attribute_id = ' + cast(@attribute_id_dst as varchar(20)) +
        ' WHEN MATCHED and x.' + quotename(@col_name_dst) + ' is null THEN delete' +
        ' WHEN MATCHED THEN update set av.value = ' + @convert_dst_expr +
        ' WHEN NOT MATCHED BY TARGET and x.' + quotename(@col_name_dst) + ' is not null
          THEN insert (attribute_id, node_id, value, is_deleted) values (' + cast(@attribute_id_dst as varchar(20)) + ', x.node_id, ' + @convert_dst_expr + ', 0);';

    if @diags_level > 0 begin
	    print cast(sysdatetime() as varchar(23)) + '@merge_sql: ' + @merge_sql;
	  end;
    exec(@merge_sql);

	  /*
	  declare @ssql nvarchar(max) = 'select @retvalout=' + @convert_dst_expr + ' from ' + @temp_tab_name_dst;
  		
	  declare @retval varchar(max);   

	  exec sp_executesql @ssql, N'@retvalout varchar(max) output', @retvalout=@retval output;

      declare @attr_def_id_dst int = (select id from bik_attr_def where name = @col_name_dst);

      if @attr_def_id_dst is not null
      begin
	    declare @attribute_id_dst int = (select id from bik_attribute where node_kind_id = @node_kind_id and attr_def_id = @attr_def_id_dst);     
      
        if @retval is null or @retval = ''
        begin
          delete from bik_attribute_linked 
		  where attribute_id = @attribute_id_dst and node_id = @node_id;
        end
        else
        begin
          update bik_attribute_linked 
		  set value = @retval, is_deleted = 0
		  where attribute_id = @attribute_id_dst and node_id = @node_id;
  	      
		  if @@rowcount = 0
		  begin
		    insert into bik_attribute_linked (attribute_id, node_id, value, is_deleted)
		    values (@attribute_id_dst, @node_id, @retval, 0);
		  end;
        end;
              
        --select top 10 * from bik_attribute_linked
      end;
      */
      
      fetch next from cur_dst into @col_name_dst, @type_name_dst;
    end;
    
    close cur_dst;
    deallocate cur_dst;
    
    declare @drop_dyn_tab_sql_dst varchar(max) = 'drop table ' + @temp_tab_name_dst;
    exec(@drop_dyn_tab_sql_dst);
  end;
    
  declare @dependent_nodes_sql varchar(max) = replace(@attr_dependent_nodes_sql, '${node_ids}$', 
    '(select node_id from ' + @temp_tab_name_src + ')');  

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': @dependent_nodes_sql=' + coalesce(@dependent_nodes_sql, '<null>');
  end;    

  if @dependent_nodes_sql is not null and @dependent_nodes_sql <> '' 
  begin
    declare @nodes_to_recalc table (node_id int not null, node_kind_id int);

    if @opt_dependent_node_ids_before_change is not null
    begin
      insert into @nodes_to_recalc (node_id)
      select cast(str as int) from dbo.fn_split_by_sep(@opt_dependent_node_ids_before_change, ',', 7);      
    end;
    
    --declare @dep_nodes_with_kinds_sql varchar(max) = 'select x.node_id, n.node_kind_id from (' + @dependent_nodes_sql + ') x inner join bik_node n on x.node_id = n.id';
    --insert into @nodes_to_recalc (node_id, node_kind_id) exec(@dep_nodes_with_kinds_sql);

    insert into @nodes_to_recalc (node_id) exec(@dependent_nodes_sql);
    update ntc set node_kind_id = n.node_kind_id from @nodes_to_recalc as ntc inner join bik_node n on n.id = ntc.node_id;

    declare dn_cur cursor local for select distinct node_id, node_kind_id from @nodes_to_recalc where node_kind_id is not null order by node_kind_id;  

    open dn_cur;
    
    declare @dn_node_id int, @dn_node_kind_id int;
    
    declare @dn_node_ids_as_str varchar(max) = '';
    declare @dn_node_ids_for_in varchar(max) = '';
    
    fetch next from dn_cur into @dn_node_id, @dn_node_kind_id;
    declare @prev_dn_node_kind_id int = @dn_node_kind_id;
    
    while @@fetch_status = 0
    begin
      if @prev_dn_node_kind_id <> @dn_node_kind_id
      begin
        declare @select_dn_ids_sql varchar(max) = 'select node_id from (values ' + @dn_node_ids_as_str + ') as xxx(node_id)';
        --select node_id from (values (10), (20), (30)) as x(node_id) 
        
        exec sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind @select_dn_ids_sql, null;      
        
        set @prev_dn_node_kind_id = @dn_node_kind_id;
        set @dn_node_ids_as_str = '';      
      end;
    
      set @dn_node_ids_as_str = @dn_node_ids_as_str + case when @dn_node_ids_as_str <> '' then ',' else '' end + '(' + cast(@dn_node_id as varchar(20)) + ')';
      set @dn_node_ids_for_in = @dn_node_ids_for_in + case when @dn_node_ids_for_in <> '' then ',' else '' end + cast(@dn_node_id as varchar(20));
    
      fetch next from dn_cur into @dn_node_id, @dn_node_kind_id;
    end;

    close dn_cur;
    deallocate dn_cur;  

    if @dn_node_ids_as_str <> ''
    begin
      set @select_dn_ids_sql = 'select node_id from (values ' + @dn_node_ids_as_str + ') as xxx(node_id)';
      exec sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind @select_dn_ids_sql, null;      
    end;    
    
    if @dn_node_ids_for_in <> ''
    begin
      declare @node_filter varchar(max) = 'n.id in (' + @dn_node_ids_for_in + ')';
      
      if @diags_level > 0 begin
        print cast(sysdatetime() as varchar(23)) + ': @node_filter=' + coalesce(@node_filter, '<null>');
      end;    
            
      exec sp_verticalize_node_attrs_inner @node_filter;
    end;    
  end;
    
  declare @drop_dyn_tab_sql varchar(max) = 'drop table ' + @temp_tab_name_src;
  exec(@drop_dyn_tab_sql);
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_calc_formula_on_attr_vals]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_calc_formula_on_attr_vals](@node_id int) as
begin
  set nocount on;
  
  declare @sql varchar(max) = 'select ' + cast(@node_id as varchar(20));
  
  exec sp_calc_formula_on_attr_vals_of_nodes_in_one_node_kind @sql, null;
end;
GO
/****** Object:  Table [dbo].[aaa_universe_connection_networklayer]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_connection_networklayer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_connection]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_connection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[connetion_name] [varchar](1000) NOT NULL,
	[server] [varchar](255) NULL,
	[user_name] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[database_source] [varchar](500) NULL,
	[connetion_networklayer_id] [int] NOT NULL,
	[type] [varchar](255) NULL,
	[database_enigme] [varchar](255) NULL,
	[client_number] [int] NULL,
	[language] [varchar](10) NULL,
	[system_number] [varchar](255) NULL,
	[system_id] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[connetion_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[unique_id] [varchar](300) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[file_name] [varchar](255) NOT NULL,
	[author] [varchar](120) NULL,
	[universe_connetion_id] [int] NULL,
	[path] [varchar](500) NOT NULL,
	[global_props_si_id] [int] NULL,
	[statistic] [varchar](500) NULL,
	[update_ts] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_id_path_aaa_universe] UNIQUE NONCLUSTERED 
(
	[unique_id] ASC,
	[path] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_connection_idt]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_connection_idt](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[si_id] [int] NOT NULL,
	[connetion_name] [varchar](900) NOT NULL,
	[connetion_path] [varchar](900) NOT NULL,
	[server] [varchar](255) NULL,
	[user_name] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[database_source] [varchar](500) NULL,
	[network_layer] [varchar](500) NOT NULL,
	[type] [varchar](255) NULL,
	[database_engine] [varchar](255) NULL,
	[client_number] [int] NULL,
	[language] [varchar](10) NULL,
	[system_number] [varchar](255) NULL,
	[system_id] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[si_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_table]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_table](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[their_id] [int] NULL,
	[name] [varchar](255) NOT NULL,
	[is_alias] [int] NOT NULL,
	[is_derived] [int] NOT NULL,
	[original_table] [int] NULL,
	[sql_of_derived_table] [varchar](max) NULL,
	[sql_of_derived_table_with_alias] [varchar](max) NULL,
	[universe_id] [int] NULL,
	[universe_branch] [varchar](1000) NULL,
	[idt_connection_id] [int] NULL,
	[idt_id] [varchar](500) NULL,
	[idt_original_id] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_unique_their_id_idt_id_aaa_universe_table] UNIQUE NONCLUSTERED 
(
	[their_id] ASC,
	[idt_id] ASC,
	[universe_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_node_kind_id_by_code]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_node_kind_id_by_code](@code varchar(256))
returns int
as
begin
  return (select id from bik_node_kind where code = @code)
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_bo_actual_connection_tree_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_bo_actual_connection_tree_id]()
		returns int
		as
		begin
			return 82;
		end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_bo_actual_report_tree_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_bo_actual_report_tree_id]()
		returns int
		as
		begin
			return 80;
		end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_bo_actual_server_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_bo_actual_server_id]()
		returns int
		as
		begin
			return 9;
		end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_bo_actual_universe_tree_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_bo_actual_universe_tree_id]()
		returns int
		as
		begin
			return 81;
		end
GO
/****** Object:  StoredProcedure [dbo].[sp_prepare_bik_joined_objs_tmp]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_prepare_bik_joined_objs_tmp]
as
begin
	if exists(select 1 from sysobjects where id = object_id('tmp_bik_joined_objs'))
	drop table tmp_bik_joined_objs

	CREATE TABLE tmp_bik_joined_objs(
	[src_node_id] [int]  NULL,
	[dst_node_id] [int]  NULL,
	[type] [int]  NULL,
	[inherit_to_descendants] [int]  not null default 0
	)
end
GO
/****** Object:  Table [dbo].[bik_joined_objs]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_joined_objs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[src_node_id] [int] NOT NULL,
	[dst_node_id] [int] NOT NULL,
	[type] [int] NOT NULL,
	[inherit_to_descendants] [int] NOT NULL,
 CONSTRAINT [pk_bik_joined_objs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_joined_objs_src_dst] UNIQUE NONCLUSTERED 
(
	[src_node_id] ASC,
	[dst_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_joined_objs_dst_node_id_src_node_id_inherit_to_descendants] ON [dbo].[bik_joined_objs] 
(
	[dst_node_id] ASC,
	[src_node_id] ASC,
	[inherit_to_descendants] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_joined_objs_src_node_id_dst_node_id_inherit_to_descendants] ON [dbo].[bik_joined_objs] 
(
	[src_node_id] ASC,
	[dst_node_id] ASC,
	[inherit_to_descendants] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_server]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_server](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[name] [varchar](200) NULL,
	[server] [varchar](200) NULL,
	[user_name] [varchar](200) NULL,
	[password] [varchar](200) NULL,
	[with_users] [int] NOT NULL,
	[open_report_template] [varchar](200) NULL,
	[check_rights] [int] NOT NULL,
	[report_tree_id] [int] NOT NULL,
	[universe_tree_id] [int] NOT NULL,
	[connection_tree_id] [int] NOT NULL,
	[is_active] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
	[is_bo40] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_bik_joined_objs_by_kinds_fast]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_delete_bik_joined_objs_by_kinds_fast] @kinds_one_str varchar(max), @kinds_two_str varchar(max) as
begin
  set nocount on

  declare @timestamp_start_total datetime = current_timestamp
  declare @diags_level int = 0 -- wartość 0 oznacza brak logowania, 1 i więcej - jest logowanie
  declare @rc int
  declare @timestamp_start datetime 
  declare @timestamp_end datetime
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: start' +
      ', @kinds_one_str = ' + @kinds_one_str + 
      ', @kinds_two_str = ' + @kinds_two_str
  end

  -- declare @kinds_one varchar(max) = 'Universe', @kinds_two varchar(max) = 'TeradataSchema'
  declare @codes_one table (code varchar(255) not null primary key)
  declare @codes_two table (code varchar(255) not null primary key)

  insert into @codes_one (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_one_str, ',', 7)

  insert into @codes_two (code)
  select distinct str
  from dbo.fn_split_by_sep(@kinds_two_str, ',', 7)

  declare @kind_one_id table (id int not null primary key)
  declare @kind_two_id table (id int not null primary key)

  insert into @kind_one_id (id)
  select distinct k.id
  from @codes_one x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'
  
  insert into @kind_two_id (id)
  select distinct k.id
  from @codes_two x inner join bik_node_kind k on x.code = k.code
  where substring(x.code, 1, 1) <> '@'

  declare @tree_one_id table (id int not null primary key)
  declare @tree_two_id table (id int not null primary key)

  delete from @codes_one where substring(code, 1, 1) <> '@'
  delete from @codes_two where substring(code, 1, 1) <> '@'

  update @codes_one set code = substring(code, 2, len(code))
  update @codes_two set code = substring(code, 2, len(code))

  -- fix tree codes
  declare @reportTreeCode varchar(255), @universeTreeCode varchar(255), @connectionTreeCode varchar(255);
  select @reportTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_report_tree_id()
  select @universeTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_universe_tree_id()  
  select @connectionTreeCode = code from bik_tree where id = dbo.fn_get_bo_actual_connection_tree_id()
  
  update @codes_one
  set code = @reportTreeCode
  where code = 'Reports'
  
  update @codes_one
  set code = @universeTreeCode
  where code = 'ObjectUniverses'
  
  update @codes_one
  set code = @connectionTreeCode
  where code = 'Connections'
  
  update @codes_two
  set code = @reportTreeCode
  where code = 'Reports'
  
  update @codes_two
  set code = @universeTreeCode
  where code = 'ObjectUniverses'
  
  update @codes_two
  set code = @connectionTreeCode
  where code = 'Connections'  
  -- end fix


  insert into @tree_one_id (id)
  select distinct t.id
  from @codes_one x inner join bik_tree t on x.code = t.code

  insert into @tree_two_id (id)
  select distinct t.id
  from @codes_two x inner join bik_tree t on x.code = t.code

  declare @tree_id_to_kind_id table (tree_id int not null, node_kind_id int not null)
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @tree_id_to_kind_id'
  end

  set @timestamp_start = current_timestamp
  
  insert into @tree_id_to_kind_id (tree_id, node_kind_id)
  select distinct tree_id, node_kind_id
  from bik_node where is_deleted = 0 and linked_node_id is null

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @tree_id_to_kind_id, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end

  insert into @kind_one_id (id)
  select t2k.node_kind_id
  from @tree_one_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_one_id k on k.id = t2k.node_kind_id
  where k.id is null

  insert into @kind_two_id (id)
  select t2k.node_kind_id
  from @tree_two_id t inner join @tree_id_to_kind_id t2k on t.id = t2k.tree_id
  left join @kind_two_id k on k.id = t2k.node_kind_id
  where k.id is null

  declare @joined_ids_to_del table (id int not null primary key)

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before insert into @joined_ids_to_del'
  end

  set @timestamp_start = current_timestamp
  
  declare @actualServerId int = dbo.fn_get_bo_actual_server_id();
  declare @actualSourceType int;
  select @actualSourceType = source_id from bik_sapbo_server where id = @actualServerId
  declare @otherInstanceBOTree table (
	 tree_id int not null
  );
  
  insert into @otherInstanceBOTree(tree_id)
  select report_tree_id from bik_sapbo_server
  where id <> @actualServerId
  --and source_id = @actualSourceType
  union all
  select universe_tree_id from bik_sapbo_server
  where id <> @actualServerId
  --and source_id = @actualSourceType
  union all
  select connection_tree_id from bik_sapbo_server
  where id <> @actualServerId
  --and source_id = @actualSourceType  

  insert into @joined_ids_to_del (id)
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.src_node_id = n_one.id
  inner join bik_node n_two on jo.dst_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  and n_one.tree_id not in (select tree_id from @otherInstanceBOTree)
  and n_two.tree_id not in (select tree_id from @otherInstanceBOTree)
  union -- bez all, bo mogą być duplikaty, a chcemy je właśnie wyeliminować
  select jo.id 
  from bik_joined_objs jo inner join bik_node n_one on jo.dst_node_id = n_one.id
  inner join bik_node n_two on jo.src_node_id = n_two.id
  inner join @kind_one_id k1 on n_one.node_kind_id = k1.id
  inner join @kind_two_id k2 on n_two.node_kind_id = k2.id
  where jo.type = 1
  and n_one.tree_id not in (select tree_id from @otherInstanceBOTree)
  and n_two.tree_id not in (select tree_id from @otherInstanceBOTree)

  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after insert into @joined_ids_to_del, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
  end
  
  --select count(*) from @joined_ids_to_del

  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: before final delete'
  end

  set @timestamp_start = current_timestamp
  
  delete from bik_joined_objs where id in (select id from @joined_ids_to_del)
  
  set @rc = @@rowcount
  set @timestamp_end = current_timestamp
  declare @timestamp_end_total datetime = current_timestamp
  
  if @diags_level > 0 begin
    print cast(sysdatetime() as varchar(23)) + ': sp_delete_bik_joined_objs_by_kinds_fast: after final delete, @rc='
      + cast(@rc as varchar(20)) + ', time taken: ' + cast(datediff(ms, @timestamp_start, @timestamp_end) / 1000.0 as varchar(20)) + ' s'
      + ', TOTAL TIME TAKEN: ' + cast(datediff(ms, @timestamp_start_total, @timestamp_end_total) / 1000.0 as varchar(20)) + ' s'
  end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_move_bik_joined_objs_tmp]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_move_bik_joined_objs_tmp] (@oneGroup varchar(max), @twoGroup varchar(max))
as
begin
	if exists(select 1 from sysobjects where id = object_id('tmp_unique_bik_joined_objs'))
	drop table tmp_unique_bik_joined_objs
	
	create table [dbo].[tmp_unique_bik_joined_objs](
	[src_node_id] [int]  NULL,
	[dst_node_id] [int]  NULL,
	[type] [int]  NULL,
	[inherit_to_descendants] [int] not null default 0
	);
	
	-- wykonanie usuwania
	exec sp_delete_bik_joined_objs_by_kinds_fast @oneGroup, @twoGroup

	insert into tmp_unique_bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
	select distinct *
	from tmp_bik_joined_objs
	
	insert into bik_joined_objs(src_node_id, dst_node_id, type, inherit_to_descendants)
	select u.src_node_id, u.dst_node_id, u.type,u.inherit_to_descendants from tmp_unique_bik_joined_objs u left join bik_joined_objs bjo on bjo.src_node_id=u.src_node_id
    and bjo.dst_node_id=u.dst_node_id
    where bjo.id is null
    
    update bik_joined_objs
    set type=1
    from tmp_unique_bik_joined_objs u
    where     
    bik_joined_objs.src_node_id=u.src_node_id 
    and bik_joined_objs.dst_node_id=u.dst_node_id 
    and bik_joined_objs.type = 0
	
	if exists(select 1 from sysobjects where id = object_id('tmp_bik_joined_objs'))
	drop table tmp_bik_joined_objs
	
	if exists(select 1 from sysobjects where id = object_id('tmp_unique_bik_joined_objs'))
	drop table tmp_unique_bik_joined_objs
end
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_universe_objects]
as
begin
	declare @repTree int = dbo.fn_get_bo_actual_report_tree_id();
	declare @uniTree int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();
	declare @webiKind int = dbo.fn_node_kind_id_by_code('Webi');
    declare @uniKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcKind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    
    -- dodanie polaczen universe <-> connection
    exec sp_prepare_bik_joined_objs_tmp
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, bnu.id, 1 
    from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bnu.id, bn.id, 1 
    from aaa_universe uni
	inner join aaa_universe_connection con on uni.universe_connetion_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.name = con.connetion_name 
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id = @uniKind
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	-- dla IDT
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id, bnu.id, 1 
    from aaa_universe uni
    inner join aaa_universe_table tab on uni.id = tab.universe_id
	inner join aaa_universe_connection_idt con on tab.idt_connection_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.obj_id = convert(varchar(30), con.si_id)
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id in (@uniKind, @uniNewKind)
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bnu.id, bn.id, 1 
    from aaa_universe uni
    inner join aaa_universe_table tab on uni.id = tab.universe_id
	inner join aaa_universe_connection_idt con on tab.idt_connection_id = con.id
	inner join bik_node bn on bn.tree_id = @connTree and bn.obj_id = convert(varchar(30), con.si_id)
	inner join bik_node bnu on bnu.tree_id = @uniTree and bnu.obj_id = convert(varchar(30), uni.global_props_si_id)
	where bn.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bnu.node_kind_id in (@uniKind, @uniNewKind)
	and bnu.is_deleted = 0
	and bnu.linked_node_id is null
	
	/*
	-- dodanie polaczen universe <-> conn OLAP
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_DSL__METADATAFILE')
	begin
		declare @connColCode varchar(255) = 'SI_SL_UNIVERSE_TO_CONNECTIONS__'
		declare @num int = 1
		declare @connTableCodeBase varchar(255) = @connColCode
		set @connColCode = @connColCode + cast(@num as varchar(20))
		
		declare @table table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @connColCode and Object_ID = Object_ID('APP_DSL__METADATAFILE'))
		begin
			insert into @table(col_name)
			values (@connColCode)
			
			set @num = @num + 1  
			set @connColCode = @connTableCodeBase + cast(@num as varchar(20))
		end
		
		declare curs cursor for 
		select col_name from @table order by col_name 

		open curs;
		declare @column varchar(50);

		fetch next from curs into @column;
		while @@fetch_status = 0
		begin
			declare @sql1 varchar(max);
			declare @sql2 varchar(max);
			set @sql1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'
			
			set @sql2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from APP_DSL__METADATAFILE uni
			inner join bik_node bnu on bnu.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),uni.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),uni.' + @column + ')
			where bnu.is_deleted = 0
			and bnc.is_deleted = 0'

			exec(@sql1);
			exec(@sql2);
			fetch next from curs into @column;
		end
		close curs;
		deallocate curs;
	end
	*/
	
    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile', 'DataConnection,CCIS.DataConnection,CommonConnection'
    
    /*
    -- dodanie polaczen report <-> uni,conn
    -- tylko dla BO 4.0, gdyż nie działa Report SDK
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'INFO_WEBI')
	begin
	    
		exec sp_prepare_bik_joined_objs_tmp
	    
		---------------------
		declare @webiColCode varchar(255) = 'SI_UNIVERSE__'
		declare @numWebi int = 1
		declare @webiTableCodeBase varchar(255) = @webiColCode
		set @webiColCode = @webiColCode + cast(@numWebi as varchar(20))
		
		declare @tableWebi table (
			col_name varchar(max) not null
		);
		
		while exists(select 1 from sys.columns where name = @webiColCode and Object_ID = Object_ID('INFO_WEBI'))
		begin
			insert into @tableWebi(col_name)
			values (@webiColCode)
			
			set @numWebi = @numWebi + 1  
			set @webiColCode = @webiTableCodeBase + cast(@numWebi as varchar(20))
		end
		
		declare cursWebi cursor for 
		select col_name from @tableWebi order by col_name 

		open cursWebi;
		declare @columnWebi varchar(50);

		fetch next from cursWebi into @columnWebi;
		while @@fetch_status = 0
		begin
			declare @sqlWebi1 varchar(max);
			declare @sqlWebi2 varchar(max);
			declare @sqlWebi3 varchar(max);
			declare @sqlWebi4 varchar(max);
			
			set @sqlWebi1 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnc.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			where bnu.is_deleted = 0 and bnc.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')'
			
			set @sqlWebi2 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnc.id, bnu.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			where bnu.is_deleted = 0 and bnc.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')'
			
			set @sqlWebi3 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnj.id, bnu.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			inner join bik_joined_objs obj on obj.src_node_id = bnc.id and obj.type = 1
			inner join bik_node bnj on obj.dst_node_id = bnj.id
			where bnu.is_deleted = 0 and bnc.is_deleted = 0 and bnj.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')
			and bnj.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnj.node_kind_id in (' + cast(@dcKind as varchar(30)) + ',' + cast(@dcNewKind as varchar(30)) + ',' + cast(@dcNewOLAPKind as varchar(30)) + ')'

			set @sqlWebi4 = 'insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
			select bnu.id, bnj.id, 1 from INFO_WEBI web
			inner join bik_node bnu on bnu.tree_id = ' + cast(@repTree as varchar(30)) + ' and bnu.obj_id = convert(varchar(30),web.si_id)
			inner join bik_node bnc on bnc.tree_id = ' + cast(@uniTree as varchar(30)) + ' and bnc.obj_id = convert(varchar(30),web.' + @columnWebi + ')
			inner join bik_joined_objs obj on obj.src_node_id = bnc.id and obj.type = 1
			inner join bik_node bnj on obj.dst_node_id = bnj.id
			where bnu.is_deleted = 0 and bnc.is_deleted = 0 and bnj.is_deleted = 0
			and bnu.node_kind_id = ' + cast(@webiKind as varchar(30)) + ' and bnc.node_kind_id in (' + cast(@uniKind as varchar(30)) + ',' + cast(@uniNewKind as varchar(30)) + ')
			and bnj.tree_id = ' + cast(@connTree as varchar(30)) + ' and bnj.node_kind_id in (' + cast(@dcKind as varchar(30)) + ',' + cast(@dcNewKind as varchar(30)) + ',' + cast(@dcNewOLAPKind as varchar(30)) + ')'

			exec(@sqlWebi1);
			exec(@sqlWebi2);
			exec(@sqlWebi3);
			exec(@sqlWebi4);
			
			--print @sqlWebi1
			--print @sqlWebi2
			--print @sqlWebi3
			--print @sqlWebi4
			
			fetch next from cursWebi into @columnWebi;
		end
		close cursWebi;
		deallocate cursWebi;
		---------------------
		
		exec sp_move_bik_joined_objs_tmp 'Webi', 'DataConnection,CCIS.DataConnection,CommonConnection,Universe,DSL.MetaDataFile'
	end
	*/
    
	-- dodanie polaczen universe objcets <-> uni,conn
    create table #tmpBranch (branch_id varchar(max) collate DATABASE_DEFAULT, node_id int);

    insert into #tmpBranch(branch_id,node_id)
    select bn.branch_ids, bn2.id 
    from bik_node bn
    inner join bik_joined_objs jo on jo.src_node_id = bn.id
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    where bn.tree_id = @uniTree
    and bn.node_kind_id in (@uniKind, @uniNewKind)
    and bn.linked_node_id is null
    and bn.is_deleted = 0
    and bn2.tree_id = @connTree
    and bn2.node_kind_id in (@dcKind, @dcNewKind, @dcNewOLAPKind)
    and bn2.linked_node_id is null
    and bn2.is_deleted = 0

    exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn.id, a.node_id, 1 
	from #tmpBranch a 
	inner join bik_node bn on bn.branch_ids like a.branch_id + '%'
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	where node_kind_id not in (@uniKind, @uniNewKind)
		
	insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
	select bn2.id, bn.id, 1 
	from bik_node bn 
	inner join bik_node bn2 on bn2.branch_ids like bn.branch_ids + '%'
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id not in (@uniKind, @uniNewKind)
	and bn.tree_id = @uniTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id in (@uniKind, @uniNewKind)

    exec sp_move_bik_joined_objs_tmp 'Universe,DSL.MetaDataFile,DataConnection,CCIS.DataConnection,CommonConnection', 'UniverseClass,Measure,Dimension,Detail,Attribute,Filter,UniverseAliasTable,UniverseDerivedTable,UniverseTable,UniverseTablesFolder'
	
	drop table #tmpBranch
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_connections_for_universe_objects]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_connections_for_universe_objects]
as
begin
	exec sp_insert_into_bik_joined_objs_universe_objects;
end
GO
/****** Object:  Table [dbo].[amg_node]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[amg_node](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[obj_id] [varchar](900) NOT NULL,
	[parent_obj_id] [varchar](900) NULL,
	[node_kind_code] [varchar](255) NOT NULL,
	[name] [varchar](900) NOT NULL,
	[descr] [varchar](max) NULL,
	[visual_order] [int] NOT NULL,
	[branch_ids] [varchar](900) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_amg_node_node_kind_code] ON [dbo].[amg_node] 
(
	[node_kind_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_amg_node_parent_obj_id] ON [dbo].[amg_node] 
(
	[parent_obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_amg_node_obj_id] ON [dbo].[amg_node] 
(
	[obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  UserDefinedTableType [dbo].[bik_unique_not_null_id_table_type]    Script Date: 09/28/2016 11:08:59 ******/
CREATE TYPE [dbo].[bik_unique_not_null_id_table_type] AS TABLE(
	[id] [int] NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO
/****** Object:  StoredProcedure [dbo].[sp_node_init_branch_idEx]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_node_init_branch_idEx](@node_ids bik_unique_not_null_id_table_type readonly)
as
begin  
  set nocount on

  declare @diags_level int = 0;


	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #1'

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select id from @node_ids

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #3'
    
    update bik_node
    set branch_ids = coalesce(parent.branch_ids, '') + CAST(bik_node.id as varchar(10)) + '|'
    from bik_node inner join #ids on bik_node.id = #ids.id
      left join bik_node as parent on parent.id = bik_node.parent_node_id;    

	/*
    update bik_node
    set branch_ids = coalesce((select parent.branch_ids from bik_node as parent where parent.id = bik_node.parent_node_id), '')
    + CAST(id as varchar(10)) + '|'
    where bik_node.id in (select id from #ids);
    */
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #4'

    truncate table #child_ids;
    
    insert into #child_ids (id)
    select id from bik_node where is_deleted = 0 and parent_node_id in (select id from #ids);
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #5'

    truncate table #ids;
    
    insert into #ids (id) select ID from #child_ids;

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #6'    
  end
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #7'

  drop table #ids;
  drop table #child_ids;
end
GO
/****** Object:  StoredProcedure [dbo].[sp_node_init_branch_id]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_node_init_branch_id] (@opt_tree_id int, @opt_node_id int)
as
begin  
  set nocount on

  declare @node_ids bik_unique_not_null_id_table_type;
  
  insert into @node_ids (id) 
  select id from bik_node 
  where is_deleted = 0 and
    (@opt_tree_id is null or tree_id = @opt_tree_id)
    and 
    (  @opt_node_id is null and parent_node_id is null 
    or 
       @opt_node_id is not null and id = @opt_node_id
    )
   ;

  exec sp_node_init_branch_idEx @node_ids
end
GO
/****** Object:  StoredProcedure [dbo].[sp_generic_insert_into_bik_node_ex]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_generic_insert_into_bik_node_ex](@treeCode varchar(max), @objId varchar(900), @forceDelete int)
as
begin
-- założenia:
	-- branch_ids są uzupełnione w bik_node i amg_node (@objId is not null)
	-- jeśli @objId is not null wtedy musi istniec ten obiekt w bik_node (inny mechanizm go wstawil wczesniej)
	-- 
	declare @treeId int = (select id from bik_tree where code = @treeCode);
	declare @parentNodeId int = (select id from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @branchIds varchar(900) = (select branch_ids from bik_node where tree_id = @treeId and obj_id = @objId);
	declare @amgBranchIds varchar(900) = (select branch_ids from amg_node where obj_id = @objId);

	-- update
	update bik_node 
	set is_deleted = 0, name = gen.name, descr = gen.descr, visual_order = gen.visual_order, node_kind_id = bnk.id
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.obj_id = gen.obj_id
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @parentNodeId, bnk.id, gen.name, gen.descr, @treeId, gen.obj_id, gen.visual_order
	from amg_node as gen
	inner join bik_node_kind as bnk on gen.node_kind_code = bnk.code
	left join (bik_node as bn inner join bik_tree as bt on bn.tree_id = bt.id and bn.tree_id = @treeId and (@branchIds is null or bn.branch_ids like @branchIds + '%')) on bn.obj_id = gen.obj_id
	where bn.id is null
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	order by gen.obj_id
	
	exec sp_node_init_branch_id @treeId, @parentNodeId;

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join amg_node as gen on bik_node.obj_id = gen.obj_id
	inner join bik_node as bk on bk.obj_id = gen.parent_obj_id
	where bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @treeId
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	if (@forceDelete > 0) begin
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join amg_node as gen on bik_node.obj_id = gen.obj_id 
		and (@amgBranchIds is null or gen.branch_ids like @amgBranchIds + '%')
	where gen.name is null
	and bik_node.tree_id = @treeId
	and (@branchIds is null or bik_node.branch_ids like @branchIds + '%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node as bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @treeId
	and (@branchIds is null or bn2.branch_ids like @branchIds + '%')
	and bn2.is_deleted = 1
	end;

	exec sp_node_init_branch_id @treeId, null
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_generic_insert_into_bik_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_generic_insert_into_bik_node](@treeCode varchar(max), @objId varchar(900))
as
begin
	exec sp_generic_insert_into_bik_node_ex @treeCode, @objId, 1;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_fix_nodes_visual_order_ex]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_fix_nodes_visual_order_ex](@parent_id int, @tree_id int, @ignore_nodekindcodes_in_larch int) as
begin
  set nocount on

  declare @vofix table (id int not null primary key, visual_order_fix int not null)

  insert into @vofix (id, visual_order_fix)
  select n.id, (ROW_NUMBER() OVER (ORDER BY n.visual_order, k.is_folder desc, n.name)) - 1 AS visual_order_fix --, * 
  from 
  bik_node n inner join bik_node_kind k on n.node_kind_id = k.id
  where n.tree_id = @tree_id and n.is_deleted = 0 and (@parent_id is null and n.parent_node_id is null or @parent_id is not null and n.parent_node_id = @parent_id)
  and (@ignore_nodekindcodes_in_larch = 0 or n.obj_id not like '&%')
  
  update 
  bik_node
  set visual_order = visual_order_fix
  from @vofix v
  where v.id = bik_node.id
  and bik_node.tree_id = @tree_id and bik_node.is_deleted = 0 and (@parent_id is null and bik_node.parent_node_id is null or @parent_id is not null and bik_node.parent_node_id = @parent_id)
  and (@ignore_nodekindcodes_in_larch = 0 or obj_id not like '&%')
end
GO
/****** Object:  StoredProcedure [dbo].[sp_fix_nodes_visual_order]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_fix_nodes_visual_order](@parent_id int, @tree_id int) as
begin
  set nocount on
  exec sp_fix_nodes_visual_order_ex @parent_id, @tree_id, 0
end
GO
/****** Object:  StoredProcedure [dbo].[sp_set_node_visual_order_ex]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_set_node_visual_order_ex](@node_id int, @visual_order int, @ignore_nodekindcodes_in_larch int) as
begin
  set nocount on

  declare @parent_id int, @tree_id int

  select @parent_id = parent_node_id, @tree_id = tree_id from bik_node where id = @node_id

  exec sp_fix_nodes_visual_order_ex @parent_id, @tree_id, @ignore_nodekindcodes_in_larch
  
  update bik_node
  set visual_order = visual_order + 1
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
    and visual_order >= @visual_order
  and (@ignore_nodekindcodes_in_larch = 0 or obj_id not like '&%')
    
  update bik_node set visual_order = @visual_order where id = @node_id  
end
GO
/****** Object:  StoredProcedure [dbo].[sp_set_node_visual_order]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_set_node_visual_order](@node_id int, @visual_order int) as
begin
  set nocount on
  exec sp_set_node_visual_order_ex @node_id, @visual_order, 0
end
GO
/****** Object:  Table [dbo].[bik_custom_right_role]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_custom_right_role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](255) NOT NULL,
	[visual_order] [tinyint] NULL,
	[tree_selector] [varchar](8000) NULL,
	[selector_mode] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_custom_right_role_rut_entry]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_custom_right_role_rut_entry](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tree_id] [int] NOT NULL,
	[node_id] [int] NULL,
	[is_deleted] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[tree_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_system_user_group]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_system_user_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](512) NOT NULL,
	[domain] [varchar](512) NULL,
	[is_built_in] [int] NOT NULL,
	[is_deleted] [tinyint] NOT NULL,
	[distinguished_name] [varchar](900) NULL,
	[scc_root_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_system_user_group_distinguished_name] ON [dbo].[bik_system_user_group] 
(
	[distinguished_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_custom_right_role_user_entry]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_custom_right_role_user_entry](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[role_id] [int] NOT NULL,
	[tree_id] [int] NULL,
	[node_id] [int] NULL,
	[group_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_custom_right_role_user_entry] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[role_id] ASC,
	[tree_id] ASC,
	[node_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_action]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_action](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_action_in_custom_right_role]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_action_in_custom_right_role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[action_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_custom_right_role_action_branch_grant]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_custom_right_role_action_branch_grant](
	[tree_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[action_id] [int] NOT NULL,
	[branch_ids] [varchar](1000) NOT NULL,
UNIQUE NONCLUSTERED 
(
	[action_id] ASC,
	[user_id] ASC,
	[tree_id] ASC,
	[branch_ids] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_right_role]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_right_role](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](255) NOT NULL,
	[rank] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_right_role_code] UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_user_right]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_user_right](
	[user_id] [int] NOT NULL,
	[right_id] [int] NOT NULL,
	[tree_id] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_user_right_user_right] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[right_id] ASC,
	[tree_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_system_user_in_group_prepared]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_system_user_in_group_prepared](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[group_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Action_Branch_Grants] as
begin
	if coalesce((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), '') = ''
	begin
		return;
	end;
  
	declare @role_on_tree table(role_id int, tree_id int)
    insert into @role_on_tree 
    select distinct crr.id as role_id, t.id as tree_id
	from
		bik_custom_right_role crr cross apply dbo.fn_split_by_sep(crr.tree_selector, ',', 7) x inner join bik_tree t on
		substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
		substring(x.str, 1, 1) <> '@' and t.code = x.str where t.is_hidden=0; 

	declare @user_role_on_tree_node table (user_id int, role_id int, tree_id int, node_id int);
	insert into @user_role_on_tree_node(user_id, role_id, tree_id, node_id)
	select user_id, role_id, tree_id, node_id from bik_custom_right_role_user_entry where user_id is not null
	union  
	select user_id, right_id as role_id, tree_id, null as node_id from bik_user_right 
	order by tree_id DESC, node_id;
	
	insert into @user_role_on_tree_node(user_id, role_id, tree_id, node_id)
	select urotn.user_id as user_id, urotn.role_id as role_id , rot.tree_id as tree_id, null as node_id 
	from @user_role_on_tree_node urotn join @role_on_tree rot on urotn.role_id=rot.role_id
	where urotn.tree_id is null group by urotn.user_id, urotn.role_id, rot.tree_id, node_id;

	delete from @user_role_on_tree_node where tree_id is null

	declare @group_role_on_tree_node table (group_id int, role_id int, tree_id int, node_id int);
	insert into @group_role_on_tree_node
	select group_id, role_id, tree_id, node_id from bik_custom_right_role_user_entry where user_id is null;
	
	insert into @group_role_on_tree_node(group_id, role_id, tree_id, node_id)
	select grotn.group_id as group_id, grotn.role_id as role_id , rot.tree_id as tree_id, null as node_id 
	from @group_role_on_tree_node grotn join @role_on_tree rot on grotn.role_id=rot.role_id
	where grotn.tree_id is null group by grotn.group_id, grotn.role_id, rot.tree_id, node_id;
	
	delete from @group_role_on_tree_node where tree_id is null
	
	insert into @user_role_on_tree_node(user_id, role_id, tree_id, node_id)
	select x.user_id, grotn.role_id, grotn.tree_id, grotn.node_id
	from bik_system_user_in_group_prepared x join @group_role_on_tree_node grotn on x.group_id=grotn.group_id
	--select count(1) from @user_role_on_tree_node
	--select * from bik_system_user su cross apply fn_get_system_user_groups(su.id) x join bik_system_user_group sg on sg.id=x.group_id where su.id =18
	--select * from @user_role_on_tree_node order by tree_id 
	--Liczymy tabele bik_custom_right_role_action_branch_grant
	truncate table bik_custom_right_role_action_branch_grant;
	insert into bik_custom_right_role_action_branch_grant(user_id, tree_id, action_id, branch_ids) 
	select urotn.user_id, urotn.tree_id, naicrr.action_id, case when urotn.node_id is null then '' else bn.branch_ids end as branch_ids
	from @user_role_on_tree_node urotn join bik_node_action_in_custom_right_role naicrr on urotn.role_id=naicrr.role_id  
	left join bik_node bn on urotn.node_id=bn.id
	group by urotn.user_id, urotn.tree_id, naicrr.action_id, urotn.node_id, branch_ids
end
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_RutsEx]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_RutsEx](@opt_user_id int) as
begin
  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
  
  if @regUserCrrId is null return;
  
  merge into bik_custom_right_role_user_entry crrue
  using bik_custom_right_role_rut_entry crrre cross join bik_system_user su
  on crrue.role_id = @regUserCrrId and crrue.user_id = su.id and (@opt_user_id is null or su.id = @opt_user_id)
    and crrre.tree_id = crrue.tree_id and (crrre.node_id is null and crrue.node_id is null or crrre.node_id = crrue.node_id)
  when matched and crrre.is_deleted <> 0 then
    delete
  when not matched by target and crrre.is_deleted = 0 and (@opt_user_id is null or su.id = @opt_user_id) then
    insert (user_id, tree_id, node_id, role_id) values (su.id, crrre.tree_id, crrre.node_id, @regUserCrrId)
  ;
  
  delete from bik_custom_right_role_rut_entry where is_deleted <> 0;
    
  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Ruts]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Ruts] as
begin
  exec sp_recalculate_Custom_Right_Role_RutsEx null;
end
GO
/****** Object:  StoredProcedure [dbo].[sp_update_searchable_attr_val]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_update_searchable_attr_val](@nodeId int, @properAttrName varchar(255), @value varchar(max))
as
begin
  declare @source varchar(max) = '(select ' + cast(@nodeId as varchar(20)) + ' as node_id, 
    ''' + replace(@value, '''', '''''') + ''' as val)'
  
  declare @nodeFilter varchar(max) = 'n.id = ' + cast(@nodeId as varchar(20))
    
  exec sp_verticalize_node_attrs_one_table @source, 'node_id', 'val', @properAttrName, @nodeFilter
end
GO
/****** Object:  StoredProcedure [dbo].[sp_update_searchable_attrs_weight_for_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_update_searchable_attrs_weight_for_node](@node_id int)
as
begin
  declare @filterTxt varchar(1000) = 'n.id = ' + cast(@node_id as varchar(20))
  exec sp_verticalize_node_attrs_inner @filterTxt
end
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_dqm_test]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_dqm_test](@optNodeFilter varchar(max))
as
begin
	set nocount on
	
	declare @sqlText varchar(max) = '(select te.test_node_id, te.description, te.long_name, te.benchmark_definition, te.sampling_method, te.additional_information, te.data_scope, te.error_threshold, te.create_date, te.modify_date, te.activate_date, te.deactivate_date, def.description as source_name, bn1.name as proc_name, bn2.name as err_proc_name, te.sql_text, te.error_sql_text, db.name as database_name, te.result_file_name, serv.name as serv_name
		from bik_dqm_test as te 
		inner join bik_data_source_def as def on def.id = te.source_id
		left join bik_node as bn1 on bn1.id = te.procedure_node_id
		left join bik_node as bn2 on bn2.id = te.error_procedure_node_id
		left join bik_node as db on te.database_node_id = db.id
		left join bik_node as serv on serv.id = db.parent_node_id
		where te.is_deleted = 0)';
	
	exec sp_verticalize_node_attrs_one_table @sqlText, 'test_node_id', 'description, long_name, benchmark_definition, sampling_method, additional_information, data_scope, error_threshold, create_date, modify_date, activate_date, deactivate_date, proc_name, err_proc_name, source_name, sql_text, error_sql_text, database_name, result_file_name, serv_name', 'Opis testu, Nazwa biznesowa, Definicja testu, Metoda próbkowania, Informacje dodatkowe, Zakres danych, Próg błędu, Data utworzenia, Data modyfikacji, Data aktywacji, Data dezaktywacji, Funkcja testująca, Funkcja zwracająca błędne dane, Źródło, Zapytanie testujące, Zapytanie zwracające błędne dane, Baza danych, Prefiks nazwy pliku na SFTP, System', @optNodeFilter
end
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs](@optNodeFilter varchar(max))
as
begin
  set nocount on

  declare @has_fulltext_catalog int = (select case when exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') then 1 else 0 end has_fulltext_catalog);
  
  if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
    exec('drop fulltext index on bik_searchable_attr_val')
    --exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING MANUAL')
  end
  
  exec sp_verticalize_node_attrs_inner @optNodeFilter

  if @has_fulltext_catalog = 1 begin
    if not exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
      declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
      exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
    end
  
    if (select change_tracking_state from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) <> 'A' begin
      exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO')
    end
  end;
end
GO
/****** Object:  Table [dbo].[bik_statistic]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_statistic](
	[counter_name] [varchar](255) NOT NULL,
	[user_id] [int] NOT NULL,
	[event_date] [date] NOT NULL,
	[event_datetime] [datetime] NOT NULL,
	[parametr] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_statistic_counter_name] ON [dbo].[bik_statistic] 
(
	[counter_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_biks_statistic_search]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_biks_statistic_search] as
	select 
		parametr as search_text,
		login_name as user_name,
		event_datetime 
	from bik_statistic bs 
    left join bik_system_user bsu on bsu.id=bs.user_id
    where counter_name='newSearch'
GO
/****** Object:  View [dbo].[vw_biks_statistic_first_login]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_biks_statistic_first_login] as 
    select bsu.login_name as user_name,
	coalesce(bsu.date_added, bs.event_datetime) as create_datetime
	from bik_system_user bsu 
    left join bik_statistic bs on bsu.id = bs.user_id and bs.counter_name='userLoginOnce'
GO
/****** Object:  View [dbo].[vw_biks_statistic_daily_user_login]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_biks_statistic_daily_user_login] as 
    select 
		login_name as user_name,
		event_datetime 
	from bik_statistic bs 
    left join bik_system_user bsu on bsu.id=bs.user_id
    where counter_name='userLogin'
GO
/****** Object:  Table [dbo].[bik_system_group_in_group]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_system_group_in_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[member_group_id] [int] NOT NULL,
	[is_deleted] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[group_id] ASC,
	[member_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[member_group_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_system_user_in_group]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_system_user_in_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[is_deleted] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[group_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_system_user_groups]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_system_user_groups](@user_id int)
returns @tab table (group_id int not null primary key)
as
begin
  declare @parent_groups table (group_id int not null primary key);

  declare @tmp_groups table (group_id int not null primary key);

  declare @new_grp_cnt int;

  insert into @parent_groups (group_id)
  select group_id from bik_system_user_in_group where user_id = @user_id and is_deleted = 0;

  set @new_grp_cnt = @@rowcount;

  while @new_grp_cnt > 0 begin
    insert into @tab (group_id) select group_id from @parent_groups;

    delete from @tmp_groups;

    insert into @tmp_groups (group_id)
    select distinct gig.group_id from bik_system_group_in_group gig inner join @parent_groups pg on gig.member_group_id = pg.group_id
      left join @tab mog on mog.group_id = gig.group_id
    where gig.is_deleted = 0 and mog.group_id is null;

    delete from @parent_groups;

    insert into @parent_groups (group_id)
    select group_id from @tmp_groups;

    set @new_grp_cnt = @@rowcount;   
  end;
     
  return;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_System_User_In_Group_Prepared]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_System_User_In_Group_Prepared] as
begin
	truncate table bik_system_user_in_group_prepared;
	insert into bik_system_user_in_group_prepared(user_id, group_id)
	select su.id, x.group_id 
	from bik_system_user su cross apply fn_get_system_user_groups(su.id) x
end;
GO
/****** Object:  Table [dbo].[bik_db_server]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_db_server](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[server] [varchar](512) NULL,
	[instance] [varchar](512) NULL,
	[db_user] [varchar](512) NULL,
	[db_password] [varchar](512) NULL,
	[is_active] [bit] NOT NULL,
	[port] [int] NULL,
	[database_filter] [varchar](max) NULL,
	[object_filter] [varchar](max) NULL,
	[database_list] [varchar](max) NULL,
	[tree_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_test]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_test](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_node_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[description] [varchar](max) NULL,
	[long_name] [varchar](max) NULL,
	[benchmark_definition] [varchar](max) NULL,
	[sampling_method] [varchar](max) NULL,
	[additional_information] [varchar](max) NULL,
	[error_threshold] [decimal](18, 8) NOT NULL,
	[create_date] [datetime] NOT NULL,
	[modify_date] [datetime] NULL,
	[activate_date] [datetime] NULL,
	[deactivate_date] [datetime] NULL,
	[db_server_id] [int] NULL,
	[error_db_server_id] [int] NULL,
	[procedure_node_id] [int] NULL,
	[error_procedure_node_id] [int] NULL,
	[source_id] [int] NULL,
	[type] [varchar](50) NOT NULL,
	[is_active] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
	[database_node_id] [int] NULL,
	[sql_text] [varchar](max) NULL,
	[error_sql_text] [varchar](max) NULL,
	[result_file_name] [varchar](512) NULL,
	[opt_Test_Grants_Per_Objects] [varchar](max) NULL,
	[data_scope] [varchar](max) NULL,
	[connection_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_bik_dqm_test_test_node_id] ON [dbo].[bik_dqm_test] 
(
	[test_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_test_multi]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_test_multi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_id] [int] NOT NULL,
	[test_node_id] [int] NOT NULL,
	[source_id] [int] NOT NULL,
	[database_node_id] [int] NULL,
	[db_server_id] [int] NULL,
	[sql_test] [varchar](max) NULL,
	[code_name] [varchar](512) NOT NULL,
	[result_file_name] [varchar](512) NULL,
	[is_deleted] [int] NOT NULL,
	[biks_table_name] [varchar](512) NULL,
	[opt_Test_Grants_Per_Objects] [varchar](max) NULL,
	[data_scope] [varchar](max) NULL,
	[connection_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_schedule]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_schedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_id] [int] NOT NULL,
	[hour] [int] NOT NULL,
	[minute] [int] NOT NULL,
	[interval] [int] NOT NULL,
	[date_started] [date] NULL,
	[is_schedule] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
	[priority] [int] NOT NULL,
	[multi_source_id] [int] NULL,
	[status_flag] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_request]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_request](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[test_id] [int] NOT NULL,
	[test_node_id] [int] NOT NULL,
	[case_count] [bigint] NULL,
	[error_count] [bigint] NULL,
	[error_percentage] [decimal](18, 8) NOT NULL,
	[passed] [int] NULL,
	[start_timestamp] [datetime] NOT NULL,
	[end_timestamp] [datetime] NULL,
	[is_manual] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
	[xlsx_file_name] [varchar](256) NULL,
	[csv_file_name] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_dqm_request_test_id_error_count] ON [dbo].[bik_dqm_request] 
(
	[test_id] ASC,
	[error_count] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_dqm_request_test_node_id] ON [dbo].[bik_dqm_request] 
(
	[test_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_request_error]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_request_error](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_id] [bigint] NOT NULL,
	[row] [int] NOT NULL,
	[column_ordinal_position] [int] NOT NULL,
	[column_name] [varchar](512) NULL,
	[value] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_bik_dqm_request_error_request_id_row_column_ordinal_position] ON [dbo].[bik_dqm_request_error] 
(
	[request_id] ASC,
	[row] ASC,
	[column_ordinal_position] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_log]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_log](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[test_id] [int] NOT NULL,
	[start_time] [datetime] NOT NULL,
	[finish_time] [datetime] NULL,
	[description] [varchar](max) NULL,
	[multi_source_id] [int] NULL,
	[data_read_secs] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_lisa_logs]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_logs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_user_id] [int] NOT NULL,
	[time_added] [datetime] NOT NULL,
	[teradata_login] [varchar](64) NOT NULL,
	[dictionary_name] [varchar](128) NOT NULL,
	[info] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_joined_obj_ids_for_node]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_joined_obj_ids_for_node](@node_id int)
returns varchar(max)
as
begin
  declare @res varchar(max) = '';

  select @res = @res + '|' + cast(dst_node_id as varchar(20)) from bik_joined_objs where src_node_id = @node_id;

  return case when @res = '' then '' else stuff(@res, 1, 1, '') end;
end;
GO
/****** Object:  StoredProcedure [dbo].[api_delete_joined_obj]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_delete_joined_obj](@src_node_id int, @dst_node_id int, @bidirection int)
as 
begin
	delete bik_joined_objs  
    where src_node_id = @src_node_id and dst_node_id = @dst_node_id
    
    if (@bidirection > 0) 
	delete bik_joined_objs
    where src_node_id = @dst_node_id and dst_node_id = @src_node_id
end
GO
/****** Object:  StoredProcedure [dbo].[api_add_joined_obj]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_add_joined_obj](@src_node_id int, @dst_node_id int, @bidirection int)
as 
begin
	insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_To_Descendants)
    select @src_node_id, @dst_node_id, 0, 0
    where not exists (select 1 from bik_joined_objs
    where src_node_id = @src_node_id and dst_node_id = @dst_node_id)
    
    if (@bidirection > 0) 
	insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_To_Descendants)
    select @dst_node_id, @src_node_id, 0, 0
    where not exists (select 1 from bik_joined_objs
    where src_node_id = @dst_node_id and dst_node_id = @src_node_id)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_reset_visual_order_for_nodes]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_reset_visual_order_for_nodes](@parent_id int, @tree_id int) as
begin
  set nocount on

  update bik_node
  set visual_order = 0
  where tree_id = @tree_id and is_deleted = 0 and (@parent_id is null and parent_node_id is null or @parent_id is not null and parent_node_id = @parent_id)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_linked_nodes_where_orignal_is_deleted]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_delete_linked_nodes_where_orignal_is_deleted]
as
	declare @rc int = 1;
begin
	while @rc > 0 
	begin

		update bik_node
		set is_deleted = 1
		from bik_node join bik_node oryginal on bik_node.linked_node_id = oryginal.id
		where oryginal.is_deleted = 1 and bik_node.is_deleted = 0

		set @rc = @@ROWCOUNT;

		update bik_node
		set is_deleted = 1
		from bik_node join bik_node parent on bik_node.parent_node_id = parent.id
		where parent.is_deleted = 1 and bik_node.is_deleted = 0

		set @rc = @rc + @@ROWCOUNT
	
	end;--end loop
end;--end procedure
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_visual_order_for_new_node]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------
------------------------------------------------------------------
------------------------------------------------------------------
--poprzednia poprawna  "alter db for v1.1.4.7 pm.sql"
--moja jednak koncepcyjnie niezgodna/niepoprawna w pliku "alter db for v1.1.9.22 pg.sql"
create function [dbo].[fn_get_visual_order_for_new_node](@parent_id int, @tree_id int)
returns int
as
begin
  declare @res int = (select max(visual_order) from bik_node where tree_id = @tree_id and is_deleted = 0 and 
    ((@parent_id is null and parent_node_id is null) or (@parent_id is not null and parent_node_id = @parent_id)))
  return case when coalesce(@res, 0) = 0 then 0 else @res + 1 end
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_bik_node_ancestorIds]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_bik_node_ancestorIds] (@node_id int)
RETURNS @tab TABLE (node_id int not null, level int not null)
WITH EXECUTE AS CALLER
AS
BEGIN
  declare @level int = 0;

  while 1 = 1 begin
    declare @parent_node_id int
    select @parent_node_id = parent_node_id from bik_node where id = @node_id
    if @parent_node_id is null break
    else insert into @tab (node_id, level) values (@parent_node_id, @level)
    
    set @node_id = @parent_node_id;
    set @level = @level - 1;
  end

  update @tab set level = level - @level - 1  
     
  RETURN;
END;
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'xDDD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_bik_node_ancestorIds'
GO
/****** Object:  Table [dbo].[bik_role_for_node]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_role_for_node](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[caption] [varchar](255) NOT NULL,
	[node_id] [int] NOT NULL,
	[code] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_role_for_node_node_id] ON [dbo].[bik_role_for_node] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_user_in_node]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_user_in_node](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[node_id] [int] NOT NULL,
	[role_for_node_id] [int] NOT NULL,
	[inherit_to_descendants] [int] NOT NULL,
	[is_auxiliary] [int] NOT NULL,
	[is_built_in] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_user_in_node_node_id] ON [dbo].[bik_user_in_node] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_user_in_node_user_id_node_id_role_for_node_id] ON [dbo].[bik_user_in_node] 
(
	[user_id] ASC,
	[node_id] ASC,
	[role_for_node_id] ASC
)
INCLUDE ( [is_auxiliary],
[inherit_to_descendants],
[is_built_in]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_search_cache]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_search_cache](
	[ticket] [varchar](256) NULL,
	[node_id] [int] NULL,
	[appearance] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_universe_table]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_universe_table](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[is_alias] [int] NOT NULL,
	[is_derived] [int] NOT NULL,
	[original_table] [varchar](256) NULL,
	[sql_of_derived_table] [varchar](max) NULL,
	[sql_of_derived_table_with_alias] [varchar](max) NULL,
	[their_id] [varchar](256) NOT NULL,
	[type] [varchar](255) NULL,
	[name_for_teradata] [varchar](255) NULL,
	[schema_name] [varchar](255) NULL,
	[node_id] [int] NULL,
	[default_schema_name] [varchar](max) NULL,
	[database_name] [varchar](max) NULL,
	[mssql_table_name] [varchar](max) NULL,
	[server_name] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_sapbo_universe_table_their_id_node_id] UNIQUE NONCLUSTERED 
(
	[their_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_sapbo_universe_table_node_id] ON [dbo].[bik_sapbo_universe_table] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_universe_object]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_universe_object](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text_of_select] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[aggregate_function] [varchar](50) NULL,
	[node_id] [int] NULL,
	[text_of_where] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_sapbo_universe_object_node_id] ON [dbo].[bik_sapbo_universe_object] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_universe_connection]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_universe_connection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](255) NULL,
	[user_name] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[database_source] [varchar](500) NULL,
	[connetion_networklayer_name] [varchar](255) NOT NULL,
	[node_id] [int] NULL,
	[type] [varchar](255) NULL,
	[database_engine] [varchar](255) NULL,
	[client_number] [int] NULL,
	[language] [varchar](10) NULL,
	[system_number] [varchar](255) NULL,
	[system_id] [varchar](255) NULL,
	[default_schema_name] [varchar](max) NULL,
	[database_name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_sapbo_universe_connection_node_id] ON [dbo].[bik_sapbo_universe_connection] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_universe_column]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_universe_column](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[table_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_name_id_bik_sapbo_universe_column] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[table_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_schedule]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_schedule](
	[node_id] [int] NOT NULL,
	[destination] [varchar](max) NULL,
	[owner] [varchar](max) NOT NULL,
	[creation_time] [datetime] NOT NULL,
	[nextruntime] [datetime] NOT NULL,
	[expire] [datetime] NULL,
	[type] [int] NOT NULL,
	[interval_minutes] [int] NOT NULL,
	[interval_hours] [int] NOT NULL,
	[interval_days] [int] NOT NULL,
	[interval_months] [int] NOT NULL,
	[interval_nth_day] [int] NOT NULL,
	[format] [varchar](max) NOT NULL,
	[parameters] [varchar](max) NULL,
	[schedule_type] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[destination_email_to] [varchar](max) NULL,
	[destination_folder_path] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_report_extradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_report_extradata](
	[node_id] [int] NOT NULL,
	[name] [varchar](max) NULL,
	[author] [varchar](max) NULL,
	[description] [varchar](max) NULL,
	[refresh_on_open] [bit] NULL,
	[last_refresh_duration] [int] NULL,
	[last_saved_by] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_query]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_query](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_text] [varchar](max) NULL,
	[filtr_text] [varchar](max) NULL,
	[sql_text] [varchar](max) NULL,
	[universe_cuid] [varchar](max) NULL,
	[node_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_sapbo_query_node_id] ON [dbo].[bik_sapbo_query] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_olap_connection]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_olap_connection](
	[node_id] [int] NOT NULL,
	[type] [varchar](max) NULL,
	[provider_caption] [varchar](max) NULL,
	[cube_caption] [varchar](max) NULL,
	[cuid] [varchar](max) NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
	[owner] [varchar](max) NULL,
	[network_layer] [varchar](max) NULL,
	[md_username] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_object_table]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_object_table](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[object_node_id] [int] NOT NULL,
	[table_node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_extradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_extradata](
	[node_id] [int] NOT NULL,
	[author] [varchar](max) NULL,
	[owner] [varchar](max) NULL,
	[keyword] [varchar](max) NULL,
	[file_path] [varchar](max) NULL,
	[file_name] [varchar](max) NULL,
	[cuid] [varchar](100) NULL,
	[files_value] [varchar](max) NULL,
	[size] [varchar](max) NULL,
	[has_children] [int] NULL,
	[guid] [varchar](max) NULL,
	[instance] [int] NULL,
	[owner_id] [int] NULL,
	[progid] [varchar](max) NULL,
	[obtype] [int] NULL,
	[flags] [int] NULL,
	[children] [int] NULL,
	[ruid] [varchar](max) NULL,
	[runnable_object] [int] NULL,
	[content_locale] [varchar](max) NULL,
	[is_schedulable] [int] NULL,
	[webi_doc_properties] [varchar](max) NULL,
	[read_only] [int] NULL,
	[last_successful_instance_id] [varchar](max) NULL,
	[progid_machine] [varchar](max) NULL,
	[schedule_status] [varchar](max) NULL,
	[recurring] [varchar](max) NULL,
	[doc_sender] [varchar](max) NULL,
	[revisionnum] [int] NULL,
	[application_object] [int] NULL,
	[shortname] [varchar](max) NULL,
	[dataconnection_total] [int] NULL,
	[universe_total] [int] NULL,
	[statistic] [varchar](500) NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
	[last_run_time] [datetime] NULL,
	[timestamp] [datetime] NULL,
	[endtime] [datetime] NULL,
	[starttime] [datetime] NULL,
	[nextruntime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_sapbo_extradata_cuid_node_id] ON [dbo].[bik_sapbo_extradata] 
(
	[cuid] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_sapbo_extradata_node_id] ON [dbo].[bik_sapbo_extradata] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_edited_description]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_edited_description](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[descr] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_sapbo_edited_description] UNIQUE NONCLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbo_connection_to_db]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_connection_to_db](
	[connection_node_id] [int] NOT NULL,
	[server_name] [varchar](200) NULL,
	[database_name] [varchar](200) NULL,
	[default_schema_name] [varchar](200) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[connection_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_profile_item_extradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_profile_item_extradata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[item_node_id] [int] NOT NULL,
	[libs] [varchar](256) NOT NULL,
	[fid] [varchar](256) NOT NULL,
	[di] [varchar](256) NOT NULL,
	[nod] [varchar](256) NULL,
	[len] [int] NULL,
	[dft] [varchar](256) NULL,
	[dom] [varchar](256) NULL,
	[tbl] [varchar](256) NULL,
	[ptn] [varchar](256) NULL,
	[xpo] [varchar](256) NULL,
	[xpr] [varchar](256) NULL,
	[typ] [varchar](256) NULL,
	[des] [varchar](256) NULL,
	[itp] [varchar](256) NULL,
	[min] [varchar](256) NULL,
	[max] [varchar](256) NULL,
	[dec] [int] NULL,
	[req] [varchar](256) NULL,
	[cmp] [varchar](256) NULL,
	[sfd] [varchar](256) NULL,
	[sfd1] [int] NULL,
	[sfd2] [int] NULL,
	[sfp] [int] NULL,
	[sft] [varchar](256) NULL,
	[siz] [int] NULL,
	[del] [int] NULL,
	[pos] [int] NULL,
	[rhd] [varchar](256) NULL,
	[srl] [varchar](256) NULL,
	[cnv] [int] NULL,
	[ltd] [datetime] NULL,
	[usr] [varchar](256) NULL,
	[mdd] [varchar](256) NULL,
	[val4ext] [varchar](256) NULL,
	[deprep] [varchar](256) NULL,
	[depostp] [varchar](256) NULL,
	[nullind] [varchar](256) NULL,
	[mddfid] [varchar](256) NULL,
	[validcmp] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_bik_profile_item_extradata_item_node_id] ON [dbo].[bik_profile_item_extradata] 
(
	[item_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_profile_file_extradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_profile_file_extradata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[item_node_id] [int] NOT NULL,
	[libs] [varchar](256) NOT NULL,
	[fid] [varchar](256) NOT NULL,
	[global] [varchar](256) NULL,
	[akey1] [varchar](256) NULL,
	[akey2] [varchar](256) NULL,
	[akey3] [varchar](256) NULL,
	[akey4] [varchar](256) NULL,
	[akey5] [varchar](256) NULL,
	[akey6] [varchar](256) NULL,
	[akey7] [varchar](256) NULL,
	[del] [int] NULL,
	[syssn] [varchar](256) NULL,
	[netloc] [int] NULL,
	[parfid] [varchar](256) NULL,
	[mplctdd] [varchar](256) NULL,
	[dftdes] [varchar](256) NULL,
	[dftord] [varchar](256) NULL,
	[dfthdr] [varchar](256) NULL,
	[dftdes1] [varchar](256) NULL,
	[ltd] [datetime] NULL,
	[usr] [varchar](256) NULL,
	[filetyp] [int] NULL,
	[exist] [int] NULL,
	[extendlength] [varchar](256) NULL,
	[fsn] [varchar](256) NULL,
	[fdoc] [varchar](256) NULL,
	[qid1] [varchar](256) NULL,
	[acckeys] [varchar](256) NULL,
	[val4ext] [varchar](256) NULL,
	[predaen] [varchar](256) NULL,
	[screen] [varchar](256) NULL,
	[rflag] [varchar](256) NULL,
	[dflag] [varchar](256) NULL,
	[udacc] [varchar](256) NULL,
	[udfile] [varchar](256) NULL,
	[fpn] [varchar](256) NULL,
	[udpre] [varchar](256) NULL,
	[udpost] [varchar](256) NULL,
	[publish] [varchar](256) NULL,
	[zrsfile] [varchar](256) NULL,
	[glref] [varchar](256) NULL,
	[rectyp] [int] NULL,
	[ptruser] [varchar](256) NULL,
	[ptrtld] [varchar](256) NULL,
	[log] [varchar](256) NULL,
	[archfiles] [varchar](256) NULL,
	[archkey] [varchar](256) NULL,
	[ptrtim] [varchar](256) NULL,
	[ptruseru] [varchar](256) NULL,
	[ptrtldu] [varchar](256) NULL,
	[ptrtimu] [varchar](256) NULL,
	[listdft] [varchar](512) NULL,
	[listreq] [varchar](512) NULL,
	[des] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_bik_profile_file_extradata_item_node_id] ON [dbo].[bik_profile_file_extradata] 
(
	[item_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_object_in_history]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_object_in_history](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[node_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_object_in_history_node_id] ON [dbo].[bik_object_in_history] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_object_in_fvs_change]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_object_in_fvs_change](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[fvs_id] [int] NOT NULL,
	[action_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_object_in_fvs_change_node_id] ON [dbo].[bik_object_in_fvs_change] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_object_excluded_from_statistic]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_object_excluded_from_statistic](
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_object_excluded_from_statistic_node_id] ON [dbo].[bik_object_excluded_from_statistic] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_object_author]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_object_author](
	[node_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_object_author_node_id] ON [dbo].[bik_object_author] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_blog]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_blog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [varchar](max) NOT NULL,
	[subject] [nvarchar](150) NOT NULL,
	[user_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
	[node_id] [int] NULL,
	[user_id_old] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_blog_node_id] ON [dbo].[bik_blog] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_obj_in_body_node]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_obj_in_body_node](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[linked_node_id] [int] NOT NULL,
	[blog_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_obj_in_body_node_id_node_id] UNIQUE NONCLUSTERED 
(
	[linked_node_id] ASC,
	[blog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_note]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_note](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NOT NULL,
	[body] [varchar](max) NULL,
	[user_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
	[node_id] [int] NOT NULL,
	[user_id_old] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_note_node_id] ON [dbo].[bik_note] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_note', @level2type=N'COLUMN',@level2name=N'body'
GO
/****** Object:  Table [dbo].[bik_node_vote]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_vote](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[value] [int] NOT NULL,
	[user_id_old] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_node_vote] UNIQUE NONCLUSTERED 
(
	[node_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_author]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_author](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_user_id_node_id] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_metapedia]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_metapedia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[body] [varchar](max) NOT NULL,
	[official] [smallint] NULL,
	[node_id] [int] NULL,
	[def_status] [int] NOT NULL,
	[user_id] [int] NULL,
	[versions] [varchar](500) NULL,
	[user_id_old] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_metapedia_node_id] ON [dbo].[bik_metapedia] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_lisa_extradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_extradata](
	[node_id] [int] NOT NULL,
	[ref_table_name] [varchar](256) NOT NULL,
	[ref_column_name] [varchar](256) NOT NULL,
	[dictionary_table] [varchar](256) NOT NULL,
	[dictionary_db] [varchar](256) NULL,
	[dictionary_table_physical] [varchar](256) NULL,
	[dictionary_db_physical] [varchar](256) NULL,
	[is_versioned] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_lisa_extradata_dictionary] ON [dbo].[bik_lisa_extradata] 
(
	[dictionary_table] ASC,
	[dictionary_db] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_lisa_extradata_dictionary_real] ON [dbo].[bik_lisa_extradata] 
(
	[dictionary_table_physical] ASC,
	[dictionary_db_physical] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_lisa_auth]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_auth](
	[node_id] [int] NULL,
	[login_mode] [int] NULL,
	[teradata_login] [varchar](128) NULL,
	[teradata_pwd] [varchar](128) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_home_page_hint]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_home_page_hint](
	[name] [varchar](max) NOT NULL,
	[node_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_fts_processed_obj]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_processed_obj](
	[id] [int] NOT NULL,
	[finished] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_frequently_asked_questions]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_frequently_asked_questions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[question_text] [varchar](max) NOT NULL,
	[answer_text] [varchar](max) NOT NULL,
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_frequently_asked_questions_node_id] ON [dbo].[bik_frequently_asked_questions] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_file_system_info]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_file_system_info](
	[node_id] [int] NOT NULL,
	[target_file_path] [varchar](max) NOT NULL,
	[file_size] [bigint] NOT NULL,
	[last_modified] [bigint] NOT NULL,
	[download_link] [varchar](max) NULL,
	[file_name] [varchar](max) NOT NULL,
UNIQUE NONCLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_report_evaluation]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_report_evaluation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[report_node_id] [int] NOT NULL,
	[evaluation_node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_report]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_report](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[date_from] [date] NOT NULL,
	[date_to] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_evaluation]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_evaluation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[group_node_id] [int] NOT NULL,
	[date_from] [date] NOT NULL,
	[date_to] [date] NOT NULL,
	[leader_node_id] [int] NOT NULL,
	[created_date] [date] NOT NULL,
	[prev_quality_level] [varchar](512) NULL,
	[actual_quality_level] [varchar](512) NOT NULL,
	[significance] [varchar](512) NOT NULL,
	[action_plan] [varchar](max) NULL,
	[is_accepted] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_data_error_issue]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_data_error_issue](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NULL,
	[group_node_id] [int] NULL,
	[leader_node_id] [int] NULL,
	[error_status] [int] NULL,
	[created_date] [datetime] NULL,
	[action_plan] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_confluence_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_confluence_extradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[obj_id] [varchar](100) NOT NULL,
	[parent_id] [varchar](100) NULL,
	[space_key] [varchar](20) NOT NULL,
	[content] [varchar](max) NULL,
	[kind] [varchar](100) NOT NULL,
	[url] [varchar](512) NOT NULL,
	[author] [varchar](128) NULL,
	[publish_date] [datetime] NULL,
	[is_as_root] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[author_full_name] [varchar](512) NULL,
	[short_link] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_confluence_extradata_node_id_short_link] ON [dbo].[bik_confluence_extradata] 
(
	[node_id] ASC,
	[short_link] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_confluence_extradata_node_id_url] ON [dbo].[bik_confluence_extradata] 
(
	[node_id] ASC,
	[url] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_confluence_extradata_short_link_node_id] ON [dbo].[bik_confluence_extradata] 
(
	[short_link] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_confluence_extradata_url_node_id] ON [dbo].[bik_confluence_extradata] 
(
	[url] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_confluence_attachment]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_confluence_attachment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[obj_id] [varchar](100) NOT NULL,
	[page_id] [varchar](100) NULL,
	[space_key] [varchar](20) NOT NULL,
	[comment] [varchar](max) NULL,
	[type] [varchar](100) NOT NULL,
	[url] [varchar](512) NOT NULL,
	[creator] [varchar](128) NULL,
	[created_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_blog_linked]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_blog_linked](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[blog_id] [int] NOT NULL,
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_blog_linked] UNIQUE NONCLUSTERED 
(
	[node_id] ASC,
	[blog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_authors]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_authors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[is_admin] [int] NOT NULL,
	[user_id_old] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_authors] UNIQUE NONCLUSTERED 
(
	[node_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_attachment]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attachment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[href] [varchar](max) NOT NULL,
	[caption] [varchar](max) NOT NULL,
	[node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_attachment_node_id] ON [dbo].[bik_attachment] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_branch_path]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_branch_path] (@nodeId int)
returns varchar(max)
as
begin
    --declare @nodeId int = 2233418;
	declare @branchIds varchar(max) = (select branch_ids from bik_node where id = @nodeId);
	declare @treeCaption varchar(max) = (select bt.name from bik_tree bt inner join bik_node bn on bn.tree_id = bt.id where bn.id = @nodeId)

	declare @tmp varchar(max) = '';
	select @tmp = case when @tmp = '' then @tmp + bn.name else @tmp + ' » ' + bn.name end 
	from dbo.fn_split_by_sep(@branchIds, '|', 0) bra
	inner join bik_node bn on bra.str = bn.id
	order by bra.idx;

	set @tmp = @treeCaption + ' » ' + @tmp;
	
    return(@tmp);
end;
GO
/****** Object:  StoredProcedure [dbo].[api_list_joined_objs]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_list_joined_objs](@node_id int)
as 
begin
	select bn.name, dbo.fn_get_branch_path(bn.id) as path, bn.descr 
	from bik_node bn join bik_joined_objs bjo on bn.id = bjo.src_node_id 
	where bjo.src_node_id = @node_id and bn.is_deleted = 0
end
GO
/****** Object:  StoredProcedure [dbo].[api_edit_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure  [dbo].[api_edit_node](@id int, @name varchar(4000), @descr varchar(max))
as
begin
	if (@name is not null)
	update bik_node set name = @name, descr = @descr where id = @id and is_deleted = 0
	else update bik_node set descr = @descr where id = @id and is_deleted = 0
end
GO
/****** Object:  StoredProcedure [dbo].[api_delete_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_delete_node](@id int)
as
begin
	declare @branch_ids varchar(1000) = (select branch_ids from bik_node where id = @id)
	update bik_node set is_deleted = 1 where branch_ids like @branch_ids + '%'
end
GO
/****** Object:  Table [dbo].[aaa_query]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_query](
	[id] [int] NOT NULL,
	[name] [varchar](max) NULL,
	[id_text] [varchar](max) NULL,
	[filtr_text] [varchar](max) NULL,
	[sql_text] [varchar](max) NULL,
	[universe_cuid] [varchar](max) NULL,
	[report_node_id] [int] NULL,
	[universe_name] [varchar](max) NULL,
	[wrong_universe_cuid] [varchar](100) NULL,
	[report_si_id] [int] NOT NULL,
	[report_update] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_ids_for_report]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_ids_for_report](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[query_node_id] [int] NULL,
	[object_node_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_md5_hash_varcharmax]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_md5_hash_varcharmax] ( @inputDataString varchar(max))
		returns varbinary(32)
		with schemabinding
		as
		begin
		declare
			@index int,
			@inputDataLength int,
			@returnMD5Hash varbinary(32),
			@inputDataStringTmp varchar(8000)

		set @returnMD5Hash = 0
		set @index = 1
		set @inputDataLength = len(@InputDataString)

		while @index <= @inputDataLength
		begin
			set @inputDataStringTmp = substring(@InputDataString, @index, 7968)+(case when @index>1 then convert(nvarchar(32), @returnMD5Hash,2) else '' end)
			set @returnMD5Hash = hashbytes('md5', @inputDataStringTmp)
			set @index = @index + 7968
		end

		return @returnMD5Hash
end
GO
/****** Object:  Table [dbo].[bik_news]    Script Date: 09/28/2016 11:08:55 ******/
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ARITHABORT ON
GO
CREATE TABLE [dbo].[bik_news](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NOT NULL,
	[text] [varchar](max) NOT NULL,
	[author_id] [int] NOT NULL,
	[date_added] [datetime] NOT NULL,
	[only_for_user_id] [int] NULL,
	[text_hash]  AS (CONVERT([nvarchar](32),[dbo].[fn_md5_hash_varcharmax]([text]),(2))),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
CREATE NONCLUSTERED INDEX [idx_bik_news_text_hash] ON [dbo].[bik_news] 
(
	[text_hash] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_news_readed_user]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_news_readed_user](
	[user_id] [int] NOT NULL,
	[news_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_mssql_index]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_mssql_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_node_id] [int] NOT NULL,
	[column_node_id] [int] NOT NULL,
	[server] [varchar](500) NULL,
	[column_name] [sysname] NOT NULL,
	[name] [sysname] NULL,
	[column_position] [int] NOT NULL,
	[is_unique] [int] NULL,
	[type] [varchar](50) NULL,
	[is_primary_key] [int] NOT NULL,
	[partition_ordinal] [int] NOT NULL,
	[partition_schema] [varchar](100) NULL,
	[partition_function] [varchar](100) NULL,
	[partition_function_type] [varchar](100) NULL,
	[database_name] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_index_column_node_id] ON [dbo].[bik_mssql_index] 
(
	[column_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_index_table_node_id] ON [dbo].[bik_mssql_index] 
(
	[table_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_mssql_foreign_key]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_mssql_foreign_key](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NOT NULL,
	[description] [varchar](500) NULL,
	[server] [varchar](500) NOT NULL,
	[fk_table] [sysname] NOT NULL,
	[fk_table_node_id] [int] NOT NULL,
	[fk_column] [sysname] NOT NULL,
	[fk_column_node_id] [int] NOT NULL,
	[pk_table] [sysname] NOT NULL,
	[pk_table_node_id] [int] NOT NULL,
	[pk_column] [sysname] NOT NULL,
	[pk_column_node_id] [int] NOT NULL,
	[fk_owner] [sysname] NULL,
	[pk_owner] [sysname] NULL,
	[database_name] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_foreign_key_fk_column_node_id] ON [dbo].[bik_mssql_foreign_key] 
(
	[fk_column_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_foreign_key_fk_table_node_id] ON [dbo].[bik_mssql_foreign_key] 
(
	[fk_table_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_mssql_dependency]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_mssql_dependency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NOT NULL,
	[ref_node_id] [int] NOT NULL,
	[dep_node_id] [int] NOT NULL,
	[dep_name] [sysname] NOT NULL,
	[dep_owner] [sysname] NOT NULL,
	[ref_name] [sysname] NULL,
	[ref_owner] [sysname] NULL,
	[database_name] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_dependency_ref_node_id] ON [dbo].[bik_mssql_dependency] 
(
	[ref_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_mssql_column_extradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_mssql_column_extradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NOT NULL,
	[server] [varchar](500) NULL,
	[table_node_id] [int] NOT NULL,
	[column_node_id] [int] NOT NULL,
	[column_id] [int] NOT NULL,
	[description] [varchar](500) NULL,
	[is_nullable] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[default_field] [varchar](512) NULL,
	[seed_value] [bigint] NULL,
	[increment_value] [bigint] NULL,
	[database_name] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_column_extradata_column_node_id] ON [dbo].[bik_mssql_column_extradata] 
(
	[column_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_column_extradata_table_node_id] ON [dbo].[bik_mssql_column_extradata] 
(
	[table_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_mssql]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_mssql](
	[name] [varchar](512) NOT NULL,
	[type] [varchar](100) NOT NULL,
	[extra_info] [varchar](max) NULL,
	[definition_text] [varchar](max) NULL,
	[branch_names] [varchar](900) NOT NULL,
	[parent_branch_names] [varchar](max) NULL,
	[visual_order] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_mssql_branch_names] ON [dbo].[bik_mssql] 
(
	[branch_names] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[amg_attribute]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[amg_attribute](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[obj_id] [varchar](900) NOT NULL,
	[name] [varchar](900) NOT NULL,
	[value] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_amg_attribute_obj_id_name] ON [dbo].[amg_attribute] 
(
	[obj_id] ASC,
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_profile_item_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_profile_item_extradata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[libs] [varchar](256) NOT NULL,
	[fid] [varchar](256) NOT NULL,
	[di] [varchar](256) NOT NULL,
	[nod] [varchar](256) NULL,
	[len] [int] NULL,
	[dft] [varchar](256) NULL,
	[dom] [varchar](256) NULL,
	[tbl] [varchar](256) NULL,
	[ptn] [varchar](256) NULL,
	[xpo] [varchar](256) NULL,
	[xpr] [varchar](256) NULL,
	[typ] [varchar](256) NULL,
	[des] [varchar](256) NULL,
	[itp] [varchar](256) NULL,
	[min] [varchar](256) NULL,
	[max] [varchar](256) NULL,
	[dec] [int] NULL,
	[req] [varchar](256) NULL,
	[cmp] [varchar](256) NULL,
	[sfd] [varchar](256) NULL,
	[sfd1] [int] NULL,
	[sfd2] [int] NULL,
	[sfp] [int] NULL,
	[sft] [varchar](256) NULL,
	[siz] [int] NULL,
	[del] [int] NULL,
	[pos] [int] NULL,
	[rhd] [varchar](256) NULL,
	[srl] [varchar](256) NULL,
	[cnv] [int] NULL,
	[ltd] [datetime] NULL,
	[usr] [varchar](256) NULL,
	[mdd] [varchar](256) NULL,
	[val4ext] [varchar](256) NULL,
	[deprep] [varchar](256) NULL,
	[depostp] [varchar](256) NULL,
	[nullind] [varchar](256) NULL,
	[mddfid] [varchar](256) NULL,
	[validcmp] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_aaa_profile_item_extradata_libs_fi_di] ON [dbo].[aaa_profile_item_extradata] 
(
	[libs] ASC,
	[fid] ASC,
	[di] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_profile_file_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_profile_file_extradata](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[libs] [varchar](256) NOT NULL,
	[fid] [varchar](256) NOT NULL,
	[global] [varchar](256) NULL,
	[akey1] [varchar](256) NULL,
	[akey2] [varchar](256) NULL,
	[akey3] [varchar](256) NULL,
	[akey4] [varchar](256) NULL,
	[akey5] [varchar](256) NULL,
	[akey6] [varchar](256) NULL,
	[akey7] [varchar](256) NULL,
	[del] [int] NULL,
	[syssn] [varchar](256) NULL,
	[netloc] [int] NULL,
	[parfid] [varchar](256) NULL,
	[mplctdd] [varchar](256) NULL,
	[dftdes] [varchar](256) NULL,
	[dftord] [varchar](256) NULL,
	[dfthdr] [varchar](256) NULL,
	[dftdes1] [varchar](256) NULL,
	[ltd] [datetime] NULL,
	[usr] [varchar](256) NULL,
	[filetyp] [int] NULL,
	[exist] [int] NULL,
	[extendlength] [varchar](256) NULL,
	[fsn] [varchar](256) NULL,
	[fdoc] [varchar](256) NULL,
	[qid1] [varchar](256) NULL,
	[acckeys] [varchar](256) NULL,
	[val4ext] [varchar](256) NULL,
	[predaen] [varchar](256) NULL,
	[screen] [varchar](256) NULL,
	[rflag] [varchar](256) NULL,
	[dflag] [varchar](256) NULL,
	[udacc] [varchar](256) NULL,
	[udfile] [varchar](256) NULL,
	[fpn] [varchar](256) NULL,
	[udpre] [varchar](256) NULL,
	[udpost] [varchar](256) NULL,
	[publish] [varchar](256) NULL,
	[zrsfile] [varchar](256) NULL,
	[glref] [varchar](256) NULL,
	[rectyp] [int] NULL,
	[ptruser] [varchar](256) NULL,
	[ptrtld] [varchar](256) NULL,
	[log] [varchar](256) NULL,
	[archfiles] [varchar](256) NULL,
	[archkey] [varchar](256) NULL,
	[ptrtim] [varchar](256) NULL,
	[ptruseru] [varchar](256) NULL,
	[ptrtldu] [varchar](256) NULL,
	[ptrtimu] [varchar](256) NULL,
	[listdft] [varchar](512) NULL,
	[listreq] [varchar](512) NULL,
	[des] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_aaa_profile_file_extradata_libs_fi_di] ON [dbo].[aaa_profile_file_extradata] 
(
	[libs] ASC,
	[fid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_postgres_index]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_postgres_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](500) NULL,
	[server] [varchar](500) NULL,
	[owner] [varchar](500) NOT NULL,
	[database_name] [varchar](500) NOT NULL,
	[table_name] [varchar](500) NOT NULL,
	[column_name] [varchar](500) NOT NULL,
	[column_position] [int] NULL,
	[is_unique] [int] NULL,
	[type] [varchar](50) NULL,
	[is_primary_key] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_postgres_foreign_key]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_postgres_foreign_key](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](500) NOT NULL,
	[description] [varchar](max) NULL,
	[server] [varchar](500) NULL,
	[database_name] [varchar](500) NOT NULL,
	[fk_table] [varchar](500) NOT NULL,
	[fk_schema] [varchar](500) NOT NULL,
	[fk_column] [varchar](500) NOT NULL,
	[pk_table] [varchar](500) NOT NULL,
	[pk_schema] [varchar](500) NOT NULL,
	[pk_column] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_postgres_dependency]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_postgres_dependency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NULL,
	[database_name] [varchar](500) NOT NULL,
	[ref_name] [varchar](500) NOT NULL,
	[ref_owner] [varchar](500) NOT NULL,
	[dep_name] [varchar](500) NOT NULL,
	[dep_owner] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_postgres_definition]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_postgres_definition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NULL,
	[database_name] [varchar](500) NOT NULL,
	[owner] [varchar](500) NOT NULL,
	[name] [varchar](500) NOT NULL,
	[definition] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_postgres_column_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_postgres_column_extradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](500) NOT NULL,
	[owner] [varchar](500) NOT NULL,
	[table_name] [varchar](500) NOT NULL,
	[database_name] [varchar](500) NOT NULL,
	[server_name] [varchar](500) NOT NULL,
	[column_id] [int] NOT NULL,
	[description] [varchar](max) NULL,
	[is_nullable] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[default_field] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_oracle_index]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_oracle_index](
	[name] [varchar](500) NOT NULL,
	[server] [varchar](500) NULL,
	[owner] [varchar](500) NOT NULL,
	[table_name] [varchar](500) NOT NULL,
	[column_name] [varchar](500) NOT NULL,
	[column_position] [int] NULL,
	[descend] [varchar](20) NULL,
	[constraint_type] [varchar](30) NULL,
	[is_unique] [int] NULL,
	[is_primary_key] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_oracle_foreign_key]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_oracle_foreign_key](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](500) NOT NULL,
	[description] [varchar](max) NULL,
	[server] [varchar](500) NULL,
	[fk_table] [varchar](500) NOT NULL,
	[fk_schema] [varchar](500) NOT NULL,
	[fk_column] [varchar](500) NOT NULL,
	[pk_table] [varchar](500) NOT NULL,
	[pk_schema] [varchar](500) NOT NULL,
	[pk_column] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_oracle_dependency]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_oracle_dependency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NULL,
	[ref_name] [varchar](500) NOT NULL,
	[ref_owner] [varchar](500) NOT NULL,
	[dep_name] [varchar](500) NOT NULL,
	[dep_owner] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_oracle_definition]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_oracle_definition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NULL,
	[owner] [varchar](500) NOT NULL,
	[name] [varchar](500) NOT NULL,
	[definition] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_oracle_column_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_oracle_column_extradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](500) NOT NULL,
	[owner] [varchar](500) NOT NULL,
	[table_name] [varchar](500) NOT NULL,
	[server_name] [varchar](500) NOT NULL,
	[column_id] [int] NOT NULL,
	[description] [varchar](max) NULL,
	[is_nullable] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[default_field] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_mssql_index]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_mssql_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NULL,
	[server] [varchar](500) NULL,
	[owner] [varchar](50) NOT NULL,
	[database_name] [varchar](50) NOT NULL,
	[table_name] [varchar](50) NOT NULL,
	[column_name] [varchar](50) NOT NULL,
	[column_position] [int] NULL,
	[is_unique] [int] NULL,
	[type] [varchar](50) NULL,
	[is_primary_key] [int] NOT NULL,
	[partition_ordinal] [int] NOT NULL,
	[partition_schema] [varchar](100) NULL,
	[partition_function] [varchar](100) NULL,
	[partition_function_type] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_mssql_foreign_key]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_mssql_foreign_key](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NOT NULL,
	[description] [varchar](500) NULL,
	[server] [varchar](500) NULL,
	[database_name] [varchar](50) NOT NULL,
	[fk_table] [sysname] NOT NULL,
	[fk_schema] [sysname] NOT NULL,
	[fk_column] [sysname] NOT NULL,
	[pk_table] [sysname] NOT NULL,
	[pk_schema] [sysname] NOT NULL,
	[pk_column] [sysname] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_mssql_extended_properties]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_mssql_extended_properties](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NOT NULL,
	[database_name] [sysname] NOT NULL,
	[type] [varchar](50) NOT NULL,
	[owner] [sysname] NULL,
	[table_name] [sysname] NULL,
	[column_name] [sysname] NULL,
	[name] [varchar](512) NOT NULL,
	[value] [varchar](max) NULL,
	[branch_names] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_mssql_dependency]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_mssql_dependency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[server] [varchar](500) NULL,
	[database_name] [sysname] NOT NULL,
	[ref_name] [sysname] NOT NULL,
	[ref_owner] [sysname] NOT NULL,
	[dep_name] [sysname] NOT NULL,
	[dep_owner] [sysname] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_mssql_column_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_mssql_column_extradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NOT NULL,
	[owner] [sysname] NOT NULL,
	[table_name] [sysname] NOT NULL,
	[database_name] [sysname] NOT NULL,
	[server_name] [sysname] NOT NULL,
	[column_id] [int] NOT NULL,
	[description] [varchar](500) NULL,
	[is_nullable] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[default_field] [varchar](512) NULL,
	[seed_value] [bigint] NULL,
	[increment_value] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_mssql]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_mssql](
	[name] [varchar](512) NOT NULL,
	[type] [varchar](100) NOT NULL,
	[extra_info] [varchar](max) NULL,
	[definition_text] [varchar](max) NULL,
	[branch_names] [varchar](900) NOT NULL,
	[parent_branch_names] [varchar](max) NULL,
	[visual_order] [int] NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_aaa_mssql_branch_names] ON [dbo].[aaa_mssql] 
(
	[branch_names] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_teradata_index]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_teradata_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_branch_names] [varchar](max) NOT NULL,
	[name] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[unique_flag] [varchar](255) NOT NULL,
	[column_name] [varchar](max) NOT NULL,
	[column_position] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_teradata_data_model_process_object_rel]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_teradata_data_model_process_object_rel](
	[symb_proc] [varchar](80) NOT NULL,
	[id_obj] [int] NOT NULL,
	[obj_type] [char](1) NOT NULL,
	[id_typ_process] [varchar](3) NOT NULL,
	[bik_node_obj_id] [varchar](800) NOT NULL,
	[area] [varchar](80) NOT NULL,
	[database_name] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_teradata_data_model_object]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_teradata_data_model_object](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_load_log_id] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[extra_info] [varchar](max) NULL,
	[branch_names] [varchar](1000) NULL,
	[parent_branch_names] [varchar](max) NULL,
	[request_text] [varchar](max) NULL,
	[database_name] [varchar](100) NULL,
	[table_name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_sapbw]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_sapbw](
	[info_cube] [varchar](max) NULL,
	[ip_level] [int] NULL,
	[info_name] [varchar](max) NULL,
	[obj_name] [varchar](max) NULL,
	[file_type] [varchar](max) NULL,
	[info_type] [varchar](max) NULL,
	[parent] [varchar](max) NULL,
	[info_descr] [varchar](max) NULL,
	[obj_descr] [varchar](max) NULL,
	[info_area] [varchar](max) NULL,
	[obj_type] [varchar](max) NULL,
	[query_update] [varchar](max) NULL,
	[query_owner] [varchar](max) NULL,
	[query_last_edit] [varchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_report_schedule]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_report_schedule](
	[id] [int] NOT NULL,
	[parent_id] [int] NOT NULL,
	[name] [varchar](max) NOT NULL,
	[destination] [varchar](max) NULL,
	[owner] [varchar](max) NOT NULL,
	[creation_time] [datetime] NOT NULL,
	[nextruntime] [datetime] NOT NULL,
	[expire] [datetime] NULL,
	[type] [int] NOT NULL,
	[interval_minutes] [int] NOT NULL,
	[interval_hours] [int] NOT NULL,
	[interval_days] [int] NOT NULL,
	[interval_months] [int] NOT NULL,
	[interval_nth_day] [int] NOT NULL,
	[format] [varchar](max) NOT NULL,
	[parameters] [varchar](max) NULL,
	[schedule_type] [int] NULL,
	[destination_email_to] [varchar](max) NULL,
	[destination_folder_path] [varchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_report_object]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_report_object](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[query_id] [int] NULL,
	[universe_CUID] [varchar](300) NULL,
	[object_name] [varchar](255) NULL,
	[class_name] [varchar](255) NULL,
	[universe_name] [varchar](max) NULL,
	[wrong_universe_cuid] [varchar](100) NULL,
	[report_si_id] [int] NOT NULL,
	[report_update] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_global_props]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_global_props](
	[si_id] [int] NOT NULL,
	[si_kind] [varchar](max) NOT NULL,
	[si_name] [varchar](max) NULL,
	[si_parentid] [int] NULL,
	[SI_AUTHOR] [varchar](max) NULL,
	[SI_SIZE] [int] NULL,
	[SI_HAS_CHILDREN] [int] NULL,
	[SI_CONTENT_LOCALE] [varchar](max) NULL,
	[SI_WEBI_DOC_PROPERTIES] [varchar](max) NULL,
	[SI_READ_ONLY] [int] NULL,
	[SI_LAST_SUCCESSFUL_INSTANCE_ID] [int] NULL,
	[SI_LAST_RUN_TIME] [datetime] NULL,
	[SI_ENDTIME] [datetime] NULL,
	[SI_STARTTIME] [datetime] NULL,
	[SI_SCHEDULE_STATUS] [int] NULL,
	[SI_RECURRING] [int] NULL,
	[SI_NEXTRUNTIME] [datetime] NULL,
	[SI_DOC_SENDER] [varchar](max) NULL,
	[SI_OBJECT_IS_CONTAINER] [int] NULL,
	[SI_DESCRIPTION] [varchar](max) NULL,
	[SI_TABLE] [int] NULL,
	[SI_HIDDEN_OBJECT] [int] NULL,
	[SI_FLAGS] [int] NULL,
	[SI_OBTYPE] [int] NULL,
	[SI_SYSTEM_OBJECT] [int] NULL,
	[SI_IS_SCHEDULABLE] [int] NULL,
	[SI_RUID] [varchar](max) NULL,
	[SI_CHILDREN] [int] NULL,
	[SI_OWNER] [varchar](max) NULL,
	[SI_RUNNABLE_OBJECT] [int] NULL,
	[SI_PARENT_FOLDER] [int] NULL,
	[SI_COMPONENT] [int] NULL,
	[SI_APPLICATION_OBJECT] [int] NULL,
	[SI_INSTANCE_OBJECT] [int] NULL,
	[SI_GUID] [varchar](max) NULL,
	[SI_INSTANCE] [int] NULL,
	[SI_CUID] [varchar](max) NULL,
	[SI_PLUGIN_OBJECT] [int] NULL,
	[SI_OWNERID] [int] NULL,
	[SI_UPDATE_TS] [datetime] NULL,
	[SI_CREATION_TIME] [datetime] NULL,
	[SI_PROGID] [varchar](max) NULL,
	[SI_PARENT_CUID] [varchar](max) NULL,
	[SI_PARENT_FOLDER_CUID] [varchar](max) NULL,
	[SI_SENDABLE] [int] NULL,
	[SI_SHORTCUTS__SI_TOTAL] [int] NULL,
	[__3498__] [int] NULL,
	[__4040__] [int] NULL,
	[__4047__] [int] NULL,
	[__3796__] [varchar](max) NULL,
	[__3797__] [varchar](max) NULL,
	[SI_ROLES_ON_OBJECT__SI_TOTAL] [int] NULL,
	[SI_PUBLICATIONS__SI_TOTAL] [int] NULL,
	[SI_CORPORATE_CATEGORIES__SI_TOTAL] [int] NULL,
	[SI_PERSONAL_CATEGORIES__SI_TOTAL] [int] NULL,
	[__4012____SI_TOTAL] [int] NULL,
	[SI_SHARED_DOC__SI_TOTAL] [int] NULL,
	[SI_REFERRING_DOCS__SI_TOTAL] [int] NULL,
	[__4045____SI_TOTAL] [int] NULL,
	[__4050__] [int] NULL,
	[SI_PLATFORM_SEARCH_CONTAINERS__SI_TOTAL] [int] NULL,
	[SI_ARTIFACTS_SCOPEBATCH__SI_TOTAL] [int] NULL,
	[SI_ARTIFACTS_SCOPEBATCHDOC__SI_TOTAL] [int] NULL,
	[SI_SCOPEBATCHDOC_ARTIFACTS__SI_TOTAL] [int] NULL,
	[SI_ARTIFACTS_SOURCEDOC__SI_TOTAL] [int] NULL,
	[SI_SOURCEDOC_ARTIFACTS__SI_TOTAL] [int] NULL,
	[__4044__] [int] NULL,
	[SI_FILES__SI_FILE1] [varchar](max) NULL,
	[SI_FILES__SI_VALUE1] [int] NULL,
	[SI_FILES__SI_NUM_FILES] [int] NULL,
	[SI_FILES__SI_PATH] [varchar](max) NULL,
	[SI_TEMPLATE_CUID] [varchar](max) NULL,
	[table_code] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[si_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_file_system_info]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_file_system_info](
	[target_file_path] [varchar](max) NULL,
	[obj_id] [varchar](max) NULL,
	[file_size] [bigint] NULL,
	[last_modified] [bigint] NULL,
	[download_link] [varchar](max) NULL,
	[file_name] [varchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_admin]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_admin](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[param] [varchar](256) NOT NULL,
	[value] [varchar](1024) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_bik_admins] UNIQUE NONCLUSTERED 
(
	[param] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_active_directory_ou]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_active_directory_ou](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[domain] [varchar](512) NOT NULL,
	[name] [varchar](max) NOT NULL,
	[distinguished_name] [varchar](max) NOT NULL,
	[parent_distinguished_name] [varchar](max) NULL,
	[description] [varchar](max) NULL,
	[objectclass] [varchar](max) NULL,
	[objectcategory] [varchar](max) NULL,
	[is_deleted] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_active_directory_group]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_active_directory_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[domain] [varchar](512) NOT NULL,
	[s_a_m_account_name] [varchar](512) NOT NULL,
	[s_a_m_account_type] [varchar](512) NULL,
	[distinguished_name] [varchar](max) NULL,
	[description] [varchar](max) NULL,
	[mail] [varchar](max) NULL,
	[member] [varchar](max) NULL,
	[memberof] [varchar](max) NULL,
	[group_type] [varchar](max) NULL,
	[objectclass] [varchar](max) NULL,
	[objectcategory] [varchar](max) NULL,
	[is_deleted] [tinyint] NOT NULL,
	[parent_distinguished_name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_active_directory]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_active_directory](
	[s_a_m_account_name] [varchar](250) NOT NULL,
	[distinguished_name] [varchar](max) NULL,
	[user_principal_name] [varchar](max) NULL,
	[w_w_w_home_page] [varchar](max) NULL,
	[url] [varchar](400) NULL,
	[title] [varchar](max) NULL,
	[telephone_number] [varchar](max) NULL,
	[street_address] [varchar](max) NULL,
	[st] [varchar](max) NULL,
	[sn] [varchar](max) NULL,
	[post_office_box] [varchar](max) NULL,
	[postal_code] [varchar](max) NULL,
	[physical_delivery_office_name] [varchar](max) NULL,
	[pager] [varchar](max) NULL,
	[other_telephone] [varchar](max) NULL,
	[other_pager] [varchar](max) NULL,
	[other_mobile] [varchar](max) NULL,
	[other_ip_phone] [varchar](max) NULL,
	[other_facsimile_telephone_number] [varchar](max) NULL,
	[mobile] [varchar](max) NULL,
	[manager] [varchar](max) NULL,
	[l] [varchar](max) NULL,
	[ip_phone] [varchar](max) NULL,
	[initials] [varchar](max) NULL,
	[home_phone] [varchar](max) NULL,
	[given_name] [varchar](max) NULL,
	[facsimile_telephone_number] [varchar](max) NULL,
	[direct_reports] [varchar](max) NULL,
	[department] [varchar](max) NULL,
	[display_name] [varchar](max) NULL,
	[description] [varchar](max) NULL,
	[notes] [varchar](max) NULL,
	[co] [varchar](max) NULL,
	[c] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[company] [varchar](max) NULL,
	[mail] [varchar](max) NULL,
	[domain] [varchar](250) NULL,
	[logon_count] [varchar](max) NULL,
	[memberof] [varchar](max) NULL,
	[is_deleted] [tinyint] NOT NULL,
	[parent_distinguished_name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_active_directory_domain_s_a_m_account_name_url] ON [dbo].[bik_active_directory] 
(
	[domain] ASC,
	[s_a_m_account_name] ASC,
	[url] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_active_directory_s_a_m_account_name_domain_url] ON [dbo].[bik_active_directory] 
(
	[s_a_m_account_name] ASC,
	[domain] ASC,
	[url] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_delta]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_delta](
	[si_id] [int] NULL,
	[name] [varchar](max) NULL,
	[folderCUID] [varchar](max) NULL,
	[to_delete] [int] NULL,
	[to_load] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_admin_backup_log]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_admin_backup_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_time] [datetime] NULL,
	[end_time] [datetime] NULL,
	[job_cuid] [varchar](128) NULL,
	[status] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_attr_hint]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attr_hint](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](350) NOT NULL,
	[hint] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_attr_hint_name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedTableType [dbo].[bik_joined_obj_modes_table_type]    Script Date: 09/28/2016 11:08:59 ******/
CREATE TYPE [dbo].[bik_joined_obj_modes_table_type] AS TABLE(
	[dst_id] [int] NOT NULL,
	[mode] [int] NULL,
	[level] [int] NOT NULL
)
GO
/****** Object:  Table [dbo].[bik_fts_word]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_word](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[word] [varchar](800) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[word] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_fts_word_in_obj]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_word_in_obj](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[obj_id] [int] NOT NULL,
	[word_id] [int] NOT NULL,
	[origin_flag] [int] NOT NULL,
	[occurence_cnt] [int] NOT NULL,
	[attr_cnt] [int] NOT NULL,
	[max_weight] [decimal](9, 8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[obj_id] ASC,
	[word_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_fts_stemmed_word]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_stemmed_word](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[word] [varchar](800) NOT NULL,
	[weight] [decimal](9, 8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_fts_attr]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_attr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](800) NOT NULL,
	[weight] [decimal](9, 8) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_fts_word_in_obj_attr]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_word_in_obj_attr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[obj_id] [int] NOT NULL,
	[attr_id] [int] NOT NULL,
	[word_id] [int] NOT NULL,
	[original_cnt] [int] NOT NULL,
	[synonym_cnt] [int] NOT NULL,
	[dictstemmed_cnt] [int] NOT NULL,
	[algostemmed_cnt] [int] NOT NULL,
	[accentfree_cnt] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[obj_id] ASC,
	[attr_id] ASC,
	[word_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_calc_dependent_nodes_for_attrs]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_calc_dependent_nodes_for_attrs](@node_id int, @dep_node_ids_as_str varchar(max) output) as
begin
  set nocount on;

  declare @attr_dependent_nodes_sql varchar(max) = (select ltrim(rtrim(attr_dependent_nodes_sql)) from bik_node_kind nk inner join bik_node n on n.node_kind_id = nk.id
    where n.id = @node_id);

  declare @dependent_nodes_sql varchar(max) = replace(@attr_dependent_nodes_sql, '${node_ids}$', 
    '(select ' + cast(@node_id as varchar(20)) + ' as node_id)');  

  declare @tab table (node_id int);

  insert into @tab (node_id)
  exec(@dependent_nodes_sql);

  declare @hlp varchar(max) = '';  
  
  select @hlp = @hlp + ',' + cast(node_id as varchar(20))
  from (select distinct node_id from @tab) as x;
  
  set @dep_node_ids_as_str = substring(@hlp, 2, len(@hlp));  
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_is_unconnected_user_node_or_has_unconnected_user_subnode]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_is_unconnected_user_node_or_has_unconnected_user_subnode](@id int, @node_kind_id int, @linked_node_id int, @branch_ids varchar(1000), @tree_id int)
returns int
as
begin

  declare @user_node_kind_id int = (select id from bik_node_kind where code='User');
 
  return case when

 (
      @node_kind_id = @user_node_kind_id
  
      and @id not in (select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id) 

      and (@linked_node_id is null 

          or @linked_node_id not in(select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id)
      ) 
   ) 

   or @node_kind_id <> @user_node_kind_id and exists(

      select bndi.id from bik_node bndi /* !!! >> */ 
    --with (index (idx_bik_node_tree_id_is_deleted))
/*with (index (idx_bik_node_branch_ids))*/  /* << !!! */ inner join bik_node_kind bnkdi on bndi.node_kind_id=bnkdi.id 
      where bndi.branch_ids like @branch_ids + '%' 
        and bndi.id not in (select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id) 
        and (bndi.linked_node_id is null or bndi.linked_node_id not in(select node_id from bik_user bu inner join bik_system_user bsu on bsu.user_id=bu.id)) 
        and bnkdi.code = 'User' and bndi.is_deleted = 0 

        and bndi.tree_id = @tree_id

   )
  then 1 else 0 end;

end;
GO
/****** Object:  Table [dbo].[bik_lisa_instance]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_instance](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](max) NULL,
	[url] [varchar](max) NULL,
	[port] [varchar](max) NULL,
	[db_name] [varchar](max) NULL,
	[charset] [varchar](max) NULL,
	[tmode] [varchar](max) NULL,
	[schema_name] [varchar](max) NULL,
	[default_unassigned_category_name] [varchar](max) NULL,
	[is_deleted] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_lisa_dict_instance]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_dict_instance](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[node_id] [int] NULL,
	[instance_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[node_id] ASC,
	[instance_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_lisa_dict_import_cache]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_dict_import_cache](
	[server_file_name] [varchar](max) NULL,
	[line_nr] [int] NULL,
	[pk_value] [varchar](max) NULL,
	[action_code] [varchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_joined_obj_attribute]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_joined_obj_attribute](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[type] [varchar](30) NOT NULL,
	[value_opt] [varchar](max) NULL,
	[value_opt_to] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_joined_obj_attribute_linked]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_joined_obj_attribute_linked](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[joined_obj_id] [int] NOT NULL,
	[attribute_id] [int] NOT NULL,
	[value] [varchar](max) NULL,
	[is_main] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_jdbc_source]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_jdbc_source](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](512) NOT NULL,
	[driver_name] [varchar](512) NOT NULL,
	[jdbc_url_template] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_jdbc_source_name] ON [dbo].[bik_jdbc_source] 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_help]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_help](
	[help_key] [varchar](255) NOT NULL,
	[caption] [varchar](255) NOT NULL,
	[text_help] [varchar](max) NULL,
	[is_hidden] [int] NOT NULL,
	[lang] [varchar](3) NOT NULL,
 CONSTRAINT [PK_bik_help_help_key_lang] PRIMARY KEY CLUSTERED 
(
	[help_key] ASC,
	[lang] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Help', @value=N'Jak pomagać? o_O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_help'
GO
/****** Object:  Table [dbo].[bik_icon]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_icon](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'DUpa', @value=N'xDD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_icon', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'asdsadsa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_icon', @level2type=N'COLUMN',@level2name=N'id'
GO
/****** Object:  Table [dbo].[bik_db_index]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_db_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[name] [varchar](512) NULL,
	[server_name] [varchar](512) NOT NULL,
	[database_name] [varchar](512) NULL,
	[table_node_id] [int] NOT NULL,
	[column_node_id] [int] NOT NULL,
	[column_name] [varchar](512) NOT NULL,
	[column_position] [int] NOT NULL,
	[is_unique] [int] NOT NULL,
	[type] [varchar](512) NULL,
	[is_primary_key] [int] NOT NULL,
	[partition_ordinal] [int] NOT NULL,
	[descend] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_db_column_extradata_column_node_id_table_node_id] ON [dbo].[bik_db_index] 
(
	[column_node_id] ASC,
	[table_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_db_column_extradata_table_node_id_column_node_id] ON [dbo].[bik_db_index] 
(
	[table_node_id] ASC,
	[column_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_db_foreign_key]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_db_foreign_key](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[description] [varchar](max) NULL,
	[server_name] [varchar](512) NOT NULL,
	[database_name] [varchar](512) NULL,
	[fk_table] [varchar](512) NOT NULL,
	[fk_table_node_id] [int] NOT NULL,
	[fk_schema] [varchar](512) NOT NULL,
	[fk_column] [varchar](512) NOT NULL,
	[fk_column_node_id] [int] NOT NULL,
	[pk_table] [varchar](512) NOT NULL,
	[pk_table_node_id] [int] NOT NULL,
	[pk_schema] [varchar](512) NOT NULL,
	[pk_column] [varchar](512) NOT NULL,
	[pk_column_node_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_db_dependency]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_db_dependency](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[server_name] [varchar](512) NOT NULL,
	[database_name] [varchar](512) NULL,
	[ref_node_id] [int] NOT NULL,
	[dep_node_id] [int] NOT NULL,
	[ref_name] [varchar](512) NOT NULL,
	[ref_owner] [varchar](512) NOT NULL,
	[dep_name] [varchar](512) NOT NULL,
	[dep_owner] [varchar](512) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_db_definition]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_db_definition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[server_name] [varchar](512) NOT NULL,
	[database_name] [varchar](512) NULL,
	[object_node_id] [int] NOT NULL,
	[definition] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uq_bik_db_definition_object_node_id] ON [dbo].[bik_db_definition] 
(
	[object_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_db_column_extradata]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_db_column_extradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[server_name] [varchar](512) NOT NULL,
	[database_name] [varchar](512) NULL,
	[table_node_id] [int] NOT NULL,
	[column_node_id] [int] NOT NULL,
	[column_id] [int] NOT NULL,
	[description] [varchar](max) NULL,
	[is_nullable] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[default_field] [varchar](512) NULL,
	[seed_value] [bigint] NULL,
	[increment_value] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_db_column_extradata_column_node_id_table_node_id_column_id] ON [dbo].[bik_db_column_extradata] 
(
	[column_node_id] ASC,
	[table_node_id] ASC,
	[column_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_db_column_extradata_table_node_id_column_node_id_column_id] ON [dbo].[bik_db_column_extradata] 
(
	[table_node_id] ASC,
	[column_node_id] ASC,
	[column_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_connection]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_connection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[name] [varchar](512) NOT NULL,
	[server] [varchar](512) NULL,
	[instance] [varchar](512) NULL,
	[db_user] [varchar](512) NULL,
	[db_password] [varchar](512) NULL,
	[port] [int] NULL,
	[database_name] [varchar](max) NULL,
	[is_deleted] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqc_test_parameters]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqc_test_parameters](
	[test_id] [int] NULL,
	[parameter_name] [varchar](300) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqc_test]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqc_test](
	[id_test] [int] NOT NULL,
	[id_template_test] [int] NULL,
	[id_test_stage] [int] NOT NULL,
	[id_importance_level] [int] NOT NULL,
	[id_person] [int] NOT NULL,
	[name] [varchar](32) NOT NULL,
	[long_name] [varchar](128) NOT NULL,
	[description] [text] NOT NULL,
	[sampling_method] [text] NULL,
	[verified_attributes] [text] NULL,
	[expected_result] [text] NULL,
	[logging_details] [text] NULL,
	[benchmark_definition] [text] NULL,
	[error_threshold] [decimal](18, 8) NULL,
	[results_object] [varchar](128) NULL,
	[additional_information] [text] NULL,
	[create_date] [datetime] NOT NULL,
	[modify_date] [datetime] NOT NULL,
	[suspend_date] [datetime] NULL,
	[__deleted] [bit] NOT NULL,
	[__obj_id] [varchar](max) NOT NULL,
	[test_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_test] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqc_request]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqc_request](
	[id_request] [int] NOT NULL,
	[id_alias_sql] [int] NOT NULL,
	[id_alias_mdx] [int] NOT NULL,
	[id_test] [int] NOT NULL,
	[id_person] [int] NOT NULL,
	[id_request_state] [int] NOT NULL,
	[case_count] [bigint] NULL,
	[error_count] [bigint] NULL,
	[error_percentage] [decimal](18, 8) NULL,
	[error_level_exceeded] [bit] NULL,
	[comments] [text] NULL,
	[passed] [bit] NULL,
	[log] [text] NULL,
	[start_timestamp] [datetime] NOT NULL,
	[end_timestamp] [datetime] NULL,
	[last_trial_timestamp] [datetime] NULL,
	[last_trial_count] [int] NULL,
	[is_manual] [bit] NOT NULL,
	[finished_steps] [int] NULL,
	[number_of_steps] [int] NULL,
	[__deleted] [bit] NOT NULL,
	[test_obj_id] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_request] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_dqc_request__deleted_id_request] ON [dbo].[bik_dqc_request] 
(
	[__deleted] ASC,
	[id_request] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_dqc_request_id_test__deleted_start_timestamp] ON [dbo].[bik_dqc_request] 
(
	[id_test] ASC,
	[__deleted] ASC,
	[start_timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqc_group_test]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqc_group_test](
	[id_group] [int] NOT NULL,
	[id_test] [int] NOT NULL,
	[__deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_group] ASC,
	[id_test] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqc_group]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqc_group](
	[id_group] [int] NOT NULL,
	[name] [varchar](32) NOT NULL,
	[description] [varchar](128) NOT NULL,
	[__deleted] [bit] NOT NULL,
	[__obj_id] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_dqm_def3000tr_profile_file]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_dqm_def3000tr_profile_file](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[file_name] [varchar](512) NULL,
	[is_def_file] [int] NOT NULL,
	[ztjd] [date] NOT NULL,
	[tamt] [decimal](17, 2) NOT NULL,
	[custacc] [varchar](100) NOT NULL,
	[custad1] [varchar](255) NULL,
	[custad2] [varchar](255) NULL,
	[custad3] [varchar](255) NULL,
	[custad4] [varchar](255) NULL,
	[benacc] [varchar](100) NOT NULL,
	[benad1] [varchar](255) NULL,
	[benad2] [varchar](255) NULL,
	[benad3] [varchar](255) NULL,
	[benad4] [varchar](255) NULL,
	[ref1] [varchar](255) NULL,
	[ref2] [varchar](255) NULL,
	[ref3] [varchar](255) NULL,
	[ref4] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_relationship]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_relationship](
	[long_id] [varchar](67) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[relation_type] [int] NOT NULL,
	[key_group_long_id] [varchar](67) NULL,
	[parent_table_long_id] [varchar](67) NOT NULL,
	[child_table_long_id] [varchar](67) NOT NULL,
	[relationship_comment] [varchar](2048) NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_relation_shape]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_relation_shape](
	[long_id] [varchar](67) NOT NULL,
	[model_object_ref] [varchar](67) NOT NULL,
	[parent_connector_point] [varchar](23) NOT NULL,
	[child_connector_point] [varchar](23) NOT NULL,
	[child_shape_ref] [varchar](67) NOT NULL,
	[parent_shape_ref] [varchar](67) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_key_group_member]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_key_group_member](
	[long_id] [varchar](67) NOT NULL,
	[key_group_long_id] [varchar](67) NOT NULL,
	[table_column_long_id] [varchar](67) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_key_group]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_key_group](
	[long_id] [varchar](67) NOT NULL,
	[name] [varchar](100) NULL,
	[key_group_type] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_subject_area_object_ref]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_subject_area_object_ref](
	[subject_area_long_id] [varchar](67) NOT NULL,
	[object_long_id] [varchar](67) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[subject_area_long_id] ASC,
	[object_long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_subject_area]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_subject_area](
	[long_id] [varchar](67) NOT NULL,
	[name] [varchar](100) NULL,
	[object_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_searchable_attr_props]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_searchable_attr_props](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[caption] [varchar](255) NOT NULL,
	[search_weight] [int] NOT NULL,
	[type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_search_hint]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_search_hint](
	[name] [varchar](max) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbw_query]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbw_query](
	[obj_name] [varchar](max) NOT NULL,
	[descr] [varchar](max) NULL,
	[provider] [varchar](max) NULL,
	[update_time] [varchar](max) NULL,
	[owner] [varchar](max) NULL,
	[last_edit] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbw_provider]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbw_provider](
	[obj_name] [varchar](max) NOT NULL,
	[descr] [varchar](max) NULL,
	[type] [varchar](100) NOT NULL,
	[info_area] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbw_object]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbw_object](
	[obj_name] [varchar](max) NOT NULL,
	[descr] [varchar](max) NULL,
	[type] [varchar](100) NULL,
	[provider] [varchar](max) NULL,
	[provider_parent] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbw_flow]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbw_flow](
	[obj_name] [varchar](max) NOT NULL,
	[parent] [varchar](max) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_sapbw_area]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbw_area](
	[obj_name] [varchar](max) NOT NULL,
	[descr] [varchar](max) NULL,
	[parent] [varchar](max) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_ww_foreignkeys]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_ww_foreignkeys] as
select 
cstr.name as [ForeignKey_Name],
fk.constraint_column_id as [ID] ,
tbl.name as [Table_Name] ,
cfk.name as [Name] ,
reftab.name As ReferencedTableName,
crk.name as [ReferencedColumn]
from sys.tables as tbl
inner join sys.foreign_keys as cstr
on cstr.parent_object_id=tbl.object_id
inner join sys.foreign_key_columns as fk
on fk.constraint_object_id=cstr.object_id
inner join sys.columns as cfk
on fk.parent_column_id = cfk.column_id
and fk.parent_object_id = cfk.object_id
inner join sys.columns as crk
on fk.referenced_column_id = crk.column_id
and fk.referenced_object_id = crk.object_id
inner join sys.tables reftab on reftab.object_id = cstr.referenced_object_id
--order by [Table_Name] asc , ReferencedTableName, [ForeignKey_Name] asc, [ID] asc
GO
EXEC sys.sp_addextendedproperty @name=N'Help', @value=N'WTF?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_ww_foreignkeys'
GO
/****** Object:  View [dbo].[vw_ww_db_table_stats]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_ww_db_table_stats] as
SELECT 
    t.NAME AS TableName,
    p.rows AS RowCounts,
    SUM(a.total_pages) * 8 AS TotalSpaceKB, 
    SUM(a.used_pages) * 8 AS UsedSpaceKB, 
    (SUM(a.total_pages) - SUM(a.used_pages)) * 8 AS UnusedSpaceKB
FROM 
    sys.tables t
INNER JOIN      
    sys.indexes i ON t.OBJECT_ID = i.object_id
INNER JOIN 
    sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
INNER JOIN 
    sys.allocation_units a ON p.partition_id = a.container_id
WHERE 
    t.NAME NOT LIKE 'dt%' 
    AND t.is_ms_shipped = 0
    AND i.OBJECT_ID > 255 
GROUP BY 
    t.Name, p.Rows
GO
/****** Object:  StoredProcedure [dbo].[sp_shrink_file]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_shrink_file]
WITH EXECUTE AS OWNER
AS
BEGIN
	declare @log_name varchar(max);
    select @log_name=name from sys.database_files where type = 1

    exec('DBCC SHRINKFILE (N''' + @log_name + '''  , 0)')
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_manage_biks_fts_catalog]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_manage_biks_fts_catalog](@drop_it int)
as
begin
  set nocount on

  declare @has_fulltext_catalog int = (select case when exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') then 1 else 0 end has_fulltext_catalog);
  
  print 'sp_manage_biks_fts_catalog(' + cast(@drop_it as varchar(20)) + '), @has_fulltext_catalog=' + cast(@has_fulltext_catalog as varchar(20));

  if @has_fulltext_catalog = 0 and @drop_it <> 0 begin
    print 'sp_manage_biks_fts_catalog: done, no fulltext catalog, nothing to drop';

    return; -- nie ma katalogu, nie ma co usuwać ;-)
  end;

  if @drop_it <> 0 begin
    print '  drop (if exists)';
     
    if exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
      print '  drop fulltext index on bik_searchable_attr_val';
      exec('drop fulltext index on bik_searchable_attr_val')
    end;
    if exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') begin
      print '  drop fulltext CATALOG BIKS_FTS_Catalog';
      exec('DROP FULLTEXT CATALOG BIKS_FTS_Catalog');
    end;
  end 
  else
  begin
    print '  create (if not exists)';
    if not exists(select 1 from sys.fulltext_catalogs where name = 'BIKS_FTS_Catalog') begin
      print '  create fulltext CATALOG BIKS_FTS_Catalog';
      exec('CREATE FULLTEXT CATALOG BIKS_FTS_Catalog AS DEFAULT');
    end;

    if not exists(select 1 from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) begin
      print '  CREATE FULLTEXT INDEX ON bik_searchable_attr_val';
      declare @pk_name sysname = (select name from sys.indexes where object_id = object_id('bik_searchable_attr_val') and is_primary_key = 1)
      exec('CREATE FULLTEXT INDEX ON bik_searchable_attr_val (value, fixed_value) key index ' + @pk_name + ' with stoplist = off')
    end
  
    if (select change_tracking_state from sys.fulltext_indexes where object_id = object_id('bik_searchable_attr_val')) <> 'A' begin
      print '  ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO';
      exec('ALTER FULLTEXT INDEX ON bik_searchable_attr_val SET CHANGE_TRACKING AUTO')
    end
  end;

  print 'sp_manage_biks_fts_catalog: done';
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_node_history_by_node_ids]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_delete_node_history_by_node_ids](@node_ids bik_unique_not_null_id_table_type READONLY)
as
begin
	declare @change_ids bik_unique_not_null_id_table_type;
    insert into @change_ids(id)
    select distinct change_id from bik_node_change_detail where node_id in (
    select id from @node_ids)
    
    delete from bik_node_change_detail_value where change_detail_id in (select id from bik_node_change_detail where node_id in (select id from @node_ids))
    delete from bik_node_change_detail where node_id in (select id from @node_ids)
    delete from bik_node_change where id in (select id from @change_ids)
end;
GO
/****** Object:  Table [dbo].[bik_statistic_dict]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_statistic_dict](
	[counter_name] [varchar](255) NOT NULL,
	[description] [varchar](255) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[counter_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_tutorial]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_tutorial](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NOT NULL,
	[text] [varchar](max) NOT NULL,
	[movie] [varchar](250) NULL,
	[code] [varchar](64) NULL,
	[lang] [varchar](3) NOT NULL,
	[visual_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_tutorial_lang_code] UNIQUE NONCLUSTERED 
(
	[lang] ASC,
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_tutorial_readed_user]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_tutorial_readed_user](
	[user_id] [int] NOT NULL,
	[tutorial_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_user_id_tutorial_id] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[tutorial_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dbau_process]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dbau_process](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_added] [datetime] NOT NULL,
	[init_db_ver] [varchar](100) NOT NULL,
	[app_ver] [varchar](100) NOT NULL,
	[spid] [int] NULL,
	[status] [int] NOT NULL,
	[final_db_ver] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_escape_and_quote_str]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_escape_and_quote_str](@str varchar(max)) returns varchar(max) as
begin
  return case when @str is null then 'null' else '''' + replace(@str, '''', '''''') + '''' end
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_discard_quote_for_string]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_discard_quote_for_string](@string varchar(max))
returns varchar(max)
as
begin
  return replace(replace(replace(replace(@string, '''', ''), '"',''),']',''),'[','')
end
GO
/****** Object:  Table [dbo].[bik_service_request]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_service_request](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[method_name] [varchar](1000) NOT NULL,
	[parameters] [varchar](max) NOT NULL,
	[start_time] [datetime] NOT NULL,
	[duration_millis] [int] NOT NULL,
	[ip_addr] [varchar](1000) NOT NULL,
	[session_id] [varchar](1000) NOT NULL,
	[exception_msg] [varchar](max) NULL,
	[extra_info] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_searchable_attr2]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_searchable_attr2](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[search_weight] [int] NOT NULL,
	[caption] [varchar](255) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_translation]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_translation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](890) NOT NULL,
	[txt] [varchar](max) NOT NULL,
	[lang] [varchar](3) NULL,
	[kind] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_code_language_kind] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[lang] ASC,
	[kind] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_teradata_index]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_teradata_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_branch_names] [varchar](max) NOT NULL,
	[name] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[unique_flag] [varchar](255) NOT NULL,
	[column_name] [varchar](max) NOT NULL,
	[column_position] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_teradata_data_model_process_object_rel]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_teradata_data_model_process_object_rel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[symb_proc] [varchar](80) NOT NULL,
	[id_obj] [int] NOT NULL,
	[obj_type] [char](1) NOT NULL,
	[id_typ_process] [varchar](3) NOT NULL,
	[bik_node_obj_id] [varchar](800) NOT NULL,
	[area] [varchar](80) NOT NULL,
	[database_name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_tdmpor_bik_node_obj_id] ON [dbo].[bik_teradata_data_model_process_object_rel] 
(
	[bik_node_obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_teradata_data_model_index]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_teradata_data_model_index](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[table_branch_names] [varchar](max) NOT NULL,
	[name] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[unique_flag] [varchar](255) NOT NULL,
	[column_name] [varchar](max) NOT NULL,
	[column_position] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_last_charindex]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_last_charindex](@what_to_search_for varchar(max), @search_in varchar(max), @opt_start_from_pos int) returns int as
begin
  if @what_to_search_for is null return null;
  
  declare @src_len int = datalength(@search_in);
  
  if @opt_start_from_pos is null set @opt_start_from_pos = @src_len;
  
  declare @dst_len int = datalength(@what_to_search_for);
  
  if @dst_len = 0 return @opt_start_from_pos;
  
  if @dst_len > @src_len return 0;
  
  if @dst_len = @src_len
    if @what_to_search_for = @search_in return 1 else return 0;
  
  declare @pos_limit int = @src_len - @dst_len + 1;
  
  if @opt_start_from_pos > @pos_limit set @opt_start_from_pos = @pos_limit;
  
  declare @src_trimmed varchar(max) = substring(@search_in, 1, @opt_start_from_pos + @dst_len - 1)
  
  if @opt_start_from_pos = 1   
    if @what_to_search_for = @src_trimmed return 1 else return 0;
    
  declare @src_rev varchar(max) = reverse(@src_trimmed);
  declare @dst_rev varchar(max) = reverse(@what_to_search_for);
  
  declare @rev_pos int = charindex(@dst_rev, @src_rev);
  
  if @rev_pos = 0 return 0;
  
  declare @src_trimmed_len int = datalength(@src_trimmed);
  
  return @src_trimmed_len - @rev_pos + 1 - @dst_len + 1;
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_KEYWORD_ID]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_KEYWORD_ID](@body varchar(2000))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN  
  return (select ID from aaa_keyword where body = @body);
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_TREE_NODE_ID]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_TREE_NODE_ID](@tree_num int, @caption varchar(1000))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN  
  return (select ID from aaa_tree_node where tree_num = @tree_num and caption = @caption);
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_level_for_universe_table]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_level_for_universe_table](@table_name varchar(max))
returns int
as
begin
  declare @tmp int = 0
  declare @result int = 0
  while @tmp <> -1
  begin
	 set @tmp = charindex('.',@table_name, @tmp + 1);
	 if @tmp<>0 set @result += 1;
	 else set @tmp = -1;
  end
  return @result
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_is_custom_right_role_tree]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_is_custom_right_role_tree](@tree_id int) returns int as begin return case when @tree_id in (1,2,3,6,7,8,12,13,14,15,17,19,20,21,22,29,33,37,38,39,40,63,79,80,81,82) then 1 else 0 end; end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_user_has_right_for_action_on_node]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_user_has_right_for_action_on_node](@user_id int, @action_id int, @tree_id int, @linked_node_id int, @branch_ids varchar(1000))
returns int
as
begin
  return case when dbo.fn_is_custom_right_role_tree(@tree_id) = 0
         or
        (
        @linked_node_id is null and
        (
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = @tree_id and @branch_ids like nbr.branch_ids + '%')
        or
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = @tree_id and nbr.branch_ids like @branch_ids + '%')
        )
        ) or
        (
        @linked_node_id is not null and
        (
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and lbn.branch_ids like nbr.branch_ids + '%'
        where lbn.id = @linked_node_id)
        or
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and nbr.branch_ids like lbn.branch_ids + '%'
        where lbn.id = @linked_node_id)
        )
        )        
  then 1 else 0 end;
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_node_has_no_children]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_node_has_no_children](@node_id int, @action_id int, @user_id int)
returns int
as
begin
/*
  declare @crr_tree_ids table (tree_id int not null primary key);

  insert into @crr_tree_ids (tree_id)
  select distinct t.id 
  from dbo.fn_split_by_sep((select val from bik_app_prop where name = 'customRightRolesTreeSelector'), ',', 7) x inner join bik_tree t on t.code = x.str;
*/

  return case when /*bn.disable_linked_subtree = 0 and*/ exists
        (select 1 from bik_node cbn 
--with (index (idx_bik_node_is_deleted_parent_node_id)) 
--with (index (idx_bik_node_parent_deleted_tree_linked_branch_x)) 

where is_deleted = 0 and parent_node_id = @node_id --coalesce(bn.linked_node_id, bn.id)
        
        
        
        --/*
and 
       
    
    
        
        ( --cbn.tree_id not in (select tree_id from @crr_tree_ids) --(4,6,8,12,13,14,15,17,19,21,22,29,30,31,33,34,35,36,38,39) 
          dbo.fn_is_custom_right_role_tree(cbn.tree_id) = 0
         or
        (
        cbn.linked_node_id is null and
        (
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = cbn.tree_id and cbn.branch_ids like nbr.branch_ids + '%')
        or
        exists (select 1 from bik_custom_right_role_action_branch_grant nbr
        where nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = cbn.tree_id and nbr.branch_ids like cbn.branch_ids + '%')
        )
        ) or
        (
        cbn.linked_node_id is not null and
        (
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and lbn.branch_ids like nbr.branch_ids + '%'
        where lbn.id = cbn.linked_node_id)
        or
        exists (select 1 from bik_node lbn inner join bik_custom_right_role_action_branch_grant nbr
        on nbr.action_id = @action_id and nbr.user_id = @user_id and nbr.tree_id = lbn.tree_id and nbr.branch_ids like lbn.branch_ids + '%'
        where lbn.id = cbn.linked_node_id)
        )
        )
        )
    --*/

        
        )
        then 0 else 1 end;
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_take_branch_from_branches]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_take_branch_from_branches](@expressionToFind char(1), @expressionToSearch varchar(1000), @branchNo int)
returns varchar(255)
as
begin
	
	if @branchNo <=0
		return ''

	declare @positionCurr int, @positionPrev int, @positionBeg int,@positionEnd int, @counter int
	-- Initialize the current position and counter.
	set @positionCurr = 0
	set @positionBeg = 0
	set @positionEnd = 0
	set @positionPrev = 0
	set @counter = 0
	set @branchNo = @branchNo -1
	
	set @positionCurr = charindex(@expressionToFind, @expressionToSearch, @positionCurr)
	while @positionCurr>0
	   begin
		   if @counter=@branchNo 
			 begin
				set @positionBeg = @positionPrev+1
				set @positionEnd = @positionCurr
			 end --@counter=@branchNo

		   set @counter = @counter + 1
		   set @positionPrev = @positionCurr
		   set @positionCurr = charindex(@expressionToFind, @expressionToSearch, @positionCurr+1)
	   end --while end
	
	if @counter=@branchNo 
	  begin
		set @positionBeg = @positionPrev+1
		set @positionEnd = len(@expressionToSearch)+1
	  end

	return substring(@expressionToSearch,@positionBeg, @positionEnd-@positionBeg)
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_string_counter]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_string_counter](@expressionToFind char(1), @expressionToSearch varchar(1000))
returns int
as
begin
	declare @position int, @counter int
	-- Initialize the current position and counter.
	set @position = 1
	set @counter = 0

	set @position = charindex(@expressionToFind, @expressionToSearch, @position)
	while @position>0
	   begin
	   set @counter = @counter + 1
	   set @position = charindex(@expressionToFind, @expressionToSearch, @position+1)
	   end

	return @counter
end
GO
/****** Object:  Table [dbo].[bik_node_name_chunk]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_name_chunk](
	[node_id] [int] NOT NULL,
	[tree_id] [int] NOT NULL,
	[chunk_txt] [varchar](3) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_node_name_chunk_cn] ON [dbo].[bik_node_name_chunk] 
(
	[chunk_txt] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_node_name_chunk_ctn] ON [dbo].[bik_node_name_chunk] 
(
	[chunk_txt] ASC,
	[tree_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_node_name_chunk_n] ON [dbo].[bik_node_name_chunk] 
(
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Trigger [trg_bik_node_chunking]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_bik_node_chunking]
ON [dbo].[bik_node]
FOR insert, update, delete AS
begin
    -- diag: print cast(sysdatetime() as varchar(23)) + ': start!'

set nocount on

declare @diags_level int = 0;

declare @to_delete table (id int not null primary key);

declare @bik_node_name_chunk table (node_id int not null, tree_id int not null, chunk_txt varchar(3) not null)

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_delete'
    end
   
insert into @to_delete (id)    
select d.id from deleted d left join inserted i on d.id = i.id
where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1) or i.tree_id <> d.tree_id
   
   declare @rowcnt int;
   
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids'
      set @rowcnt = (select count(*) from @to_delete)
      print cast(sysdatetime() as varchar(23)) + ': before delete by ids, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
delete from bik_node_name_chunk where node_id in (select id from @to_delete
--select d.id from deleted d left join inserted i on d.id = i.id
--where i.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 0 and i.is_deleted = 1)
);

declare @to_insert table (id int not null, name varchar(max) not null, tree_id int not null);

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before calc @to_insert'
    end
   
insert into @to_insert (id, name, tree_id)
select i.id, i.name, i.tree_id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or i.chunked_ver <> d.chunked_ver or (d.is_deleted = 1 and i.is_deleted = 0) or i.tree_id <> d.tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor'
      set @rowcnt = (select count(*) from @to_insert)
      print cast(sysdatetime() as varchar(23)) + ': before declare cursor, rowcnt=' + cast(@rowcnt as varchar(20))
    end
   
declare nodes cursor for select id, name, tree_id from @to_insert;
/*select id, name, tree_id from bik_node where id in (
select i.id from inserted i left join deleted d on d.id = i.id
where d.name is null or i.name <> d.name or (d.is_deleted = 1 and i.is_deleted = 0)
)*/;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before open cursor'
    end
   
open nodes;

declare @id int, @name varchar(8000), @tree_id int;

  if @diags_level > 0 begin
  -- diag:
    print cast(sysdatetime() as varchar(23)) + ': before first fetch'
  end
 
fetch next from nodes into @id, @name, @tree_id;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': before loop'
    end
   
    declare @cnt int = 0;
   
while @@fetch_status = 0
begin
  insert into @bik_node_name_chunk (node_id, chunk_txt, tree_id)
  select distinct @id, chunk_str, @tree_id
  from dbo.fn_split_string(@name, 3);
 
    if @diags_level > 1 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop after insert'
    end
   
  set @cnt = @cnt + 1;
 
  if (@cnt % 1000 = 0) begin
 
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': in loop, before insert into bik_node_name_chunk, cnt:' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
   
    delete from @bik_node_name_chunk
  end;   
 
  if @diags_level > 1 begin
     -- diag:
     print cast(sysdatetime() as varchar(23)) + ': in loop before fetch next'
  end
 
  fetch next from nodes into @id, @name, @tree_id;
  if @diags_level > 1 begin
    -- diag:
    print cast(sysdatetime() as varchar(23)) + ': in loop after fetch next'
  end
 
end;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': after loop, before insert into bik_node_name_chunk, @cnt=' + cast(@cnt as varchar(10))
    end
   
    insert into bik_node_name_chunk (node_id, chunk_txt, tree_id)
    select node_id, chunk_txt, tree_id
    from @bik_node_name_chunk
     
    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': closing, deallocating cursor'
    end
   
close nodes;

deallocate nodes;

    if @diags_level > 0 begin
    -- diag:
      print cast(sysdatetime() as varchar(23)) + ': done, stop!'
    end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_update_Joined_Objs_For_Node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_update_Joined_Objs_For_Node](@src_id int, @no_linked_nodes int, @joined_obj_modes bik_joined_obj_modes_table_type readonly) as
begin
  set nocount on
  declare @diags_level int = 1 -- 0 oznacza brak logowania

  declare @tmp_joined_objs table (dst_node_id int not null primary key, type int not null, inherit_to_descendants int not null)
  
  declare @to_join table (dst_id int not null, mode int null, level int not null, branch_ids varchar(2000) not null)

  insert into @to_join (dst_id, mode, level, branch_ids)
  select t.dst_id, t.mode, t.level, n.branch_ids
  from @joined_obj_modes t inner join bik_node n on t.dst_id = n.id

  declare to_join cursor for 
  select dst_id, mode, level, branch_ids from @to_join
  order by branch_ids

  open to_join

  declare @dst_id int, @mode int, @level int, @branch_ids varchar(2000)

  fetch next from to_join into @dst_id, @mode, @level, @branch_ids

  while @@fetch_status = 0
  begin
    if @diags_level > 0 begin
      print 'row in loop: @dst_id=' + cast(@dst_id as varchar(20)) + ', @mode=' + cast(@mode as varchar(20)) + ', @branch_ids=' + @branch_ids
    end
  
    declare @br_ids_for_like varchar(3000) = @branch_ids + '%'

    if @level = 0 -- this node only (@dst_id)
    begin
      delete from @tmp_joined_objs
      where dst_node_id = @dst_id

  	  if @mode is not null begin
  	    declare @linked_node_id_single int = (select linked_node_id from bik_node where id = @dst_id)
  	  
        insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
        values (coalesce(@linked_node_id_single, @dst_id), case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end)
        if @diags_level > 0 begin
          print 'row in loop: inserted single'
        end        
      end
    end else if @level = 1 -- direct subnodes only
    begin
        delete from @tmp_joined_objs
        where dst_node_id in (select id from bik_node where parent_node_id = @dst_id)

  	  if @mode is not null begin
  	    insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
	    select coalesce(n.linked_node_id, n.id), case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end
	    from bik_node n left join bik_joined_objs jo on n.id = jo.dst_node_id and jo.src_node_id = @src_id
	    where n.parent_node_id = @dst_id and (jo.id is null or jo.type = 0) and n.is_deleted = 0 and (@no_linked_nodes = 0 or n.linked_node_id is null)
      end
    end else -- all subdones
    begin
      delete from @tmp_joined_objs
      where dst_node_id <> @dst_id and dst_node_id in (select id from bik_node where branch_ids like @br_ids_for_like)

	  if @mode is not null begin
        insert into @tmp_joined_objs (dst_node_id, type, inherit_to_descendants)
        select coalesce(n.linked_node_id, n.id), case when @mode = 0 then -1 else 0 end, case when @mode = 1 then 1 else 0 end
        from bik_node n left join bik_joined_objs jo on n.id = jo.dst_node_id and jo.src_node_id = @src_id
        where n.id <> @dst_id and n.branch_ids like @br_ids_for_like and (jo.id is null or jo.type = 0) and n.is_deleted = 0 and (@no_linked_nodes = 0 or n.linked_node_id is null)
      end
    end
    fetch next from to_join into @dst_id, @mode, @level, @branch_ids
  end

  close to_join;

  deallocate to_join;

  if @diags_level > 0 begin
    select * from @tmp_joined_objs
  end

  declare @src_branch_ids varchar(2000) = (select branch_ids from bik_node where id = @src_id)

  declare @fixed_dst_node_tab table (node_id int not null primary key)

  insert into @fixed_dst_node_tab (node_id)
  select distinct jo.dst_node_id from bik_joined_objs jo inner join bik_node n on jo.dst_node_id = n.id
  where src_node_id in (
  select ancestor_id from (
  select cast(str as int) as ancestor_id, idx 
  from dbo.fn_split_by_sep(@src_branch_ids, '|', 7)
  ) x 
  where ancestor_id <> @src_id
  ) and jo.inherit_to_descendants = 1 and n.is_deleted = 0

  insert into @fixed_dst_node_tab (node_id)
  select dst_node_id from bik_joined_objs where src_node_id = @src_id and type = 1

  delete from bik_joined_objs where dst_node_id in (select dst_node_id from @tmp_joined_objs) and src_node_id = @src_id and
  dst_node_id not in (select node_id from @fixed_dst_node_tab)

  insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_to_descendants)
  select @src_id, dst_node_id, 0, inherit_to_descendants
  from @tmp_joined_objs
  where type <> -1 and dst_node_id not in (select node_id from @fixed_dst_node_tab) and dst_node_id <> @src_id

  declare @fixed_src_node_tab table (node_id int not null primary key)

  insert into @fixed_src_node_tab (node_id)
  select jo.src_node_id from bik_joined_objs jo inner join @tmp_joined_objs t on jo.src_node_id = t.dst_node_id
  where jo.dst_node_id = @src_id and jo.type = 1

  delete from bik_joined_objs where src_node_id in (select dst_node_id from @tmp_joined_objs) and dst_node_id = @src_id and
  src_node_id not in (select node_id from @fixed_src_node_tab)

  insert into bik_joined_objs (src_node_id, dst_node_id, type, inherit_to_descendants)
  select dst_node_id, @src_id, 0, inherit_to_descendants
  from @tmp_joined_objs
  where type <> -1 and dst_node_id not in (select node_id from @fixed_src_node_tab) and dst_node_id <> @src_id
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_node_name_path]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_node_name_path] (@nodeId int)
returns varchar(max)
as
begin
	declare @branchIds varchar(max) = (select branch_ids from bik_node where id = @nodeId);

	declare @tmp varchar(max) = '';
	select @tmp = case when @tmp = '' then @tmp + bn.name else @tmp + ' » ' + bn.name end 
	from dbo.fn_split_by_sep(@branchIds, '|', 0) bra
	inner join bik_node bn on bra.str = bn.id
	order by bra.idx;

	--set @tmp = @treeCaption + ' » ' + @tmp;
	
    return(@tmp);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_nr_zlec_fix]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_nr_zlec_fix] (@nr_zlec varchar(1000))
RETURNS varchar(1000)
WITH EXECUTE AS CALLER
AS
BEGIN
  set @nr_zlec = UPPER(@nr_zlec)
  
  set @nr_zlec = REPLACE(@nr_zlec, '/', '');
  set @nr_zlec = REPLACE(@nr_zlec, '\', '');
  set @nr_zlec = REPLACE(@nr_zlec, '|', '');
  set @nr_zlec = REPLACE(@nr_zlec, ' ', '');
  set @nr_zlec = REPLACE(@nr_zlec, '-', '');
  set @nr_zlec = REPLACE(@nr_zlec, '.', '');
  
  if (LEN(@nr_zlec) < 24)
    set @nr_zlec = @nr_zlec + REPLICATE('!', 24 - len(@nr_zlec))
  
  return @nr_zlec;
END
GO
/****** Object:  StoredProcedure [dbo].[sp_finalizeTransfer]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_finalizeTransfer] as
begin
  truncate table a00_global_props;
  
  insert into a00_global_props
  select * from tmp_a00_global_props;
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_string_to_n_char]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_string_to_n_char](@string varchar(max), @char varchar(max), @n int)
returns varchar(max)
as
begin
	declare @pos int = 1;
	declare @len int = DATALENGTH(@string);
	declare @pos2 int;
	declare @idx int = 1;
	declare @returnString varchar(max);
	
	while @pos <= @len begin
		set @pos2 = charindex(@char, @string, @pos)
		if @pos2 = 0 break;
		if @idx = @n
		begin
			set @returnString = substring(@string, 1, @pos2)
			break;
		end
		else
		begin
			set @idx = @idx + 1;
			set @pos = @pos2 + 1;
		end
	end
	return @returnString;
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_user_node_name]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_user_node_name](@login_name varchar(250), @old_login_name varchar(250), @name varchar(250), @is_deleted int)
returns varchar(4000)
with execute as caller
as
begin
  declare @login varchar(250)
  declare @new_name varchar(4000);
  if @is_deleted = 0
  begin
    set @login = @login_name
  end
  else
  begin
    set @login = @old_login_name
  end
  if @login != @name
  begin
    set @new_name = @name + ' (' + @login + ')'
  end
  else
  begin
    set @new_name = @name
  end
  return @new_name
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_USER_ID]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_USER_ID](@name varchar(1000))
RETURNS int
WITH EXECUTE AS CALLER
AS
BEGIN  
  return (select ID from aaa_user where name = @name);
END;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_calc_searchable_attr_val_weight]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_calc_searchable_attr_val_weight](@attr_search_weight int, @node_search_rank int, @kind_search_rank int, @positive_vote_cnt int)
returns int as
begin
  return (@attr_search_weight + @node_search_rank + @kind_search_rank) * 100 + @positive_vote_cnt;
end
GO
/****** Object:  UserDefinedFunction [dbo].[list2table]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[list2table]
(
	@list varchar(max),
	@delim char
)
returns
	@parsedlist table
	(
		item varchar(max)
	)
	as
	begin
		declare @item varchar(max), @pos int
		set @list = ltrim(rtrim(@list))+ @delim
		set @pos = charindex(@delim, @list, 1)
		
		while @pos > 0
			begin
				set @item = ltrim(rtrim(left(@list, @pos - 1)))
				if @item <> ''
				  begin
				   insert into @parsedlist (item)
				   values (cast(@item as varchar(max)))
				  end
				set @list = right(@list, len(@list) - @pos)
				set @pos = charindex(@delim, @list, 1)
			end
		return
	end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ww_table_key_cols]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_ww_table_key_cols](@table_name sysname, @with_col_types int, @key_level int)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''

  declare @idx_name sysname = (select top 1 constraint_name from
  (
select distinct i.name as constraint_name,
  case when i.is_primary_key = 1 then 'pk' when i.is_unique_constraint = 1 then 'uk' else 'ux' end as constraint_kind
 from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
where i.index_id > 0    
and i.type in (1, 2) -- clustered & nonclustered only
and i.is_disabled = 0
and i.is_hypothetical = 0
and (i.is_primary_key = 1 or @key_level > 0 and i.is_unique_constraint = 1 or @key_level > 1 and i.is_unique = 1)
and t.object_id = object_id(@table_name)
) x order by case constraint_kind when 'pk' then 0 when 'uk' then 1 else 2 end)
    
select 
  @txt = @txt +
  c.column_name + 
  case when @with_col_types <> 0 then
  ' ' + c.DATA_TYPE + case when c.character_maximum_length is null then ''
    when c.character_maximum_length < 0 then '(max)' else '(' + cast(c.CHARACTER_MAXIMUM_LENGTH as varchar(10)) + ')' end + 
    ' ' + case when c.is_nullable = 'NO' then 'not' else '' end + ' null'
  else '' end + ','
from 
  (
select s.name as table_schema, t.name as table_name, i.name as constraint_name, c.name as column_name,
  case when i.is_primary_key = 1 then 'pk' when i.is_unique_constraint = 1 then 'uk' else 'ux' end as constraint_kind,
  ic.key_ordinal as ordinal_position
 from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
inner join sys.index_columns ic on ic.object_id = t.object_id and i.index_id = ic.index_id
	inner join sys.columns c on c.object_id = t.object_id and
		ic.column_id = c.column_id
where i.name = @idx_name
and i.index_id > 0    
and i.type in (1, 2) -- clustered & nonclustered only
and i.is_disabled = 0
and i.is_hypothetical = 0
and ic.key_ordinal > 0
and (i.is_primary_key = 1 or @key_level > 0 and i.is_unique_constraint = 1 or @key_level > 1 and i.is_unique = 1)
) tc inner join information_schema.columns c 
    on tc.table_schema = c.TABLE_SCHEMA
      and tc.TABLE_NAME = c.TABLE_NAME and tc.COLUMN_NAME = c.COLUMN_NAME
order by tc.ORDINAL_POSITION

  return case when @txt = '' then 
    '/* no ' + case @key_level when 0 then 'pk' when 1 then 'uk' else 'ux' end + ' on ' + @table_name + ' */'
     else substring(@txt, 1, len(@txt) - 1) end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_drop_column_if_exists_with_opt_default]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_drop_column_if_exists_with_opt_default](@table_name sysname, @column_name sysname) as
begin
  --declare @table_name sysname = 'bik_node', @column_name sysname = 'disable_linked_subtree'
  DECLARE @default_constraint_name sysname
  SELECT @default_constraint_name = object_name(cdefault)
  FROM syscolumns
  WHERE id = object_id(@table_name)
  AND name = @column_name
  --select @default_constraint_name
  if @default_constraint_name is not null
    EXEC ('ALTER TABLE ' + @table_name + ' DROP CONSTRAINT ' + @default_constraint_name)
  if exists(select 1 from syscolumns sc where id = OBJECT_ID(@table_name) and name = @column_name)
    EXEC ('ALTER TABLE ' + @table_name + ' DROP column ' + @column_name)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_amg_node_init_branch_id]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_amg_node_init_branch_id]
as
begin
	set nocount on
	declare @diags_level int = 0;


	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #1'

  create table #ids (id int not null primary key);
  
  insert into #ids (id) 
  select id from amg_node where parent_obj_id is null

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #2'
    
  create table #child_ids (id int not null primary key);
    
  while exists (select 1 from #ids)
  begin
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #3'
    
    update amg_node
    set branch_ids = coalesce(parent.branch_ids, '') + CAST(amg_node.id as varchar(10)) + '|'
    from amg_node inner join #ids on amg_node.id = #ids.id
      left join amg_node as parent on parent.obj_id = amg_node.parent_obj_id;    
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #4'

    truncate table #child_ids;
    
    insert into #child_ids (id)
    select id from amg_node where parent_obj_id in (select nd.obj_id from #ids as tm inner join amg_node as nd on nd.id = tm.id);
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #5'

    truncate table #ids;
    
    insert into #ids (id) select id from #child_ids;

	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #6'    
  end
    
	if @diags_level > 0 --diag
	print cast(sysdatetime() as varchar(23)) + ': sp_node_init_branch_id #7'

  drop table #ids;
  drop table #child_ids;
  
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ww_self_join_cond]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_ww_self_join_cond](@cols varchar(max), @ree_alias sysname, @red_alias sysname)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''
  declare @and_sep varchar(max) = ' and '

  select @txt = @txt + @and_sep + @ree_alias + '.' + str + '=' + @red_alias + '.' + str from dbo.fn_split_by_sep(@cols, ',', 3)
  order by idx
  
  return substring(@txt, len(@and_sep) + 1, len(@txt))
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ww_fk_join_cond]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_ww_fk_join_cond](@fk_name sysname, @ree_alias sysname, @red_alias sysname)
returns varchar(max)
as
begin
  declare @txt varchar(max) = ''
  declare @and_sep varchar(max) = ' and '

  select @txt = @txt + @and_sep + @ree_alias + '.' + name + '=' + @red_alias + '.' + ReferencedColumn
  from vw_ww_foreignkeys
  where ForeignKey_Name = @fk_name

  return substring(@txt, len(@and_sep) + 1, len(@txt))
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_bik_node_name_chunk_search_sql]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create function [dbo].[fn_bik_node_name_chunk_search_sql] (@substr varchar(1000), @tree_ids_filter varchar(8000))--@opt_tree_id int)
returns varchar(8000)
WITH EXECUTE AS CALLER
as
begin
  declare @sql varchar(8000) = 'select node_id from bik_node_name_chunk
  where (';

  if len(@substr) > 3 begin

  declare @substr_chunks table (chunk_str varchar(3) not null);
  
  insert into @substr_chunks
  select chunk_str from (
  select distinct top 5 chunk_str, len(chunk_Str) as cln from dbo.fn_split_string(@substr, 3) 
  where len(chunk_str) = 3 order by len(chunk_str) desc
  ) x
  
  declare bnncs cursor for select chunk_str from @substr_chunks;
  
  open bnncs;
  
  declare @chunk_str varchar(3);
  
  fetch next from bnncs into @chunk_str
  
  declare @is_first int = 1;
  
  while @@fetch_status = 0 begin
    if @is_first = 0 begin
      set @sql = @sql + ' or ';
    end else begin
      set @is_first = 0;
    end

	set @chunk_str = replace(@chunk_str, '''', '''''');
  
    set @sql = @sql + 'chunk_txt = ''' + @chunk_str + ''''
  
    fetch next from bnncs into @chunk_str
  end;
  end else begin
	set @chunk_str = replace(@substr, '''', '''''');
  
    if len(@chunk_str) < 3 begin
      set @sql = @sql + 'chunk_txt like ''' + @chunk_str + '%'''
    end else begin
      set @sql = @sql + 'chunk_txt = ''' + @chunk_str + ''''
    end;
  end  
  set @sql = @sql + ')'
	/*
	--zmiana
	if @opt_tree_id is not null begin
		set @sql = @sql + ' and tree_id = ' + cast(@opt_tree_id as varchar(20));
	end;
  */
  if @tree_ids_filter is not  null
  begin
	set @sql = @sql + ' and ' + @tree_ids_filter;
  end
  if len(@substr) > 3 begin
  set @sql = @sql + ' group by node_id having count(*) = ' + cast((select count(*) from @substr_chunks) as varchar(10))
  end
  
  return @sql;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_filter_bik_nodes]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_filter_bik_nodes] (@txt varchar(max), 
  @opt_Sub_Tree_Root_Node_Id int,
  @tree_ids_filter varchar(8000) /*@optTreeId int*/, @opt_bik_node_filter varchar(max),
  @opt_extra_fields varchar(max))
as
begin
  set nocount on
  
  declare @diag_level int = 0
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': start!'
  
  declare @subtree_branch_ids varchar(max) = null;

  if @opt_Sub_Tree_Root_Node_Id is not null begin

    set @subtree_branch_ids = (select branch_ids from bik_node where id = @opt_Sub_Tree_Root_Node_Id);

    declare @branch_ids_patt varchar(max) = @subtree_branch_ids + '%';
    
    set @opt_bik_node_filter = case when @opt_bik_node_filter is null then '' else @opt_bik_node_filter + ' and ' end
      + ' branch_ids like ' + QUOTENAME(@branch_ids_patt, '''');
  end;

declare @sql varchar(max);

set @sql = 'select id from bik_node where id in (' + dbo.fn_bik_node_name_chunk_search_sql(@txt, @tree_ids_filter)--@optTreeId) 
  + ') and name like ''%' + replace(@txt, '''', '''''') + '%'' and is_deleted = 0' +
  case when @opt_bik_node_filter is null then '' else ' and (' + @opt_bik_node_filter + ')' end +
  case when @tree_ids_filter is null then '' else ' and (' + @tree_ids_filter + ')' end
  
  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': have sql: sql=' + @sql
  -- print @sql
-- ostatnio wrzucone
declare @t table (id int not null primary key, parent_node_id int null);
-- parent_node_ids ostatnio wrzuconych - te będziemy wrzucać teraz
declare @p table (id int not null primary key);
-- wszystkie

if not exists(select 1 from tempdb..sysobjects where id = object_id('tempdb..##temp_tab_sp_filter_bik_nodes'))
  create table ##temp_tab_sp_filter_bik_nodes (spid int not null, id int not null, unique (spid, id));
else
  delete from ##temp_tab_sp_filter_bik_nodes where spid = @@spid;
  
  --declare @a table (id int not null primary key);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': before exec sql'

  insert into @p (id)
  exec(@sql)

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': sql executed'
  
  while exists (select 1 from @p) begin
    --insert into @a select id from @p;
    insert into ##temp_tab_sp_filter_bik_nodes (spid, id) select @@spid, id from @p;

    delete from @t;

    insert into @t (id, parent_node_id) select id, parent_node_id from bik_node where id in (select id from @p);

    delete from @p;

    insert into @p 
    select distinct parent_node_id 
    from @t 
    --where parent_node_id is not null and parent_node_id not in (select id from @a);
    where parent_node_id is not null and parent_node_id not in (select id from ##temp_tab_sp_filter_bik_nodes where spid = @@spid);
  end;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after loop'

  declare @sql_txt_x varchar(max) = '
select bn.*,
    bnk.caption as node_kind_caption,
    bnk.code as node_kind_code,
    bnk.is_folder as is_folder,
    bt.name as tree_name, bt.tree_kind, bt.code as tree_code
    , case when disable_linked_subtree = 0 and exists 
        (select 1 from bik_node where is_deleted = 0 and parent_node_id = coalesce(bn.linked_node_id, bn.id)) then 0 else 1 end as has_no_children' +
        case when ltrim(rtrim(coalesce(@opt_extra_fields, ''))) <> '' then ', ' + @opt_extra_fields else '' end + '
    from bik_node bn inner join bik_node_kind bnk on bn.node_kind_id = bnk.id
    inner join bik_tree bt on bn.tree_id = bt.id
    --where bn.id in (select id from @a)
    where bn.id in (select id from ##temp_tab_sp_filter_bik_nodes where spid = ' + cast(@@spid as varchar(20)) + ')
    ' + case when @opt_Sub_Tree_Root_Node_Id is not null then ' and branch_ids like ' + QUOTENAME(@branch_ids_patt, '''') else '' end + '
    order by bn.visual_order, bnk.is_folder desc, bn.name
';

  exec(@sql_txt_x);

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': after final select'
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_123]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_123](
    @inpurparam varchar(4000)
)
as
begin

 select * from bik_node_kind
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_sapbo_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_sapbo_connections]
as
begin

	create table #tmp_network (name varchar(max) collate DATABASE_DEFAULT);
	create table #tmp_engine (db_engine varchar(max) collate DATABASE_DEFAULT, network_layer varchar(max) collate DATABASE_DEFAULT);
	create table #tmp_conn (conn_node_id int, parent_node_id int);

	declare @connTree int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @connFolderKind int = dbo.fn_node_kind_id_by_code('ConnectionFolder');
	declare @connEngineFolderKind int = dbo.fn_node_kind_id_by_code('ConnectionEngineFolder');
	declare @connNetworkFolderKind int = dbo.fn_node_kind_id_by_code('ConnectionNetworkFolder');
	declare @parentNodeId int;
	select @parentNodeId = id from bik_node where obj_id = '#BO$ConnectionFolder'/*name='Połączenia'*/ and tree_id = @connTree and node_kind_id = @connFolderKind and is_deleted = 0 and linked_node_id is null and parent_node_id is null

	-- wrzucenie network layers folders
	insert into #tmp_network(name)
	select distinct connetion_networklayer_name from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connTree

	-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
	update bik_node
	set is_deleted = 0
	where tree_id = @connTree
	and is_deleted = 1
	and linked_node_id is null
	and node_kind_id = @connNetworkFolderKind
	and parent_node_id = @parentNodeId
	and name in (select name from #tmp_network)

	-- dodanie nowych, jeśli są nowe
	insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select @parentNodeId,@connNetworkFolderKind, case when #tmp_network.name is null then 'Inne' else #tmp_network.name end, @connTree
	from #tmp_network 
	left join bik_node bn on bn.tree_id = @connTree and bn.is_deleted = 0 and bn.name = #tmp_network.name
	and bn.linked_node_id is null and bn.node_kind_id = @connNetworkFolderKind
	where bn.id is null;

	-- usuniecie starych/niepotrzebnych
	update bik_node
	set is_deleted = 1
	where tree_id = @connTree 
	and is_deleted = 0
	and node_kind_id = @connNetworkFolderKind
	and name not in (select name from #tmp_network)
	and linked_node_id is null

	-- wrzucenie DB engine folders
	insert into #tmp_engine(db_engine, network_layer)
	select distinct case when database_engine is null then 'Inne' else database_engine end,connetion_networklayer_name from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connTree

	-- przywrocenie dobrych folderkow, usunietych po 1 fazie zasilania
	update bik_node
	set is_deleted = 0
	where tree_id = @connTree
	and is_deleted = 1
	and linked_node_id is null
	and node_kind_id = @connEngineFolderKind
	and parent_node_id in (select id from bik_node where tree_id = @connTree and is_deleted = 0 and linked_node_id is null and node_kind_id = @connNetworkFolderKind)
	and name in (select db_engine from #tmp_engine)

	-- dodanie nowych, jeśli są nowe
	insert into bik_node (parent_node_id, node_kind_id, name, tree_id)
	select (select id from bik_node where tree_id = @connTree and is_deleted = 0 and name = #tmp_engine.network_layer and linked_node_id is null and node_kind_id = @connNetworkFolderKind),@connEngineFolderKind, #tmp_engine.db_engine, @connTree
	from #tmp_engine 
	left join bik_node bn on bn.tree_id = @connTree and bn.is_deleted = 0 and bn.name = #tmp_engine.db_engine
	and bn.linked_node_id is null and bn.node_kind_id = @connEngineFolderKind
	and bn.parent_node_id = (select id from bik_node where tree_id = @connTree and is_deleted = 0 and name = #tmp_engine.network_layer and linked_node_id is null and node_kind_id = @connNetworkFolderKind)
	where bn.id is null;

	-- usuniecie starych/niepotrzebnych	
	update bik_node
	set is_deleted = 1
	where tree_id = @connTree 
	and is_deleted = 0 
	and node_kind_id = @connEngineFolderKind
	and name not in (select db_engine from #tmp_engine)
	and linked_node_id is null

	-- podłączenie połączeń pod odpowiednie foldery
	insert into #tmp_conn(conn_node_id,parent_node_id)
	select bsuc.node_id, bn.id 
	from bik_sapbo_universe_connection bsuc
	inner join bik_node nod on nod.id = bsuc.node_id and nod.tree_id = @connTree
	inner join bik_node bn on bn.tree_id = @connTree 
	and bn.is_deleted = 0
	and bn.name = case when bsuc.database_engine is null then 'Inne' else bsuc.database_engine end
	and bn.linked_node_id is null 
	and bn.node_kind_id = @connEngineFolderKind
	inner join bik_node bn2 on bn.parent_node_id = bn2.id
	and bn2.name = bsuc.connetion_networklayer_name

	update bik_node
	set parent_node_id = #tmp_conn.parent_node_id
	from #tmp_conn
	where bik_node.id = #tmp_conn.conn_node_id

	drop table #tmp_engine;
	drop table #tmp_network;
	drop table #tmp_conn;

	exec sp_node_init_branch_id @connTree, null;

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_metadata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');

    ----------   zapytanie  -->  universe    ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnu.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.id = ex.node_id 
	where bn.tree_id = @reportTreeId and bn.is_deleted = 0 
		and bnu.tree_id = @universeTreeId and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind,@uniNewKind)

    ----------   zapytanie  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select bn.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from bik_sapbo_query sbq
    inner join bik_node bn on sbq.node_id = bn.id
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
		and bnu.id = ex.node_id and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
		and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
		and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	where bn.tree_id = @reportTreeId
		and bn.is_deleted = 0 

    ----------   universe  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnu.id as src_node_id, bnr.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

    ----------   raport  -->  universe   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnr.id as src_node_id, bnu.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId 
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

    ----------   raport  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnr.id as src_node_id, bnc.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
		and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
		and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

	----------   connection  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bnc.id as src_node_id, bnr.id as dst_node_id, 1 as type
    from aaa_query sbq
    --inner join APP_UNIVERSE uni on sbq.universe_cuid = uni.SI_CUID
    inner join bik_sapbo_extradata ex on ex.cuid = sbq.universe_cuid
    inner join bik_node bnu on bnu.tree_id = @universeTreeId
		and bnu.id = ex.node_id
		and bnu.is_deleted = 0 and bnu.node_kind_id in (@universeNodeKind, @uniNewKind)
    inner join bik_joined_objs bjo on bjo.src_node_id = bnu.id and bjo.type = 1
    inner join bik_node bnc on bjo.dst_node_id = bnc.id
		and bnc.tree_id = @connectionTreeId and bnc.is_deleted = 0 
		and bnc.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	inner join bik_node bnr on bnr.id = sbq.report_node_id
		and bnr.tree_id = @reportTreeId and bnr.is_deleted = 0

	----------   obiekt  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct ids.object_node_id, bn.parent_node_id, 1 
    from aaa_ids_for_report ids 
    inner join bik_node bn on ids.query_node_id = bn.id
		and bn.tree_id = @reportTreeId
		and bn.is_deleted = 0
    inner join bik_node bno on bno.id = ids.object_node_id
		and bno.tree_id = @universeTreeId
		and bno.is_deleted = 0
	
	----------   UniverseAliasTable  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bsut1.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_sapbo_universe_table bsut1 on bsut.original_table = bsut1.their_id
	inner join bik_node bn on bn.id = bsut.node_id
	inner join bik_node bn2 on bn2.id = bsut1.node_id
	where bsut.is_alias = 1 and bn2.parent_node_id = bn.parent_node_id
		and bn.tree_id = @universeTreeId
		and bn2.tree_id = @universeTreeId

	-- usuniecie alias table - univ table
	exec sp_delete_bik_joined_objs_by_kinds_fast 'UniverseTable', 'UniverseAliasTable'
	
    exec sp_move_bik_joined_objs_tmp 'Webi,ReportQuery', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter'
	
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_index_info]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_index_info] (@table_name_patt varchar(max))
returns @tab table (table_name sysname not null, index_name sysname not null, cols varchar(max) not null, include_cols varchar(max) null, 
  idx_level tinyint not null, has_identity_col tinyint not null, has_fk_col tinyint not null)
as
begin

  declare @fk_cols table (table_name sysname not null, col_name sysname not null, unique (table_name, col_name));
  
  insert into @fk_cols
  select distinct table_name, name from dbo.vw_ww_foreignkeys where Table_Name like @table_name_patt;

  declare @cur cursor;

  set @cur = cursor for 
  select 
       tablename = t.name,
       indexname = ind.name,
       --indexid = ind.index_id,
       --columnid = ic.index_column_id,
       columnname = col.name,
       --ind.*,
       --ic.*,
       --col.*
       col.is_identity,
       ind.is_primary_key,
       ind.is_unique,
       ind.is_unique_constraint,
       ic.is_included_column
  from 
       sys.indexes ind 
  inner join 
       sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
  inner join 
       sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id 
  inner join 
       sys.tables t on ind.object_id = t.object_id 
  where 
       --ind.is_primary_key = 1
       --and ind.is_unique = 0 
       --and ind.is_unique_constraint = 0 
       -- and 
       t.is_ms_shipped = 0 
       --and t.name = 'bik_fts_stemmed_word'
       and t.name like @table_name_patt
       --and (ind.is_unique = 1 or ind.is_unique_constraint = 1)
  order by 
       t.name, ind.name, ind.index_id, ic.index_column_id 
  ;

  open @cur;

  declare @table_name sysname, @index_name sysname, @col_name sysname, @is_identity tinyint,
    @is_primary_key tinyint,
    @is_unique tinyint,
    @is_unique_constraint tinyint, @is_included_column tinyint;
  
  declare @is_fk_col tinyint, @prev_table_name sysname = '', @prev_index_name sysname = '', @idx_col_names varchar(max) = null,
    @include_cols varchar(max) = null, @idx_level tinyint = null, @has_identity_col tinyint = null,
    @has_fk_col tinyint = null;

  fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
    @is_unique, @is_unique_constraint, @is_included_column;

  while @@fetch_status = 0
  begin
    if @prev_table_name <> @table_name or @prev_index_name <> @index_name
    begin
      if @idx_col_names is not null
      begin
        --print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
        insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
        values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
      end;
            
      select @idx_col_names = null, @include_cols = null, @has_identity_col = 0, @has_fk_col = 0;
      
      set @idx_level = case when @is_primary_key <> 0 then 3
                            when @is_unique_constraint <> 0 then 2
                            when @is_unique <> 0 then 1
                            else 0 end;
    end;
    
    if @is_included_column = 0
    begin   
      set @idx_col_names = case when @idx_col_names is null then '' else @idx_col_names + ', ' end + @col_name;

      set @is_fk_col = case when exists(select 1 from @fk_cols where table_name = @table_name and col_name = @col_name) then 1 else 0 end;
      
      if @is_fk_col <> 0 set @has_fk_col = 1;
      
      if @is_identity <> 0 set @has_identity_col = 1;    
    end
    else
      set @include_cols = case when @include_cols is null then '' else @include_cols + ', ' end + @col_name;
    
    select @prev_table_name = @table_name, @prev_index_name = @index_name;

    fetch next from @cur into @table_name, @index_name, @col_name, @is_identity, @is_primary_key,
      @is_unique, @is_unique_constraint, @is_included_column;
  end;

  --if @idx_col_names <> '' print @prev_table_name + ' <' + @prev_index_name + '> (' + @idx_col_names + ')';
  insert into @tab (table_name, index_name, cols, include_cols, idx_level, has_identity_col, has_fk_col)
  values (@prev_table_name, @prev_index_name, @idx_col_names, @include_cols, @idx_level, @has_identity_col, @has_fk_col);
  
  return;
end;
GO
/****** Object:  Table [dbo].[bik_statistic_ext]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_statistic_ext](
	[tree_id] [int] NOT NULL,
	[clicked_node_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[event_date] [date] NOT NULL,
	[event_datetime] [datetime] NOT NULL,
	[level_in_tree] [int] NOT NULL,
	[grouping_node_id] [int] NULL,
 CONSTRAINT [pk_bik_statistic_ext] PRIMARY KEY CLUSTERED 
(
	[tree_id] ASC,
	[clicked_node_id] ASC,
	[user_id] ASC,
	[event_datetime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_statistic_ext_clicked_node_id] ON [dbo].[bik_statistic_ext] 
(
	[clicked_node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_menu_path_by_tree_code]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_menu_path_by_tree_code] (@tree_code varchar(255))
returns varchar(max)
as
begin
  declare @tree_of_trees_id int = (select id from bik_tree where code = 'TreeOfTrees');

  -- select * from bik_tree

  declare @node_id int = (select top 1 id from bik_node where tree_id = @tree_of_trees_id and obj_id = '$' + @tree_code and is_deleted = 0);

  declare @node_name varchar(max) = (select name from bik_node where id = @node_id);

  declare @res varchar(max) = case when @node_name = '@' then (select name from bik_tree where code = @tree_code) else @node_name end;

  while 1 = 1 begin
    set @node_id = (select parent_node_id from bik_node where id = @node_id);
    if @node_id is null break;

    set @node_name = (select name from bik_node where id = @node_id);

    set @res = @node_name + ' » ' + @res;
  end;

  return @res;
end;
GO
/****** Object:  View [dbo].[vw_biks_statistic]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_biks_statistic] as
   select 
		dbo.fn_get_menu_path_by_tree_code(bt.code)as menu_path,
		dbo.fn_get_node_name_path(bn.id) as object_path,
		bn.name as object_name,
		bt.name as tree_name,
		bnk.caption as kind_name,
		bsu.login_name as user_name,
		event_datetime
	from bik_statistic_ext bse 
	left join bik_node bn on bse.clicked_node_id=bn.id
	left join bik_tree bt on bse.tree_id=bt.id
	left join bik_system_user bsu on bsu.id=bse.user_id
	left join bik_node_kind bnk on bnk.id=bn.node_kind_id
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL] as
		begin
		  set nocount on;
		  
		  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
		  if @regUserCrrId is null return;
		  
		  declare @tree_selector_codes_all varchar(max) = '';
		  declare @tree_selector_codes_ru varchar(max) = '';
		  
		  select 
			@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
			@tree_selector_codes_ru =  case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code  
		  from bik_tree where code not in ( 'TreeOfTrees') and is_hidden = 0;
		  
		  update bik_app_prop set val = @tree_selector_codes_all+','+ 'TreeOfTrees' where name = 'customRightRolesTreeSelector';
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_all+','+ 'TreeOfTrees' where code in('AppAdmin','Administrator','Author');
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
		  
		  /* DLA CP */
		  
		  
			declare @tree_selector_codes_autor varchar(max) = 'TreeOfTrees';

			select 
			@tree_selector_codes_autor = @tree_selector_codes_autor + ','  + code
			from bik_tree where id in(
			select distinct(tree_id) from bik_custom_right_role_user_entry where group_id=(select id from bik_system_user_group where name='polsatc'))
			update bik_custom_right_role set tree_selector = @tree_selector_codes_autor where code = 'ExpertCP';

		  /*DLA Pol */
			select @tree_selector_codes_autor='TreeOfTrees';
			select 
			@tree_selector_codes_autor = @tree_selector_codes_autor + ','+ code
			from bik_tree where id in(
			select distinct(tree_id) from bik_custom_right_role_user_entry where group_id=(select id from bik_system_user_group where name='corp'))
			update bik_custom_right_role set tree_selector = @tree_selector_codes_autor where code = 'ExpertPol';

		end;
GO
/****** Object:  StoredProcedure [dbo].[sp_node_init_missing_branch_ids_of_users]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_node_init_missing_branch_ids_of_users]
as
begin  
  set nocount on

declare @user_roles_tree_id int = (select id from bik_tree where code = 'UserRoles')
declare @user_node_kind_id int = (select id from bik_node_kind where code = 'User')
declare @node_ids bik_unique_not_null_id_table_type

insert into @node_ids (id)
select id from bik_node where branch_ids is null and is_deleted = 0
and tree_id = @user_roles_tree_id and node_kind_id = @user_node_kind_id

exec sp_node_init_branch_idEx @node_ids
end
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_sapbw]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_sapbw](@tree_code varchar(255))
as
begin
	declare @diag_level int = 0;
	declare @AreaNodeKind int;
	declare @BExNodeKind int;
	declare @CUBENodeKind int;
	declare @MPRONodeKind int;
	declare @ODSONodeKind int;
	declare @ISETNodeKind int;
	declare @ObjectsFolderNodeKind int;
	declare @UNINodeKind int;
	declare @TIMNodeKind int;
	declare @KYFNodeKind int;
	declare @DPANodeKind int;
	declare @CHANodeKind int;
	declare @tree_id int;
	
	select @AreaNodeKind = id from bik_node_kind where code = 'BWArea'
	select @BExNodeKind = id from bik_node_kind where code = 'BWBEx'
	select @CUBENodeKind = id from bik_node_kind where code = 'BWCUBE'
	select @MPRONodeKind = id from bik_node_kind where code = 'BWMPRO'
	select @ODSONodeKind = id from bik_node_kind where code = 'BWODSO'
	select @ISETNodeKind = id from bik_node_kind where code = 'BWISET'
	select @ObjectsFolderNodeKind = id from bik_node_kind where code = 'BWObjsFolder'
	select @UNINodeKind = id from bik_node_kind where code = 'BWUni'
	select @TIMNodeKind = id from bik_node_kind where code = 'BWTim'
	select @KYFNodeKind = id from bik_node_kind where code = 'BWKyf'
	select @DPANodeKind = id from bik_node_kind where code = 'BWDpa'
	select @CHANodeKind = id from bik_node_kind where code = 'BWCha'
	select @tree_id = id from bik_tree where code = @tree_code;
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'
	
	create table #tmpNodes (si_id varchar(900) collate DATABASE_DEFAULT primary key, si_parentid varchar(1000) collate DATABASE_DEFAULT, si_kind varchar(100) collate DATABASE_DEFAULT, si_name varchar(1000), descr varchar(max) collate DATABASE_DEFAULT, visual_order int default 0);
	create table #tmpNodesLinked (si_id varchar(900) collate DATABASE_DEFAULT, si_parentid varchar(1000) collate DATABASE_DEFAULT);
	
	-- wrzucanie obszarów inf.
	insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
	select obj_name, parent, @AreaNodeKind, descr, null, -1 from bik_sapbw_area 
		
	if(@tree_code = 'BWReports')
	begin
		-- wrzucanie obszaru na zapytania nieprzypisane
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący zapytania BEx, dla których nie odnaleziono dostawcy informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie zapytań BEx
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr)
		select convert(varchar(200),obj_name), coalesce((select ar.obj_name from bik_sapbw_provider pv left join bik_sapbw_area ar on pv.info_area=ar.obj_name where pv.obj_name = q.provider),'$$BIKS_TEMP_ARCHIWUM$$'), @BExNodeKind, case when rtrim(descr) <> '' then rtrim(descr) else obj_name end,'' from bik_sapbw_query q
	end;
	if(@tree_code = 'BWProviders')
	begin
		-- wrzucanie obszaru na dostawców nieprzypisanych
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		values('$$BIKS_TEMP_ARCHIWUM$$', null, @AreaNodeKind, '(Archiwum)', 'Obszar gromadzący dostawców informacji, dla których nie odnaleziono obszaru informacji. Obszar został wygenerowany automatycznie i nie znajduje się w repozytorium SAP BW.',100)

		-- wrzucanie folderów na cechy i wskaźniki
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select obj_name + '|objectsFolder|', obj_name, @ObjectsFolderNodeKind, 'Cechy i wskaźniki', 'Folder zawierający obiekty informacji', -1 from bik_sapbw_provider

		-- wrzucanie dostawców informacji
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order) -- visual_order zgodny z tym w SAP GUI
		select obj_name, coalesce(area,'$$BIKS_TEMP_ARCHIWUM$$'), case type when 'MPRO' then @MPRONodeKind
																			when 'CUBE' then @CUBENodeKind
																			when 'ODSO' then @ODSONodeKind
																			when 'ISET' then @ISETNodeKind end,  
			case when rtrim(descr) <> '' then rtrim(descr) else obj_name end, null, case type when 'CUBE' then 1
																							when 'ISET' then 2
																							when 'MPRO' then 3
																							when 'ODSO' then 4 end  
		from bik_sapbw_provider xx left join (select obj_name as area from bik_sapbw_area) yy on xx.info_area = yy.area
		
		-- wrzucanie obiektów
		insert into #tmpNodes(si_id, si_parentid, si_kind, si_name, descr, visual_order)
		select provider + '|' + obj_name, provider + '|objectsFolder|' ,case type when 'CHA' then @CHANodeKind 
																				  when 'UNI' then @UNINodeKind
																				  when 'KYF' then @KYFNodeKind
																				  when 'TIM' then @TIMNodeKind
																				  when 'DPA' then @DPANodeKind
																				  else @CHANodeKind end, 
																				  descr, null, case type when 'CHA' then 1
																										 when 'KYF' then 2
																										 when 'TIM' then 3
																										 when 'UNI' then 4
																										 when 'DPA' then 5
																										 else 6 end 
		from bik_sapbw_object 
		where provider_parent is null
		
		-- wrzucanie przepływów
		insert into #tmpNodesLinked(si_id,si_parentid)
		select obj_name, parent from bik_sapbw_flow
	end;
	
	-- usuwanie pustych folderów
	declare @rc int = 1

	while @rc > 0 
	begin
		delete from #tmpNodes
		where si_kind in (@AreaNodeKind, @ObjectsFolderNodeKind) and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	
	-- wdrażnie danych z tabeli tymczasowej to bik_node
	
	-- update istniejących
	update bik_node 
	set /*node_kind_id = #tmpNodes.si_kind,*/ name = ltrim(rtrim(#tmpNodes.si_name)), descr = #tmpNodes.descr,
		visual_order = #tmpNodes.visual_order, is_deleted = 0
	from #tmpNodes
	where bik_node.tree_id = @tree_id
	and bik_node.obj_id = #tmpNodes.si_id 
	
	-- wrzucanie nowych
	insert into bik_node(name,node_kind_id,tree_id,descr,obj_id, visual_order)
	select ltrim(rtrim(#tmpNodes.si_name)), #tmpNodes.si_kind, @tree_id, #tmpNodes.descr, #tmpNodes.si_id, #tmpNodes.visual_order
	from #tmpNodes --inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	--left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id and bik_node.linked_node_id is null
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @tree_id and bn.linked_node_id is null) on bn.obj_id = #tmpNodes.si_id
	where bn.id is null;
	
	-- wrzucanie podlinkowanych
	insert into bik_node(name,parent_node_id,node_kind_id,tree_id,obj_id, linked_node_id, disable_linked_subtree)
	select lin.name, par.id, lin.node_kind_id, lin.tree_id, tmp.si_id, lin.id, 1 from #tmpNodesLinked tmp 
	left join bik_node lin on lin.obj_id = tmp.si_id and lin.is_deleted = 0 and lin.tree_id = @tree_id and lin.linked_node_id is null
	left join bik_node par on par.obj_id = tmp.si_parentid and par.is_deleted = 0 and par.tree_id = @tree_id and par.linked_node_id is null
	left join bik_node oryg on oryg.obj_id = tmp.si_id and oryg.is_deleted = 0 and oryg.tree_id = @tree_id and oryg.linked_node_id is not null
	where oryg.id is null and par.id is not null and lin.id is not null
	
	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join #tmpNodes as pt on bik_node.tree_id = @tree_id and bik_node.obj_id = pt.si_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid 
	where bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.is_deleted = 0
	and bk.linked_node_id is null
	
	-- usuwanie starych
	update bik_node
	set is_deleted = 1
	from bik_node
	left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id
	where #tmpNodes.si_id is null
	and bik_node.tree_id = @tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @tree_id 
	and bn2.is_deleted = 1
	
	-- usuwanie starych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join #tmpNodesLinked on bik_node.obj_id = #tmpNodesLinked.si_id 
	where #tmpNodesLinked.si_id is null 
	and bik_node.tree_id = @tree_id
	and bik_node.linked_node_id is not null
	
	
	drop table #tmpNodes;
	drop table #tmpNodesLinked;
	
	exec sp_node_init_branch_id @tree_id, null;
	
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_oracle]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_oracle]
as
begin

	declare @oracle_tree_id int;
	select @oracle_tree_id = id from bik_tree where code='Oracle'

	-- update
	update bik_node 
	set is_deleted = 0, descr = orcl.descr, visual_order = orcl.visual_order
	from aaa_oracle orcl
	where bik_node.tree_id = @oracle_tree_id
	and bik_node.obj_id = orcl.branch_names
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select null, bnk.id, orcl.name, orcl.descr, @oracle_tree_id, orcl.branch_names, orcl.visual_order
	from aaa_oracle orcl
	inner join bik_node_kind bnk on orcl.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @oracle_tree_id) on bn.obj_id = orcl.branch_names
	where bn.id is null
	order by orcl.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join aaa_oracle as orcl on bik_node.obj_id = orcl.branch_names
	inner join bik_node bk on bk.obj_id = orcl.parent_branch_names 
	where bik_node.tree_id = @oracle_tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @oracle_tree_id
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join aaa_oracle orcl on bik_node.obj_id = orcl.branch_names
	where orcl.name is null
	and tree_id = @oracle_tree_id
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @oracle_tree_id 
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @oracle_tree_id, null
	
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_mssql_one_server_and_database]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_mssql_one_server_and_database](@serverName varchar(max), @database varchar(255))
as
begin

	declare @mssql_tree_id int;
	select @mssql_tree_id = id from bik_tree where code = 'MSSQL'

	-- update
	update bik_node 
	set name = mssql.name, node_kind_id = bnk.id, is_deleted = 0, descr = mssql.extra_info, visual_order = mssql.visual_order
	from bik_mssql mssql
	inner join bik_node_kind bnk on mssql.type = bnk.code
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id like (@serverName + '|' + @database + '|%')
	and bik_node.obj_id = mssql.branch_names
	and bik_node.linked_node_id is null

	declare @serverNodeId int = (select id from bik_node where tree_id = @mssql_tree_id and obj_id = @serverName + '|' and parent_node_id is null);
	if @serverNodeId is null
	begin
		insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
		values(null, dbo.fn_node_kind_id_by_code('MSSQLServer'), @serverName, null, @mssql_tree_id, @serverName + '|')
		
		set @serverNodeId = (select convert(int, scope_identity()) as id);
	end
	else
	begin
		update bik_node set is_deleted = 0 where id = @serverNodeId
	end

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select @serverNodeId, bnk.id, ms.name, ms.extra_info, @mssql_tree_id, ms.branch_names, ms.visual_order
	from bik_mssql ms
	inner join bik_node_kind bnk on ms.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @mssql_tree_id and bn.obj_id like (@serverName + '|' + @database + '|%')) on bn.obj_id = ms.branch_names
	where ms.branch_names like (@serverName + '|' + @database + '|%')
	and bn.id is null
	order by ms.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join bik_mssql as ms on bik_node.obj_id = ms.branch_names
	inner join bik_node bk on bk.obj_id = ms.parent_branch_names 
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id like (@serverName + '|' + @database + '|%')
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @mssql_tree_id
	and bk.obj_id like (@serverName + '|%')
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join bik_mssql on bik_node.obj_id = bik_mssql.branch_names and bik_mssql.branch_names like (@serverName + '|' + @database + '|%')
	where bik_mssql.name is null
	and tree_id = @mssql_tree_id
	and obj_id like (@serverName + '|' + @database + '|%')
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @mssql_tree_id 
	and bn2.obj_id like (@serverName + '|' + @database + '|%')
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @mssql_tree_id,null

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_mssql_one_server]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_mssql_one_server](@serverName varchar(max), @databases varchar(max))
as
begin

	declare @db_name varchar(255);
	declare database_curs cursor for 
	select str from dbo.fn_split_by_sep(@databases, ',', 3)
		
	open database_curs
	fetch next from database_curs into @db_name
	while @@fetch_status = 0
	begin 
		exec sp_insert_into_bik_node_mssql_one_server_and_database @serverName, @db_name;
		fetch next from database_curs into @db_name;
	end
	close database_curs;
	deallocate database_curs;
	
	declare @mssql_tree_id int = (select id from bik_tree where code = 'MSSQL');
	exec sp_node_init_branch_id @mssql_tree_id,null

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_mssql]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_mssql]
as
begin

	declare @mssql_tree_id int;
	select @mssql_tree_id = id from bik_tree where code='MSSQL'

	-- update
	update bik_node 
	set name = mssql.name, node_kind_id = bnk.id, is_deleted = 0, descr = mssql.extra_info, visual_order = mssql.visual_order
	from bik_mssql mssql
	inner join bik_node_kind bnk on mssql.type = bnk.code
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.obj_id = mssql.branch_names
	and bik_node.linked_node_id is null

	-- insert nowych
	insert into bik_node(parent_node_id, node_kind_id, name, descr, tree_id, obj_id, visual_order)
	select null, bnk.id, ms.name, ms.extra_info, @mssql_tree_id, ms.branch_names, ms.visual_order
	from bik_mssql ms
	inner join bik_node_kind bnk on ms.type = bnk.code
	left join (bik_node bn inner join bik_tree bt on bn.tree_id = bt.id and bn.tree_id = @mssql_tree_id) on bn.obj_id = ms.branch_names
	where bn.id is null
	order by ms.branch_names

	-- update parentów
	update bik_node 
	set parent_node_id = bk.id
	from bik_node 
	inner join bik_mssql as ms on bik_node.obj_id = ms.branch_names
	inner join bik_node bk on bk.obj_id = ms.parent_branch_names 
	where bik_node.tree_id = @mssql_tree_id
	and bik_node.is_deleted = 0
	and bik_node.linked_node_id is null
	and bk.tree_id = @mssql_tree_id
	and bk.is_deleted = 0
	and bk.linked_node_id is null

	-- usunięcie zbędnych
	update bik_node
	set is_deleted = 1
	from bik_node 
	left join bik_mssql on bik_node.obj_id = bik_mssql.branch_names
	where bik_mssql.name is null
	and tree_id = @mssql_tree_id
	and is_deleted = 0
	and linked_node_id is null
	
	-- update zbędnych podlinkowanych
	update bik_node
	set is_deleted = 1
	from bik_node
	inner join bik_node bn2 on bik_node.linked_node_id = bn2.id
	where bn2.tree_id = @mssql_tree_id 
	and bn2.is_deleted = 1

	exec sp_node_init_branch_id @mssql_tree_id,null

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_dqc]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_dqc]
as
begin

declare @dqc_group int;
select @dqc_group=id from bik_node_kind where code='DQCGroup'
declare @dqc_test_s int;
select @dqc_test_s=id from bik_node_kind where code='DQCTestSuccess'
declare @dqc_test_f int;
select @dqc_test_f=id from bik_node_kind where code='DQCTestFailed'
declare @dqc_test_n int;
select @dqc_test_n=id from bik_node_kind where code='DQCTestInactive'
declare @dqc_alltests int;
select @dqc_alltests=id from bik_node_kind where code='DQCAllTestsFolder'
declare @dqc_tree_id int;
select @dqc_tree_id=id from bik_tree where code='DQC'
declare @alltestsid int;
select @alltestsid = id from bik_node where is_built_in=1 and is_deleted=0 and linked_node_id is null and node_kind_id=@dqc_alltests

-- update grup
update bik_node 
set parent_node_id = null, node_kind_id = @dqc_group, name = dqc.name,
	is_deleted = 0, descr = dqc.description, tree_id = @dqc_tree_id
from bik_dqc_group dqc
where bik_node.obj_id = dqc.__obj_id 
and bik_node.linked_node_id is null
and dqc.__deleted=0

-- dodanie nowych grup
insert into bik_node (parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select null,@dqc_group, dqc.name, dqc.description, @dqc_tree_id, dqc.__obj_id
	from bik_dqc_group dqc 
	left join bik_node bn on bn.obj_id=dqc.__obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null 
	and bn.node_kind_id=@dqc_group 
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 
	and dqc.__deleted=0

-- usuniecie niepotrzebnych grup
update bik_node
set is_deleted=1
where node_kind_id=@dqc_group
and obj_id not in (select __obj_id from bik_dqc_group where __deleted=0)
and linked_node_id is null
and is_deleted=0

-- update testów
update bik_node 
set parent_node_id = @alltestsid, node_kind_id = case when dqc.test_type = 0 then @dqc_test_s when dqc.test_type = 1 then @dqc_test_f else @dqc_test_n end, name = dqc.name,
	is_deleted = 0, descr = dqc.description, tree_id = @dqc_tree_id
from bik_dqc_test dqc
where bik_node.obj_id = dqc.__obj_id 
and bik_node.linked_node_id is null
and dqc.__deleted=0

-- dodanie nowych testów
insert into bik_node (parent_node_id, node_kind_id, name, descr, tree_id, obj_id)
	select @alltestsid,case when dqc.test_type = 0 then @dqc_test_s when dqc.test_type = 1 then @dqc_test_f else @dqc_test_n end, dqc.name, dqc.description, @dqc_tree_id, dqc.__obj_id
	from bik_dqc_test dqc 
	left join bik_node bn on bn.obj_id=dqc.__obj_id 
	and bn.is_deleted=0
	and bn.linked_node_id is null 
	and bn.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 
	and dqc.__deleted=0

-- usuniecie niepotrzebnych testów
update bik_node
set is_deleted=1
where node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
and obj_id not in (select __obj_id from bik_dqc_test where __deleted=0)
and linked_node_id is null
and is_deleted=0

create table #tmp_test_group (test_node_id int,group_node_id int, test_name varchar(max), obj_id varchar(max), test_type int);

insert into #tmp_test_group(test_node_id,group_node_id,test_name,obj_id,test_type)
	select bn2.id, bn.id, bn2.name, bn2.obj_id, bn2.node_kind_id from bik_dqc_group_test dgt
	join bik_dqc_group dg on dg.id_group=dgt.id_group
	join bik_dqc_test dt on dt.id_test=dgt.id_test
	join bik_node bn on bn.obj_id=dg.__obj_id
	and bn.is_deleted = 0 and bn.linked_node_id is null
	and bn.node_kind_id = @dqc_group and bn.parent_node_id is null
	join bik_node bn2 on bn2.obj_id=dt.__obj_id
	and bn2.is_deleted = 0 and bn2.linked_node_id is null
	and bn2.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n) and bn2.parent_node_id=@alltestsid
	where dgt.__deleted = 0 and dt.__deleted = 0 and dg.__deleted = 0

-- update testów podlinkowanych
update bik_node 
set parent_node_id = dqc.group_node_id, node_kind_id = dqc.test_type, name = dqc.test_name,
	is_deleted = 0, tree_id = @dqc_tree_id, linked_node_id = dqc.test_node_id
from #tmp_test_group dqc
where bik_node.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
and bik_node.linked_node_id is not null
and bik_node.tree_id = @dqc_tree_id
and bik_node.linked_node_id=dqc.test_node_id
and bik_node.parent_node_id=dqc.group_node_id


-- dodanie nowych testów podlinkowanych
insert into bik_node (parent_node_id, node_kind_id, name, tree_id, obj_id, linked_node_id)
	select dqc.group_node_id,dqc.test_type, dqc.test_name, @dqc_tree_id, dqc.obj_id, dqc.test_node_id
	from #tmp_test_group dqc 
	left join bik_node bn on bn.obj_id=dqc.obj_id 
	and bn.parent_node_id=dqc.group_node_id
	and bn.linked_node_id = dqc.test_node_id
	and bn.is_deleted=0
	and bn.linked_node_id is not null 
	and bn.node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
	and bn.tree_id=@dqc_tree_id
	where bn.id is null 

-- usuniecie niepotrzebnych testów podlinkowanych
update bik_node
set is_deleted=1
where node_kind_id in (@dqc_test_s, @dqc_test_f, @dqc_test_n)
and obj_id not in (select obj_id from #tmp_test_group)
and linked_node_id is not null
and is_deleted=0

drop table #tmp_test_group;

exec sp_node_init_branch_id @dqc_tree_id,null

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_sapbw_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_sapbw_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
    declare @BEx_kind int;
    declare @Cube_kind int;
    declare @Iset_kind int;
    declare @Mpro_kind int;
    declare @Odso_kind int;
    declare @Reports_tree_id int;
    declare @Providers_tree_id int;
    select @BEx_kind=id from bik_node_kind where code='BWBEx'    
    select @Cube_kind=id from bik_node_kind where code='BWCUBE'
    select @Iset_kind=id from bik_node_kind where code='BWISET'
    select @Mpro_kind=id from bik_node_kind where code='BWMPRO'
    select @Odso_kind=id from bik_node_kind where code='BWODSO'
    select @Reports_tree_id = id from bik_tree where code = 'BWReports'
    select @Providers_tree_id = id from bik_tree where code = 'BWProviders'

	---------- BEx query --> Provider -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, prov.id, 1 from bik_sapbw_query q
	inner join bik_node bn on bn.obj_id = convert(varchar(200),q.obj_name)
		and bn.is_deleted = 0 
		and bn.linked_node_id is null
		and bn.tree_id = @Reports_tree_id
	inner join bik_node prov on prov.obj_id = convert(varchar(200),q.provider)
		and prov.is_deleted = 0 
		and prov.linked_node_id is null
		and prov.tree_id = @Providers_tree_id
	
	---------- Provider --> BEx query -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select prov.id, bn.id, 1 from bik_sapbw_query q
	inner join bik_node bn on bn.obj_id = convert(varchar(200),q.obj_name)
		and bn.is_deleted = 0 
		and bn.linked_node_id is null
		and bn.tree_id = @Reports_tree_id
	inner join bik_node prov on prov.obj_id = convert(varchar(200),q.provider)
		and prov.is_deleted = 0 
		and prov.linked_node_id is null
		and prov.tree_id = @Providers_tree_id
	
	---------- Provider źródłowy --> Provider -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bn2.id, 1 from bik_sapbw_flow fl
    inner join bik_node bn on bn.obj_id=fl.obj_name 
		and bn.is_deleted = 0 
		and bn.linked_node_id is null 
		and bn.tree_id = @Providers_tree_id
	inner join bik_node bn2 on bn2.obj_id=fl.parent 
		and bn2.is_deleted = 0 
		and bn2.linked_node_id is null 
		and bn2.tree_id = @Providers_tree_id
		
	---------- Provider --> Provider źródłowy -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn2.id, bn.id, 1 from bik_sapbw_flow fl
    inner join bik_node bn on bn.obj_id=fl.obj_name 
		and bn.is_deleted = 0 
		and bn.linked_node_id is null 
		and bn.tree_id = @Providers_tree_id
	inner join bik_node bn2 on bn2.obj_id=fl.parent 
		and bn2.is_deleted = 0 
		and bn2.linked_node_id is null 
		and bn2.tree_id = @Providers_tree_id
	
    exec sp_move_bik_joined_objs_tmp 'BWBEx, BWCUBE, BWISET, BWMPRO, BWODSO', 'BWCUBE, BWISET, BWMPRO, BWODSO'
	
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_get_all_statistics_ext_dict]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_get_all_statistics_ext_dict](@lang varchar(3)) as
begin
	 set nocount on
	 
    create table #tmp
    (
            counter_name varchar(255) collate DATABASE_DEFAULT,
            description varchar(255) collate DATABASE_DEFAULT,
            best_parameter varchar(255) collate DATABASE_DEFAULT,
            day_cnt int,
            week_cnt int,
            thirty_cnt int,
            all_cnt int,
            total int
    )

    declare @treeId int
    declare @counterName varchar(255)
    declare @description varchar(255)
    declare @bestParameter varchar(255)
    declare @cnt1 int, @cnt7 int, @cnt30 int, @cntAll int, @total int 

    declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.is_hidden = 0

    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
					select @bestParameter = '', @cnt1 = 0, @cnt7 = 0, @cnt30 = 0, @cntAll = 0, @total = 0;
                    with table_stat
                    as
                    (
                            select bse.grouping_node_id, bn.name, count(*) as all_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
                            sum(case when datediff(d, bse.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
                            from bik_statistic_ext bse
                            left join bik_node bn on bn.id = bse.grouping_node_id
                            where bse.tree_id = @treeId
                            group by bse.tree_id, bse.grouping_node_id, bn.name
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc
                    --select top 1 @bestParameter=name, @cnt1=day_cnt, @cnt7=week_cnt, @cnt30=thirty_cnt, @cntAll=all_cnt 
                    --,@total=(select sum( all_cnt) from table_stat) 
                    --from table_stat order by all_cnt desc			

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs


	declare tree_to_statistics_curs cursor for 
            select bt.id, bsd.counter_name, bsd.description
                    from bik_statistic_dict bsd
                    left join bik_tree bt on bsd.counter_name = bt.code
                    where 
                     bt.id is null


    open tree_to_statistics_curs
    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description

    while @@fetch_status = 0
            begin
					select @bestParameter = '', @cnt1 = 0, @cnt7 = 0, @cnt30 = 0, @cntAll = 0, @total = 0;
                    with table_stat
                    as
                    (
							select bs.parametr as name,
							count(*) as all_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime()) <= 1 then 1 else 0 end) as day_cnt, 
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 7 then 1 else 0 end) as week_cnt,
							sum(case when datediff(D, bs.event_date, sysdatetime())  <= 30 then 1 else 0 end) as thirty_cnt
							from 
							bik_statistic bs 
							left join bik_tree bt on bs.counter_name = bt.code  
							where 
							bs.counter_name = @counterName and (bt.id is null or bt.is_hidden = 0)
							group by bs.parametr 
                            
                    )
                    select top 1 @bestParameter=name, @cntAll=all_cnt, @cnt1=statGroup.day_cnt, 
                    @cnt7=statGroup.week_cnt, @cnt30=statGroup.thirty_cnt, @total=statGroup.total
                    from table_stat,
                    (select sum(day_cnt) day_cnt, sum(week_cnt) week_cnt, sum(thirty_cnt) thirty_cnt , sum( all_cnt) total from table_stat) as statGroup
                    order by table_stat.all_cnt desc

                    insert into #tmp(counter_name, description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total)
                            values(@counterName, @description, @bestParameter, @cnt1, @cnt7, @cnt30, @cntAll, @total )

                    fetch next from tree_to_statistics_curs into @treeId, @counterName, @description
            end	

    close tree_to_statistics_curs;
    deallocate tree_to_statistics_curs
    
    select counter_name,coalesce(txt, description) as description, best_parameter, day_cnt, week_cnt, thirty_cnt, all_cnt, total
	from #tmp left join bik_translation bt on (#tmp.counter_name=bt.code and lang=@lang and (kind='tree' or kind='stat'))
	
	drop table #tmp;
end
GO
/****** Object:  StoredProcedure [dbo].[sp_fix_linked_node_ids_for_metadata]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_fix_linked_node_ids_for_metadata]
as
begin
update bik_node
set linked_node_id = (select n.id from bik_node n inner join bik_tree t on n.tree_id = t.id
  inner join bik_node_kind nk on nk.id = n.node_kind_id
where t.tree_kind = 0 and n.obj_id = bik_node.obj_id
and (
t.name = 'Połączenia' and nk.code = 'MetaData.DataConnection' or
t.name = 'Raporty' and nk.code = 'Folder' or
t.name = 'Raporty' and nk.code = 'Webi' or
t.name = 'Światy obiektów' and nk.code = 'Folder' or
t.name = 'Światy obiektów' and nk.code = 'Universe' or
t.name = 'Teradata' and nk.code = 'PROCEDURE' or
t.name = 'Teradata' and nk.code = 'SCHEMA' or
t.name = 'Teradata' and nk.code = 'TABLE' or
t.name = 'Teradata' and nk.code = 'VIEW'
)
)
from bik_node inner join bik_tree t on bik_node.tree_id = t.id
  inner join bik_node_kind nk on nk.id = bik_node.node_kind_id
where
  t.tree_kind = 0
and
 not (
t.name = 'Połączenia' and nk.code = 'MetaData.DataConnection' or
t.name = 'Raporty' and nk.code = 'Folder' or
t.name = 'Raporty' and nk.code = 'Webi' or
t.name = 'Światy obiektów' and nk.code = 'Folder' or
t.name = 'Światy obiektów' and nk.code = 'Universe' or
t.name = 'Teradata' and nk.code = 'PROCEDURE' or
t.name = 'Teradata' and nk.code = 'SCHEMA' or
t.name = 'Teradata' and nk.code = 'TABLE' or
t.name = 'Teradata' and nk.code = 'VIEW'
)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_add_missing_users_in_roles_for_existing_objs]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_add_missing_users_in_roles_for_existing_objs]
as
begin  
  set nocount on
  
    declare @tree_id int = (select id from bik_tree where code='UserRoles')
    declare @rc int

    --ww: to jest kod Beaty (przeniesiony z adHocSqls.html), miał błąd - generuje [TSB:8], 
    -- bo dodaje też użytkowników w rolach gdy oryginalny użytkownik jest usunięty
	--
	/*
    insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
    select x.role_id2, (select id from bik_node_kind where code='User'), x.name, @tree_id, x.user_id
    from (select bu.node_id as user_id,brfn.node_id as role_id2,bu.name from bik_user_in_node buin
    join bik_role_for_node brfn on buin.role_for_node_id=brfn.id
    join bik_user bu on bu.id=buin.user_id
    group by bu.node_id,brfn.node_id,bu.name
    )x left join (select linked_node_id,parent_node_id as role_id,name from bik_node where is_deleted=0
    and tree_id=@tree_id and linked_node_id is not null 
    group by linked_node_id,parent_node_id ,name) y
    on (y.role_id=x.role_id2  and x.user_id=y.linked_node_id )
    where y.linked_node_id is null or x.user_id is null
	--*/
	
	--mm: trzeba dodać distinct, ponieważ możemy dodać kilka powiązań w nowej roli naraz
	-- wtedy inner join bik_user_in_node zwielokrotni nam rekordy, a tym samym dostaniemy
	-- za dużo podlinkowanych użytkowników do jednej roli (zamiast jednego)
	--ww: powyższe było zbyt zagmatwane, ciężko się połapać, napiszę to prościej, od razu
	-- ze stosowną poprawką do [TSB:8]	
	-- generalnie są 3 zagadnienia, na które trzeba uważać
	-- 1. obiekt powiązany z użytkownikiem w roli jest skasowany
	-- 2. rola jest skasowana (a powiązanie user/obj w tej roli jest)
	-- 3. użytkownik jest skasowany (oryginał)
	declare @user_node_kind_id int = (select id from bik_node_kind where code='User')

	insert into bik_node (parent_node_id, node_kind_id, name, tree_id, linked_node_id)
	select distinct roln.id, @user_node_kind_id, u.name, @tree_id, orgn.id
	from
	bik_user u inner join
	bik_node orgn on orgn.id = u.node_id inner join
	bik_user_in_node uin on u.id = uin.user_id inner join 
	bik_node objn on uin.node_id = objn.id inner join
	bik_role_for_node rfn on uin.role_for_node_id = rfn.id inner join
	bik_node roln on roln.id = rfn.node_id left join
	bik_node un on un.linked_node_id = u.node_id and un.parent_node_id = rfn.node_id
	  and un.is_deleted = 0
	where orgn.is_deleted = 0 and objn.is_deleted = 0 and roln.is_deleted = 0 and un.id is null
	
    set @rc = @@ROWCOUNT

    if @rc > 0 begin
		-- ww: poprawia branch_ids tam gdzie trzeba - dla nowo dodanych
		-- kod powyżej doda chyba tylko jednego (tak wynika z wywołań), ale
		-- skoro jest napisany w ogólności (że niby może dodać wiele użytkowników
		-- w różnych rolach) to tutaj też stosujemy ogólne podejście
		exec sp_node_init_missing_branch_ids_of_users
    end

    return @rc
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_menu_path_by_tree_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_menu_path_by_tree_id] (@tree_id int)
returns varchar(max)
as
begin
  declare @tree_code varchar(255) = (select code from bik_tree where id = @tree_id);
  return [dbo].[fn_get_menu_path_by_tree_code](@tree_code);
end;
GO
/****** Object:  Table [dbo].[bik_confluence_missing_joined_node]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_confluence_missing_joined_node](
	[conf_node_id] [int] NOT NULL,
	[dst_tree_code] [varchar](255) NULL,
	[dst_tree_id] [int] NULL,
	[opt_dst_subnode_name] [varchar](max) NULL,
	[opt_dst_subnode_parent_id] [int] NULL,
	[opt_dst_branch_ids_like] [varchar](max) NULL,
	[dst_node_id] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[api_tree_objects_list]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_tree_objects_list](@tree_code varchar(255))
as 
begin 
	select bn.name, bn.id, bn.parent_node_id as parent_id, dbo.fn_get_branch_path(bn.id) as path, bn.descr 
	from bik_node bn join bik_tree bt on bn.tree_id = bt.id 
	where bt.code = @tree_code and bn.is_deleted = 0
end
GO
/****** Object:  StoredProcedure [dbo].[api_add_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_add_node](@tree_code varchar(255), @parent_id int, @name varchar(4000), @descr varchar(max), @node_kind_code varchar(255))
as
begin
	declare @node_kind_id int = (select id from bik_node_kind where code = @node_kind_code)
	declare @tree_id int = (select id from bik_tree where code = @tree_code)
	
	if @parent_id is null or exists (select 1 from bik_node where id = @parent_id and tree_id = @tree_id and is_deleted = 0)
	begin 
		begin tran
		insert into bik_node(parent_node_id, node_kind_id, name, tree_id, descr) 
		values(@parent_id, @node_kind_id, @name, @tree_id, @descr)
	
		declare @node_id int = scope_identity()
		declare @nodeFilter varchar(max) = 'n.id in (' + cast(@node_id as varchar(max)) + ')'
		exec sp_node_init_branch_id @tree_id, @node_id
		exec sp_verticalize_node_attrs_inner @nodeFilter
		commit
	end
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_delete_items]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_delete_items](@items1 varchar(max), @items2 varchar(max), @sep varchar(max), @trimOpt int)
returns varchar(max)
as
begin
  declare @tab table (str varchar(max) not null);

  insert into @tab (str)
  select
    str 
  from
    dbo.fn_split_by_sep(@items1, @sep, @trimOpt)
  except
  select
    str 
  from
    dbo.fn_split_by_sep(@items2, @sep, @trimOpt)
  ;

  declare @res varchar(max) = '';

  select @res = @res + @sep + str from @tab;

  return case when @res = '' then '' else stuff(@res, 1, datalength(@sep), '') end;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_update_fvs_for_joined_objs_change_single]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_update_fvs_for_joined_objs_change_single](@node_id int, @added_joined_obj_ids varchar(max),
  @deleted_joined_obj_ids varchar(max))
as
begin
  set nocount on;

    declare @branch_ids varchar(1000) = (select branch_ids from bik_node where id = @node_id);

    merge into bik_object_in_fvs_change_ex as ex
    using (
      select f.id, @node_id
      from bik_object_in_fvs f inner join bik_node n on f.node_id = n.id
      where @branch_ids like n.branch_ids + '%'
    ) as c (fvs_id, node_id) on ex.fvs_id = c.fvs_id and ex.node_id = c.node_id
    when not matched by target then
      insert (node_id, fvs_id, node_status, joined_obj_ids_added, joined_obj_ids_deleted)
      values (c.node_id, c.fvs_id, 0, @added_joined_obj_ids, @deleted_joined_obj_ids)
    when matched then
      update set 
        ex.joined_obj_ids_added = dbo.fn_add_items(dbo.fn_delete_items(ex.joined_obj_ids_added, @deleted_joined_obj_ids, '|', 7), 
          dbo.fn_delete_items(@added_joined_obj_ids, ex.joined_obj_ids_deleted, '|', 7), '|', 7),
        ex.joined_obj_ids_deleted = dbo.fn_add_items(dbo.fn_delete_items(ex.joined_obj_ids_deleted, @added_joined_obj_ids, '|', 7), 
          dbo.fn_delete_items(@deleted_joined_obj_ids, ex.joined_obj_ids_added, '|', 7), '|', 7)
    ;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_update_fvs_for_joined_objs_change]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_update_fvs_for_joined_objs_change](@node_id int, @old_joined_obj_ids varchar(max))
as
begin
  set nocount on;

  declare @curr_joined_obj_ids varchar(max) = dbo.fn_joined_obj_ids_for_node(@node_id);

  declare @added_joined_obj_ids varchar(max) = dbo.fn_delete_items(@curr_joined_obj_ids, @old_joined_obj_ids, '|', 7);
  declare @deleted_joined_obj_ids varchar(max) = dbo.fn_delete_items(@old_joined_obj_ids, @curr_joined_obj_ids, '|', 7);
  
  exec sp_update_fvs_for_joined_objs_change_single @node_id, @added_joined_obj_ids, @deleted_joined_obj_ids;

  declare @node_id_str varchar(max) = cast(@node_id as varchar(20));

  declare @cur cursor, @dst_node_id int, @is_added int;

  set @cur = cursor for select cast(str as int), 1 as is_added from dbo.fn_split_by_sep(@added_joined_obj_ids, '|', 7) union all
    select cast(str as int), 0 as is_added from dbo.fn_split_by_sep(@deleted_joined_obj_ids, '|', 7);

  open @cur;

  fetch next from @cur into @dst_node_id, @is_added;

  while @@fetch_status = 0 begin
    if @is_added > 0 begin
      exec sp_update_fvs_for_joined_objs_change_single @dst_node_id, @node_id_str, '';    
    end else begin
      exec sp_update_fvs_for_joined_objs_change_single @dst_node_id, '', @node_id_str;    
    end;

    fetch next from @cur into @dst_node_id, @is_added;
  end;

  close @cur;
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_confluence_extract_short_link]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_confluence_extract_short_link](@html varchar(max)) returns varchar(max) as
begin
  -- <link rel="shortlink" href="http://cyfropedia01-t2/x/NABJAw">

  declare @len int = datalength(@html);
  
  declare @pos int = 1;
  
  while @pos <= @len
  begin
  
    set @pos = charindex('shortlink', @html, @pos);
  
	if @pos = 0 return null;

    declare @old_pos int = @pos;
    
    set @pos = @pos + 9; -- shortlink 9 znaków
    
	declare @left_angular_pos int = dbo.fn_last_charindex('<', @html, @old_pos);
  
    if @left_angular_pos = 0
      --return 'no left <'; --
      continue;
  
    declare @part varchar(100) = substring(@html, @left_angular_pos + 1, 4); -- link 4 znaki  
  
    if @part <> 'link'
      --return 'no link'; --
      continue;
      
    declare @end_pos int = charindex('>', @html, @pos);
    
    if @end_pos = 0
      return --'no end pos'; --
        null;
      
    set @part = substring(@html, @left_angular_pos + 1, @end_pos - @left_angular_pos - 1);
    
    declare @href_pos int = charindex('href', @part);
    
    if @href_pos = 0
      --return 'no href pos'; --
       continue;
    
    set @part = substring(@part, @href_pos + 4, datalength(@part)); -- href 4 znaki
      
    declare @left_apos_pos int = patindex('%[''"]%', @part);
    
    if @left_apos_pos = 0
      --return 'no left apos';-- 
      continue;
      
    declare @right_apos_pos int = charindex(substring(@part, @left_apos_pos, 1), @part, @left_apos_pos + 1);
    
    if @right_apos_pos = 0
      --return 'no right apos: ' + @part + ', left pos=' + cast(@left_apos_pos as varchar(10)) --
      continue;
    
    return ltrim(rtrim(substring(@part, @left_apos_pos + 1, @right_apos_pos - @left_apos_pos - 1)));
  end;
  
  return null;
end
GO
/****** Object:  Table [dbo].[dbau_log]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dbau_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_added] [datetime] NOT NULL,
	[proc_id] [int] NOT NULL,
	[msg] [varchar](max) NOT NULL,
	[spid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_schedule]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_schedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [int] NOT NULL,
	[hour] [int] NOT NULL,
	[minute] [int] NOT NULL,
	[interval] [int] NOT NULL,
	[date_started] [date] NULL,
	[is_schedule] [bit] NOT NULL,
	[priority] [int] NOT NULL,
	[instance_name] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_tree_kind]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_tree_kind](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](255) NOT NULL,
	[caption] [varchar](max) NOT NULL,
	[branch_node_kind_id] [int] NOT NULL,
	[leaf_node_kind_id] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
	[allow_linking] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node](@tree_code varchar(255))

as
	declare @table_folders varchar(255);
	declare @tree_id int;
	declare @kinds varchar(255);
begin
	set nocount on;
	
	declare @diag_level int = 0;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': poczatek'

	if(@tree_code = 'Connections')
	begin
		set @kinds = '''MetaData.DataConnection'', ''CCIS.DataConnection'', ''CommonConnection''';		
	end;

	if(@tree_code = 'ObjectUniverses')
	begin
		set @kinds = '''Universe'', ''DSL.MetaDataFile''';
	end;		
	
	if(@tree_code='Reports')
	begin
		-- wybranie folderw raportów bez raportów użytkowników
		set @table_folders = ' INFO_FOLDER where si_name not like ''~Webintelligence%''';
		--set @table_folders = ' INFO_FOLDER where SI_PATH__SI_FOLDER_NAME2 is null or SI_PATH__SI_FOLDER_NAME2 != ''User Folders''';
		set @kinds = '''Webi'', ''Flash'', ''CrystalReport'',''Excel'',''FullClient'',''Pdf'',''Hyperlink''
		,''Rtf'',''Txt'',''Powerpoint'',''Word''';
	end;
	
    declare @sql nvarchar(max);
				
	declare @node_kind_id int;
	set @sql = N'';
	if(@tree_code = 'Teradata')
	begin
		select @tree_id = id from bik_tree where code = @tree_code;
		set @sql = N'select branch_names as SI_ID, parent_branch_names	as SI_PARENTID, 
						type as SI_KIND, name as SI_NAME, extra_info as DESCR
				 from bik_teradata';
	end;
	if(@tree_code = 'TeradataDataModel')
	begin
		select @tree_id = id from bik_tree where code = @tree_code;
		set @sql = N'select SI_ID, SI_PARENTID, SI_KIND, SI_NAME, DESCR
					from(
						select 
						branch_names as SI_ID,
						row_number () over ( partition by branch_names order by branch_names  ) as kolejnosc
							, parent_branch_names as SI_PARENTID, type as SI_KIND, name as SI_NAME, extra_info as DESCR
						from bik_teradata_data_model
					) as tab									 
					where 
					 kolejnosc =1';
	end;		
	if(@tree_code = 'Reports')
	begin
		set @tree_id = dbo.fn_get_bo_actual_report_tree_id();
		
		declare @rootFolderName varchar(max) = null, @userFolderName varchar(max) = null;
		select @rootFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$RootFolder' and is_deleted = 0
		select @userFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UserFolder' and is_deleted = 0
		
		-- sztuczny podzial na root folder i user folders dla raportów
		set @sql = N'select distinct convert(varchar(30),SI_PARENT_FOLDER) as SI_ID, ''#BO$UserFolder'' as SI_PARENTID, SI_KIND, SI_PATH__SI_FOLDER_NAME1 as SI_NAME, NULL as DESCR
				 from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') 
				 and SI_NAME not like ''~Webintelligence%'' 
				
				 union all
				
				 select case when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4'' then ''#BO$RootFolder'' when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'' then ''#BO$UserFolder'' else convert(varchar(30),si_id) end as si_id, case
									when SI_PARENTID = 0 and SI_CUID <> ''AWigQI18AAZJoXfRHLzWJ2c''
										then ''#BO$RootFolder''
									when si_id in (select si_id from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''))
										then convert(varchar(30),SI_PARENTID)
									when SI_PARENTID in(select si_id from INFO_FOLDER)
										then convert(varchar(30),SI_PARENTID)
									else ''0'' end as SI_PARENTID, SI_KIND, case
																			when SI_CUID = ''ASHnC0S_Pw5LhKFbZ.iA_j4''
																				then coalesce(' + coalesce(QUOTENAME(substring(@rootFolderName,1,128),''''),'null') + ',SI_NAME)
																			when SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c''
																				then coalesce(' + coalesce(QUOTENAME(substring(@userFolderName,1,128), ''''),'null') + ',SI_NAME)
																			else SI_NAME end, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from ' + @table_folders + '
				
				 union all
				
				 select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
				 from aaa_global_props
				 where SI_PARENTID in (select si_id from ' + @table_folders + ') 
						and SI_KIND in ('+ @kinds +') and SI_INSTANCE = 0
				
				union all
				
				select convert(varchar(30),SI_ID), convert(varchar(30),SI_PARENTID), SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR
				from aaa_global_props where si_parentid in (select si_parentid from INFO_FOLDER where SI_PATH__SI_FOLDER_ID2 in (select si_id from INFO_FOLDER where SI_CUID = ''AWigQI18AAZJoXfRHLzWJ2c'') and SI_NAME <> ''~Webintelligence'') and si_kind <> ''Folder''
				
				union all
				
				select convert(varchar(30),id), convert(varchar(30),parent_id), ''ReportSchedule'', name, null
				from aaa_report_schedule sch
				inner join aaa_global_props gp on gp.si_id = sch.parent_id';
	end;
	if(@tree_code = 'Connections')
	begin
		set @tree_id = dbo.fn_get_bo_actual_connection_tree_id();
		
		declare @conenctionFolderName varchar(max) = null;
		select @conenctionFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$ConnectionFolder' and is_deleted = 0
		
		set @sql = N'select ''#BO$ConnectionFolder'' as si_id, ''0'' as si_parentid, si_kind, coalesce(' + coalesce(QUOTENAME(substring(@conenctionFolderName,1,128), ''''),'null') + ',SI_NAME) as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR from app_folder where SI_CUID = ''AZVXOgKIBEdOkiXzUuybja4''
		
			 union all
			 
			 select ''#BO$ConnectionOLAPFolder'', ''#BO$ConnectionFolder'', ''Folder'', ''OLAP'', null
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_KIND = ''CommonConnection'' then ''#BO$ConnectionOLAPFolder'' else ''#BO$ConnectionFolder'' end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_KIND in ('+ @kinds +') and SI_OBJECT_IS_CONTAINER = 0';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @tree_id = dbo.fn_get_bo_actual_universe_tree_id();
		
		declare @universeFolderName varchar(max) = null;
		select @universeFolderName = name from bik_node where tree_id = @tree_id and obj_id = '#BO$UniverseFolder' and is_deleted = 0
		
		set @sql = N'select case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then ''#BO$UniverseFolder'' else convert(varchar(30),si_id) end as si_id, case 
								when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk''
									then ''0''
								when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') 
									then ''#BO$UniverseFolder''
								when SI_PARENTID in(select si_id from APP_FOLDER) 
									then convert(varchar(30),SI_PARENTID)
								else ''0'' end as SI_PARENTID, SI_KIND, case when SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'' then coalesce(' + coalesce(QUOTENAME(substring(@universeFolderName,1,128), ''''),'null') + ',SI_NAME) else SI_NAME end as SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from APP_FOLDER
			 
			 union all
			 
			 select convert(varchar(30),SI_ID), case when SI_PARENTID in (select si_id from app_folder where SI_CUID=''AWcPjwbDdBxPoXPBOUCsKkk'') then ''#BO$UniverseFolder'' else convert(varchar(30),SI_PARENTID) end, SI_KIND, SI_NAME, nullif(ltrim(rtrim(SI_DESCRIPTION)), '''') as DESCR 
			 from aaa_global_props
			 where SI_PARENTID in (select si_id from APP_FOLDER) 
					and SI_KIND in ('+ @kinds +')';
	end;					

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed zapytaniem'
	print 'zapytanie: ' + @sql;

	create table #tmpNodes (si_id varchar(900) collate DATABASE_DEFAULT primary key, si_parentid varchar(900) collate DATABASE_DEFAULT, si_kind varchar(max) collate DATABASE_DEFAULT, si_name varchar(max) collate DATABASE_DEFAULT, descr varchar(max) collate DATABASE_DEFAULT);
	if(@tree_code = 'Reports')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ReportFolder'' 
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Connections')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''ConnectionFolder''
										when si_kind=''MetaData.DataConnection'' then ''DataConnection''
										else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'ObjectUniverses')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, case 
										when si_kind=''Folder'' then ''UniversesFolder'' else si_kind end, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	if(@tree_code = 'Teradata' or @tree_code = 'TeradataDataModel')
	begin
		set @sql = 'insert into #tmpNodes (si_id, si_parentid, si_kind, si_name, descr) 
					select si_id, si_parentid, si_kind, si_name, DESCR
					from (' + @sql + ') xxx';
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po stworzeniu drugiego @sql'
			   
	EXECUTE(@sql);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po wrzuceniu do #tmpNodes'

	insert into bik_node_kind (code,caption) 
	select distinct si_kind, si_kind
	from #tmpNodes 
	where not exists (select 1 from bik_node_kind where code = si_kind);
	
	-- fix na foldery główne
	if(@tree_code = 'Reports' or @tree_code = 'ObjectUniverses' or @tree_code = 'Connections')
	begin
		delete from #tmpNodes
		where si_parentid = '0' and si_id not in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder')
		
		declare @rc1 int = 1
		while @rc1 > 0 begin
			delete from #tmpNodes
			where si_parentid <> '0' and not exists(select 1 from #tmpNodes g where g.si_id = #tmpNodes.si_parentid)
			
			set @rc1 = @@ROWCOUNT
		end;
	end;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po fixie rootów dla #tmpNodes'
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/

	declare @rc int = 1

	while @rc > 0 begin
	
	delete from #tmpNodes
	where (si_kind = 'UniversesFolder' or si_kind = 'ConnectionFolder' or si_kind = 'ReportFolder') and not exists(select 1 from #tmpNodes g where g.si_parentid = #tmpNodes.si_id)
		set @rc = @@ROWCOUNT
	end;--end loop
	
	/*****************************************************************************
	*****************************************************************************
	*****************************************************************************/
	
	declare @report_tree_id int = dbo.fn_get_bo_actual_report_tree_id();

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updatetami'

	update bik_node 
	set parent_node_id = null, node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.descr, tree_id = @tree_id
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.linked_node_id is null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed update podlinkowanych'
	--update podlinkowanych
	update bik_node 
	set /*parent_node_id = null, */node_kind_id = bik_node_kind.id, name = ltrim(rtrim(#tmpNodes.si_name)),
		 descr = #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
	where bik_node.obj_id = #tmpNodes.si_id and (tree_id = @report_tree_id or tree_id in (select id from bik_tree where tree_kind in (select code from bik_tree_kind where allow_linking = 1))) and bik_node.linked_node_id is not null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed insertami'
	
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select bik_node_kind.id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.descr
	from #tmpNodes inner join bik_node_kind on #tmpNodes.si_kind = bik_node_kind.code
		left join bik_node on bik_node.obj_id = #tmpNodes.si_id and bik_node.tree_id=@tree_id
	where bik_node.id is null;

	set nocount on;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed updateami parentow'
	

	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.obj_id = pt.si_id and bik_node.tree_id = @tree_id
	inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid where bik_node.linked_node_id is null

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': przed usuwaniem'
	
	update --delete from 
	bik_node
	set is_deleted = 1
	--select *
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id join bik_node_kind on bik_node.node_kind_id = bik_node_kind.id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and not bik_node_kind.code in 
	('Measure', 'Dimension', 'Detail', 'UniverseClass', 'ReportQuery', 'Filter', 'UniverseColumn', 'UniverseTable', 'UniverseAliasTable', 'UniverseDerivedTable', 'UniverseTablesFolder', 'ConnectionEngineFolder', 'ConnectionNetworkFolder')
				
	drop table #tmpNodes
	
	-- fix na is_built_in
	update bik_node
	set is_built_in = 1
	where tree_id = @tree_id
	and obj_id in ('#BO$RootFolder', '#BO$UserFolder', '#BO$UniverseFolder', '#BO$ConnectionFolder', '#BO$ConnectionOLAPFolder')
	and is_built_in = 0
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': po usuwaniu i po dropie'

	exec sp_delete_linked_nodes_where_orignal_is_deleted;
	exec sp_node_init_branch_id @tree_id, null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': koniec'
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_set_app_prop]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_set_app_prop](@name varchar(255), @val varchar(255))
as
begin
  if @val is null
    delete from bik_app_prop where name = @name
  else
  begin
    update bik_app_prop set val = @val where name = @name
    if @@rowcount = 0
      insert into bik_app_prop (name, val, is_editable) values (@name, @val, 1);
  end;
end
GO
/****** Object:  StoredProcedure [dbo].[sp_drop_tables]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_drop_tables] (@tablesNames varchar(max)) as
begin
  set nocount on;

	------------------------------------------------------------
	-- Tabele do usunięcia podajemy po przecinku
	-- 
	-- Proces usunięcia tabel realizowany jest w nastepujących krokach:
	-- 1. sprawdzanie czy jakieś FK nie odwołuje się z poza listy tabel - wtedy rzucamy błąd
	-- 2. usunięcie FK, do i z tabel, które usuwamy
	-- 3. usunięcie tabel
	-------------------------------------------------------------

	--declare @tablesNames varchar(max) = 'a00_global_props,a00_number,a00_teradata_test,aa0_node,aa0_node_connection,aa0_node_kind,aaa_additional_descr,aaa_attribute,aaa_attribute_linked,aaa_aux_teradata_object,aaa_aux_tree_node'
	declare @tablesToDrop table(name varchar(512));
	declare @dropSql varchar(max) = '';

	insert into @tablesToDrop(name)
	select str from dbo.fn_split_by_sep(@tablesNames, ',', 7) where exists(select * from sys.objects where object_id = object_id(str) and type in (N'U'));

	if exists(select * from vw_ww_foreignkeys where ReferencedTableName in (select Name from @tablesToDrop) and Table_Name not in (select Name from @tablesToDrop))
	begin
		print 'Błąd! Próbujesz usunąć tabelę, do której odwołują się inne tabele (poprzez klucze obce)!'
		return;
	end;

	set @dropSql = '';
	select @dropSql = @dropSql + 'alter table ' + Table_Name + ' drop constraint ' + ForeignKey_Name + '; ' from vw_ww_foreignkeys where ReferencedTableName in (select Name from @tablesToDrop) or Table_Name in (select Name from @tablesToDrop)
	--select @dropSql
	exec (@dropSql);

	set @dropSql = '';
	select @dropSql = @dropSql + 'drop table ' + name + '; ' from @tablesToDrop
	--select @dropSql
	exec (@dropSql);

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_check_version]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_check_version] (@ver varchar(255)) as
begin	
	declare @version varchar(255)
	select @version = val from bik_app_prop where name = 'bik_ver';
	if(@version != @ver)
	begin
	    declare @txt nvarchar(4000) = N'!!! Wrong version number: ' + @ver + 
	      '. Current version is ' + @version + '. !!!'
	    
	    while 1 = 1 begin
	    print @txt
	    
/*
		raiserror (@txt,
				20,
				-1) with LOG;

		raiserror(@txt,
				15,
				-1) with nowait;
*/
				
		print 'you must manually stop execution of this script!!!'
		print 'proszę przerwać dalsze wykonanie tego skryptu ręcznie!!!'
		
		select @txt
		select 'you must manually stop execution of this script!!!'
		select 'proszę przerwać dalsze wykonanie tego skryptu ręcznie!!!'
		
		WAITFOR DELAY '00:00:10';
	    end
	end	
end
GO
/****** Object:  StoredProcedure [dbo].[sp_fix_system_users_and_groups_from_ad]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_fix_system_users_and_groups_from_ad](@domain varchar(512)) as
begin
	set nocount on

	-- bik_system_user_group
	merge into bik_system_user_group as sug
	using (select grr.s_a_m_account_name, grr.domain, grr.distinguished_name from bik_active_directory_group as grr where grr.domain = @domain and grr.is_deleted = 0
			union --all 
			select ouu.name, ouu.domain, ouu.distinguished_name from bik_active_directory_ou as ouu where ouu.domain = @domain and ouu.is_deleted = 0) as adg 
		on sug.distinguished_name = adg.distinguished_name
	when matched then
	update set is_deleted = 0
	when not matched by target and adg.domain = @domain then
	insert (name, domain, distinguished_name) values (adg.s_a_m_account_name, adg.domain, adg.distinguished_name)
	when not matched by source and sug.domain = @domain then
	update set is_deleted = 1;

	-- bik_system_user_in_group
	update bik_system_user_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_user_in_group as uig
	using (select usr.id as usr_id, sug.id as gr_id from bik_active_directory as ad 
		cross apply dbo.fn_split_by_sep(ad.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as sug on sp.str = sug.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain and ad.is_deleted = 0
		union --all
		select usr.id, gr.id from bik_active_directory as ad 
		inner join bik_system_user_group as gr on ad.parent_distinguished_name = gr.distinguished_name
		inner join bik_system_user as usr on usr.sso_login = ad.s_a_m_account_name and usr.sso_domain = @domain
		where ad.domain = @domain and ad.is_deleted = 0) as x on uig.user_id = x.usr_id and uig.group_id = x.gr_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (user_id, group_id) values (x.usr_id, x.gr_id);

	-- bik_system_group_in_group
	update bik_system_group_in_group set is_deleted = 1 where group_id in (select id from bik_system_user_group where domain = @domain) or member_group_id in (select id from bik_system_user_group where domain = @domain)

	merge into bik_system_group_in_group as gig
	using(select par.id as par_id, gr.id as child_id
		from bik_active_directory_ou as ou
		inner join bik_system_user_group as gr on gr.distinguished_name = ou.distinguished_name
		inner join bik_system_user_group as par on par.distinguished_name = ou.parent_distinguished_name
		where ou.domain = @domain and ou.is_deleted = 0
		union --all 
		select ugr.id, cgr.id from bik_active_directory_group as gr
		cross apply dbo.fn_split_by_sep(gr.memberof, '%|%', 7) as sp 
		inner join bik_system_user_group as ugr on sp.str = ugr.distinguished_name
		inner join bik_system_user_group as cgr on gr.distinguished_name = cgr.distinguished_name
		where gr.domain = @domain and gr.is_deleted = 0
		union --all 
		select pargr.id, chilgr.id from bik_active_directory_group as grp
		inner join bik_system_user_group as pargr on grp.parent_distinguished_name = pargr.distinguished_name
		inner join bik_system_user_group as chilgr on grp.distinguished_name = chilgr.distinguished_name
		where grp.domain = @domain and grp.is_deleted = 0) as x on x.par_id = gig.group_id and x.child_id = gig.member_group_id
	when matched then
	update set is_deleted = 0
	when not matched by target then
	insert (group_id, member_group_id) values (x.par_id, x.child_id);

end;
GO
/****** Object:  Table [dbo].[bik_lisa_dict_column]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_dict_column](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[column_id] [int] NOT NULL,
	[sequence] [int] NOT NULL,
	[name_in_db] [varchar](100) NOT NULL,
	[description] [varchar](255) NULL,
	[title_to_display] [varchar](100) NOT NULL,
	[data_type] [varchar](100) NULL,
	[is_main_key] [int] NULL,
	[dictionary_name] [varchar](100) NULL,
	[access_level] [int] NULL,
	[column_length] [int] NULL,
	[nullable] [varchar](10) NULL,
	[decimal_total_digits] [int] NULL,
	[decimal_fractional_digits] [int] NULL,
	[dict_validation] [varchar](100) NULL,
	[col_validation] [varchar](100) NULL,
	[text_displayed] [varchar](100) NULL,
	[validate_level] [int] NULL,
	[instance_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_data_load_log]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_data_load_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_source_type_id] [int] NOT NULL,
	[status] [int] NOT NULL,
	[status_description] [varchar](max) NULL,
	[start_time] [datetime] NOT NULL,
	[finish_time] [datetime] NULL,
	[server_name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[api_node_kind_list]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_node_kind_list]
as
begin
	select code, caption as name from bik_node_kind 
end
GO
/****** Object:  Table [dbo].[bik_fts_word_stemming]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_fts_word_stemming](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[word_id] [int] NOT NULL,
	[stemmed_word_id] [int] NOT NULL,
	[origin_flag] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_admin_backup_file]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_admin_backup_file](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[backup_log_id] [int] NULL,
	[start_time] [datetime] NULL,
	[end_time] [datetime] NULL,
	[status] [varchar](max) NULL,
	[file_name] [varchar](512) NULL,
	[obj_id] [int] NULL,
	[obj_cuid] [varchar](128) NULL,
	[obj_name] [varchar](max) NULL,
	[obj_last_modification] [datetime] NULL,
	[obj_kind] [varchar](256) NULL,
	[is_success] [int] NULL,
	[file_size] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_crr_label]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_crr_label](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[role_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[is_default_for_new_node] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[role_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_crr_labels_for_node]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_crr_labels_for_node] (@node_id int)
returns varchar(max)
as
begin
  declare @tree_id int, @branch_ids varchar(1000);

  select @tree_id = tree_id, @branch_ids = branch_ids from bik_node where id = @node_id;

  declare @res varchar(max) = '', @sep varchar(max) = ', ';

  select @res = @res + @sep + name + case when is_inherited = 0 then '' else '*' end
  from (
    select l.name as name, max(case when @branch_ids = n.branch_ids then 0 else 1 end) as is_inherited
    from bik_custom_right_role_user_entry crrue 
      inner join bik_crr_label l on crrue.role_id = l.role_id and crrue.group_id = l.group_id
      left join bik_node n on n.id = crrue.node_id
    where crrue.tree_id = @tree_id and crrue.user_id is null and (n.id is null or @branch_ids like n.branch_ids + '%')
    group by l.name
  ) x
  order by name;

  return substring(@res, datalength(@sep) + 1, len(@res));
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_set_crr_labels_on_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_set_crr_labels_on_node](@labels varchar(max), @node_id int, @diag_level int = 0) as
begin
	set nocount on;

  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: start'; exec sp_log_msg @log_msg;
  end;

  declare @current_labels table (name varchar(100) not null primary key, is_inherited tinyint not null);

  insert into @current_labels (name, is_inherited)  
  select name, max(is_inherited)
  from
  (
  select case when str like '%*' then substring(str, 1, len(str) - 1) else str end as name, case when str like '%*' then 1 else 0 end as is_inherited
  from --dbo.fn_split_by_sep('PLK*, CP, CP, PLK', ',', 7) -- 
    dbo.fn_split_by_sep(dbo.fn_get_crr_labels_for_node(@node_id), ',', 7)
  ) x
  group by name;
  
  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: @current_labels filled'; exec sp_log_msg @log_msg;
  end;

  --select * from @current_labels;

  --select * from dbo.fn_split_by_sep('PLK*, CP', ',', 7)
  
  declare @expected_labels table (name varchar(100) not null primary key);

  insert into @expected_labels (name)  
  select distinct case when str like '%*' then substring(str, 1, len(str) - 1) else str end as name
  from --dbo.fn_split_by_sep('PLK*, CP, CP, PLK', ',', 7) -- 
    dbo.fn_split_by_sep(@labels, ',', 7);
  
  --select * from @expected_labels;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: @expected_labels filled'; exec sp_log_msg @log_msg;
  end;

  declare @label_mods table (name varchar(100) not null primary key, will_add tinyint not null);
  declare @mods_cnt int;

  insert into @label_mods (name, will_add)
  select coalesce(cl.name, el.name), case when cl.name is null then 1 else 0 end from
    @current_labels cl full outer join @expected_labels el on cl.name = el.name
  where
    (cl.name is not null and cl.is_inherited = 0 and el.name is null) -- aktualna jest niedziedziczona i ma jej nie być
    or cl.name is null -- aktualnie nie ma, a ma być
  ;

  set @mods_cnt = @@rowcount;

  --select * from @label_mods;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: @label_mods filled'; exec sp_log_msg @log_msg;
  end;

  if @mods_cnt = 0 return;

  delete crrue
  from bik_custom_right_role_user_entry crrue inner join bik_crr_label crrl on crrue.group_id = crrl.group_id and crrue.role_id = crrl.role_id
    inner join @label_mods lm on crrl.name = lm.name
  where crrue.node_id = @node_id and lm.will_add = 0;

  declare @tree_id int = (select tree_id from bik_node where id = @node_id);

  insert into bik_custom_right_role_user_entry (user_id, role_id, tree_id, node_id, group_id)
  select null, crrl.role_id, @tree_id, @node_id, crrl.group_id
  from  
    bik_crr_label crrl inner join @label_mods lm on crrl.name = lm.name
  where lm.will_add <> 0;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: bik_custom_right_role_user_entry updated'; exec sp_log_msg @log_msg;
  end;

  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_set_crr_labels_on_node: after sp_recalculate_Custom_Right_Role_Action_Branch_Grants, all done'; exec sp_log_msg @log_msg;
  end;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_apply_default_crr_labels_on_node_modified_by_user]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_apply_default_crr_labels_on_node_modified_by_user](@node_id int, @user_id int, @is_new_node tinyint) as
begin
	set nocount on;

  if @is_new_node = 0 return;

  if not exists (select 1 from bik_crr_label where is_default_for_new_node <> 0) return;

  create table #crr_labs_member_of_groups (group_id int not null primary key);
  create table #crr_labs_parent_groups (group_id int not null primary key);

  declare @tmp_groups table (group_id int not null primary key);

  declare @new_grp_cnt int;

  insert into #crr_labs_parent_groups (group_id)
  select group_id from bik_system_user_in_group where user_id = @user_id and is_deleted = 0;

  set @new_grp_cnt = @@rowcount;

  while @new_grp_cnt > 0 begin
    insert into #crr_labs_member_of_groups (group_id) select group_id from #crr_labs_parent_groups;

    delete from @tmp_groups;

    insert into @tmp_groups (group_id)
    select distinct gig.group_id from bik_system_group_in_group gig inner join #crr_labs_parent_groups pg on gig.member_group_id = pg.group_id
      left join #crr_labs_member_of_groups mog on mog.group_id = gig.group_id
    where gig.is_deleted = 0 and mog.group_id is null;

    truncate table #crr_labs_parent_groups;

    insert into #crr_labs_parent_groups (group_id)
    select group_id from @tmp_groups;

    set @new_grp_cnt = @@rowcount;   
  end;

  declare @labels varchar(max) = dbo.fn_get_crr_labels_for_node(@node_id);

  select @labels = @labels + ',' + l.name 
  from bik_crr_label l inner join #crr_labs_member_of_groups mog on l.group_id = mog.group_id
  where l.is_default_for_new_node <> 0;

  exec sp_set_crr_labels_on_node @labels, @node_id;  
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_verticalize_node_attrs_delete_attr]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_verticalize_node_attrs_delete_attr](@attribute_id int)
as
begin
  set nocount on
  
  declare @node_kind_id int = (select node_kind_id from bik_attribute where id = @attribute_id)
  
  declare @attr_def_id int = (select attr_def_id from bik_attribute where id = @attribute_id)
  
  declare @attr_name varchar(255) = (select name from bik_attr_def where id = @attr_def_id)
  
  declare @searchable_attr_id int = (select id from bik_searchable_attr where name = @attr_name)
  
  delete from bik_searchable_attr_val where attr_id = @searchable_attr_id and node_id in (select id from bik_node where node_kind_id = @node_kind_id)
end
GO
/****** Object:  Table [dbo].[bik_attribute_linked]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_attribute_linked](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_id] [int] NOT NULL,
	[value] [varchar](max) NULL,
	[node_id] [int] NOT NULL,
	[is_deleted] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_attribute_linked_a_n_v] ON [dbo].[bik_attribute_linked] 
(
	[attribute_id] ASC,
	[node_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_attribute_linked_n_a_v] ON [dbo].[bik_attribute_linked] 
(
	[node_id] ASC,
	[attribute_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_generic_insert_system_attr_values]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_generic_insert_system_attr_values] (@treeCode varchar(max), @categoryName varchar(max)) as
begin
	set nocount on;
	
	--procka
	if not exists(select * from bik_attr_category where name = @categoryName and is_built_in = 1)
		insert into bik_attr_category(name, is_built_in) values (@categoryName, 1);

	declare @categoryId int = (select id from bik_attr_category where name = @categoryName and is_built_in = 1);

	-- merge na attr_def
	merge bik_attr_def as def
	using (select name from amg_attribute group by name) x on x.name = def.name
	when matched and def.is_built_in = 1 and def.attr_category_id = @categoryId then
	update set def.is_deleted = 0
	when not matched by target then
	insert (name, attr_category_id, is_built_in, type_attr) values (x.name, @categoryId, 1, 'shortText');

	-- merge na bik_attr_system
	merge bik_attr_system as asys
	using (select def.id as attr_id, bn.node_kind_id from amg_attribute as att
	inner join bik_attr_def as def on def.name = att.name and def.is_built_in = 1 and def.attr_category_id = @categoryId
	inner join bik_node as bn on bn.obj_id = att.obj_id
	inner join bik_tree as bt on bt.id = bn.tree_id
	where bt.code = @treeCode
	group by def.id, bn.node_kind_id) x on asys.node_kind_id = x.node_kind_id and x.attr_id = asys.attr_id
	when not matched by target then
	insert (node_kind_id, attr_id, is_visible) values (x.node_kind_id, x.attr_id, 1);

	-- merge na wartości 
	merge bik_attr_system_linked as lin
	using (
	select bsys.id as attr_id, bn.id as node_id, att.value from amg_attribute as att
	inner join bik_node as bn on bn.obj_id = att.obj_id
	inner join bik_tree as bt on bt.id = bn.tree_id
	inner join bik_attr_def as def on def.name = att.name and def.is_built_in = 1
	inner join bik_attr_system as bsys on bsys.node_kind_id = bn.node_kind_id and bsys.attr_id = def.id
	where bt.code = @treeCode
	) as x on lin.attr_system_id = x.attr_id and lin.node_id = x.node_id
	when matched then
	update set lin.is_deleted = 0, lin.value = x.value
	when not matched by target then
	insert (attr_system_id, node_id, value) values (x.attr_id, x.node_id, x.value)
	when not matched by source and lin.node_id in (select bn.id from bik_node as bn inner join bik_tree as bt on bt.id = bn.tree_id and bt.code = @treeCode) then
	update set lin.is_deleted = 1;

end;
GO
/****** Object:  StoredProcedure [dbo].[api_tree_list]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_tree_list]
as 
begin 
	select code, name, is_hidden from bik_tree
end
GO
/****** Object:  StoredProcedure [dbo].[api_show_or_hide_tree]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[api_show_or_hide_tree](@tree_code varchar(255), @is_hidden int)
as
begin
	update bik_tree set is_hidden = @is_hidden where code = @tree_code;
end
GO
/****** Object:  Table [dbo].[aaa_teradata_object]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_teradata_object](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_load_log_id] [int] NOT NULL,
	[type] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[extra_info] [varchar](max) NULL,
	[branch_names] [varchar](1000) NULL,
	[parent_branch_names] [varchar](max) NULL,
	[request_text] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_lisa_dict_column_lookup]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_lisa_dict_column_lookup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[column_id] [int] NULL,
	[value] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_jdbc]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_jdbc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](800) NOT NULL,
	[jdbc_url] [varchar](256) NULL,
	[db_user] [varchar](256) NULL,
	[db_password] [varchar](256) NULL,
	[query] [varchar](max) NULL,
	[tree_id] [int] NULL,
	[is_active] [int] NOT NULL,
	[jdbc_source_id] [int] NOT NULL,
	[query_joined] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_file_system]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_file_system](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](512) NOT NULL,
	[path] [varchar](max) NULL,
	[domain] [varchar](max) NULL,
	[user] [varchar](max) NULL,
	[password] [varchar](max) NULL,
	[is_active] [int] NOT NULL,
	[download_files] [int] NOT NULL,
	[tree_id] [int] NULL,
	[max_file_size] [numeric](10, 2) NULL,
	[file_name_patterns] [varchar](max) NULL,
	[file_since] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_bik_file_system_name] ON [dbo].[bik_file_system] 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_table_column]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_table_column](
	[long_id] [varchar](67) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[physical_name] [varchar](100) NULL,
	[column_type] [int] NOT NULL,
	[table_long_id] [varchar](67) NOT NULL,
	[data_load_log_id] [int] NULL,
	[column_comment] [varchar](2048) NULL,
	[parent_col_ref_long_id] [varchar](67) NULL,
	[parent_relationship_ref_long_id] [varchar](67) NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_table]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_table](
	[long_id] [varchar](67) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[schema_name] [varchar](100) NULL,
	[physical_name] [varchar](100) NOT NULL,
	[table_type] [int] NOT NULL,
	[shapes_ref] [varchar](67) NOT NULL,
	[data_load_log_id] [int] NULL,
	[table_comment] [varchar](2048) NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_erwin_shape]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_erwin_shape](
	[long_id] [varchar](67) NOT NULL,
	[area_long_id] [varchar](67) NOT NULL,
	[model_object_ref] [varchar](67) NOT NULL,
	[anchor_point] [varchar](23) NOT NULL,
	[fixed_size_point] [varchar](23) NULL,
	[data_load_log_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_erwin_shape_area_long_id] ON [dbo].[bik_erwin_shape] 
(
	[area_long_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_fix_databases_nodes_after_deactivate]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_fix_databases_nodes_after_deactivate](@dbServerId int)
as
begin
	declare @servName varchar(255) = (select name from bik_db_server where id = @dbServerId);
	declare @sourceId int = (select source_id from bik_db_server where id = @dbServerId);
	declare @treeId int = (select tree_id from bik_db_server where id = @dbServerId);
	declare @activeDBs varchar(255) = '';
	declare @branchsToDelete table (branch varchar(900));

	if(charindex('#', @servName) <> 0)
	begin
		set @servName = substring(@servName, 1, charindex('#', @servName) - 1);
	end;

	select @activeDBs = @activeDBs + ',' + case when database_filter is not null and database_filter <> '' then database_filter when database_list is not null and  database_list <> '' then database_list else '' end
	from bik_db_server as ser
	where source_id = @sourceId and is_active = 1 
	and tree_id = @treeId and (name = @servName or name like @servName + '#%')

	insert into @branchsToDelete(branch)
	select branch_ids 
	from bik_node 
	where parent_node_id in (
		select id from bik_node 
		where tree_id = @treeId 
		and is_deleted = 0 
		and obj_id = @servName + '|')
	and is_deleted = 0
	and name not in (select str 
		from (select * from dbo.fn_split_by_sep(@activeDBs, ',',3 )) x 
		where str <> '')
		
		
	declare @branch varchar(900);	
	declare db_del_cur cursor for select branch from @branchsToDelete
	open db_del_cur
	fetch next from db_del_cur into @branch
	while @@fetch_status = 0
		begin 
		update bik_node 
		set is_deleted = 1
		where tree_id = @treeId
		and branch_ids like @branch + '%'
		fetch next from db_del_cur into @branch
		end
	close db_del_cur;
	deallocate db_del_cur;
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_db_server_id_by_procedure_node_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_db_server_id_by_procedure_node_id](@procedure_node_id int, @sourceId int)
returns int
as
begin
	declare @objId varchar(max) = (select obj_id from bik_node where id = @procedure_node_id);
	declare @tab table (str varchar(max), idx int);
	
	insert into @tab(str, idx)
	select str, idx from dbo.fn_split_by_sep(@objId, '|', 0);
	
	declare @serverName varchar(max) = (select str from @tab where idx = 1);
	declare @databaseName varchar(max) = (select str from @tab where idx = 2);
	declare @dbServerId int = null;

	select top 1 @dbServerId = id 
	from bik_db_server ser
	where source_id = @sourceId
	and (name = @serverName or name like @serverName + '#%')
	and exists(select * from dbo.fn_split_by_sep(coalesce(ser.database_list, ''), ',', 3) where str = @databaseName)

	return @dbServerId;
end
GO
/****** Object:  Table [dbo].[bik_data_load_log_details]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_data_load_log_details](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[log_id] [int] NOT NULL,
	[description] [varchar](max) NULL,
	[start_time] [datetime] NOT NULL,
 CONSTRAINT [pk_bik_data_load_log_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_sapbo_extradata]
as
begin
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	declare @boServer int = dbo.fn_get_bo_actual_server_id();

	delete from bik_sapbo_extradata
	from bik_sapbo_extradata
	inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
	where bn.tree_id in (@reportTreeId , @universeTreeId, @connectionTreeId)
	
	
	declare @tempTable table (
		id int identity(1,1) not null,
		column_name varchar(50) not null,
		table_name varchar(50) not null
	);
	
	-- ważna jest kolejnosc wstawiania
	insert into @tempTable(column_name,table_name)
	values
		('SI_AUTHOR','INFO_WEBI'),
		('SI_OWNER','aaa_global_props'),
		('SI_KEYWORD','aaa_global_props'),
		('SI_FILES__SI_PATH','aaa_global_props'),
		('SI_FILES__SI_FILE1','aaa_global_props'),
		('SI_CREATION_TIME','aaa_global_props'),
		('SI_UPDATE_TS','aaa_global_props'),
		('SI_CUID','aaa_global_props'),
		('SI_FILES__SI_VALUE1','aaa_global_props'),
		('SI_SIZE','aaa_global_props'),
		('SI_HAS_CHILDREN','aaa_global_props'),
		('SI_GUID','aaa_global_props'),
		('SI_INSTANCE','aaa_global_props'),
		('SI_OWNERID','aaa_global_props'),
		('SI_PROGID','aaa_global_props'),
		('SI_OBTYPE','aaa_global_props'),
		('SI_FLAGS','aaa_global_props'),
		('SI_CHILDREN','aaa_global_props'),
		('SI_RUID','aaa_global_props'),
		('SI_RUNNABLE_OBJECT','aaa_global_props'),
		('SI_CONTENT_LOCALE','aaa_global_props'),
		('SI_IS_SCHEDULABLE','aaa_global_props'),
		('SI_WEBI_DOC_PROPERTIES','aaa_global_props'),
		('SI_READ_ONLY','aaa_global_props'),
		('SI_LAST_SUCCESSFUL_INSTANCE_ID','aaa_global_props'),
		('SI_LAST_RUN_TIME','aaa_global_props'),
		('SI_TIMESTAMP','INFO_WEBI'),
		('SI_PROGID_MACHINE','INFO_WEBI'),
		('SI_ENDTIME','aaa_global_props'),
		('SI_STARTTIME','aaa_global_props'),
		('SI_SCHEDULE_STATUS','aaa_global_props'),
		('SI_RECURRING','aaa_global_props'),
		('SI_NEXTRUNTIME','aaa_global_props'),
		('SI_DOC_SENDER','aaa_global_props'),
		('SI_REVISIONNUM','APP_UNIVERSE'),
		('SI_APPLICATION_OBJECT','APP_UNIVERSE'),
		('SI_SHORTNAME','APP_UNIVERSE'),
		('SI_DATACONNECTION__SI_TOTAL','APP_UNIVERSE'),
		('SI_UNIVERSE__SI_TOTAL','INFO_WEBI')
		
	declare @sqlText varchar(max);
	declare @count int;
	select @count = count(*) from @tempTable
	
	set @sqlText = 'insert into bik_sapbo_extradata (node_id,author,owner,keyword,file_path,file_name,created,modified,cuid,files_value,size,has_children,guid,instance,owner_id,progid,obtype,flags,children,ruid,runnable_object,content_locale,is_schedulable,webi_doc_properties,read_only,last_successful_instance_id,last_run_time,timestamp,progid_machine,endtime,starttime,schedule_status,recurring,nextruntime,doc_sender,revisionnum,application_object,shortname,dataconnection_total,universe_total )
    select bik.id as node_id, '
		
	declare kurs cursor for 
	select column_name, table_name from @tempTable order by id 

	open kurs;
	declare @column varchar(50);
	declare @table varchar(50);
	declare @i int = 1;

	fetch next from kurs into @column, @table;
	while @@fetch_status = 0
	begin
		if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
		begin
			set @sqlText = @sqlText + @table + '.' + @column
		end
		else
		begin
			set @sqlText = @sqlText + 'null'
		end
		if(@i <> @count)
		begin
			set @sqlText = @sqlText + ', '
		end
		--
		set @i = @i + 1;
		fetch next from kurs into @column, @table;
	end
	close kurs;
	deallocate kurs;	

	set @sqlText = @sqlText + ' from aaa_global_props
    left join INFO_WEBI on aaa_global_props.si_id = INFO_WEBI.si_id
    left join APP_UNIVERSE on aaa_global_props.si_id = APP_UNIVERSE.si_id
    inner join bik_node bik on convert(varchar(30),aaa_global_props.si_id) = bik.obj_id
    where bik.tree_id in (' + convert(varchar(10),@reportTreeId) + ', ' + convert(varchar(10),@universeTreeId) + ', ' + convert(varchar(10),@connectionTreeId) + ') 
    and bik.is_deleted = 0
    and bik.linked_node_id is null
    order by bik.id'
    
    exec(@sqlText);
    
    delete from bik_sapbo_schedule
    from bik_sapbo_schedule
	inner join bik_node bn on bn.id = bik_sapbo_schedule.node_id
	where bn.tree_id = @reportTreeId
    
    insert into bik_sapbo_schedule(node_id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to, destination_folder_path)
    select bn.id,destination,owner,creation_time,nextruntime,expire,type,schedule_type,interval_minutes,interval_hours,interval_days,interval_months,interval_nth_day,format,parameters,destination_email_to, destination_folder_path
    from aaa_report_schedule sch
    inner join bik_node bn on convert(varchar(30),sch.id) = bn.obj_id
    --inner join bik_tree bt on bt.id = bn.tree_id
    where bn.tree_id = @reportTreeId 
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    --and bt.code in ('Reports')
    

    
	if exists(select 1 from bik_sapbo_server ser 
		inner join bik_data_source_def def on ser.source_id = def.id
		where ser.id = @boServer and def.description = 'Business Objects 4.x') and exists(select 1 from sys.objects where type = 'U' and name = 'APP_COMMONCONNECTION')
	begin
		    delete from bik_sapbo_olap_connection
			from bik_sapbo_olap_connection
			inner join bik_node bn on bik_sapbo_olap_connection.node_id = bn.id
			where bn.tree_id = @connectionTreeId
			
			delete from @tempTable;
			
			insert into @tempTable(column_name,table_name)
			values
				('SI_CONNECTION_TYPE','APP_COMMONCONNECTION'),
				('SI_PROVIDER_CAPTION','APP_COMMONCONNECTION'),
				('SI_CUBE_CAPTION','APP_COMMONCONNECTION'),
				('SI_CUID','APP_COMMONCONNECTION'),
				('SI_CREATION_TIME','APP_COMMONCONNECTION'),
				('SI_UPDATE_TS','APP_COMMONCONNECTION'),
				('SI_OWNER','APP_COMMONCONNECTION'),
				('SI_NETWORK_LAYER','APP_COMMONCONNECTION'),
				('SI_MD_USERNAME','APP_COMMONCONNECTION')
				
			set @sqlText = '';
			set @count = 0;;
			select @count = count(*) from @tempTable
			
			set @sqlText = 'insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, ';
				
			declare kurs cursor for 
			select column_name, table_name from @tempTable order by id 

			open kurs;
			set @column = '';
			set @table = '';
			set @i = 1;

			fetch next from kurs into @column, @table;
			while @@fetch_status = 0
			begin
				if exists(select * from sys.columns where name = @column and Object_ID = Object_ID(@table))    
				begin
					set @sqlText = @sqlText + @table + '.' + @column
				end
				else
				begin
					set @sqlText = @sqlText + 'null'
				end
				if(@i <> @count)
				begin
					set @sqlText = @sqlText + ', '
				end
				--
				set @i = @i + 1;
				fetch next from kurs into @column, @table;
			end
			close kurs;
			deallocate kurs;	

			set @sqlText = @sqlText + ' from APP_COMMONCONNECTION  
			inner join bik_node bn on bn.obj_id = convert(varchar(30),APP_COMMONCONNECTION.si_id) and bn.tree_id = ' + convert(varchar(10),@connectionTreeId);
		    
			exec(@sqlText);
			
			/*
			insert into bik_sapbo_olap_connection(node_id, type, provider_caption, cube_caption, cuid, created, modified, owner, network_layer, md_username)
			select bn.id, app.SI_CONNECTION_TYPE, app.SI_PROVIDER_CAPTION, app.SI_CUBE_CAPTION, app.SI_CUID, app.SI_CREATION_TIME, app.SI_UPDATE_TS, app.SI_OWNER, app.SI_NETWORK_LAYER, app.SI_MD_USERNAME 
			from APP_COMMONCONNECTION app 
			inner join bik_node bn on bn.obj_id = convert(varchar(30),app.si_id) and tree_id = @connectionTreeId
			*/
	end
end;
GO
/****** Object:  Table [dbo].[bik_plain_file]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_plain_file](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source_file_path] [varchar](max) NULL,
	[is_remote] [int] NULL,
	[name] [varchar](max) NULL,
	[domain] [varchar](max) NULL,
	[username] [varchar](max) NULL,
	[password] [varchar](max) NULL,
	[tree_id] [int] NULL,
	[is_active] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_node_kind_4_tree_kind]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_node_kind_4_tree_kind](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tree_kind_id] [int] NOT NULL,
	[node_kind_id] [int] NOT NULL,
	[is_branch] [int] NOT NULL,
	[is_leaf] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_biks_statistic_data_load]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_biks_statistic_data_load] as
	select
		def.description as system,
		log.server_name as server,
		log.start_time,
		log.finish_time,
		DATEDIFF(ss,log.start_time,log.finish_time) as duration_in_sec,
		log.status_description as description
	from bik_data_load_log as log
	inner join bik_data_source_def as def
	on def.id = log.data_source_type_id
GO
/****** Object:  StoredProcedure [dbo].[sp_update_version]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_update_version](@ver_current varchar(255), @ver_upper varchar(255))
as
	declare @version varchar(255)
begin
	exec sp_check_version @ver_current;
	
	update bik_app_prop set val=@ver_upper where name='bik_ver';	
end
GO
/****** Object:  StoredProcedure [dbo].[sp_recreate_fn_is_custom_right_role_tree]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recreate_fn_is_custom_right_role_tree] as
begin
	set nocount on;

  if object_id (N'dbo.fn_is_custom_right_role_tree', N'fn') is not null
    exec('drop function dbo.fn_is_custom_right_role_tree');

  declare @customRightRolesTreeSelector varchar(max) = (select val from bik_app_prop where name = 'customRightRolesTreeSelector');

  declare @sql_txt nvarchar(max) = null;

  if coalesce(@customRightRolesTreeSelector, '') = '' begin
    set @sql_txt = '1';
  end else begin

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ',' end + cast(tree_id as varchar(20))
    from (
    select distinct t.id as tree_id
    from dbo.fn_split_by_sep(@customRightRolesTreeSelector, ',', 7) x inner join bik_tree t on --t.code = x.str
      substring(x.str, 1, 1) = '@' and t.tree_kind = substring(x.str, 2, len(x.str)) or
      substring(x.str, 1, 1) <> '@' and t.code = x.str
    ) x;

    set @sql_txt = 'case when @tree_id in (' + @sql_txt + ') then 1 else 0 end';
  end;

  set @sql_txt = 'create function dbo.fn_is_custom_right_role_tree(@tree_id int) returns int as begin return ' + @sql_txt + '; end';
  
  --print @sql_txt;
  exec(@sql_txt);
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_PB]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_PB] as
		begin
		  set nocount on;
		  
		  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
		  if @regUserCrrId is null return;
		  
		  declare @tree_selector_codes_all varchar(max) = '';
		  declare @tree_selector_codes_ru varchar(max) = '';
		  
		  select 
			@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
			@tree_selector_codes_ru = case when code in ('DQM', 'DQMDokumentacja') then @tree_selector_codes_ru 
			  else case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code end 
		  from bik_tree where code not in (/*'DQM', 'DQMDokumentacja',*/ 'TreeOfTrees') and is_hidden = 0;
		  
		  -- select * from bik_custom_right_role
		  
		  update bik_app_prop set val = @tree_selector_codes_all where name = 'customRightRolesTreeSelector';
		  
		  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
		  
		  -- select * from bik_app_prop  
		end;
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors] as
		begin
		 /* if (select val from bik_app_prop where name = 'biks_id') = '7CB602FD-4C29-4C5D-A3A9-82E814EFADD2' 
			or (select val from bik_app_prop where name = 'biks_id') = 'B424EC5D-2D08-45AE-874D-A4E95FA61880' 
			begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
			end else 
			*/
			if ((select val from bik_app_prop where name = 'biks_id') = '' 
			or (select val from bik_app_prop where name = 'biks_id') = '5005832A-6D1D-4F70-BB6D-32146BF003AA')  
			begin
	 
			/*PB */
			  exec sp_recalculate_Custom_Right_Role_Tree_Selectors_PB
			/*inne*/
			  declare @regUserCrrId int = (select id from bik_custom_right_role where code = 'RegularUser');
		  
			  if @regUserCrrId is null return;
			  
			  declare @tree_selector_codes_all varchar(max) = '';
			  declare @tree_selector_codes_ru varchar(max) = '';
			  
			  select 
				@tree_selector_codes_all = case when @tree_selector_codes_all = '' then '' else @tree_selector_codes_all + ',' end + code,
				@tree_selector_codes_ru =  case when @tree_selector_codes_ru = '' then '' else @tree_selector_codes_ru + ',' end + code  
			  from bik_tree where code not in ( 'TreeOfTrees') and is_hidden = 0;
			  
			  update bik_app_prop set val = @tree_selector_codes_all+','+ 'TreeOfTrees' where name = 'customRightRolesTreeSelector';
			  
			  update bik_custom_right_role set tree_selector = @tree_selector_codes_all+','+ 'TreeOfTrees' where code in('AppAdmin','Administrator','Author','Expert');
			  
			  update bik_custom_right_role set tree_selector = @tree_selector_codes_ru where id = @regUserCrrId;
			end
			else
			begin
			exec sp_recalculate_Custom_Right_Role_Tree_Selectors_CP_POL
			end
			
			
		end;
		/*end; */
GO
/****** Object:  StoredProcedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex] as
begin
	set nocount on;

  exec sp_recalculate_Custom_Right_Role_Tree_Selectors;

  exec sp_recreate_fn_is_custom_right_role_tree;

  exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_calc_itscc_for_sugs]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_calc_itscc_for_sugs] @diag_level int = 0 as
begin
  set nocount on;

  declare @start_time datetime = getdate();

  --declare @diag_level int = 0;

  if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': [sp_calc_itscc_for_sugs] start';

  declare @idx int = 1;

  create table #stack (i int not null identity primary key, w_id int null, v_id int not null, f int not null, unique (w_id, v_id, f));

  --insert into #stack (w_id, v_id, f) select null, node_id, 0 from itscc_graph_node;
  insert into #stack (w_id, v_id, f) select null, id, 0 from bik_system_user_group where is_deleted = 0;

  create table #node_state (node_id int not null primary key, state int not null, idx int not null, lowlink int not null);

  create index idx_node_state_idx on #node_state (idx, state);

  insert into #node_state (node_id, state, idx, lowlink) select id, 0, 0, 0 from bik_system_user_group where is_deleted = 0;

  declare @pass int = 0;

  while 1 = 1
  begin
    if @diag_level > 1
      print '
    ----------------------------- pass #' + cast(@pass as varchar(20)) + ' -----------------------------

    ';
    if @diag_level > 2
    begin
      select * from #stack order by i;
      select * from #node_state order by node_id;
    end;

    declare @w_id int, @v_id int, @f int;
    declare @v_state int, @v_idx int, @v_lowlink int;
    
    declare @max_i int;
    set @max_i = null;
    select top 1 @max_i = i, @w_id = w_id, @v_id = v_id, @f = f from #stack order by i desc;
    if @max_i is null break;
    
    delete from #stack where i = @max_i;
    
    select @v_state = state, @v_idx = idx, @v_lowlink = lowlink from #node_state where node_id = @v_id;

    if @diag_level > 1
    print '@w_id = ' + coalesce(@w_id, '<null>') + ', @v_id = ' + @v_id + ', @f = ' + cast(@f as varchar(20)) +
      ', @v_state = ' + cast(@v_state as varchar(20)) + ', @v_idx = ' + cast(@v_idx as varchar(20)) + ', @v_lowlink = ' + cast(@v_lowlink as varchar(20));

    /*if @diag_level > 0
    begin
      declare @real_stack_size int = (select count(*) from #stack);
      if @stack_size <> @real_stack_size begin
        print '@stack_size = ' + cast(@stack_size as varchar(20)) + ', @real_stack_size = ' + cast(@real_stack_size as varchar(20));
        break;
      end;
    end;*/

    if @f = 0
    begin
      if @v_state <> 0 -- = 2
      begin
        continue;
      end;
      
      insert into #stack (w_id, v_id, f) values (@w_id, @v_id, 2);
      
      insert into #stack (w_id, v_id, f) select @v_id, e.member_group_id, 0
      from bik_system_group_in_group e inner join #node_state s on e.member_group_id = s.node_id
      where e.group_id = @v_id and e.is_deleted = 0 and s.state = 0;

      declare @lowlink int;
      
      select @lowlink = min(s.idx)
      from bik_system_group_in_group e inner join #node_state s on e.member_group_id = s.node_id
      where e.group_id = @v_id and e.is_deleted = 0 and s.state = 1;

      if @lowlink is null set @lowlink = @idx;

      update #node_state set state = 1, idx = @idx, lowlink = @lowlink where node_id = @v_id;
      
      set @idx += 1;
            
    end else if @f = 2
    begin
      if @v_idx = @v_lowlink
        update #node_state set state = 2, lowlink = @v_idx where idx >= @v_idx and state = 1;
      
      if @w_id is not null
        update #node_state set lowlink = case when lowlink > @v_lowlink then @v_lowlink else lowlink end where node_id = @w_id;        
    end;

    set @pass += 1;
    
    --if @pass > 10 break;
    
    -- exec sp_calc_itscc_for_sugs;

    --if @pass > 5000 set @diag_level = 1;
  end;

  declare @final_stack_size int = (select count(*) from #stack);

  if @final_stack_size > 0 print 'stack is not empty!!! stack size: ' + cast(@final_stack_size as varchar(20));

  if @diag_level > 0 
  begin
    declare @end_time datetime = getdate();

    declare @node_cnt int = (select count(*) from bik_system_user_group where is_deleted = 0);
    declare @edge_cnt int = (select count(*) from bik_system_group_in_group where is_deleted = 0);

    declare @ms_taken int = datediff(ms, @start_time, @end_time);

    print cast(sysdatetime() as varchar(23)) + ': [sp_calc_itscc_for_sugs] finished after ' + cast(@pass as varchar(20)) + ' passes, time taken: ' 
    + cast(@ms_taken as varchar(30)) + ' ms, nodes: ' + cast(@node_cnt as varchar(20)) + ', edges: ' + cast(@edge_cnt as varchar(20)) +
    ', passes per (n+e) = ' + cast(cast(@pass as float) / (@node_cnt + @edge_cnt) as varchar(20)) + ', time per (n+e) = ' + 
    cast(cast(@ms_taken as float) / (@node_cnt + @edge_cnt) as varchar(20));
  end;
  
  --select * from #node_state;
  
  update sug
  set sug.scc_root_id = ps.node_id
  from bik_system_user_group sug      -- użyty left join spowoduje przypisanie scc_root_id = null dla grup z is_deleted <> 0
  left join (#node_state ns inner join #node_state ps on ns.lowlink = ps.idx) on sug.id = ns.node_id;
end;
GO
/****** Object:  Table [dbo].[bik_tree_icon]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_tree_icon](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tree_id] [int] NOT NULL,
	[node_kind_id] [int] NOT NULL,
	[icon] [varchar](512) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_bik_tree_ikon_tree_id_node_kind_id] UNIQUE NONCLUSTERED 
(
	[tree_id] ASC,
	[node_kind_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_teradata_data_model]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_teradata_data_model](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_load_log_id] [int] NULL,
	[type] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[extra_info] [varchar](max) NULL,
	[branch_names] [varchar](1000) NULL,
	[parent_branch_names] [varchar](max) NULL,
	[request_text] [varchar](max) NULL,
	[database_name] [varchar](100) NULL,
	[table_name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_teradata_data_model_branch_names] ON [dbo].[bik_teradata_data_model] 
(
	[branch_names] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_teradata_data_model_database_name] ON [dbo].[bik_teradata_data_model] 
(
	[database_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_teradata_data_model_name] ON [dbo].[bik_teradata_data_model] 
(
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_bik_teradata_data_model_table_name] ON [dbo].[bik_teradata_data_model] 
(
	[table_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bik_teradata]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_teradata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[data_load_log_id] [int] NULL,
	[type] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[extra_info] [varchar](max) NULL,
	[branch_names] [varchar](1000) NULL,
	[parent_branch_names] [varchar](max) NULL,
	[request_text] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tree_id_by_code]    Script Date: 09/28/2016 11:08:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_tree_id_by_code](@code varchar(256))
returns int
as
begin
  return (select id from bik_tree where code = @code)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_teradata_model_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_teradata_model_connections] as
begin

	-------------- Teradata Table <-> Teradata Data Model Table
	exec sp_prepare_bik_joined_objs_tmp
	
	declare @modelTreeId int = dbo.fn_tree_id_by_code('TeradataDataModel');
	declare @teradataTreeId int = dbo.fn_tree_id_by_code('Teradata');

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct bn.id, tera.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0 and bn.name = mode.name
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' and tera.is_deleted = 0
	where mode.type = 'TeradataTable'

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct tera.id, bn.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0 and bn.name = mode.name
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' and tera.is_deleted = 0
	where mode.type = 'TeradataTable'

	-- TF: na razie nie potrzebne są powiązania między kolumnami - komentuje
	/*
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct bn.id, tera.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' + rtrim(mode.name) + '|' and tera.is_deleted = 0
	where mode.type in('TeradataColumn', 'TeradataColumnIDX', 'TeradataColumnPK')

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct tera.id, bn.id, 1, 0 
	from bik_teradata_data_model as mode
	inner join bik_node as bn on bn.tree_id = @modelTreeId and mode.branch_names = bn.obj_id and bn.is_deleted = 0
	inner join bik_node as tera on tera.tree_id = @teradataTreeId and tera.obj_id = rtrim(mode.database_name) + '|' + rtrim(mode.table_name) + '|' + rtrim(mode.name) + '|' and tera.is_deleted = 0
	where mode.type in('TeradataColumn', 'TeradataColumnIDX', 'TeradataColumnPK')
	*/
	
	exec sp_move_bik_joined_objs_tmp '@TeradataDataModel', '@Teradata'

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_teradata_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_teradata_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
	
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();

    declare @teradataKind int = dbo.fn_tree_id_by_code('Teradata');
    declare @schemaKind int = dbo.fn_node_kind_id_by_code('TeradataSchema');
    declare @tabKind int = dbo.fn_node_kind_id_by_code('TeradataTable');
    declare @viewKind int = dbo.fn_node_kind_id_by_code('TeradataView');
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @universeNodeKind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    
    ----------   obiekt  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)

    ----------   obiekt  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct a.object_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_node obj on a.object_node_id = obj.id
    and obj.tree_id = @universeTreeId and obj.is_deleted = 0
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind

    ----------   universum  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    where b.type = 'Teradata' and b.is_derived = 0 
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   universum  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bntf.parent_node_id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  universum   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bntf.parent_node_id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Schema  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0 and bn2.id is not null
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  connection   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn2.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   connection  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn2.id, bn.id, 1 
    from bik_sapbo_universe_table b
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@tabKind, @viewKind)
    inner join bik_node bnt on b.node_id = bnt.id
    inner join bik_node bntf on bnt.parent_node_id = bntf.id
    inner join bik_joined_objs jo on jo.src_node_id = bntf.parent_node_id
    and jo.type = 1
    inner join bik_node bn2 on jo.dst_node_id = bn2.id
    and bn2.tree_id = @connectionTreeId
    and bn2.is_deleted = 0
    and bn2.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
    where b.type = 'Teradata' and b.is_derived = 0
    and bnt.tree_id = @universeTreeId and bnt.is_deleted = 0

    ----------   teradata Table/View  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   teradata Schema  -->  raport   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn.id, bn4.parent_node_id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   raport  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.parent_node_id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Schema   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.schema_name
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id = @schemaKind
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId

    ----------   zapytanie  -->  teradata Table/View   ----------
    insert into tmp_bik_joined_objs(src_node_id, dst_node_id, type)
    select distinct bn4.id, bn.id, 1 
    from bik_sapbo_object_table a
    inner join bik_sapbo_universe_table b on a.table_node_id = b.node_id
    and b.type = 'Teradata'
    and b.is_derived = 0
    inner join bik_node bn on bn.obj_id = b.name_for_teradata
    and bn.is_deleted = 0
    and bn.linked_node_id is null
    and bn.tree_id = @teradataKind
    and bn.node_kind_id in (@viewKind,@tabKind)
    inner join bik_node bn3 on bn3.linked_node_id = a.object_node_id
    and bn3.is_deleted = 0
    and bn3.tree_id = @reportTreeId
    inner join bik_node bn4 on bn3.parent_node_id = bn4.id
    and bn4.node_kind_id = @query_kind
    and bn4.is_deleted = 0
    and bn4.tree_id = @reportTreeId    
    
    ----------   UniverseTable  -->  teradata Table/View   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   teradata Table/View  -->  UniverseTable   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bsut.node_id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.name_for_teradata = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

	----------   UniverseTable  -->  teradata Schema   ----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bsut.node_id, bn.id, 1 
	from bik_sapbo_universe_table bsut 
	inner join bik_node bn on bsut.schema_name = bn.obj_id 
	and bn.tree_id = @teradataKind
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bsut.type = 'Teradata'
	inner join bik_node tab on tab.id = bsut.node_id
	and tab.tree_id = @universeTreeId
	and tab.is_deleted = 0

    exec sp_move_bik_joined_objs_tmp 'TeradataView,TeradataTable,TeradataSchema', 'Universe,DSL.MetaDataFile,DataConnection,CommonConnection,CCIS.DataConnection,Measure,Dimension,Detail,Filter,Webi,ReportQuery,UniverseAliasTable,UniverseDerivedTable,UniverseTable'
	
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_oracle_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @oracleTree int = dbo.fn_tree_id_by_code('Oracle');

	---------- universe table --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- universe table --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- object --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- object --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- query --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- query --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- report --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null	

	---------- universe --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> oracle DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- oracle table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- oracle table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	
	---------- oracle table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.name_for_teradata
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- oracle DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- oracle DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.server_name + '|' + ut.schema_name
	and bn.tree_id = @oracleTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ot.object_node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@Oracle', '@Reports, @ObjectUniverses, @Connections'
    
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_mssql_connections]
as
begin

	exec sp_prepare_bik_joined_objs_tmp
    
	declare @reportTreeId int = dbo.fn_get_bo_actual_report_tree_id();
	declare @universeTreeId int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
    declare @connection_kind int = dbo.fn_node_kind_id_by_code('DataConnection');    
    declare @universe_kind int = dbo.fn_node_kind_id_by_code('Universe');
    declare @uniNewKind int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
    declare @dcNewKind int = dbo.fn_node_kind_id_by_code('CCIS.DataConnection');
    declare @dcNewOLAPKind int = dbo.fn_node_kind_id_by_code('CommonConnection');
    declare @query_kind int = dbo.fn_node_kind_id_by_code('ReportQuery');
    declare @mssqlTree int = dbo.fn_tree_id_by_code('MSSQL');

	---------- universe table --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- universe table --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ut.node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ut.node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- object --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select ot.object_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	
	---------- query --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- query --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- report --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bnparent.parent_node_id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnu on bnu.id = ot.object_node_id
	and bnu.tree_id = @universeTreeId
	and bnu.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId

	---------- universe --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- universe --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- connection --> mssql DB -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bndst.id, bn.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null


	
	---------- mssql table --> universe table -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, ut.node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted=0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	
	---------- mssql table --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null
	and bnparent.tree_id = @reportTreeId
	
	---------- mssql table --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql table --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.mssql_table_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> connection -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @connectionTreeId
	and bndst.node_kind_id in (@connection_kind, @dcNewOLAPKind, @dcNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null

	---------- mssql DB --> universe -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bndst.id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bnt on bnt.id = ut.node_id
	and bnt.tree_id = @universeTreeId
	and bnt.is_deleted = 0
	inner join bik_joined_objs obj on obj.src_node_id = ut.node_id
	and obj.type = 1
	inner join bik_node bndst on bndst.id = obj.dst_node_id
	and bndst.tree_id = @universeTreeId
	and bndst.node_kind_id in (@universe_kind, @uniNewKind)
	and bndst.is_deleted = 0
	and bndst.linked_node_id is null
	
	---------- mssql DB --> report -----------
	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type)
	select bn.id, bnparent.parent_node_id, 1 
	from bik_sapbo_universe_table ut 
	inner join bik_sapbo_object_table ot on ot.table_node_id = ut.node_id
	inner join bik_node bn on bn.obj_id = ut.database_name
	and bn.tree_id = @mssqlTree
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	inner join bik_node bno on bno.id = ot.object_node_id
	and bno.tree_id = @universeTreeId
	and bno.is_deleted = 0
	inner join bik_node bnlinked on bnlinked.linked_node_id = ot.object_node_id
	and bnlinked.is_deleted = 0
	inner join bik_node bnparent on bnparent.id = bnlinked.parent_node_id
	and bnparent.tree_id = @reportTreeId
	and bnparent.is_deleted = 0
	and bnparent.node_kind_id = @query_kind
	and bnparent.linked_node_id is null		
	
    exec sp_move_bik_joined_objs_tmp '@MSSQL', '@Reports, @ObjectUniverses, @Connections'
    
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_connections_for_dqc]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_connections_for_dqc] as
begin

	-------------- Test <-> Teradata
	exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct test.id, ter.id, 1, 0
	from bik_dqc_test_parameters dtp
	join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
	join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select distinct ter.id, test.id, 1, 0
	from bik_dqc_test_parameters dtp
	join bik_node test on test.obj_id='t:' + convert(varchar(max), dtp.test_id) and test.is_deleted=0 and test.linked_node_id is null
	join bik_node ter on ter.obj_id=convert(varchar(max), dtp.parameter_name) and ter.is_deleted=0 and ter.linked_node_id is null

	exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Teradata'

	----------------- Test <-> BO, TeradataModel

	create table #tmp_jo (teradata int, dqc int);
	create table #tmp_to_insert (src int, dst int);

	declare @teradataTree int = dbo.fn_tree_id_by_code('Teradata');
	declare @dqcTree int = dbo.fn_tree_id_by_code('DQC');

	insert into #tmp_jo(teradata, dqc)
	select src.id, dst.id 
	from bik_joined_objs as jo
	join bik_node as src on src.id = jo.src_node_id
	join bik_node as dst on dst.id = jo.dst_node_id
	where src.tree_id = @teradataTree and src.is_deleted = 0 and src.linked_node_id is null
	and dst.tree_id = @dqcTree and dst.is_deleted = 0 and dst.linked_node_id is null
	and jo.type = 1 -- tylko automatyczne

	insert into #tmp_to_insert(src,dst)
	select distinct bn.id, tmp.dqc 
	from #tmp_jo as tmp
	join bik_joined_objs as jo on tmp.teradata = jo.dst_node_id 
	join bik_node as bn on bn.id = jo.src_node_id
	where bn.is_deleted = 0 and linked_node_id is null and bn.id <> tmp.dqc
	and bn.tree_id in (select report_tree_id from bik_sapbo_server where is_deleted = 0 
					   union all 
					   select universe_tree_id from bik_sapbo_server where is_deleted = 0 
					   union all 
					   select connection_tree_id from bik_sapbo_server where is_deleted = 0
					   union all 
					   select id from bik_tree where code = 'TeradataDataModel')
	and jo.type = 1 -- tylko automatyczne

	exec sp_prepare_bik_joined_objs_tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select src, dst, 1, 0
	from #tmp_to_insert as tmp

	insert into tmp_bik_joined_objs(src_node_id,dst_node_id,type,inherit_to_descendants)
	select dst, src, 1, 0
	from #tmp_to_insert as tmp
	
	exec sp_move_bik_joined_objs_tmp 'DQCTestSuccess,DQCTestFailed,DQCTestInactive', '@Reports, @ObjectUniverses, @Connections, @TeradataDataModel'

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_joined_objs_confluence_connections]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_joined_objs_confluence_connections]
as
begin

	declare @roleForNodeId int = (select id from bik_role_for_node where code = 'AuthorOfObject')
    declare @roleNode int = (select node_id from bik_role_for_node where id = @roleForNodeId)
    declare @userNodeKindId int = dbo.fn_node_kind_id_by_code('User')
    declare @userTreeId int = dbo.fn_tree_id_by_code('UserRoles')

    insert into bik_user_in_node (user_id, node_id, role_for_node_id, inherit_to_descendants, is_auxiliary, is_built_in)
    select us.id as user_node_id, ex.node_id as element_node_id, @roleForNodeId, 0, 0, 1
    from bik_confluence_extradata ex
    inner join bik_system_user as sus on ex.author = sus.sso_login
    inner join bik_user as us on sus.user_id = us.id
    left join bik_user_in_node as uin on uin.user_id = us.id and uin.node_id = ex.node_id and uin.role_for_node_id = @roleForNodeId
    where uin.id is null

    insert into bik_node(parent_node_id, node_kind_id, name, tree_id, linked_node_id)
    select distinct @roleNode, @userNodeKindId, ubn.name, @userTreeId, ubn.id
    from bik_confluence_extradata ex
    inner join bik_system_user as sus on ex.author = sus.sso_login
    inner join bik_user as us on sus.user_id = us.id
    inner join bik_node as ubn on ubn.id = us.node_id and ubn.is_deleted = 0
    left join bik_node as bn on bn.parent_node_id = @roleNode and bn.linked_node_id = ubn.id and bn.is_deleted = 0
    where bn.id is null

    exec sp_node_init_branch_id  null, @roleNode

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_childless_folders]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_delete_childless_folders]
as
begin
	declare @treeOfTrees int = dbo.fn_tree_id_by_code('TreeOfTrees');
	
	declare @candidates bik_unique_not_null_id_table_type;
	
	insert into @candidates(id)
	select id
	from (
	select *, (select count(*) from bik_node where parent_node_id = bn.id) as childCount 
	from bik_node bn 
	where bn.tree_id = @treeOfTrees
	and substring(obj_id, 1, 1) not in ('#', '@', '$', '&')
	) x
	where x.childCount = 0;
	
	delete from bik_node_name_chunk where node_id in (select id from @candidates);
	
	exec sp_delete_node_history_by_node_ids @candidates;
	delete from bik_node where id in (select id from @candidates);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[fn_node_id_by_tree_code_and_obj_id]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_node_id_by_tree_code_and_obj_id](@tree_code varchar(256), @obj_id varchar(900))
returns int
as
begin
  return (select id from bik_node where tree_id = dbo.fn_tree_id_by_code(@tree_code) and obj_id = @obj_id)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_add_menu_node]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_add_menu_node](@parent_obj_id varchar(255), @name varchar(255), @obj_id varchar(255))
as
begin
  declare @parent_node_id int = dbo.fn_node_id_by_tree_code_and_obj_id('TreeOfTrees', @parent_obj_id)
  declare @tree_of_trees_id int = dbo.fn_tree_id_by_code('TreeOfTrees')
  declare @visual_order int = coalesce((select max(visual_order) from bik_node 
    where (@parent_node_id is null and parent_node_id is null or @parent_node_id is not null and parent_node_id = @parent_node_id) and
    is_deleted = 0 and tree_id = @tree_of_trees_id), 0) + 1	
  declare @is_deleted int = (select is_deleted from bik_node where tree_id = @tree_of_trees_id  and obj_id = @parent_obj_id)
	
	
  insert into bik_node (parent_node_id, node_kind_id, tree_id, name, obj_id, visual_order,is_deleted)
  values (@parent_node_id, dbo.fn_node_kind_id_by_code('MenuItem'), @tree_of_trees_id, @name, @obj_id, @visual_order, coalesce(@is_deleted,0))
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_system_user_group_paths]    Script Date: 09/28/2016 11:08:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fn_get_system_user_group_paths](@group_id int)
returns @res table (path varchar(max) not null, branch_ids varchar(max) not null)
as
begin
  declare @tab table (group_id int not null, path varchar(max) not null, branch_ids varchar(max) not null);
  declare @tab2 table (group_id int not null, path varchar(max) not null, branch_ids varchar(max) not null);

  insert into @tab (group_id, path, branch_ids) select id, name, cast(id as varchar(20)) from bik_system_user_group where id = @group_id;

  declare @rc int;

  while 1 = 1 begin

    delete from @tab2;

    insert into @res (path, branch_ids)
    select t.path, t.branch_ids
    from @tab t left join bik_system_group_in_group gig on t.group_id = gig.member_group_id
    where gig.id is null;

    delete from @tab
    where group_id not in (select member_group_id from bik_system_group_in_group);

    insert into @tab2 (group_id, path, branch_ids)
    select sug.id, sug.name + ' » ' + t.path, cast(sug.id as varchar(20)) + '|' + t.branch_ids
    from @tab t left join (bik_system_group_in_group gig
      inner join bik_system_user_group sug on sug.id = gig.group_id) on t.group_id = gig.member_group_id;

    set @rc = @@rowcount;

    if @rc = 0 break;  

    delete from @tab;

    insert into @tab (group_id, path, branch_ids) select group_id, path, branch_ids from @tab2;

  end;

  insert into @res (path, branch_ids) select path, branch_ids from @tab;

  return;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_add_fk_if_missing]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_add_fk_if_missing] @on_table_name sysname, @on_column_name sysname, @referenced_table_name sysname, @diag_level int = 1 as
begin
  set nocount on;

  declare @fk_name varchar(max) = (select top 1 foreignkey_name from vw_ww_foreignkeys where Table_Name = @on_table_name and Name = @on_column_name and ReferencedTableName = @referenced_table_name);

  if @fk_name is not null begin
    if @diag_level > 0 begin
	  print 'table ' + @on_table_name + ' already has FK on column ' + @on_column_name + ' referencing table ' + @referenced_table_name + ', FK name: ' + @fk_name;
	end;
    return;
  end;

  set @fk_name = 'fk_' + @on_table_name + '_' + @on_column_name;

  if len(@fk_name) > 128 or exists (select 1 from vw_ww_foreignkeys where foreignkey_name = @fk_name) begin
    set @fk_name = substring(@fk_name, 1, 128 - 37) + '_' + replace(newid(), '-', '_');
  end;

  declare @pk_column_name sysname = (select cols from dbo.fn_index_info(replace(replace(@referenced_table_name, '_', '[_]'), '%', '[%]')) 
    where idx_level = 3 and charindex(',', cols) = 0);
	
  declare @sql_txt varchar(max) = 'alter table ' + @on_table_name + ' add constraint ' + @fk_name + ' foreign key (' + @on_column_name + ') references ' +
    @referenced_table_name + ' (' + @pk_column_name + ')';

  exec(@sql_txt);
 
  if @diag_level > 0 begin
    print 'FK on table ' + @on_table_name + ' column ' + @on_column_name + ' referencing table ' + @referenced_table_name + ' column ' + @pk_column_name + ' created, FK name: ' + @fk_name;
  end;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_rows_marked_for_delete_packed]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_delete_rows_marked_for_delete_packed] @pack_size int = 10000, @diag_level int = 1 as
begin
  set nocount on;

  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_marked_for_delete_packed: start'; exec sp_log_msg @log_msg;
  end;
  
  declare @create_idxes int = 1, @drop_marker_columns_when_done int = 1;

  declare @rm4d_col_name sysname = '__row_marked_for_delete__';

  declare @tdtns_names table (table_name sysname not null primary key);

  insert into @tdtns_names (table_name)
  select o.name from sys.objects o inner join sys.columns c on o.object_id = c.object_id
  where o.type = 'u' and o.is_ms_shipped = 0 and c.name = @rm4d_col_name;

  if not exists (select 1 from @tdtns_names) begin
    set @log_msg = 'there is no table with column ' + @rm4d_col_name + ', nothing to do'; exec sp_log_msg @log_msg;
    return;
  end;

  declare @sql_txt nvarchar(max) = null;

  if @create_idxes <> 0 begin

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end +
      'create index idx_' + substring(table_name + '_' + @rm4d_col_name, 1, 128-36-4-1) + '_' + replace(newid(), '-', '_') + ' on ' +
        table_name + ' (' + @rm4d_col_name + ')'
    from 
      @tdtns_names tt
    where not exists (
      select 1 
      from 
        sys.indexes ind inner join 
        sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
        sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
        sys.tables t on ind.object_id = t.object_id 
      where 
        t.is_ms_shipped = 0 
        and col.name = @rm4d_col_name
        and t.name = tt.table_name
        and ic.index_column_id = 1
    );

    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': after create indexes for initial tables'; exec sp_log_msg @log_msg;
    end;
  end;

  set @sql_txt = null;

  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ' union all ' end +
    'select ' + quotename(table_name, '''') + ' as table_name, (select count(1) from ' + quotename(table_name) + ' where ' + @rm4d_col_name + ' = 1) as marked_count'
  from @tdtns_names;

  set @sql_txt = 'insert into #tdtns (table_name, marked_count) ' + @sql_txt;

  create table #tdtns (table_name sysname not null primary key, marked_count int not null);

  exec(@sql_txt);

  declare @tab_cnt_with_no_rows_to_del int;

  if @diag_level > 0 begin
    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end + '  ' + table_name + ': ' + cast(marked_count as varchar(20)) from #tdtns where marked_count > 0 order by marked_count desc, table_name;

    set @tab_cnt_with_no_rows_to_del = (select count(1) from #tdtns where marked_count = 0);

    set @log_msg = cast(sysdatetime() as varchar(23)) + ': initial list of tables with rows to delete:
' + coalesce(@sql_txt, '  <none>') + '
number of tables with column ' + @rm4d_col_name + ' but without rows to delete: ' + cast(@tab_cnt_with_no_rows_to_del as varchar(20)); exec sp_log_msg @log_msg;
  end;

  /*

  z kolejki weź tabelę z wierszami do usunięcia (t1) - czyli tabela t1 wylatuje z kolejki
  dla każdej tabeli t2, która ma klucz obcy do t1:
    jeżeli w tabeli t2 brakuje kolumny @rm4d_col_name - dodaj taką kolumnę
    update wartości @rm4d_col_name na 1 dla wierszy t2 powiązanych z wierszami t1, które będą usunięte z t1, ale nie są jeszcze oznaczone w t2 jako do usunięcia
    jeżeli zaktualizowano jakiekolwiek wiersze (@@rowcount > 0), to dodaj tabelę t2 do kolejki (o ile nie ma jej w kolejce)

  */

  create table #tdtns_queue (table_name sysname not null primary key);

  insert into #tdtns_queue select table_name from #tdtns where marked_count > 0;

  declare @rc int;

  declare @t1 sysname;

  while 1 = 1 begin
    set @t1 = (select top 1 table_name from #tdtns_queue);
    if @t1 is null break;

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': starting propagation of row marking for table: ' + @t1; exec sp_log_msg @log_msg;
    end;

    delete from #tdtns_queue where table_name = @t1;

    declare @cur cursor;

    --if @diag_level > 0 select table_name, name, referencedcolumn from vw_ww_foreignkeys where ReferencedTableName = @t1;

    set @cur = cursor for select table_name, name, referencedcolumn from vw_ww_foreignkeys where ReferencedTableName = @t1;

    open @cur;

    declare @t2 sysname, @fk_col sysname, @pk_col sysname;

    fetch next from @cur into @t2, @fk_col, @pk_col;

    while @@fetch_status = 0 begin
      if @diag_level > 0 begin
        set @log_msg = cast(sysdatetime() as varchar(23)) + ': propagating row marking from table: ' + @t1 + ' (' + @pk_col + ') to table ' + @t2 + ' (' + @fk_col + ')'; exec sp_log_msg @log_msg;
      end;

      declare @must_create_idx int;

      if not exists (select 1 from sys.columns where object_id = object_id(@t2) and name = @rm4d_col_name) begin
        if @diag_level > 0 begin
          set @log_msg = cast(sysdatetime() as varchar(23)) + ': adding missing column ' + @rm4d_col_name + ' on table ' + @t2; exec sp_log_msg @log_msg;
        end;

        set @sql_txt = 'alter table ' + @t2 + ' add ' + @rm4d_col_name + ' int not null default 0;';
        exec(@sql_txt);

        set @must_create_idx = 1;
      end;
      else begin
        if @create_idxes <> 0
          set @must_create_idx = case when exists (select 1 
            from 
              sys.indexes ind inner join 
              sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
              sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
              sys.tables t on ind.object_id = t.object_id 
            where 
              t.is_ms_shipped = 0 
              and col.name = @rm4d_col_name
              and t.name = @t2
              and ic.index_column_id = 1) then 0 else 1 end;
      end;      

      if @create_idxes <> 0 and @must_create_idx <> 0 begin
        set @sql_txt = 'create index idx_' + substring(@t2 + '_' + @rm4d_col_name, 1, 128-36-4-1) + '_' + replace(newid(), '-', '_') + ' on ' +
          @t2 + ' (' + @rm4d_col_name + ')';

        exec(@sql_txt);

        if @diag_level > 0 begin
          set @log_msg = cast(sysdatetime() as varchar(23)) + ': after create index on column ' + @rm4d_col_name + ' of table ' + @t2; exec sp_log_msg @log_msg;
        end;
      end;

      if @diag_level > 0 begin
        set @log_msg = cast(sysdatetime() as varchar(23)) + ': updating column ' + @rm4d_col_name + ' of table ' + @t2; exec sp_log_msg @log_msg;
      end;

      set @sql_txt = 'update top (' + cast(@pack_size as varchar(20)) + ') t2 set ' + @rm4d_col_name + ' = 1 from ' + @t2 + ' t2 inner join ' + @t1 + ' t1 on t2.' + @fk_col + ' = t1.' + @pk_col + 
        ' where t2.' + @rm4d_col_name + ' = 0 and t1.' + @rm4d_col_name + ' = 1';

      declare @upd_iter_cnt int, @total_rc int;

      set @total_rc = 0;

      while 1 = 1 begin
        exec(@sql_txt);
        set @rc = @@rowcount;
        set @total_rc += @rc;
        if @rc < @pack_size break;
      end;

      if @diag_level > 0 begin
        set @log_msg = cast(sysdatetime() as varchar(23)) + ': finished updating column ' + @rm4d_col_name + ' of table ' + @t2 + ', affected rows: ' + 
        cast(@total_rc as varchar(20)); exec sp_log_msg @log_msg;
      end;

      if @rc > 0 begin
        if not exists (select 1 from #tdtns_queue where table_name = @t2)
          insert into #tdtns_queue (table_name) values (@t2);
      end;

      if not exists (select 1 from #tdtns where table_name = @t2)
        insert into #tdtns (table_name, marked_count) values (@t2, @rc);
      else
        update #tdtns set marked_count = marked_count + @rc;

      fetch next from @cur into @t2, @fk_col, @pk_col;
    end;
    
    close @cur;
  end;

  if @diag_level > 0 begin
    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end + '  ' + table_name + ': ' + cast(marked_count as varchar(20)) from #tdtns where marked_count > 0 order by marked_count desc, table_name;

    set @tab_cnt_with_no_rows_to_del = (select count(1) from #tdtns where marked_count = 0);

    set @log_msg = cast(sysdatetime() as varchar(23)) + ': final list of tables with rows to delete:
' + coalesce(@sql_txt, '  <none>') + '
number of tables with column ' + @rm4d_col_name + ' but without rows to delete: ' + cast(@tab_cnt_with_no_rows_to_del as varchar(20)); exec sp_log_msg @log_msg;
  end;

  --select * from vw_ww_foreignkeys where ReferencedTableName in (select table_name from #tdtns);

  declare @sql_txt_enable_fks nvarchar(max) = null;

  set @sql_txt = null;

  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'alter table ' + table_name + ' nocheck constraint ' + ForeignKey_Name,
    @sql_txt_enable_fks = case when @sql_txt_enable_fks is null then '' else @sql_txt_enable_fks + ';
  ' end + 'alter table ' + table_name + ' with check check constraint ' + ForeignKey_Name
  from vw_ww_foreignkeys where ReferencedTableName in (select table_name from #tdtns where marked_count > 0);

  if @sql_txt is not null exec(@sql_txt);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': after disable foreign keys referencing tables with rows to delete'; exec sp_log_msg @log_msg;
  end;

  set @sql_txt = null;

  select @sql_txt = case when @sql_txt is null then '' else @sql_txt + ';
  ' end + 'while 1 = 1 begin delete top (' + cast(@pack_size as varchar(20)) + ') from ' + table_name + ' where ' + @rm4d_col_name + ' = 1; 
set @rc = @@rowcount; if @rc < ' + cast(@pack_size as varchar(20)) + ' break; end;' +
    case when @diag_level > 0 then '
set @log_msg = cast(sysdatetime() as varchar(23)) + '': after deleting rows from table ' + table_name + '''; exec sp_log_msg @log_msg;' else '' end
  from #tdtns where marked_count > 0;
  
  if @sql_txt is not null begin
    set @sql_txt = 'declare @rc int, @log_msg varchar(max); ' + @sql_txt;

    exec(@sql_txt);
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': after deleting rows from all tables'; exec sp_log_msg @log_msg;
  end;

  if @sql_txt_enable_fks is not null exec(@sql_txt_enable_fks);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': after enabling foreign keys referencing tables with deleted rows'; exec sp_log_msg @log_msg;
  end;

  if @drop_marker_columns_when_done <> 0 begin

    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
' end + 'drop index ' + idx_name + ' on ' + table_name
    from (
      select distinct t.name as table_name, ind.name as idx_name
      from 
        sys.indexes ind 
        inner join sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
        inner join sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id 
        inner join sys.tables t on ind.object_id = t.object_id 
      where 
        t.is_ms_shipped = 0 
        and col.name = @rm4d_col_name
    ) x;

    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': after drop indexes on column ' + @rm4d_col_name + ' on all tables'; exec sp_log_msg @log_msg;
    end;

    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
    ' end + 'alter table ' + t.name + ' drop constraint ' + df.name
    from sys.default_constraints df
    inner join sys.tables t on df.parent_object_id = t.object_id
    inner join sys.columns c on df.parent_object_id = c.object_id and df.parent_column_id = c.column_id
    where c.name = @rm4d_col_name;

    --set @log_msg = coalesce(@sql_txt, '<no constraints to drop>');
    if @sql_txt is not null exec(@sql_txt);

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': after drop constraints on column ' + @rm4d_col_name + ' of all tables'; exec sp_log_msg @log_msg;
    end;

    set @sql_txt = null;

    select @sql_txt = case when @sql_txt is null then '' else @sql_txt + '
  ' end + 'alter table ' + table_name + ' drop column ' + @rm4d_col_name
    from #tdtns;

    --set @log_msg = coalesce(@sql_txt, '<no columns to drop>');
    if @sql_txt is not null exec(@sql_txt);
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_marked_for_delete_packed: end'; exec sp_log_msg @log_msg;
  end;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_filter_users_group]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_filter_users_group] (@txt varchar(max)) as
begin
	set nocount on;
	
	create table #res (id varchar(900) not null, unique(id));
	create table #tmp (id varchar(900) not null, unique(id));
	create table #candidate (id varchar(900) not null, unique(id));
	
	insert into #res(id)
	select x.branch_ids
	from bik_system_user_group as gr
	cross apply dbo.fn_get_system_user_group_paths(gr.id) x
	where gr.name like '%' + @txt + '%' and gr.is_deleted = 0
	
	insert into #candidate
	select id from (
	select distinct case when charindex('|', res.id) = 0 then null else substring(res.id, 1, dbo.fn_last_charindex('|', res.id, null)-1) end as id
	from #res as res) x
	where x.id is not null and x.id <> ''
	
	while exists(select * from #candidate)
	begin
		insert into #tmp(id)
		select id 
		from #candidate as can
		
		delete from #candidate
	
		insert into #res(id)
		select tmp.id 
		from #tmp as tmp
		left join #res as res on res.id = tmp.id
		where res.id is null and tmp.id is not null
		
		insert into #candidate(id)
		select id from (
			select distinct case when charindex('|', tmp.id) = 0 then null else substring(tmp.id, 1, dbo.fn_last_charindex('|', tmp.id, null)-1) end as id
			from #tmp as tmp
		) x 
		where x.id is not null and x.id <> ''
		
		delete from #tmp
	end;
	
	select x.id as obj_id, x.parent_id, x.path_id as id, gr.domain, gr.name, case when exists(select * from bik_system_group_in_group where group_id = gr.id and is_deleted = 0) then 0 else 1 end as has_no_children
	from (
		select res.id, case when charindex('|', res.id) = 0 then null else substring(res.id, 1, dbo.fn_last_charindex('|', res.id, null)-1) end as parent_id, 
		case when charindex('|', res.id) = 0 then res.id else substring(res.id, dbo.fn_last_charindex('|', res.id, null) + 1, len(res.id) - dbo.fn_last_charindex('|', res.id, null)) end as path_id
		from #res as res
	) x left join bik_system_user_group as gr on gr.id = x.path_id

end;
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_rows_from_bik_node_packed]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_delete_rows_from_bik_node_packed] @pack_size int = 10000, @diag_level int = 1 as
begin
  set nocount on;

  declare @create_idxes int = 1;

  declare @log_msg varchar(max);

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_from_bik_node_packed: start, pack size: ' + cast(@pack_size as varchar(20)); exec sp_log_msg @log_msg;
  end;

  declare @has_trigger int = 0;

  if exists (select 1 from sys.triggers tr inner join sys.tables t on t.object_id = tr.parent_id
    where tr.is_disabled = 0 and t.name = 'bik_node' and tr.name = 'trg_bik_node_chunking') begin
    set @has_trigger = 1;
    exec('disable trigger trg_bik_node_chunking on bik_node');  

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': trigger trg_bik_node_chunking disabled'; exec sp_log_msg @log_msg;
    end;
  end;

  if not exists (select 1 from sys.columns where object_id = object_id('bik_node') and name = '__row_marked_for_delete__') begin
    exec('alter table bik_node add __row_marked_for_delete__ int not null default 0');

    if @diag_level > 0 begin
      set @log_msg = cast(sysdatetime() as varchar(23)) + ': column __row_marked_for_delete__ added to table bik_node'; exec sp_log_msg @log_msg;
    end;
  end;  
 
  declare @sql_txt nvarchar(max);

  if @create_idxes <> 0 begin
    if not exists (
      select 1 
      from 
        sys.indexes ind inner join 
        sys.index_columns ic on  ind.object_id = ic.object_id and ind.index_id = ic.index_id inner join 
        sys.columns col on ic.object_id = col.object_id and ic.column_id = col.column_id inner join 
        sys.tables t on ind.object_id = t.object_id 
      where 
        t.is_ms_shipped = 0 
        and col.name = '__row_marked_for_delete__'
        and t.name = 'bik_node'
        and ic.index_column_id = 1
    ) begin
      set @sql_txt = 'create index idx_bik_node___row_marked_for_delete__' + replace(newid(), '-', '_') + ' on bik_node (__row_marked_for_delete__)';
      exec (@sql_txt);

        if @diag_level > 0 begin
          set @log_msg = cast(sysdatetime() as varchar(23)) + ': index on bik_node (__row_marked_for_delete__) created'; exec sp_log_msg @log_msg;
        end;
    end;
  end;

  set @sql_txt = 'update top (' + cast(@pack_size as varchar(20)) + ') bik_node set __row_marked_for_delete__ = 1 where is_deleted <> 0 and __row_marked_for_delete__ = 0';

  declare @rc int;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': starting to mark bik_node rows'; exec sp_log_msg @log_msg;
  end;

  declare @total_marked_count int = 0;

  while 1 = 1 begin
    exec(@sql_txt);

    set @rc = @@rowcount;

    set @total_marked_count += @rc;

    if @rc < @pack_size break;
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': marked bik_node rows: ' + cast(@total_marked_count as varchar(20)) + ', before delete'; exec sp_log_msg @log_msg;
  end;

  /* -- nie usuwaj z modrzewia
  declare @tree_of_trees_id int = (select id from bik_tree where code = 'TreeOfTrees')
  set @sql_txt = 'update bik_node set __row_marked_for_delete__ = 0 where tree_id = ' + cast(@tree_of_trees_id as varchar(20));
  --print @sql_txt;
  exec (@sql_txt);
  */

  exec sp_delete_rows_marked_for_delete_packed @pack_size, @diag_level;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': finished delete rows from bik_node and referencing tables'; exec sp_log_msg @log_msg;
  end;

  if @has_trigger = 1 begin
    exec('enable trigger trg_bik_node_chunking on bik_node');  
  end;

  if @diag_level > 0 begin
    set @log_msg = cast(sysdatetime() as varchar(23)) + ': sp_delete_rows_from_bik_node_packed: end'; exec sp_log_msg @log_msg;
  end;
end;
GO
/****** Object:  StoredProcedure [dbo].[sp_create_bo_actual_instance_function]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_create_bo_actual_instance_function](@serverId int)
as
begin

	declare @reportTreeId int, @universeTreeId int, @connectionTreeId int;
	select @reportTreeId = report_tree_id, @universeTreeId = universe_tree_id, @connectionTreeId = connection_tree_id from bik_sapbo_server where id = @serverId
    
    -- drop if exists
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_report_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_report_tree_id]');
	exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_universe_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_universe_tree_id]');
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_connection_tree_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_connection_tree_id]');
    exec('if exists (select * from sys.objects where object_id = OBJECT_ID(N''[dbo].[fn_get_bo_actual_server_id]'') and type in (N''FN''))
			drop function [dbo].[fn_get_bo_actual_server_id]');
	-- create
	declare @sql varchar(max);
	
	set @sql = 'create function fn_get_bo_actual_report_tree_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@reportTreeId) + ';
		end';
		
	exec(@sql)
	
	set @sql = 'create function fn_get_bo_actual_universe_tree_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@universeTreeId) + ';
		end'
	exec(@sql)
	
	set @sql = 'create function fn_get_bo_actual_connection_tree_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@connectionTreeId) + ';
		end';
		
	exec(@sql)
	
	set @sql = 'create function fn_get_bo_actual_server_id()
		returns int
		as
		begin
			return ' + convert(varchar(10),@serverId) + ';
		end'
	exec(@sql)
end;
GO
/****** Object:  Table [dbo].[bik_sapbo_user]    Script Date: 09/28/2016 11:08:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bik_sapbo_user](
	[si_id] [int] NOT NULL,
	[name] [varchar](max) NULL,
	[server_instance_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uk_si_id_instance] UNIQUE NONCLUSTERED 
(
	[si_id] ASC,
	[server_instance_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_class]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_class](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[si_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[universe_id] [int] NOT NULL,
	[si_id_designer] [varchar](900) NULL,
	[si_parentid] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_aaa_universe_class_si_id_designer] UNIQUE NONCLUSTERED 
(
	[si_id_designer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_table]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_table](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[their_id] [int] NULL,
	[name] [varchar](255) NOT NULL,
	[is_alias] [int] NOT NULL,
	[is_derived] [int] NOT NULL,
	[original_table] [int] NULL,
	[sql_of_derived_table] [varchar](max) NULL,
	[sql_of_derived_table_with_alias] [varchar](max) NULL,
	[universe_id] [int] NULL,
	[universe_branch] [varchar](1000) NULL,
	[idt_connection_id] [int] NULL,
	[idt_id] [varchar](500) NULL,
	[idt_original_id] [varchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_obj_tables]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_obj_tables](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[universe_table_id] [int] NOT NULL,
	[universe_obj_id] [int] NOT NULL,
	[obj_type] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_obj]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_obj](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[text_of_select] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[universe_class_id] [int] NULL,
	[si_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[qualification] [varchar](50) NULL,
	[aggregate_function] [varchar](50) NULL,
	[text_of_where] [varchar](max) NULL,
	[obj_branch] [varchar](900) NULL,
	[si_parentid] [varchar](1000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_filter]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_filter](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[si_id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[where_clause] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[universe_class_id] [int] NULL,
	[filtr_branch] [varchar](900) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_delta]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_delta](
	[server_instance_id] [int] NOT NULL,
	[si_id] [int] NULL,
	[name] [varchar](max) NULL,
	[folderCUID] [varchar](max) NULL,
	[to_delete] [int] NULL,
	[to_load] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_connection_networklayer]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_connection_networklayer](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_connection_idt]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_connection_idt](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[si_id] [int] NOT NULL,
	[connetion_name] [varchar](900) NOT NULL,
	[connetion_path] [varchar](900) NOT NULL,
	[server] [varchar](255) NULL,
	[user_name] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[database_source] [varchar](500) NULL,
	[network_layer] [varchar](500) NOT NULL,
	[type] [varchar](255) NULL,
	[database_engine] [varchar](255) NULL,
	[client_number] [int] NULL,
	[language] [varchar](10) NULL,
	[system_number] [varchar](255) NULL,
	[system_id] [varchar](255) NULL,
UNIQUE NONCLUSTERED 
(
	[si_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_connection]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_connection](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[connetion_name] [varchar](1000) NOT NULL,
	[server] [varchar](255) NULL,
	[user_name] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[database_source] [varchar](500) NULL,
	[connetion_networklayer_id] [int] NOT NULL,
	[type] [varchar](255) NULL,
	[database_enigme] [varchar](255) NULL,
	[client_number] [int] NULL,
	[language] [varchar](10) NULL,
	[system_number] [varchar](255) NULL,
	[system_id] [varchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_column]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_column](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[universe_table_id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[type] [varchar](255) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe_class]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe_class](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[si_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[universe_id] [int] NOT NULL,
	[si_id_designer] [varchar](900) NULL,
	[si_parentid] [varchar](1000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_universe]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_universe](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[unique_id] [varchar](300) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[file_name] [varchar](255) NOT NULL,
	[author] [varchar](120) NULL,
	[universe_connetion_id] [int] NULL,
	[path] [varchar](500) NOT NULL,
	[global_props_si_id] [int] NULL,
	[statistic] [varchar](500) NULL,
	[update_ts] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_report_object]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_report_object](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[query_id] [int] NULL,
	[universe_CUID] [varchar](300) NULL,
	[object_name] [varchar](255) NULL,
	[class_name] [varchar](255) NULL,
	[universe_name] [varchar](max) NULL,
	[wrong_universe_cuid] [varchar](100) NULL,
	[report_si_id] [int] NOT NULL,
	[report_update] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_query]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_query](
	[server_instance_id] [int] NOT NULL,
	[id] [int] NOT NULL,
	[name] [varchar](max) NULL,
	[id_text] [varchar](max) NULL,
	[filtr_text] [varchar](max) NULL,
	[sql_text] [varchar](max) NULL,
	[universe_cuid] [varchar](max) NULL,
	[report_node_id] [int] NULL,
	[universe_name] [varchar](max) NULL,
	[wrong_universe_cuid] [varchar](100) NULL,
	[report_si_id] [int] NOT NULL,
	[report_update] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_app_universe]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_app_universe](
	[server_instance_id] [int] NOT NULL,
	[si_id] [int] NOT NULL,
	[si_name] [varchar](max) NULL,
	[si_parent_folder_cuid] [varchar](max) NULL,
	[si_update_ts] [datetime] NULL,
	[si_cuid] [varchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ami_app_dsl_metadatafile]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ami_app_dsl_metadatafile](
	[server_instance_id] [int] NOT NULL,
	[si_id] [int] NOT NULL,
	[si_name] [varchar](max) NULL,
	[si_parent_folder_cuid] [varchar](max) NULL,
	[si_update_ts] [datetime] NULL,
	[si_cuid] [varchar](max) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_column]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_column](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[universe_table_id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[type] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UNIQ_unique_name_id_aaa_universe_column] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[universe_table_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_obj_tables]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_obj_tables](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[universe_table_id] [int] NOT NULL,
	[universe_obj_id] [int] NOT NULL,
	[obj_type] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[universe_table_id] ASC,
	[universe_obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_obj]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_obj](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[text_of_select] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[universe_class_id] [int] NULL,
	[si_id] [int] NOT NULL,
	[parent_id] [int] NULL,
	[qualification] [varchar](50) NULL,
	[aggregate_function] [varchar](50) NULL,
	[text_of_where] [varchar](max) NULL,
	[obj_branch] [varchar](900) NULL,
	[si_parentid] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_aaa_universe_obj_obj_branch] UNIQUE NONCLUSTERED 
(
	[obj_branch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[aaa_universe_filter]    Script Date: 09/28/2016 11:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa_universe_filter](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[si_id] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [varchar](max) NULL,
	[where_clause] [varchar](max) NULL,
	[type] [varchar](255) NOT NULL,
	[universe_class_id] [int] NULL,
	[filtr_branch] [varchar](900) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ_aaa_universe_filter_filtr_branch] UNIQUE NONCLUSTERED 
(
	[filtr_branch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_sapbo_object_table]
as
begin

	declare @tree_id int = dbo.fn_get_bo_actual_universe_tree_id();

	declare @universe_table_kind int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @universe_alias_table_kind int = dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @universe_derived_table_kind int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
	declare @filtr_kind int = dbo.fn_node_kind_id_by_code('Filter');
	declare @detail_nki int = dbo.fn_node_kind_id_by_code('Detail');
	declare @attribute_nki int = dbo.fn_node_kind_id_by_code('Attribute');
	declare @dimension_nki int = dbo.fn_node_kind_id_by_code('Dimension');
	declare @measure_nki int = dbo.fn_node_kind_id_by_code('Measure');

	delete from bik_sapbo_object_table
	from bik_sapbo_object_table bo
	inner join bik_node obj on obj.id = bo.object_node_id
	inner join bik_node tab on tab.id = bo.table_node_id
	where obj.tree_id = @tree_id
	or tab.tree_id = @tree_id

	insert into bik_sapbo_object_table(object_node_id,table_node_id)
	select *
	from (
	select case when uot.obj_type = 1 then bn.id else bn3.id end as obj_node_ide, coalesce(bn2.id, bnidttable.id) as table_node_id
	from aaa_universe_obj_tables uot
	inner join aaa_universe_table ut on uot.universe_table_id = ut.id
	left join bik_node bn2 on bn2.tree_id = @tree_id and bn2.obj_id = convert(varchar(256), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
	and bn2.is_deleted = 0
	and bn2.linked_node_id is null
	and bn2.node_kind_id in (@universe_table_kind, @universe_alias_table_kind, @universe_derived_table_kind)
	left join bik_node bnidttable on bnidttable.tree_id = @tree_id and bnidttable.obj_id = convert(varchar(256), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
	and bnidttable.is_deleted = 0
	and bnidttable.linked_node_id is null
	and bnidttable.node_kind_id in (@universe_table_kind, @universe_alias_table_kind, @universe_derived_table_kind)
	left join aaa_universe_obj uo on uot.universe_obj_id = uo.id
	left join bik_node bn on bn.tree_id = @tree_id
	and bn.obj_id = uo.obj_branch
	and bn.is_deleted = 0
	and bn.linked_node_id is null
	and bn.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki, @attribute_nki)
	left join aaa_universe_filter uf on uot.universe_obj_id = uf.id
	left join bik_node bn3 on bn3.tree_id = @tree_id 
	and bn3.obj_id = uf.filtr_branch
	and bn3.is_deleted = 0
	and bn3.linked_node_id is null
	and bn3.node_kind_id = @filtr_kind
	) x
	where obj_node_ide is not null and table_node_id is not null

	insert into bik_joined_objs(src_node_id,dst_node_id,type)
	select object_node_id, table_node_id, 1 
	from bik_sapbo_object_table a 
	inner join bik_node obj on obj.id = a.object_node_id and obj.tree_id = @tree_id
	inner join bik_node tab on tab.id = a.table_node_id and tab.tree_id = @tree_id
	left join bik_joined_objs b on a.object_node_id = b.src_node_id and a.table_node_id = b.dst_node_id
	where b.id is null

	insert into bik_joined_objs(src_node_id,dst_node_id,type)
	select table_node_id, object_node_id, 1 
	from bik_sapbo_object_table a
	inner join bik_node obj on obj.id = a.object_node_id and obj.tree_id = @tree_id
	inner join bik_node tab on tab.id = a.table_node_id and tab.tree_id = @tree_id
	left join bik_joined_objs b on a.table_node_id = b.src_node_id and a.object_node_id = b.dst_node_id
	where b.id is null

end;--procedure
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]    Script Date: 09/28/2016 11:08:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_insert_into_bik_node_objects_from_Designer]
as
begin--begin procedure

	declare @diag_level int = 0;

	----wybranie id odpowiedniego drzewa
	declare @tree_id int = dbo.fn_get_bo_actual_universe_tree_id();
	declare @connectionTreeId int = dbo.fn_get_bo_actual_connection_tree_id();
	
	----wybranie id odpowiedniego kind'a
	declare @class_kind_id int = dbo.fn_node_kind_id_by_code('UniverseClass');
	declare @folder_universe_node_kind_id int = dbo.fn_node_kind_id_by_code('UniversesFolder');
	declare @detail_nki int = dbo.fn_node_kind_id_by_code('Detail');
	declare @attribute_nki int = dbo.fn_node_kind_id_by_code('Attribute');
	declare @dimension_nki int = dbo.fn_node_kind_id_by_code('Dimension');
	declare @measure_nki int = dbo.fn_node_kind_id_by_code('Measure');
	declare @filtr_nki int = dbo.fn_node_kind_id_by_code('Filter');
	declare @folder_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTablesFolder');
	declare @table_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseTable');
	declare @table_alias_node_kind_id int =  dbo.fn_node_kind_id_by_code('UniverseAliasTable');
	declare @table_derived_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseDerivedTable');
	declare @universe_kind_id int = dbo.fn_node_kind_id_by_code('Universe');
	declare @universe_unx_kind_id int = dbo.fn_node_kind_id_by_code('DSL.MetaDataFile');
	declare @universe_column_node_kind_id int = dbo.fn_node_kind_id_by_code('UniverseColumn');
		
	create table #tmpNodes (si_id varchar(1000) collate DATABASE_DEFAULT, si_parentid varchar(1000) collate DATABASE_DEFAULT, si_kind_id int, si_name varchar(max) collate DATABASE_DEFAULT, si_description varchar(max) collate DATABASE_DEFAULT);

	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tabeli tymczasowej #tmpNodes'
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Class
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select si_id_designer, si_parentid, @class_kind_id, name, description
	from aaa_universe_class  
	
	
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Obj
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select obj_branch, si_parentid, 
			case
				when qualification = 'dsDetailObject' then @detail_nki
				when qualification = 'dsMeasureObject' then @measure_nki
				when qualification = 'dsDimensionObject' then @dimension_nki
				when qualification = 'Attribute' then @attribute_nki
				end as node_kind_id,
		 name, description
	from aaa_universe_obj
							 						 
	--wrzucam do tabeli tymczasowej wszystkie dane z Universe_filrt
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select uf.filtr_branch,	uc.si_id_designer, @filtr_nki, uf.name, coalesce(ltrim(rtrim(uf.description)), '') 
	from aaa_universe_filter uf 
	inner join aaa_universe_class uc on uf.universe_class_id = uc.id

	--wrzucam katalogi do których są wrzucane tabele
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)
	select convert(varchar(10),global_props_si_id) + '|tableCatalog', convert(varchar(10),global_props_si_id), @folder_node_kind_id, 'Tabele'
	from aaa_universe 

							 
	----wrzucam do tymasowej tabeli wszystkie dane z Universe_Table	
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name)		
	select convert(varchar(256), their_id) + '|' + convert(varchar(256), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table
	where their_id is not null
	union all -- tabele z IDT
	select convert(varchar(256), idt_id) + '|' + convert(varchar(256), universe_branch), convert(varchar(1000), universe_branch) + '|tableCatalog', 
	case
				when is_alias = 1 then @table_alias_node_kind_id
				when is_derived = 1 then @table_derived_node_kind_id
				else @table_node_kind_id
				end as node_kind_id, name 
	from aaa_universe_table
	where idt_id is not null
	
	
	----wrzucam do tymczasowej tabeli wszystkie dane z Universe_Column
	insert into #tmpNodes(si_id, si_parentid, si_kind_id, si_name, si_description)
	select convert(varchar(256), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(256), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch), @universe_column_node_kind_id, uc.name, REPLACE(REPLACE(uc.type,'ds',''),'Column','')
	from aaa_universe_table ut 
	inner join aaa_universe_column uc on ut.id = uc.universe_table_id
	where ut.their_id is not null
	union all -- kolumny z IDT
	select convert(varchar(256), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch) + '|' + uc.name, 
		convert(varchar(256), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch), @universe_column_node_kind_id, uc.name, uc.type
	from aaa_universe_table ut 
	inner join aaa_universe_column uc on ut.id = uc.universe_table_id
	where ut.idt_id is not null
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update bik_node'
	
	--aktualizacja istniejących węzłów w drzewie							
	update bik_node 
	set parent_node_id = null, node_kind_id = #tmpNodes.si_kind_id, name = ltrim(rtrim(#tmpNodes.si_name)),
		is_deleted = 0, descr = #tmpNodes.si_description
	from #tmpNodes 
	where bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id and bik_node.linked_node_id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Insert into bik_node'
	
	--dorzucanie nowych węzłów w drzewie
	insert into bik_node (node_kind_id, name, tree_id, obj_id, descr)
	select #tmpNodes.si_kind_id, ltrim(rtrim(#tmpNodes.si_name)), @tree_id, #tmpNodes.si_id, #tmpNodes.si_description
	from #tmpNodes left join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where bik_node.id is null;	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update parentów'
	
	--uaktualnianie parentów w nodach
	update bik_node 
	set parent_node_id = bk.id
	from bik_node inner join #tmpNodes as pt on bik_node.tree_id = @tree_id and bik_node.obj_id = pt.si_id and bik_node.node_kind_id = pt.si_kind_id
		inner join bik_node bk on bk.tree_id = @tree_id and bk.obj_id = pt.si_parentid
		and  (
		 (pt.si_kind_id = @detail_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id = @dimension_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id = @measure_nki and (bk.node_kind_id=@detail_nki or bk.node_kind_id=@dimension_nki or bk.node_kind_id = @measure_nki or bk.node_kind_id = @class_kind_id)) or
		 (pt.si_kind_id = @attribute_nki and (bk.node_kind_id = @dimension_nki or bk.node_kind_id = @measure_nki)) or
		 (pt.si_kind_id = @filtr_nki and bk.node_kind_id=@class_kind_id) or
		 (pt.si_kind_id = @universe_column_node_kind_id and (bk.node_kind_id=@table_node_kind_id or bk.node_kind_id=@table_alias_node_kind_id or bk.node_kind_id=@table_derived_node_kind_id)) or
		 (pt.si_kind_id = @class_kind_id and (bk.node_kind_id = @class_kind_id or bk.node_kind_id in (@universe_kind_id, @universe_unx_kind_id)))or
		 ((pt.si_kind_id = @table_node_kind_id or pt.si_kind_id=@table_alias_node_kind_id or pt.si_kind_id=@table_derived_node_kind_id) and bk.node_kind_id=@folder_node_kind_id)or
		 (pt.si_kind_id = @folder_node_kind_id and bk.node_kind_id in (@universe_kind_id, @universe_unx_kind_id))
		 )
	where bk.linked_node_id is null and bik_node.parent_node_id is null;
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Usuwanie nodów'
		
	--usuwanie nodów
	update bik_node
	set is_deleted = 1
	from bik_node left join #tmpNodes on bik_node.obj_id = #tmpNodes.si_id and bik_node.node_kind_id = #tmpNodes.si_kind_id
	where #tmpNodes.si_id is null and bik_node.tree_id = @tree_id and bik_node.node_kind_id in (@class_kind_id, @detail_nki, @dimension_nki, @measure_nki,
	  @filtr_nki, @attribute_nki, @folder_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id, @table_node_kind_id, @universe_column_node_kind_id);
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Usuwanie błędnych danych w Universum'
	
	-- usuwanie obiektow, wrzuconych ze zlym parentem --> blad w Universum  
	update bik_node
	set is_deleted = 1
	where parent_node_id is null and tree_id = @tree_id and node_kind_id not in (@universe_kind_id, @universe_unx_kind_id, @folder_universe_node_kind_id)
	
	drop table #tmpNodes;
	--uzupełnianie danych w tabelach extra 
	--dla object
	create table #tmpObjExtra(text_of_select varchar(max) collate DATABASE_DEFAULT, text_of_where varchar(max) collate DATABASE_DEFAULT, type varchar(155) collate DATABASE_DEFAULT, aggregate_function varchar(50) collate DATABASE_DEFAULT, node_id int);
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tableli tymczasowej #tmpObjExtra'
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select univ_obj.text_of_select, univ_obj.text_of_where, univ_obj.type, univ_obj.aggregate_function, bik_node.id
	from aaa_universe_obj univ_obj join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = univ_obj.obj_branch 
		and bik_node.node_kind_id in (@measure_nki, @detail_nki, @dimension_nki, @attribute_nki)-- = @obj_node_kind_id;
	
	insert into #tmpObjExtra(text_of_select, text_of_where, type, aggregate_function, node_id)
	select null, uf.where_clause, uf.type, null, bik_node.id
	from aaa_universe_filter uf inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = uf.filtr_branch and bik_node.node_kind_id = @filtr_nki
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wrzucanie do tabel bik_sapbo...'
	
	update bik_sapbo_universe_object
	set text_of_select = #tmpObjExtra.text_of_select, text_of_where = #tmpObjExtra.text_of_where, type = #tmpObjExtra.type,  
		           aggregate_function = #tmpObjExtra.aggregate_function 
	from #tmpObjExtra join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id;
	
	insert into bik_sapbo_universe_object(text_of_select, text_of_where, type, aggregate_function, node_id)
	select #tmpObjExtra.text_of_select,#tmpObjExtra.text_of_where, #tmpObjExtra.type, #tmpObjExtra.aggregate_function, #tmpObjExtra.node_id 
	from #tmpObjExtra left join bik_sapbo_universe_object on bik_sapbo_universe_object.node_id = #tmpObjExtra.node_id
	where bik_sapbo_universe_object.id is null;

	drop table #tmpObjExtra;
	
	--dla tabel z designera
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = convert(varchar(512), ut.original_table), sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, node_id = bik_node.id, type = ucn.name
	from aaa_universe_table ut 
		inner join aaa_universe u on ut.universe_id = u.id 
		inner join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		inner join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.their_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0 
		and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.their_id is not null
	
	--dla tabel z idt
	update bik_sapbo_universe_table
	set is_alias = ut.is_alias, is_derived = ut.is_derived, original_table = convert(varchar(512), ut.idt_original_id), sql_of_derived_table = ut.sql_of_derived_table, 
		sql_of_derived_table_with_alias = ut.sql_of_derived_table_with_alias, node_id = bik_node.id, type = conidt.network_layer
	from aaa_universe_table ut 
		inner join aaa_universe_connection_idt conidt on ut.idt_connection_id = conidt.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.idt_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0 
		and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.idt_id is not null
	
	insert into bik_sapbo_universe_table(is_alias, is_derived, original_table, sql_of_derived_table, sql_of_derived_table_with_alias, their_id, node_id, type)
	select ut.is_alias, ut.is_derived, convert(varchar(512), ut.original_table), ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, convert(varchar(512), ut.their_id), 
					bik_node.id as node_id, ucn.name
	from aaa_universe_table ut 
		inner join aaa_universe u on ut.universe_id = u.id 
		inner join aaa_universe_connection uc on u.universe_connetion_id = uc.id
		inner join aaa_universe_connection_networklayer ucn on uc.connetion_networklayer_id = ucn.id 
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		left join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512),ut.their_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0
		and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.their_id is not null
	union all -- dla idt
	select ut.is_alias, ut.is_derived, convert(varchar(512), ut.idt_original_id), ut.sql_of_derived_table, ut.sql_of_derived_table_with_alias, convert(varchar(512), ut.idt_id), 
					bik_node.id as node_id, conidt.network_layer
	from aaa_universe_table ut
		inner join aaa_universe_connection_idt conidt on ut.idt_connection_id = conidt.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		left join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512),ut.idt_id) and bsut.node_id = bik_node.id
	where bik_node.linked_node_id is null and bik_node.is_deleted = 0
		and bsut.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
		and ut.idt_id is not null

	--uzupełnianie name_for_teradata: Teradata i Oracle (network_layer na razie tylko z 3.1)
	update bik_sapbo_universe_table
	set name_for_teradata = replace(bik_node.name,'.', '|') + '|'
	from bik_sapbo_universe_table 
	inner join bik_node on bik_sapbo_universe_table.node_id = bik_node.id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI', 'ODBC Drivers') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 0

	update bik_sapbo_universe_table
	set name_for_teradata = tabo.name_for_teradata
	from bik_sapbo_universe_table 
	inner join bik_sapbo_universe_table tabo on bik_sapbo_universe_table.original_table = tabo.their_id 
	inner join bik_node bn on bn.id = bik_sapbo_universe_table.node_id
	inner join bik_node bn2 on bn2.id = tabo.node_id
	where bik_sapbo_universe_table.type in ('Teradata', 'ODBC', 'Oracle OCI', 'ODBC Drivers') and bik_sapbo_universe_table.is_derived = 0 and bik_sapbo_universe_table.is_alias = 1 
	and bn2.parent_node_id = bn.parent_node_id
	
	update bik_sapbo_universe_table
	set schema_name = left(name_for_teradata, charindex('|',name_for_teradata))
	where type in ('Teradata', 'ODBC', 'Oracle OCI', 'ODBC Drivers') and is_derived = 0
	
	--dla kolumn z designera
	update bik_sapbo_universe_column
	set /*name = uc.name, */type = uc.type--, table_id = bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.their_id) and bsut.node_id = bik_node.id
		inner join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.their_id is not null
	
	--dla kolumn z idt
	update bik_sapbo_universe_column
	set /*name = uc.name, */type = uc.type--, table_id = bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.idt_id) and bsut.node_id = bik_node.id
		inner join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.idt_id is not null
	
	insert into bik_sapbo_universe_column(name, type, table_id)
	select uc.name, uc.type, bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.their_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.their_id) and bsut.node_id = bik_node.id
		left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bsuc.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.their_id is not null
	union all -- dla idt
	select uc.name, uc.type, bik_node.id
	from aaa_universe_column uc 
		inner join aaa_universe_table ut on uc.universe_table_id = ut.id
		inner join bik_node on bik_node.tree_id = @tree_id and bik_node.obj_id = convert(varchar(512), ut.idt_id) + '|' + convert(varchar(256), ut.universe_branch)
		inner join bik_sapbo_universe_table bsut on bsut.their_id = convert(varchar(512), ut.idt_id) and bsut.node_id = bik_node.id
		left join bik_sapbo_universe_column bsuc on uc.name = bsuc.name and bsuc.table_id = bik_node.id
	where linked_node_id is null and bik_node.is_deleted = 0
	and bsuc.id is null and bik_node.node_kind_id in (@table_node_kind_id, @table_alias_node_kind_id, @table_derived_node_kind_id)
	and ut.idt_id is not null

	--delete from bik_sapbo_universe_table i bik_sapbo_universe_column nie jest potrzebne bo w bik_node jest ustawiont isDeleted na 1:)
	
	delete from bik_sapbo_universe_connection 
	from bik_sapbo_universe_connection conn
	inner join bik_node bn on bn.id = conn.node_id
	where bn.tree_id = @connectionTreeId
	
	-- dla polaczen z Designera
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection.server, aaa_universe_connection.user_name, aaa_universe_connection.password, aaa_universe_connection.database_source, 
		aaa_universe_connection_networklayer.name, bik_node.id,
		aaa_universe_connection.type, aaa_universe_connection.database_enigme, aaa_universe_connection.client_number, aaa_universe_connection.language,
		aaa_universe_connection.system_number, aaa_universe_connection.system_id
	from aaa_universe_connection 
		inner join aaa_universe_connection_networklayer on aaa_universe_connection.connetion_networklayer_id = aaa_universe_connection_networklayer.id
		inner join bik_node on bik_node.tree_id = @connectionTreeId and aaa_universe_connection.connetion_name = bik_node.name and bik_node.is_deleted = 0
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_sapbo_universe_connection.id is null;
	
	-- dla polaczen z IDT
	insert into bik_sapbo_universe_connection (server, user_name, password, database_source, connetion_networklayer_name, node_id, type, database_engine, client_number, language, system_number, system_id)
	select aaa_universe_connection_idt.server, aaa_universe_connection_idt.user_name, aaa_universe_connection_idt.password, aaa_universe_connection_idt.database_source, 
		aaa_universe_connection_idt.network_layer, bik_node.id,
		aaa_universe_connection_idt.type, aaa_universe_connection_idt.database_engine, aaa_universe_connection_idt.client_number, aaa_universe_connection_idt.language,
		aaa_universe_connection_idt.system_number, aaa_universe_connection_idt.system_id
	from aaa_universe_connection_idt
		inner join bik_node on bik_node.tree_id = @connectionTreeId and convert(varchar(10),aaa_universe_connection_idt.si_id) = convert(varchar(10),bik_node.obj_id) and bik_node.is_deleted = 0
		left join bik_sapbo_universe_connection on bik_sapbo_universe_connection.node_id = bik_node.id
	where bik_sapbo_universe_connection.id is null;	
	
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Update extradaty'
	
	-- dodanie statystyk dla światów obiektów	
	update bik_sapbo_extradata
	set statistic = u.statistic
	from bik_sapbo_extradata 
		inner join bik_node bn on bn.id = bik_sapbo_extradata.node_id
		inner join aaa_universe u on convert(varchar(30),u.global_props_si_id) = bn.obj_id
	where bn.node_kind_id = @universe_kind_id
		and bn.tree_id = @tree_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Ustawianie visual_order'
	
	-- ustawianie visual order dla folderów tabel
	update bik_node set visual_order = -1 where node_kind_id = @folder_node_kind_id
	
	if @diag_level > 0 print cast(sysdatetime() as varchar(23)) + ': Wykonywanie procedur: sp_delete_linked i sp_node_init_branch'
		
	exec sp_delete_linked_nodes_where_orignal_is_deleted;	
	exec sp_node_init_branch_id @tree_id, null;
end;
GO
/****** Object:  Default [DF__aaa_mssql__visua__670242E8]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_mssql] ADD  DEFAULT ((0)) FOR [visual_order]
GO
/****** Object:  Default [DF__aaa_mssql__is_pr__36A2FB62]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_mssql_index] ADD  DEFAULT ((0)) FOR [is_primary_key]
GO
/****** Object:  Default [DF__aaa_mssql__parti__37971F9B]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_mssql_index] ADD  DEFAULT ((0)) FOR [partition_ordinal]
GO
/****** Object:  Default [DF__aaa_postg__is_pr__105A255E]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_postgres_index] ADD  DEFAULT ((0)) FOR [is_primary_key]
GO
/****** Object:  Default [DF__aaa_query__repor__01E6240E]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_query] ADD  DEFAULT ((0)) FOR [report_si_id]
GO
/****** Object:  Default [DF__aaa_repor__repor__02DA4847]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_report_object] ADD  DEFAULT ((0)) FOR [report_si_id]
GO
/****** Object:  Default [DF__amg_node__visual__03F44E79]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[amg_node] ADD  DEFAULT ((0)) FOR [visual_order]
GO
/****** Object:  Default [DF__bik_activ__is_de__3BCE9348]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_active_directory] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_activ__is_de__40934865]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_active_directory_group] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_activ__is_de__4557FD82]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_active_directory_ou] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_admin__is_su__3E40EB9F]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_admin_backup_file] ADD  DEFAULT ((0)) FOR [is_success]
GO
/****** Object:  Default [DF__bik_app_p__is_ed__612DA50B]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_app_prop] ADD  DEFAULT ((0)) FOR [is_editable]
GO
/****** Object:  Default [DF__bik_attr___is_de__4CC81A60]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_category] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_attr___is_bu__6DF302B1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_category] ADD  DEFAULT ((0)) FOR [is_built_in]
GO
/****** Object:  Default [DF__bik_attr___is_de__2C70C12A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_def] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_attr___is_bu__6CFEDE78]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_def] ADD  DEFAULT ((0)) FOR [is_built_in]
GO
/****** Object:  Default [DF__bik_attr___displ__56B793AE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_def] ADD  DEFAULT ((0)) FOR [display_as_number]
GO
/****** Object:  Default [DF__bik_attr___visua__59940059]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_def] ADD  DEFAULT ((0)) FOR [visual_order]
GO
/****** Object:  Default [DF__bik_attr___is_vi__768848B2]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system] ADD  DEFAULT ((1)) FOR [is_visible]
GO
/****** Object:  Default [DF__bik_attr___is_de__74D2067A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system_linked] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_attri__is_de__2B7C9CF1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_attri__is_de__2D64E563]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute_linked] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_autho__is_ad__3173A4F1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_authors] ADD  DEFAULT ((0)) FOR [is_admin]
GO
/****** Object:  Default [DF__bik_blog__node_i__5D873159]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog] ADD  DEFAULT (NULL) FOR [node_id]
GO
/****** Object:  Default [DF__bik_confl__is_as__3D3D4C5D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_extradata] ADD  DEFAULT ((1)) FOR [is_as_root]
GO
/****** Object:  Default [DF__bik_crr_l__is_de__5DEEA122]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_crr_label] ADD  DEFAULT ((0)) FOR [is_default_for_new_node]
GO
/****** Object:  Default [DF__bik_custo__selec__1B96CDE0]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role] ADD  DEFAULT ((0)) FOR [selector_mode]
GO
/****** Object:  Default [DF__bik_data___finis__5F6F79CB]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_data_load_log] ADD  DEFAULT (NULL) FOR [finish_time]
GO
/****** Object:  Default [DF__bik_db_in__is_pr__31BB1929]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index] ADD  DEFAULT ((0)) FOR [is_primary_key]
GO
/****** Object:  Default [DF__bik_db_in__parti__32AF3D62]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index] ADD  DEFAULT ((0)) FOR [partition_ordinal]
GO
/****** Object:  Default [DF__bik_dqc_g____del__283AAE32]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqc_group] ADD  DEFAULT ((0)) FOR [__deleted]
GO
/****** Object:  Default [DF__bik_dqc_g____del__2375F915]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqc_group_test] ADD  DEFAULT ((0)) FOR [__deleted]
GO
/****** Object:  Default [DF__bik_dqc_r__finis__2CFF634F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqc_request] ADD  DEFAULT ((0)) FOR [finished_steps]
GO
/****** Object:  Default [DF__bik_dqc_r____del__2DF38788]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqc_request] ADD  DEFAULT ((0)) FOR [__deleted]
GO
/****** Object:  Default [DF__bik_dqc_t____del__1EB143F8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqc_test] ADD  DEFAULT ((0)) FOR [__deleted]
GO
/****** Object:  Default [DF__bik_dqc_t__test___32299A80]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqc_test] ADD  DEFAULT ((0)) FOR [test_type]
GO
/****** Object:  Default [DF__bik_dqm_d__is_de__19797B44]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_def3000tr_profile_file] ADD  DEFAULT ((0)) FOR [is_def_file]
GO
/****** Object:  Default [DF__bik_dqm_r__error__69FF724C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request] ADD  DEFAULT ((0)) FOR [error_percentage]
GO
/****** Object:  Default [DF__bik_dqm_r__is_de__6AF39685]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_dqm_s__statu__5B7C48CB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_schedule] ADD  DEFAULT ((0)) FOR [status_flag]
GO
/****** Object:  Default [DF__bik_dqm_t__error__5ABD2EBC]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test] ADD  DEFAULT ((0)) FOR [error_threshold]
GO
/****** Object:  Default [DF__bik_dqm_t__is_ac__60760812]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test] ADD  DEFAULT ((1)) FOR [is_active]
GO
/****** Object:  Default [DF__bik_dqm_t__is_de__616A2C4B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_dqm_t__is_de__41BC76C8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_file___is_ac__424686AD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_file_system] ADD  DEFAULT ((0)) FOR [is_active]
GO
/****** Object:  Default [DF__bik_file___downl__433AAAE6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_file_system] ADD  DEFAULT ((0)) FOR [download_files]
GO
/****** Object:  Default [DF_bik_fts_processed_obj_finished]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_processed_obj] ADD  CONSTRAINT [DF_bik_fts_processed_obj_finished]  DEFAULT ((0)) FOR [finished]
GO
/****** Object:  Default [DF__bik_fts_s__weigh__0078C350]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_stemmed_word] ADD  DEFAULT ((1.0)) FOR [weight]
GO
/****** Object:  Default [DF__bik_help__is_hid__1883C8BB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_help] ADD  DEFAULT ((0)) FOR [is_hidden]
GO
/****** Object:  Default [DF__bik_jdbc__is_act__2F33B239]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_jdbc] ADD  DEFAULT ((0)) FOR [is_active]
GO
/****** Object:  Default [DF__bik_joine__is_ma__4E775368]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_obj_attribute_linked] ADD  DEFAULT ((0)) FOR [is_main]
GO
/****** Object:  Default [DF__bik_joined__type__16F4B8DF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_objs] ADD  DEFAULT ((0)) FOR [type]
GO
/****** Object:  Default [DF__bik_joine__inher__3C7121A8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_objs] ADD  DEFAULT ((0)) FOR [inherit_to_descendants]
GO
/****** Object:  Default [DF__bik_lisa___acces__78362534]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_column] ADD  DEFAULT ((2)) FOR [access_level]
GO
/****** Object:  Default [DF__bik_lisa___valid__6A347C76]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_column] ADD  DEFAULT ((0)) FOR [validate_level]
GO
/****** Object:  Default [ble_dictionary_table_default]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_extradata] ADD  CONSTRAINT [ble_dictionary_table_default]  DEFAULT ('') FOR [dictionary_table]
GO
/****** Object:  Default [ble_dictionary_db_default]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_extradata] ADD  CONSTRAINT [ble_dictionary_db_default]  DEFAULT ('') FOR [dictionary_db]
GO
/****** Object:  Default [ble_dictionary_table_physical_default]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_extradata] ADD  CONSTRAINT [ble_dictionary_table_physical_default]  DEFAULT ('') FOR [dictionary_table_physical]
GO
/****** Object:  Default [ble_dictionary_db_physical_default]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_extradata] ADD  CONSTRAINT [ble_dictionary_db_physical_default]  DEFAULT ('') FOR [dictionary_db_physical]
GO
/****** Object:  Default [DF__bik_lisa___is_de__7A55D5A6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_instance] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_lisa___time___7CFADA51]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_logs] ADD  DEFAULT (getdate()) FOR [time_added]
GO
/****** Object:  Default [DF__bik_metap__def_s__18DD0151]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_metapedia] ADD  DEFAULT ((0)) FOR [def_status]
GO
/****** Object:  Default [DF__bik_mssql__visua__37DA124F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql] ADD  DEFAULT ((0)) FOR [visual_order]
GO
/****** Object:  Default [DF__bik_mssql__is_pr__062AAC90]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_index] ADD  DEFAULT ((0)) FOR [is_primary_key]
GO
/****** Object:  Default [DF__bik_mssql__parti__071ED0C9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_index] ADD  DEFAULT ((0)) FOR [partition_ordinal]
GO
/****** Object:  Default [DF__bik_news__date_a__5900A288]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Default [DF__bik_node__is_del__2C24DFEF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_node__is_bui__4FF82C11]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [is_built_in]
GO
/****** Object:  Default [DF__bik_node__is_chu__1BD9698D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [chunked_ver]
GO
/****** Object:  Default [DF__bik_node__disabl__6C9F57B7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [disable_linked_subtree]
GO
/****** Object:  Default [DF__bik_node__search__462F960C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [search_rank]
GO
/****** Object:  Default [DF__bik_node__vote_s__35C42419]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [vote_sum]
GO
/****** Object:  Default [DF__bik_node__vote_c__36B84852]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [vote_cnt]
GO
/****** Object:  Default [DF__bik_node__vote_v__37AC6C8B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [vote_val]
GO
/****** Object:  Default [DF__bik_node__visual__420F8A9F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node] ADD  DEFAULT ((0)) FOR [visual_order]
GO
/****** Object:  Default [DF__bik_node___date___06F0B6B5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Default [DF__bik_node_c__spid__6837256B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value] ADD  DEFAULT (@@spid) FOR [spid]
GO
/****** Object:  Default [DF__bik_node___is_fo__51E07483]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind] ADD  DEFAULT ((0)) FOR [is_folder]
GO
/****** Object:  Default [DF__bik_node___searc__4723BA45]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind] ADD  DEFAULT ((0)) FOR [search_rank]
GO
/****** Object:  Default [DF__bik_node___is_le__10C0452E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind] ADD  DEFAULT ((0)) FOR [is_leaf_for_statistic]
GO
/****** Object:  Default [DF__bik_node___is_br__7A6AE43F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind_4_tree_kind] ADD  DEFAULT ((0)) FOR [is_branch]
GO
/****** Object:  Default [DF__bik_node___is_le__7B5F0878]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind_4_tree_kind] ADD  DEFAULT ((0)) FOR [is_leaf]
GO
/****** Object:  Default [DF__bik_note__date_a__592C9090]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_note] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Default [DF__bik_objec__date___78E8BB9D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_author] ADD  DEFAULT (sysdatetime()) FOR [date_added]
GO
/****** Object:  Default [DF__bik_objec__date___144AB1D3]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Default [DF__bik_objec__chang__17271E7E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ('') FOR [changed_attrs]
GO
/****** Object:  Default [DF__bik_objec__joine__181B42B7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ('') FOR [joined_obj_ids_added]
GO
/****** Object:  Default [DF__bik_objec__joine__190F66F0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ('') FOR [joined_obj_ids_deleted]
GO
/****** Object:  Default [DF__bik_objec__new_c__1A038B29]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ((0)) FOR [new_comment_cnt]
GO
/****** Object:  Default [DF__bik_objec__vote___1AF7AF62]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ((0)) FOR [vote_val_delta]
GO
/****** Object:  Default [DF__bik_objec__vote___1BEBD39B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ((0)) FOR [vote_cnt_delta]
GO
/****** Object:  Default [DF__bik_objec__dqc_t__1CDFF7D4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ((0)) FOR [dqc_test_fail_cnt]
GO
/****** Object:  Default [DF__bik_objec__is_pa__25753DD5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex] ADD  DEFAULT ((0)) FOR [is_pasted]
GO
/****** Object:  Default [DF__bik_plain__is_re__6FD84733]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_plain_file] ADD  DEFAULT ((0)) FOR [is_remote]
GO
/****** Object:  Default [DF__bik_plain__is_ac__71C08FA5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_plain_file] ADD  DEFAULT ((0)) FOR [is_active]
GO
/****** Object:  Default [DF__bik_role___node___10DE467D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_role_for_node] ADD  DEFAULT ((0)) FOR [node_id]
GO
/****** Object:  Default [DF__bik_role_f__code__723BBE0E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_role_for_node] ADD  DEFAULT (NULL) FOR [code]
GO
/****** Object:  Default [DF__bik_sapbo__with___35743D9E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server] ADD  DEFAULT ((1)) FOR [with_users]
GO
/****** Object:  Default [DF__bik_sapbo__check__366861D7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server] ADD  DEFAULT ((1)) FOR [check_rights]
GO
/****** Object:  Default [DF__bik_sapbo__is_ac__3A38F2BB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server] ADD  DEFAULT ((0)) FOR [is_active]
GO
/****** Object:  Default [DF__bik_sapbo__is_de__3B2D16F4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_sapbo__is_bo__63E0B34E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server] ADD  DEFAULT ((0)) FOR [is_bo40]
GO
/****** Object:  Default [DF__bik_searc__searc__168B6FE2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_searchable_attr_props] ADD  DEFAULT ((1)) FOR [search_weight]
GO
/****** Object:  Default [DF__bik_search__type__177F941B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_searchable_attr_props] ADD  DEFAULT ((1)) FOR [type]
GO
/****** Object:  Default [DF__bik_servi__durat__6B93F4AA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_service_request] ADD  DEFAULT ((-1)) FOR [duration_millis]
GO
/****** Object:  Default [DF__bik_spid_s__spid__6372704E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_spid_source] ADD  DEFAULT (@@spid) FOR [spid]
GO
/****** Object:  Default [DF__bik_stati__event__6566796D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic] ADD  DEFAULT (CONVERT([date],sysdatetime(),0)) FOR [event_date]
GO
/****** Object:  Default [DF__bik_stati__event__665A9DA6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic] ADD  DEFAULT (sysdatetime()) FOR [event_datetime]
GO
/****** Object:  Default [DF__bik_stati__event__091F2366]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext] ADD  DEFAULT (CONVERT([date],sysdatetime(),0)) FOR [event_date]
GO
/****** Object:  Default [DF__bik_stati__event__0A13479F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext] ADD  DEFAULT (sysdatetime()) FOR [event_datetime]
GO
/****** Object:  Default [DF__bik_syste__is_de__2E74982A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_group_in_group] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_syste__is_ac__38E54BF0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user] ADD  DEFAULT ((1)) FOR [is_disabled]
GO
/****** Object:  Default [DF__bik_syste__date___5CD1336C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user] ADD  DEFAULT (sysdatetime()) FOR [date_added]
GO
/****** Object:  Default [DF__bik_syste__is_se__31DB14BA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user] ADD  DEFAULT ((0)) FOR [is_send_mails]
GO
/****** Object:  Default [DF__bik_syste__is_bu__211A9D0C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_group] ADD  DEFAULT ((1)) FOR [is_built_in]
GO
/****** Object:  Default [DF__bik_syste__is_de__220EC145]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_group] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_syste__is_de__3ADA6F0F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_tree__tree_k__60639E04]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree] ADD  DEFAULT (NULL) FOR [tree_kind]
GO
/****** Object:  Default [DF__bik_tree__is_hid__0EABB37E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree] ADD  DEFAULT ((0)) FOR [is_hidden]
GO
/****** Object:  Default [DF__bik_tree__is_in___14648CD4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree] ADD  DEFAULT ((0)) FOR [is_in_ranking]
GO
/****** Object:  Default [DF__bik_tree__branch__11B46967]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree] ADD  DEFAULT ((1)) FOR [branch_level_for_statistics]
GO
/****** Object:  Default [DF__bik_tree__is_bui__2D5C83DC]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree] ADD  DEFAULT ((0)) FOR [is_built_in]
GO
/****** Object:  Default [DF__bik_tree___is_de__2897CEBF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_kind] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
/****** Object:  Default [DF__bik_tree___allow__298BF2F8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_kind] ADD  DEFAULT ((0)) FOR [allow_linking]
GO
/****** Object:  Default [DF__bik_tutor__movie__7B98DF0A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tutorial] ADD  DEFAULT (NULL) FOR [movie]
GO
/****** Object:  Default [DF__bik_user__node_i__5AAAC4AE]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user] ADD  DEFAULT (NULL) FOR [node_id]
GO
/****** Object:  Default [DF__bik_user___inher__27AC0C0D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node] ADD  DEFAULT ((0)) FOR [inherit_to_descendants]
GO
/****** Object:  Default [DF__bik_user___is_au__2301572E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node] ADD  DEFAULT ((0)) FOR [is_auxiliary]
GO
/****** Object:  Default [DF__bik_user___is_bu__7E809812]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node] ADD  DEFAULT ((0)) FOR [is_built_in]
GO
/****** Object:  Default [DF__bik_user___tree___6E6B2D2A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right] ADD  DEFAULT (NULL) FOR [tree_id]
GO
/****** Object:  Default [DF__dbau_log__date_a__60AB123C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_log] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Default [DF__dbau_log__spid__62935AAE]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_log] ADD  DEFAULT (@@spid) FOR [spid]
GO
/****** Object:  Default [DF__dbau_proc__date___59FE14AD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_process] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Default [DF__dbau_proce__spid__5AF238E6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_process] ADD  DEFAULT (@@spid) FOR [spid]
GO
/****** Object:  Default [DF__dbau_proc__statu__5BE65D1F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_process] ADD  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__log_msg__date_ad__24B623C6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[log_msg] ADD  DEFAULT (getdate()) FOR [date_added]
GO
/****** Object:  Check [CHECK_obj_type]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_obj_tables]  WITH CHECK ADD  CONSTRAINT [CHECK_obj_type] CHECK  (([obj_type]=(2) OR [obj_type]=(1)))
GO
ALTER TABLE [dbo].[aaa_universe_obj_tables] CHECK CONSTRAINT [CHECK_obj_type]
GO
/****** Object:  Check [CK__aaa_unive__is_al__0EE982C3]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD CHECK  (([is_alias]=(1) OR [is_alias]=(0)))
GO
/****** Object:  Check [CK__aaa_unive__is_al__36BFC568]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD CHECK  (([is_alias]=(1) OR [is_alias]=(0)))
GO
/****** Object:  Check [CK__aaa_unive__is_de__0FDDA6FC]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD CHECK  (([is_derived]=(1) OR [is_derived]=(0)))
GO
/****** Object:  Check [CK__aaa_unive__is_de__37B3E9A1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD CHECK  (([is_derived]=(1) OR [is_derived]=(0)))
GO
/****** Object:  Check [CK__bik_joined__type__3267C92A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_objs]  WITH CHECK ADD CHECK  (([type]=(0) OR [type]=(1) OR [type]=(2)))
GO
/****** Object:  Check [CK__bik_joined__type__38A80DDA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_objs]  WITH CHECK ADD CHECK  (([type]=(0) OR [type]=(1) OR [type]=(2)))
GO
/****** Object:  Check [CK__bik_sapbo__is_al__237A8B55]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_table]  WITH CHECK ADD CHECK  (([is_alias]=(1) OR [is_alias]=(0)))
GO
/****** Object:  Check [CK__bik_sapbo__is_al__399C3213]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_table]  WITH CHECK ADD CHECK  (([is_alias]=(1) OR [is_alias]=(0)))
GO
/****** Object:  Check [CK__bik_sapbo__is_de__246EAF8E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_table]  WITH CHECK ADD CHECK  (([is_derived]=(1) OR [is_derived]=(0)))
GO
/****** Object:  Check [CK__bik_sapbo__is_de__3A90564C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_table]  WITH CHECK ADD CHECK  (([is_derived]=(1) OR [is_derived]=(0)))
GO
/****** Object:  Check [CK_bik_system_user_is_disabled]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user]  WITH CHECK ADD  CONSTRAINT [CK_bik_system_user_is_disabled] CHECK  (([is_disabled]=(1) OR [is_disabled]=(0)))
GO
ALTER TABLE [dbo].[bik_system_user] CHECK CONSTRAINT [CK_bik_system_user_is_disabled]
GO
/****** Object:  ForeignKey [fk_aaa_ids_for_report_object_node_id]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_ids_for_report]  WITH CHECK ADD  CONSTRAINT [fk_aaa_ids_for_report_object_node_id] FOREIGN KEY([object_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[aaa_ids_for_report] CHECK CONSTRAINT [fk_aaa_ids_for_report_object_node_id]
GO
/****** Object:  ForeignKey [fk_aaa_ids_for_report_query_node_id]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_ids_for_report]  WITH CHECK ADD  CONSTRAINT [fk_aaa_ids_for_report_query_node_id] FOREIGN KEY([query_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[aaa_ids_for_report] CHECK CONSTRAINT [fk_aaa_ids_for_report_query_node_id]
GO
/****** Object:  ForeignKey [fk_aaa_query_report_node_id]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_query]  WITH CHECK ADD  CONSTRAINT [fk_aaa_query_report_node_id] FOREIGN KEY([report_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[aaa_query] CHECK CONSTRAINT [fk_aaa_query_report_node_id]
GO
/****** Object:  ForeignKey [FK__aaa_terad__data___03844B5F]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_teradata_object]  WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_terad__data___3F550B69]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_teradata_object]  WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__40492FA2]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe]  WITH CHECK ADD FOREIGN KEY([universe_connetion_id])
REFERENCES [dbo].[aaa_universe_connection] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__7CCAD288]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe]  WITH CHECK ADD FOREIGN KEY([universe_connetion_id])
REFERENCES [dbo].[aaa_universe_connection] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__0283ABDE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_class]  WITH CHECK ADD FOREIGN KEY([universe_id])
REFERENCES [dbo].[aaa_universe] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__413D53DB]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_class]  WITH CHECK ADD FOREIGN KEY([universe_id])
REFERENCES [dbo].[aaa_universe] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__15968052]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_column]  WITH CHECK ADD FOREIGN KEY([universe_table_id])
REFERENCES [dbo].[aaa_universe_table] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__42317814]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_column]  WITH CHECK ADD FOREIGN KEY([universe_table_id])
REFERENCES [dbo].[aaa_universe_table] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__conne__43259C4D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_connection]  WITH CHECK ADD FOREIGN KEY([connetion_networklayer_id])
REFERENCES [dbo].[aaa_universe_connection_networklayer] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__conne__7711F932]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_connection]  WITH CHECK ADD FOREIGN KEY([connetion_networklayer_id])
REFERENCES [dbo].[aaa_universe_connection_networklayer] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__32BCCEE5]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_filter]  WITH CHECK ADD FOREIGN KEY([universe_class_id])
REFERENCES [dbo].[aaa_universe_class] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__4419C086]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_filter]  WITH CHECK ADD FOREIGN KEY([universe_class_id])
REFERENCES [dbo].[aaa_universe_class] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__083C8534]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_obj]  WITH CHECK ADD FOREIGN KEY([universe_class_id])
REFERENCES [dbo].[aaa_universe_class] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__450DE4BF]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_obj]  WITH CHECK ADD FOREIGN KEY([universe_class_id])
REFERENCES [dbo].[aaa_universe_class] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__1D37A21A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_obj_tables]  WITH CHECK ADD FOREIGN KEY([universe_table_id])
REFERENCES [dbo].[aaa_universe_table] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__460208F8]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_obj_tables]  WITH CHECK ADD FOREIGN KEY([universe_table_id])
REFERENCES [dbo].[aaa_universe_table] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__idt_c__46F62D31]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD FOREIGN KEY([idt_connection_id])
REFERENCES [dbo].[aaa_universe_connection_idt] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__idt_c__7FFFC951]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD FOREIGN KEY([idt_connection_id])
REFERENCES [dbo].[aaa_universe_connection_idt] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__0DF55E8A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD FOREIGN KEY([universe_id])
REFERENCES [dbo].[aaa_universe] ([id])
GO
/****** Object:  ForeignKey [FK__aaa_unive__unive__47EA516A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[aaa_universe_table]  WITH CHECK ADD FOREIGN KEY([universe_id])
REFERENCES [dbo].[aaa_universe] ([id])
GO
/****** Object:  ForeignKey [FK__ami_app_d__serve__48DE75A3]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_app_dsl_metadatafile]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_app_d__serve__697DED5A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_app_dsl_metadatafile]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_app_u__serve__4792EDD9]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_app_universe]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_app_u__serve__49D299DC]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_app_universe]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_query__serve__4AC6BE15]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_query]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_query__serve__605E9BA3]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_query]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_repor__serve__4BBAE24E]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_report_object]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_repor__serve__6246E415]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_report_object]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__497B364B]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4CAF0687]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4B637EBD]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_class]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4DA32AC0]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_class]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4D4BC72F]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_column]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4E974EF9]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_column]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4F340FA1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_connection]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__4F8B7332]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_connection]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__04C47E6E]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_connection_idt]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__507F976B]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_connection_idt]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__511C5813]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_connection_networklayer]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__5173BBA4]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_connection_networklayer]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__5267DFDD]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_delta]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__5304A085]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_delta]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__535C0416]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_filter]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__54ECE8F7]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_filter]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__5450284F]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_obj]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__56D53169]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_obj]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__55444C88]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_obj_tables]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__58BD79DB]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_obj_tables]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__563870C1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_table]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__ami_unive__serve__5AA5C24D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[ami_universe_table]  WITH CHECK ADD FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
/****** Object:  ForeignKey [FK__bik_admin__backu__3C58A32D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_admin_backup_file]  WITH CHECK ADD FOREIGN KEY([backup_log_id])
REFERENCES [dbo].[bik_admin_backup_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_admin__backu__572C94FA]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_admin_backup_file]  WITH CHECK ADD FOREIGN KEY([backup_log_id])
REFERENCES [dbo].[bik_admin_backup_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attac__node___433D4F71]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attachment]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attac__node___5820B933]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attachment]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___attr___5914DD6C]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_def]  WITH CHECK ADD FOREIGN KEY([attr_category_id])
REFERENCES [dbo].[bik_attr_category] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___attr___65DB1B54]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_def]  WITH CHECK ADD FOREIGN KEY([attr_category_id])
REFERENCES [dbo].[bik_attr_category] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___attr___5A0901A5]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system]  WITH CHECK ADD FOREIGN KEY([attr_id])
REFERENCES [dbo].[bik_attr_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___attr___75942479]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system]  WITH CHECK ADD FOREIGN KEY([attr_id])
REFERENCES [dbo].[bik_attr_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___node___5AFD25DE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___node___74A00040]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___attr___5BF14A17]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system_linked]  WITH CHECK ADD FOREIGN KEY([attr_system_id])
REFERENCES [dbo].[bik_attr_system] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___attr___72E9BE08]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system_linked]  WITH CHECK ADD FOREIGN KEY([attr_system_id])
REFERENCES [dbo].[bik_attr_system] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___node___5CE56E50]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system_linked]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attr___node___73DDE241]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attr_system_linked]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__attr___5DD99289]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute]  WITH CHECK ADD FOREIGN KEY([attr_def_id])
REFERENCES [dbo].[bik_attr_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__attr___66CF3F8D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute]  WITH CHECK ADD FOREIGN KEY([attr_def_id])
REFERENCES [dbo].[bik_attr_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__node___0263B04D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__node___5ECDB6C2]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__attri__0728656A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute_linked]  WITH CHECK ADD FOREIGN KEY([attribute_id])
REFERENCES [dbo].[bik_attribute] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__attri__5FC1DAFB]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute_linked]  WITH CHECK ADD FOREIGN KEY([attribute_id])
REFERENCES [dbo].[bik_attribute] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__node___081C89A3]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute_linked]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_attri__node___60B5FF34]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_attribute_linked]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_autho__node___61AA236D]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_authors]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_autho__node___661C775A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_authors]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_authors_user_id]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_authors]  WITH CHECK ADD  CONSTRAINT [FK_bik_authors_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_authors] CHECK CONSTRAINT [FK_bik_authors_user_id]
GO
/****** Object:  ForeignKey [FK__bik_blog__node_i__5E7B5592]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_blog__node_i__63926BDF]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_blog_user_id]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog]  WITH CHECK ADD  CONSTRAINT [FK_bik_blog_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_blog] CHECK CONSTRAINT [FK_bik_blog_user_id]
GO
/****** Object:  ForeignKey [FK__bik_blog___blog___194715A5]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog_linked]  WITH CHECK ADD FOREIGN KEY([blog_id])
REFERENCES [dbo].[bik_blog] ([id])
GO
/****** Object:  ForeignKey [FK__bik_blog___blog___657AB451]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog_linked]  WITH CHECK ADD FOREIGN KEY([blog_id])
REFERENCES [dbo].[bik_blog] ([id])
GO
/****** Object:  ForeignKey [FK__bik_blog___node___1A3B39DE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog_linked]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_blog___node___666ED88A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_blog_linked]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_confl__node___505020D1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_attachment]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_confl__node___6762FCC3]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_attachment]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_confl__node___3C492824]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_confl__node___685720FC]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_confluence_missing_joined_node_conf_node]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_missing_joined_node]  WITH CHECK ADD  CONSTRAINT [fk_bik_confluence_missing_joined_node_conf_node] FOREIGN KEY([conf_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_confluence_missing_joined_node] CHECK CONSTRAINT [fk_bik_confluence_missing_joined_node_conf_node]
GO
/****** Object:  ForeignKey [fk_bik_confluence_missing_joined_node_dst_node]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_missing_joined_node]  WITH CHECK ADD  CONSTRAINT [fk_bik_confluence_missing_joined_node_dst_node] FOREIGN KEY([dst_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_confluence_missing_joined_node] CHECK CONSTRAINT [fk_bik_confluence_missing_joined_node_dst_node]
GO
/****** Object:  ForeignKey [fk_bik_confluence_missing_joined_node_opt_dst_subnode_parent]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_missing_joined_node]  WITH CHECK ADD  CONSTRAINT [fk_bik_confluence_missing_joined_node_opt_dst_subnode_parent] FOREIGN KEY([opt_dst_subnode_parent_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_confluence_missing_joined_node] CHECK CONSTRAINT [fk_bik_confluence_missing_joined_node_opt_dst_subnode_parent]
GO
/****** Object:  ForeignKey [fk_bik_confluence_missing_joined_node_tree]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_confluence_missing_joined_node]  WITH CHECK ADD  CONSTRAINT [fk_bik_confluence_missing_joined_node_tree] FOREIGN KEY([dst_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
ALTER TABLE [dbo].[bik_confluence_missing_joined_node] CHECK CONSTRAINT [fk_bik_confluence_missing_joined_node_tree]
GO
/****** Object:  ForeignKey [FK__bik_crr_l__group__5B123477]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_crr_label]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_crr_l__group__6D1BD619]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_crr_label]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_crr_l__role___5A1E103E]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_crr_label]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[bik_custom_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_crr_l__role___6E0FFA52]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_crr_label]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[bik_custom_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__actio__3B0F7939]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_action_branch_grant]  WITH CHECK ADD FOREIGN KEY([action_id])
REFERENCES [dbo].[bik_node_action] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__actio__6F041E8B]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_action_branch_grant]  WITH CHECK ADD FOREIGN KEY([action_id])
REFERENCES [dbo].[bik_node_action] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__tree___392730C7]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_action_branch_grant]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__tree___6FF842C4]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_action_branch_grant]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__user___3A1B5500]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_action_branch_grant]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__user___70EC66FD]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_action_branch_grant]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__node___500A961F]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_rut_entry]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__node___71E08B36]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_rut_entry]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__tree___4F1671E6]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_rut_entry]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__tree___72D4AF6F]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_rut_entry]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__group__464C21BB]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__group__73C8D3A8]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__node___2EA9A254]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__node___74BCF7E1]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__role___2CC159E2]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[bik_custom_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__role___75B11C1A]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[bik_custom_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__tree___2DB57E1B]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__tree___76A54053]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__user___2BCD35A9]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_custo__user___7799648C]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_custom_right_role_user_entry]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_data___data___518B6EC8]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_data_load_log]  WITH CHECK ADD FOREIGN KEY([data_source_type_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_data___data___788D88C5]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_data_load_log]  WITH CHECK ADD FOREIGN KEY([data_source_type_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_data___log_i__210832FE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_data_load_log_details]  WITH CHECK ADD FOREIGN KEY([log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_data___log_i__7981ACFE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_data_load_log_details]  WITH CHECK ADD FOREIGN KEY([log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_co__colum__2184B160]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_column_extradata]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_co__colum__7A75D137]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_column_extradata]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_co__sourc__1F9C68EE]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_column_extradata]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_co__sourc__7B69F570]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_column_extradata]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_co__table__20908D27]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_column_extradata]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_co__table__7C5E19A9]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_column_extradata]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__objec__3F151447]    Script Date: 09/28/2016 11:08:54 ******/
ALTER TABLE [dbo].[bik_db_definition]  WITH CHECK ADD FOREIGN KEY([object_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__objec__7D523DE2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_definition]  WITH CHECK ADD FOREIGN KEY([object_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__sourc__3E20F00E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_definition]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__sourc__7E46621B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_definition]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__dep_n__395C3AF1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_dependency]  WITH CHECK ADD FOREIGN KEY([dep_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__dep_n__7F3A8654]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_dependency]  WITH CHECK ADD FOREIGN KEY([dep_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__ref_n__002EAA8D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_dependency]  WITH CHECK ADD FOREIGN KEY([ref_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__ref_n__386816B8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_dependency]  WITH CHECK ADD FOREIGN KEY([ref_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__sourc__0122CEC6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_dependency]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_de__sourc__3773F27F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_dependency]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__fk_co__0216F2FF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__fk_co__2831AEEF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__fk_ta__030B1738]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__fk_ta__273D8AB6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__pk_co__03FF3B71]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__pk_co__2A19F761]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__pk_ta__04F35FAA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__pk_ta__2925D328]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__sourc__05E783E3]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_fo__sourc__2649667D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_foreign_key]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_in__colum__06DBA81C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_in__colum__30C6F4F0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_in__sourc__07CFCC55]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_in__sourc__2EDEAC7E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_in__table__08C3F08E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_in__table__2FD2D0B7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_index]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_se__sourc__09B814C7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_server]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_se__sourc__1F30C64A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_server]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_se__tree___0AAC3900]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_server]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_db_se__tree___18EF6B5F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_db_server]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_d__group__0BA05D39]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_data_error_issue]  WITH CHECK ADD FOREIGN KEY([group_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_d__group__49E7A875]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_data_error_issue]  WITH CHECK ADD FOREIGN KEY([group_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_d__leade__0C948172]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_data_error_issue]  WITH CHECK ADD FOREIGN KEY([leader_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_d__leade__4ADBCCAE]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_data_error_issue]  WITH CHECK ADD FOREIGN KEY([leader_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_d__node___0D88A5AB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_data_error_issue]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_d__node___48F3843C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_data_error_issue]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_e__group__0E7CC9E4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_evaluation]  WITH CHECK ADD FOREIGN KEY([group_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_e__group__65F9D73E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_evaluation]  WITH CHECK ADD FOREIGN KEY([group_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_e__leade__0F70EE1D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_evaluation]  WITH CHECK ADD FOREIGN KEY([leader_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_e__leade__66EDFB77]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_evaluation]  WITH CHECK ADD FOREIGN KEY([leader_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_e__node___10651256]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_evaluation]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_e__node___6505B305]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_evaluation]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_l__test___1159368F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_log]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_l__test___6FB84BA2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_log]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [fk_bik_dqm_log_multi_source_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_log]  WITH CHECK ADD  CONSTRAINT [fk_bik_dqm_log_multi_source_id] FOREIGN KEY([multi_source_id])
REFERENCES [dbo].[bik_dqm_test_multi] ([id])
GO
ALTER TABLE [dbo].[bik_dqm_log] CHECK CONSTRAINT [fk_bik_dqm_log_multi_source_id]
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__node___13417F01]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_report]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__node___6BB2B094]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_report]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__evalu__1435A33A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_report_evaluation]  WITH CHECK ADD FOREIGN KEY([evaluation_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__evalu__716B89EA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_report_evaluation]  WITH CHECK ADD FOREIGN KEY([evaluation_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__repor__1529C773]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_report_evaluation]  WITH CHECK ADD FOREIGN KEY([report_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__repor__707765B1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_report_evaluation]  WITH CHECK ADD FOREIGN KEY([report_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__test___161DEBAC]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__test___17120FE5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request]  WITH CHECK ADD FOREIGN KEY([test_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__test___681729DA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__test___690B4E13]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request]  WITH CHECK ADD FOREIGN KEY([test_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__reque__1806341E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request_error]  WITH CHECK ADD FOREIGN KEY([request_id])
REFERENCES [dbo].[bik_dqm_request] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_r__reque__747D00BF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_request_error]  WITH CHECK ADD FOREIGN KEY([request_id])
REFERENCES [dbo].[bik_dqm_request] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_s__test___0B606617]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_schedule]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_s__test___18FA5857]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_schedule]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [fk_bik_dqm_schedule_multi_source_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_schedule]  WITH CHECK ADD  CONSTRAINT [fk_bik_dqm_schedule_multi_source_id] FOREIGN KEY([multi_source_id])
REFERENCES [dbo].[bik_dqm_test_multi] ([id])
GO
ALTER TABLE [dbo].[bik_dqm_schedule] CHECK CONSTRAINT [fk_bik_dqm_schedule_multi_source_id]
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__datab__0C548A50]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([database_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__datab__1AE2A0C9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([database_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__error__1BD6C502]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([error_procedure_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__error__5E8DBFA0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([error_procedure_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__proce__1CCAE93B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([procedure_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__proce__5D999B67]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([procedure_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__sourc__1DBF0D74]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__sourc__5F81E3D9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__test___1EB331AD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([test_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__test___59C90A83]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD FOREIGN KEY([test_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_dqm_test_db_server_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD  CONSTRAINT [fk_bik_dqm_test_db_server_id] FOREIGN KEY([db_server_id])
REFERENCES [dbo].[bik_db_server] ([id])
GO
ALTER TABLE [dbo].[bik_dqm_test] CHECK CONSTRAINT [fk_bik_dqm_test_db_server_id]
GO
/****** Object:  ForeignKey [fk_bik_dqm_test_error_db_server_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test]  WITH CHECK ADD  CONSTRAINT [fk_bik_dqm_test_error_db_server_id] FOREIGN KEY([error_db_server_id])
REFERENCES [dbo].[bik_db_server] ([id])
GO
ALTER TABLE [dbo].[bik_dqm_test] CHECK CONSTRAINT [fk_bik_dqm_test_error_db_server_id]
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__datab__218F9E58]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([database_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__datab__40C8528F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([database_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__sourc__2283C291]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__sourc__3FD42E56]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__test___2377E6CA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__test___246C0B03]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([test_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__test___3DEBE5E4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([test_id])
REFERENCES [dbo].[bik_dqm_test] ([id])
GO
/****** Object:  ForeignKey [FK__bik_dqm_t__test___3EE00A1D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD FOREIGN KEY([test_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_dqm_test_multi_db_server_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_dqm_test_multi]  WITH CHECK ADD  CONSTRAINT [fk_bik_dqm_test_multi_db_server_id] FOREIGN KEY([db_server_id])
REFERENCES [dbo].[bik_db_server] ([id])
GO
ALTER TABLE [dbo].[bik_dqm_test_multi] CHECK CONSTRAINT [fk_bik_dqm_test_multi_db_server_id]
GO
/****** Object:  ForeignKey [fk_bik_erwin_shape_data_load_log]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_erwin_shape]  WITH CHECK ADD  CONSTRAINT [fk_bik_erwin_shape_data_load_log] FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
ALTER TABLE [dbo].[bik_erwin_shape] CHECK CONSTRAINT [fk_bik_erwin_shape_data_load_log]
GO
/****** Object:  ForeignKey [fk_bik_erwin_table_data_load_log]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_erwin_table]  WITH CHECK ADD  CONSTRAINT [fk_bik_erwin_table_data_load_log] FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
ALTER TABLE [dbo].[bik_erwin_table] CHECK CONSTRAINT [fk_bik_erwin_table_data_load_log]
GO
/****** Object:  ForeignKey [fk_bik_erwin_table_column_data_load_log]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_erwin_table_column]  WITH CHECK ADD  CONSTRAINT [fk_bik_erwin_table_column_data_load_log] FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
ALTER TABLE [dbo].[bik_erwin_table_column] CHECK CONSTRAINT [fk_bik_erwin_table_column_data_load_log]
GO
/****** Object:  ForeignKey [FK__bik_file___tree___2930C020]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_file_system]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_file___tree___442ECF1F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_file_system]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_file___node___2A24E459]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_file_system_info]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_file___node___7F4F94ED]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_file_system_info]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_frequ__node___2B190892]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_frequently_asked_questions]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_quest__node___67DAA29A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_frequently_asked_questions]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_fts_processed_obj_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_processed_obj]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_processed_obj_id] FOREIGN KEY([id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_fts_processed_obj] CHECK CONSTRAINT [fk_bik_fts_processed_obj_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_in_obj_obj_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_in_obj]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_in_obj_obj_id] FOREIGN KEY([obj_id])
REFERENCES [dbo].[bik_fts_processed_obj] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_in_obj] CHECK CONSTRAINT [fk_bik_fts_word_in_obj_obj_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_in_obj_word_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_in_obj]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_in_obj_word_id] FOREIGN KEY([word_id])
REFERENCES [dbo].[bik_fts_word] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_in_obj] CHECK CONSTRAINT [fk_bik_fts_word_in_obj_word_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_in_obj_attr_attr_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_in_obj_attr]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_in_obj_attr_attr_id] FOREIGN KEY([attr_id])
REFERENCES [dbo].[bik_fts_attr] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_in_obj_attr] CHECK CONSTRAINT [fk_bik_fts_word_in_obj_attr_attr_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_in_obj_attr_obj_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_in_obj_attr]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_in_obj_attr_obj_id] FOREIGN KEY([obj_id])
REFERENCES [dbo].[bik_fts_processed_obj] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_in_obj_attr] CHECK CONSTRAINT [fk_bik_fts_word_in_obj_attr_obj_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_in_obj_attr_word_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_in_obj_attr]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_in_obj_attr_word_id] FOREIGN KEY([word_id])
REFERENCES [dbo].[bik_fts_word] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_in_obj_attr] CHECK CONSTRAINT [fk_bik_fts_word_in_obj_attr_word_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_stemming_stemmed_word_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_stemming]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_stemming_stemmed_word_id] FOREIGN KEY([stemmed_word_id])
REFERENCES [dbo].[bik_fts_stemmed_word] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_stemming] CHECK CONSTRAINT [fk_bik_fts_word_stemming_stemmed_word_id]
GO
/****** Object:  ForeignKey [fk_bik_fts_word_stemming_word_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_fts_word_stemming]  WITH CHECK ADD  CONSTRAINT [fk_bik_fts_word_stemming_word_id] FOREIGN KEY([word_id])
REFERENCES [dbo].[bik_fts_word] ([id])
GO
ALTER TABLE [dbo].[bik_fts_word_stemming] CHECK CONSTRAINT [fk_bik_fts_word_stemming_word_id]
GO
/****** Object:  ForeignKey [fk_bik_home_page_hint_node]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_home_page_hint]  WITH CHECK ADD  CONSTRAINT [fk_bik_home_page_hint_node] FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_home_page_hint] CHECK CONSTRAINT [fk_bik_home_page_hint_node]
GO
/****** Object:  ForeignKey [FK__bik_jdbc__jdbc_s__34A272CC]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_jdbc]  WITH CHECK ADD FOREIGN KEY([jdbc_source_id])
REFERENCES [dbo].[bik_jdbc_source] ([id])
GO
/****** Object:  ForeignKey [FK__bik_jdbc__jdbc_s__3D81D190]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_jdbc]  WITH CHECK ADD FOREIGN KEY([jdbc_source_id])
REFERENCES [dbo].[bik_jdbc_source] ([id])
GO
/****** Object:  ForeignKey [FK__bik_jdbc__tree_i__2D4B69C7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_jdbc]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_jdbc__tree_i__35969705]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_jdbc]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_joine__attri__368ABB3E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_obj_attribute_linked]  WITH CHECK ADD FOREIGN KEY([attribute_id])
REFERENCES [dbo].[bik_joined_obj_attribute] ([id])
GO
/****** Object:  ForeignKey [FK__bik_joine__attri__4D832F2F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_obj_attribute_linked]  WITH CHECK ADD FOREIGN KEY([attribute_id])
REFERENCES [dbo].[bik_joined_obj_attribute] ([id])
GO
/****** Object:  ForeignKey [FK__bik_joine__joine__377EDF77]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_obj_attribute_linked]  WITH CHECK ADD FOREIGN KEY([joined_obj_id])
REFERENCES [dbo].[bik_joined_objs] ([id])
GO
/****** Object:  ForeignKey [FK__bik_joine__joine__4C8F0AF6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_obj_attribute_linked]  WITH CHECK ADD FOREIGN KEY([joined_obj_id])
REFERENCES [dbo].[bik_joined_objs] ([id])
GO
/****** Object:  ForeignKey [FK_bik_joined_objs_dst_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_objs]  WITH CHECK ADD  CONSTRAINT [FK_bik_joined_objs_dst_node_id] FOREIGN KEY([dst_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_joined_objs] CHECK CONSTRAINT [FK_bik_joined_objs_dst_node_id]
GO
/****** Object:  ForeignKey [FK_bik_joined_objs_src_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_joined_objs]  WITH CHECK ADD  CONSTRAINT [FK_bik_joined_objs_src_node_id] FOREIGN KEY([src_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_joined_objs] CHECK CONSTRAINT [FK_bik_joined_objs_src_node_id]
GO
/****** Object:  ForeignKey [FK__bik_lisa___node___3A5B4C22]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_auth]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___node___7996BB97]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_auth]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___insta__3B4F705B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_column]  WITH CHECK ADD FOREIGN KEY([instance_id])
REFERENCES [dbo].[bik_lisa_instance] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___insta__7B49F9DF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_column]  WITH CHECK ADD FOREIGN KEY([instance_id])
REFERENCES [dbo].[bik_lisa_instance] ([id])
GO
/****** Object:  ForeignKey [fk_bik_lisa_dict_column_lookup_lisa_dict_column]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_column_lookup]  WITH CHECK ADD  CONSTRAINT [fk_bik_lisa_dict_column_lookup_lisa_dict_column] FOREIGN KEY([column_id])
REFERENCES [dbo].[bik_lisa_dict_column] ([id])
GO
ALTER TABLE [dbo].[bik_lisa_dict_column_lookup] CHECK CONSTRAINT [fk_bik_lisa_dict_column_lookup_lisa_dict_column]
GO
/****** Object:  ForeignKey [FK__bik_lisa___insta__0102D335]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_instance]  WITH CHECK ADD FOREIGN KEY([instance_id])
REFERENCES [dbo].[bik_lisa_instance] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___insta__3D37B8CD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_instance]  WITH CHECK ADD FOREIGN KEY([instance_id])
REFERENCES [dbo].[bik_lisa_instance] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___node___000EAEFC]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_instance]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___node___3E2BDD06]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_dict_instance]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___node___1D67A9E3]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___node___3F20013F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa___syste__40142578]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_logs]  WITH CHECK ADD FOREIGN KEY([system_user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_lisa_l__info__7DEEFE8A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_lisa_logs]  WITH CHECK ADD FOREIGN KEY([system_user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK_bik_metapedia_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_metapedia]  WITH CHECK ADD  CONSTRAINT [FK_bik_metapedia_node_id] FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_metapedia] CHECK CONSTRAINT [FK_bik_metapedia_node_id]
GO
/****** Object:  ForeignKey [FK_bik_metapedia_user_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_metapedia]  WITH CHECK ADD  CONSTRAINT [FK_bik_metapedia_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_metapedia] CHECK CONSTRAINT [FK_bik_metapedia_user_id]
GO
/****** Object:  ForeignKey [FK__bik_mssql__colum__12908375]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_column_extradata]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__colum__42F09223]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_column_extradata]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__table__119C5F3C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_column_extradata]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__table__43E4B65C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_column_extradata]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__dep_n__44D8DA95]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_dependency]  WITH CHECK ADD FOREIGN KEY([dep_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__dep_n__6DC6E2DF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_dependency]  WITH CHECK ADD FOREIGN KEY([dep_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__ref_n__45CCFECE]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_dependency]  WITH CHECK ADD FOREIGN KEY([ref_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__ref_n__6CD2BEA6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_dependency]  WITH CHECK ADD FOREIGN KEY([ref_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__node___11B663A9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_extended_properties]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__node___46C12307]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_extended_properties]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__fk_co__47B54740]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__fk_co__4E2E0A2B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__fk_ta__48A96B79]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__fk_ta__4D39E5F2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([fk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__pk_co__499D8FB2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__pk_co__5016529D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__pk_ta__4A91B3EB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__pk_ta__4F222E64]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_foreign_key]  WITH CHECK ADD FOREIGN KEY([pk_table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__colum__05368857]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_index]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__colum__4B85D824]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_index]  WITH CHECK ADD FOREIGN KEY([column_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__table__0442641E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_index]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_mssql__table__4C79FC5D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_mssql_index]  WITH CHECK ADD FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_news__author__4D6E2096]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news]  WITH CHECK ADD FOREIGN KEY([author_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_news__author__580C7E4F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news]  WITH CHECK ADD FOREIGN KEY([author_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [fk_bik_news_system_user]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news]  WITH CHECK ADD  CONSTRAINT [fk_bik_news_system_user] FOREIGN KEY([only_for_user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_news] CHECK CONSTRAINT [fk_bik_news_system_user]
GO
/****** Object:  ForeignKey [FK__bik_news___news___4F566908]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news_readed_user]  WITH CHECK ADD FOREIGN KEY([news_id])
REFERENCES [dbo].[bik_news] ([id])
GO
/****** Object:  ForeignKey [FK__bik_news___news___5BDD0F33]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news_readed_user]  WITH CHECK ADD FOREIGN KEY([news_id])
REFERENCES [dbo].[bik_news] ([id])
GO
/****** Object:  ForeignKey [FK__bik_news___user___504A8D41]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news_readed_user]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_news___user___5AE8EAFA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_news_readed_user]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node__linked__05752722]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD FOREIGN KEY([linked_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node__linked__513EB17A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD FOREIGN KEY([linked_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node__node_k__5232D5B3]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node__node_k__7FBC4DCC]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node__parent__075D6F94]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD  CONSTRAINT [FK__bik_node__parent__075D6F94] FOREIGN KEY([parent_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_node] CHECK CONSTRAINT [FK__bik_node__parent__075D6F94]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'pierwszy constrain' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_node', @level2type=N'CONSTRAINT',@level2name=N'FK__bik_node__parent__075D6F94'
GO
/****** Object:  ForeignKey [FK__bik_node__parent__0945B806]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD  CONSTRAINT [FK__bik_node__parent__0945B806] FOREIGN KEY([parent_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_node] CHECK CONSTRAINT [FK__bik_node__parent__0945B806]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'drugi constrain' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bik_node', @level2type=N'CONSTRAINT',@level2name=N'FK__bik_node__parent__0945B806'
GO
/****** Object:  ForeignKey [FK__bik_node__tree_i__00B07205]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node__tree_i__550F425E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___actio__2337EFA8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_action_in_custom_right_role]  WITH CHECK ADD FOREIGN KEY([action_id])
REFERENCES [dbo].[bik_node_action] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___actio__56036697]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_action_in_custom_right_role]  WITH CHECK ADD FOREIGN KEY([action_id])
REFERENCES [dbo].[bik_node_action] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___role___242C13E1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_action_in_custom_right_role]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[bik_custom_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___role___56F78AD0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_action_in_custom_right_role]  WITH CHECK ADD FOREIGN KEY([role_id])
REFERENCES [dbo].[bik_custom_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___0DB78F45]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_author]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___57EBAF09]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_author]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___user___0CC36B0C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_author]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___user___58DFD342]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_author]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___chang__0BB56BD2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail]  WITH CHECK ADD FOREIGN KEY([change_id])
REFERENCES [dbo].[bik_node_change] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___chang__59D3F77B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail]  WITH CHECK ADD FOREIGN KEY([change_id])
REFERENCES [dbo].[bik_node_change] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___0CA9900B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___5AC81BB4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___attr___5BBC3FED]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([attr_id])
REFERENCES [dbo].[bik_searchable_attr] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___attr___5DB996F8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([attr_id])
REFERENCES [dbo].[bik_searchable_attr] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___chang__5CB06426]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([change_detail_id])
REFERENCES [dbo].[bik_node_change_detail] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___chang__5CC572BF]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([change_detail_id])
REFERENCES [dbo].[bik_node_change_detail] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___data___5DA4885F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([data_source_def_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___data___67430132]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([data_source_def_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___user___5E98AC98]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___user___664EDCF9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_change_detail_value]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___5F8CD0D1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind_4_tree_kind]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___7976C006]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind_4_tree_kind]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___tree___6080F50A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind_4_tree_kind]  WITH CHECK ADD FOREIGN KEY([tree_kind_id])
REFERENCES [dbo].[bik_tree_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___tree___78829BCD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_kind_4_tree_kind]  WITH CHECK ADD FOREIGN KEY([tree_kind_id])
REFERENCES [dbo].[bik_tree_kind] ([id])
GO
/****** Object:  ForeignKey [fk_bik_node_name_chunk_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_name_chunk]  WITH CHECK ADD  CONSTRAINT [fk_bik_node_name_chunk_node_id] FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_node_name_chunk] CHECK CONSTRAINT [fk_bik_node_name_chunk_node_id]
GO
/****** Object:  ForeignKey [fk_bik_node_name_chunk_tree_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_name_chunk]  WITH CHECK ADD  CONSTRAINT [fk_bik_node_name_chunk_tree_id] FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
ALTER TABLE [dbo].[bik_node_name_chunk] CHECK CONSTRAINT [fk_bik_node_name_chunk_tree_id]
GO
/****** Object:  ForeignKey [FK__bik_node___node___57CE5803]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_vote]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_node___node___635D61B5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_vote]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_node_vote_user_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_node_vote]  WITH CHECK ADD  CONSTRAINT [FK_bik_node_vote_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_node_vote] CHECK CONSTRAINT [FK_bik_node_vote_user_id]
GO
/****** Object:  ForeignKey [FK__bik_note__node_i__2D4E0E52]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_note]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_note__node_i__6545AA27]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_note]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_note_user_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_note]  WITH CHECK ADD  CONSTRAINT [FK_bik_note_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_note] CHECK CONSTRAINT [FK_bik_note_user_id]
GO
/****** Object:  ForeignKey [FK__bik_obj_i__blog___099ABDC1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_obj_in_body_node]  WITH CHECK ADD FOREIGN KEY([blog_id])
REFERENCES [dbo].[bik_blog] ([id])
GO
/****** Object:  ForeignKey [FK__bik_obj_i__blog___672DF299]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_obj_in_body_node]  WITH CHECK ADD FOREIGN KEY([blog_id])
REFERENCES [dbo].[bik_blog] ([id])
GO
/****** Object:  ForeignKey [FK__bik_obj_i__linke__08A69988]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_obj_in_body_node]  WITH CHECK ADD FOREIGN KEY([linked_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_obj_i__linke__682216D2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_obj_in_body_node]  WITH CHECK ADD FOREIGN KEY([linked_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___69163B0B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_author]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___7700732B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_author]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__user___6A0A5F44]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_author]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__user___77F49764]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_author]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___0FCC20F5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_excluded_from_statistic]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___6AFE837D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_excluded_from_statistic]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___402BD89C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___6BF2A7B6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_object_in_fvs_system_user]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs]  WITH CHECK ADD  CONSTRAINT [fk_bik_object_in_fvs_system_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_object_in_fvs] CHECK CONSTRAINT [fk_bik_object_in_fvs_system_user]
GO
/****** Object:  ForeignKey [FK__bik_objec__fvs_i__628A0CC2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change]  WITH CHECK ADD FOREIGN KEY([fvs_id])
REFERENCES [dbo].[bik_object_in_fvs] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__fvs_i__6DDAF028]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change]  WITH CHECK ADD FOREIGN KEY([fvs_id])
REFERENCES [dbo].[bik_object_in_fvs] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___6195E889]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___6ECF1461]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__cut_p__2481199C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex]  WITH CHECK ADD FOREIGN KEY([cut_parent_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__cut_p__6FC3389A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex]  WITH CHECK ADD FOREIGN KEY([cut_parent_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__fvs_i__1632FA45]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex]  WITH CHECK ADD FOREIGN KEY([fvs_id])
REFERENCES [dbo].[bik_object_in_fvs] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__fvs_i__70B75CD3]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex]  WITH CHECK ADD FOREIGN KEY([fvs_id])
REFERENCES [dbo].[bik_object_in_fvs] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___153ED60C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___71AB810C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_fvs_change_ex]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___44F08DB9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_history]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_objec__node___729FA545]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_history]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_object_in_history_system_user]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_object_in_history]  WITH CHECK ADD  CONSTRAINT [fk_bik_object_in_history_system_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
ALTER TABLE [dbo].[bik_object_in_history] CHECK CONSTRAINT [fk_bik_object_in_history_system_user]
GO
/****** Object:  ForeignKey [FK__bik_plain__tree___70CC6B6C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_plain_file]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_plain__tree___7487EDB7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_plain_file]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_profi__item___05A78CC1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_profile_file_extradata]  WITH CHECK ADD FOREIGN KEY([item_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_profi__item___757C11F0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_profile_file_extradata]  WITH CHECK ADD FOREIGN KEY([item_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_profi__item___76703629]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_profile_item_extradata]  WITH CHECK ADD FOREIGN KEY([item_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_profi__item___7D1246C0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_profile_item_extradata]  WITH CHECK ADD FOREIGN KEY([item_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_role___node___11D26AB6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_role_for_node]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_role___node___77645A62]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_role_for_node]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__conne__41637C89]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_connection_to_db]  WITH CHECK ADD FOREIGN KEY([connection_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__conne__78587E9B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_connection_to_db]  WITH CHECK ADD FOREIGN KEY([connection_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___671166EA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_edited_description]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___794CA2D4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_edited_description]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___5452CCDA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___7A40C70D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [fk_bik_sapbo_object_table_node]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_object_table]  WITH CHECK ADD  CONSTRAINT [fk_bik_sapbo_object_table_node] FOREIGN KEY([object_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_sapbo_object_table] CHECK CONSTRAINT [fk_bik_sapbo_object_table_node]
GO
/****** Object:  ForeignKey [fk_bik_sapbo_object_table_table_node]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_object_table]  WITH CHECK ADD  CONSTRAINT [fk_bik_sapbo_object_table_table_node] FOREIGN KEY([table_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_sapbo_object_table] CHECK CONSTRAINT [fk_bik_sapbo_object_table_table_node]
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___0684448B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_olap_connection]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___7D1D33B8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_olap_connection]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_sapbo_query_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_query]  WITH CHECK ADD  CONSTRAINT [FK_bik_sapbo_query_node_id] FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_sapbo_query] CHECK CONSTRAINT [FK_bik_sapbo_query_node_id]
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___0C199160]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_report_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___7F057C2A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_report_extradata]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_sapbo_schedule_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_schedule]  WITH CHECK ADD  CONSTRAINT [FK_bik_sapbo_schedule_node_id] FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_sapbo_schedule] CHECK CONSTRAINT [FK_bik_sapbo_schedule_node_id]
GO
/****** Object:  ForeignKey [FK__bik_sapbo__conne__00EDC49C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([connection_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__conne__3944CE82]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([connection_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__repor__01E1E8D5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([report_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__repor__375C8610]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([report_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__sourc__02D60D0E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__sourc__34801965]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__unive__03CA3147]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([universe_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__unive__3850AA49]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_server]  WITH CHECK ADD FOREIGN KEY([universe_tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK_bik_sapbo_universe_column_bik_node_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_column]  WITH CHECK ADD  CONSTRAINT [FK_bik_sapbo_universe_column_bik_node_id] FOREIGN KEY([table_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_sapbo_universe_column] CHECK CONSTRAINT [FK_bik_sapbo_universe_column_bik_node_id]
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___05B279B9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_connection]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___48570A49]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_connection]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___06A69DF2]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_object]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___372C7E47]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_object]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___079AC22B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_table]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sapbo__node___3BF1FEBB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_universe_table]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK_bik_sapbo_user_server_id]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_sapbo_user]  WITH CHECK ADD  CONSTRAINT [FK_bik_sapbo_user_server_id] FOREIGN KEY([server_instance_id])
REFERENCES [dbo].[bik_sapbo_server] ([id])
GO
ALTER TABLE [dbo].[bik_sapbo_user] CHECK CONSTRAINT [FK_bik_sapbo_user_server_id]
GO
/****** Object:  ForeignKey [FK__bik_sched__sourc__09830A9D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_schedule]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_sched__sourc__1370689B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_schedule]  WITH CHECK ADD FOREIGN KEY([source_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_searc__node___0A772ED6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_search_cache]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_searc__node___696053CE]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_search_cache]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_searc__attr___26C1D7AB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_searchable_attr_val]  WITH CHECK ADD FOREIGN KEY([attr_id])
REFERENCES [dbo].[bik_searchable_attr] ([id])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__bik_searc__node___27B5FBE4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_searchable_attr_val]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [fk_bik_searchable_attr_val_node_kind]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_searchable_attr_val]  WITH CHECK ADD  CONSTRAINT [fk_bik_searchable_attr_val_node_kind] FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
ALTER TABLE [dbo].[bik_searchable_attr_val] CHECK CONSTRAINT [fk_bik_searchable_attr_val_node_kind]
GO
/****** Object:  ForeignKey [fk_bik_searchable_attr_val_tree]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_searchable_attr_val]  WITH CHECK ADD  CONSTRAINT [fk_bik_searchable_attr_val_tree] FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
ALTER TABLE [dbo].[bik_searchable_attr_val] CHECK CONSTRAINT [fk_bik_searchable_attr_val_tree]
GO
/****** Object:  ForeignKey [FK__bik_spid___data___0F3BE3F3]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_spid_source]  WITH CHECK ADD FOREIGN KEY([data_source_def_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_spid___data___655AB8C0]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_spid_source]  WITH CHECK ADD FOREIGN KEY([data_source_def_id])
REFERENCES [dbo].[bik_data_source_def] ([id])
GO
/****** Object:  ForeignKey [FK__bik_spid___user___1030082C]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_spid_source]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_spid___user___64669487]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_spid_source]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__user___11242C65]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__user___64725534]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__click__0736DAF4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD FOREIGN KEY([clicked_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__click__1218509E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD FOREIGN KEY([clicked_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__tree___0642B6BB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__tree___130C74D7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__user___082AFF2D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_stati__user___14009910]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [fk_bik_statistic_ext_node]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_statistic_ext]  WITH CHECK ADD  CONSTRAINT [fk_bik_statistic_ext_node] FOREIGN KEY([grouping_node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
ALTER TABLE [dbo].[bik_statistic_ext] CHECK CONSTRAINT [fk_bik_statistic_ext_node]
GO
/****** Object:  ForeignKey [FK__bik_syste__group__15E8E182]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_group_in_group]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__group__2C8C4FB8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_group_in_group]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__membe__16DD05BB]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_group_in_group]  WITH CHECK ADD FOREIGN KEY([member_group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__membe__2D8073F1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_group_in_group]  WITH CHECK ADD FOREIGN KEY([member_group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__user___17D129F4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__user___3DAA010D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__scc_r__18C54E2D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_group]  WITH CHECK ADD FOREIGN KEY([scc_root_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__scc_r__48346A2D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_group]  WITH CHECK ADD FOREIGN KEY([scc_root_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__group__19B97266]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__group__39E64AD6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__user___1AAD969F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__user___38F2269D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__group__0C7485E1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group_prepared]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__group__1BA1BAD8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group_prepared]  WITH CHECK ADD FOREIGN KEY([group_id])
REFERENCES [dbo].[bik_system_user_group] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__user___0B8061A8]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group_prepared]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_syste__user___1C95DF11]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_system_user_in_group_prepared]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_terad__data___1D8A034A]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_teradata]  WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_terad__data___58386C57]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_teradata]  WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_terad__data___1E7E2783]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_teradata_data_model]  WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [FK__bik_terad__data___66150EC5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_teradata_data_model]  WITH CHECK ADD FOREIGN KEY([data_load_log_id])
REFERENCES [dbo].[bik_data_load_log] ([id])
GO
/****** Object:  ForeignKey [fk_bik_tree_node_kind]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree]  WITH CHECK ADD  CONSTRAINT [fk_bik_tree_node_kind] FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
ALTER TABLE [dbo].[bik_tree] CHECK CONSTRAINT [fk_bik_tree_node_kind]
GO
/****** Object:  ForeignKey [FK__bik_tree___node___11936C86]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_icon]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_tree___node___20666FF5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_icon]  WITH CHECK ADD FOREIGN KEY([node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
/****** Object:  ForeignKey [FK__bik_tree___tree___109F484D]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_icon]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_tree___tree___215A942E]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_icon]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [fk_bik_tree_kind_branch_node_kind]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_kind]  WITH CHECK ADD  CONSTRAINT [fk_bik_tree_kind_branch_node_kind] FOREIGN KEY([branch_node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
ALTER TABLE [dbo].[bik_tree_kind] CHECK CONSTRAINT [fk_bik_tree_kind_branch_node_kind]
GO
/****** Object:  ForeignKey [fk_bik_tree_kind_leaf_node_kind]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tree_kind]  WITH CHECK ADD  CONSTRAINT [fk_bik_tree_kind_leaf_node_kind] FOREIGN KEY([leaf_node_kind_id])
REFERENCES [dbo].[bik_node_kind] ([id])
GO
ALTER TABLE [dbo].[bik_tree_kind] CHECK CONSTRAINT [fk_bik_tree_kind_leaf_node_kind]
GO
/****** Object:  ForeignKey [FK__bik_tutor__tutor__243700D9]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tutorial_readed_user]  WITH CHECK ADD FOREIGN KEY([tutorial_id])
REFERENCES [dbo].[bik_tutorial] ([id])
GO
/****** Object:  ForeignKey [FK__bik_tutor__tutor__7F696FEE]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tutorial_readed_user]  WITH CHECK ADD FOREIGN KEY([tutorial_id])
REFERENCES [dbo].[bik_tutorial] ([id])
GO
/****** Object:  ForeignKey [FK__bik_tutor__user___252B2512]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tutorial_readed_user]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_tutor__user___7E754BB5]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_tutorial_readed_user]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user__node_i__261F494B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user__node_i__5B9EE8E7]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___node___27136D84]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___node___52157EAD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node]  WITH CHECK ADD FOREIGN KEY([node_id])
REFERENCES [dbo].[bik_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___role___26B7E7D4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node]  WITH CHECK ADD FOREIGN KEY([role_for_node_id])
REFERENCES [dbo].[bik_role_for_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___role___280791BD]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node]  WITH CHECK ADD FOREIGN KEY([role_for_node_id])
REFERENCES [dbo].[bik_role_for_node] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___user___28FBB5F6]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___user___502D363B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_in_node]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___right__00A7DEB4]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right]  WITH CHECK ADD FOREIGN KEY([right_id])
REFERENCES [dbo].[bik_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___right__29EFDA2F]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right]  WITH CHECK ADD FOREIGN KEY([right_id])
REFERENCES [dbo].[bik_right_role] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___tree___2AE3FE68]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___tree___6F5F5163]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right]  WITH CHECK ADD FOREIGN KEY([tree_id])
REFERENCES [dbo].[bik_tree] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___user___2BD822A1]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__bik_user___user___7FB3BA7B]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[bik_user_right]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[bik_system_user] ([id])
GO
/****** Object:  ForeignKey [FK__dbau_log__proc_i__2CCC46DA]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_log]  WITH CHECK ADD FOREIGN KEY([proc_id])
REFERENCES [dbo].[dbau_process] ([id])
GO
/****** Object:  ForeignKey [FK__dbau_log__proc_i__619F3675]    Script Date: 09/28/2016 11:08:55 ******/
ALTER TABLE [dbo].[dbau_log]  WITH CHECK ADD FOREIGN KEY([proc_id])
REFERENCES [dbo].[dbau_process] ([id])
GO
