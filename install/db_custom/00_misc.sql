update bik_app_prop set val='true' where name='useDrools'
update bik_app_prop set val='true'where name='enableFullImportExportTree' 
update bik_node set is_deleted=0 where obj_id like '#admin:dict:sysGroupUsers'
update bik_tree set is_hidden=1 where tree_kind= 'metaBIKS' 
update bik_tree_kind set is_deleted=1 where code = 'metaBIKS'

delete from  bik_search_hint    
update bik_app_prop set val='<h3>Witaj w Business Integration Knowledge System (BIKS)</h3>
Business Integration Knowledge System (BIKS) jest autorskim rozwiązaniem firmy BSSG łączącym najlepsze praktyki z obszarów Data Governance, Knowledge Management, Governance Risk and Compliance, czy Business Intelligence. 
<br/><br/>
BIKS zapewnia wsparcie dla procesów związanych z zarządzaniem informacją. Stanowi centralny katalog wzajemnie powiązanych hierarchii biznesowych, operacyjnych i technicznych, które mogą zawierać dane, informację i wiedzę na temat działalności organizacji.
Aplikacja zawiera przykładowe treści dotyczące, zdekomponowanych Wytycznych KNF, Rejestru klauzul niedozwolonych UOKiK oraz część Rejestru podmiotów telekomunikacyjnych UKE. 
W udostępnionej wersji BIKS pewne funkcjonalności aplikacji zostały ograniczone (zobacz porównanie wersji). Wszystkie dane i informacje, zawarte w BIKS są przykładowe. Firma BSSG nie ponosi odpowiedzialności za niewłaściwe ich użycie. Użytkownik aplikacji oświadcza, że zapoznał się z warunkami użytkowania produktu.
'where name='myBIKSDemoMsg'

update bik_app_prop set val='false' where name='showTutorials' 
GO
update bik_tree set is_hidden=0 where id in (8,12,14,15,16,21,30,53,55)
GO
exec sp_recalculate_Custom_Right_Role_Tree_Selectors_Ex; 
GO
exec sp_recalculate_Custom_Right_Role_Action_Branch_Grants; 
GO
